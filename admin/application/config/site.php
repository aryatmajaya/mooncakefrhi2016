<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Centralize configuration for `admin` application.
 * 
 * Being used in:
 * - send email confirmation
 */

$frontend_site_config = realpath(dirname(__FILE__).'/../../../frontend/config/site.php');
if (file_exists($frontend_site_config)) {

    require_once($frontend_site_config);
} else {

    // This value below must be same with `admin/application/config/site.php`
    $config['site']['name'] = '';
    $config['site']['admin_email'] = '';
    $config['site']['logo'] = '';
    $config['site']['collection_address'] = 'Szechuan Court Mooncake Booth, Level 2, Fairmont Singapore, 80 Bras Basah Road Singapore 189560';
    $config['site']['enquiry_phone_number'] = '+65 6338 8785';
    $config['site']['test_email'] = 'permana.jayanta@bullseye-digital.com';

}

