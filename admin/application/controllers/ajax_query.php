<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Ajax_query extends CI_Controller {
	
    public function dynamic_dropdown_country()
	{
		/* used for properties add form
		*/
		extract($_POST);
		//echo $client_id;
		
			$sql = 	"SELECT country_id as list_id, country as list_name".
					" FROM countries".
					" WHERE region_id = $region_id".
					" AND status = 1 AND deleted = 0";

			$this->load->model('common');
			$the_list = $this->common->dynamic_query($sql);
			//var_dump($the_list);		
		
			if ($the_list) {
				echo "<option></option>";
				foreach ($the_list as $the_list) {
					echo "<option value='$the_list->list_id'>".$the_list->list_name."</option>\n";
				}	
			}
		

		
	}


	public function dynamic_dropdown_query()
	{
		extract($_POST);
		//echo $client_id;
		if ($data_type == "countries"){

		}
		else {

			$sql = 	"SELECT brand_id as list_id, brand_name as list_name".
					" FROM brands".
					" WHERE client_id=$client_id";

			$this->load->model('common');
			$the_list[] = $this->common->dynamic_query($sql);
			//var_dump($the_list);
		}

		if ($the_list) {
			echo "<option value='all'>All Brands</option>";
			foreach ($the_list as $the_list) {
				echo "<option>".$the_list->list_name."</option>\n";
			}	
		}
		

		
	}

	

	public function add()
	{
		$this->form_validation->set_rules('client_id', 'Client', 'required|xss_clean');
		$this->form_validation->set_rules('brand_name', 'Brand', 'required|xss_clean|is_unique[brands.brand_name]');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['view_page'] = "brands/brands-form";
			$data['page_title'] = "Brands";
			$data['action'] = "add";

			$this->load->model('common');
			$data['clients_list'] = $this->common->get_table_list('clients','client_name','ASC');

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');
			extract($_POST);

			$new_filename = '';

			if ($_FILES['logo_image']['name'] != "")
				$new_filename = mktime().'_'.$_FILES['logo_image']['name'];
			
			$insert_data = array(
				'client_id' => $client_id,
				'brand_name' => $brand_name,
				'logo_filename' => $new_filename,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("brands", $insert_data);
			$latest_id = $this->db->insert_id();

			//UPLOADING LOGO IMAGE
			$path = "../uploads/clients/$latest_id/";
			if(!file_exists($path))
			{
			   mkdir($path);
			}

			if ($_FILES['logo_image']['name'] != "") {
				
				$config['upload_path'] = $path;
				$config['file_name'] = $new_filename;
				$config['allowed_types'] = 'jpg|gif|png';
				$config['overwrite'] = TRUE;
				
				$this->load->library('upload', $config);
								
				$this->upload->do_upload('logo_image');
				
			}
			// End Upload Logo Image
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new brand.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('brands','brand_id',$the_id);

		if ($data['entries']){

			
			$data['clients_list'] = $this->common->get_table_list('clients','client_name','ASC');

			$data['view_page'] = "brands/brands-form";
			$data['page_title'] = "Brands";
			$data['action'] = "edit";			

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			
			$this->form_validation->set_rules('brand_name', 'Brand Name', 'required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{

				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {
				$new_filename = '';

				if ($_FILES['logo_image']['name'] != "")
					$new_filename = mktime().'_'.$_FILES['logo_image']['name'];
				
				
				$update_data = array(
						'client_id' => $client_id,
		               'brand_name' => $brand_name,
		               'status' => $status,
		               'modified_by' => $this->session->userdata('user_id'),
		               'logo_filename' => ($new_filename)?$new_filename:$old_logo_file
		            );
				
				$this->common->update('brands', 'brand_id', $id, $update_data);

				//UPLOADING LOGO IMAGE
				$path = "../uploads/clients/$id/";
				if(!file_exists($path))
				{
				   mkdir($path);
				}

				if ($_FILES['logo_image']['name'] != "") {
					$config['upload_path'] = $path;
					$config['file_name'] = $new_filename;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['overwrite'] = TRUE;
					
					$this->load->library('upload', $config);
									
					$this->upload->do_upload('logo_image');					
				}

				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($brand_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('brands', 'brand_id', $brand_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}
        
    function get_last_notification($order_ref) {
        //$order_ref = $this->input->post('order_ref');
        $data = common::get_a_record('mc_mail_notification_logs', 'order_ref', $order_ref, 'date_sent', 'DESC');
        echo 'test';//$data->date_sent;
    }
}

/* End of file brands.php */
/* Location: ./application/controllers/brands.php */