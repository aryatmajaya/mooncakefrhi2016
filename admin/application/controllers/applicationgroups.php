<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applicationgroups extends CI_Controller {
	//private $current_app_id;
	private $the_fields = array('1' => 'category_name|', '2' => 'status|');
	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		
		

		$this->load->model('applications_model');
		$data['entries'] = $this->applications_model->get_all_application_groups();

		$data['view_page'] = "generic/generic-sort";
		$data['page_title'] = "Application Groups";
		$data['add_caption'] = " Add Group";
		$data['the_fields'] = array('1' => 'group_name', '2' => 'status');


		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function add()
	{
		
			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Application Groups";
			$data['the_fields'] = array('1' => 'group_name|', '2' => 'status|');
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');	
		
		
			
	}

	public function insert()
	{
		
		$this->form_validation->set_rules('group_name', 'Group Name', 'required|xss_clean|is_unique[application_groups.group_name]');
		$this->form_validation->set_rules('status', 'Status', 'xss_clean');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->add();
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
				
				'group_name' => $group_name,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("application_groups", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new application group.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('applications_model');
		$data['entries'] = $this->applications_model->get_appgroup_by_id($the_id);

		if ($data['entries']){	

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Application Groups";
			$data['action'] = "edit";
			$data['the_fields'] = array('1' => 'group_name|', '2' => 'status|');

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			
			$this->form_validation->set_rules('group_name', 'Group Name', 'required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{

				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {
			
				
				
				$update_data = array(
		               'group_name' => $group_name,
		               'status' => $status
		            );
				
				$this->common->update('application_groups', 'id', $id, $update_data);
				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('application_groups', 'id', $the_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

	function update_sortorder(){
		$updateRecordsArray = $_POST['tr'];
		
		$listingCounter = 1;
		foreach ($updateRecordsArray as $recordIDValue) {			
			
			$query = "UPDATE application_groups SET position = " . $listingCounter . " WHERE id = " . $recordIDValue;
			mysql_query($query) or die(mysql_error());
			
			$listingCounter = $listingCounter + 1;			
		}		
	}

}

/* End of file brands.php */
/* Location: ./application/controllers/brands.php */