<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applications extends CI_Controller {
	//private $current_app_id;
	private $the_fields = array('1' => 'category_name|', '2' => 'status|');
	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		
		

		$this->load->model('applications_model');
		$data['entries'] = $this->applications_model->get_all_applications();

		$data['view_page'] = "applications/applications";
		$data['page_title'] = "Applications";
		$data['add_caption'] = " Add Application";
		


		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function add()
	{
			$this->load->model('applications_model');
			$data['drop_list1'] = $this->applications_model->get_application_groups_list();

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Applications";
			$data['the_fields'] = array('1' => 'app_group|dropdown', '2' => 'app_name|', '3' => 'app_url|', '4' => 'status|');
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');	
		
		
			
	}

	public function insert()
	{
		$this->form_validation->set_rules('app_group', 'App Group', 'required|xss_clean');
		$this->form_validation->set_rules('app_name', 'App Name', 'required|xss_clean|is_unique[applications.app_name]');
		$this->form_validation->set_rules('app_url', 'App URL', 'required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'xss_clean');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->add();
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
				
				'app_group' => $app_group,
				'app_name' => $app_name,
				'app_url' => $app_url,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("applications", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new application.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('applications_model');
		$data['entries'] = $this->applications_model->get_app_by_id($the_id);

		if ($data['entries']){	

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Applications";
			$data['action'] = "edit";
			$data['the_fields'] = array('1' => 'app_group|dropdown', '2' => 'app_name|', '3' => 'app_url|', '4' => 'status|');

			
			$data['drop_list1'] = $this->applications_model->get_application_groups_list();	

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			
			$this->form_validation->set_rules('app_group', 'App Group', 'required|xss_clean');
			$this->form_validation->set_rules('app_name', 'App Name', 'required|xss_clean');
			$this->form_validation->set_rules('app_url', 'App URL', 'required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{

				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {
			
				
				
				$update_data = array(
		               'app_group' => $app_group,
		               'app_name' => $app_name,
		               'app_url' => $app_url,
		               'status' => $status
		            );
				
				$this->common->update('applications', 'id', $id, $update_data);
				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('applications', 'id', $the_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

	function update_app_sortorder(){
		$updateRecordsArray = $_POST['tr'];
		
		$listingCounter = 1;
		foreach ($updateRecordsArray as $recordIDValue) {			
			
			$query = "UPDATE applications SET position = " . $listingCounter . " WHERE id = " . $recordIDValue;
			mysql_query($query) or die(mysql_error());
			
			$listingCounter = $listingCounter + 1;			
		}		
	}

}

/* End of file brands.php */
/* Location: ./application/controllers/brands.php */