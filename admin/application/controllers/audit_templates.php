<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit_templates extends CI_Controller {
	//private $current_app_id;
	private $the_fields = array('1' => 'category_name|', '2' => 'status|');
	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		$this->load->model('audits_model');
		$data['entries'] = $this->audits_model->get_all_templates();

		$data['view_page'] = "audits/audit_templates";
		$data['page_title'] = $this->current_app_id->app_name;
		$data['add_caption'] = " Add";
		
		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	
	public function add()
	{
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');		
		$this->form_validation->set_rules('template_name', 'Template Name', 'required|is_unique[audit_templates.template_name]');
		$this->form_validation->set_rules('published', 'Published', 'xss_clean');		
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['view_page'] = "audits/audit_templates_form";
			$data['page_title'] = $this->current_app_id->app_name;

			$this->load->model('properties_model');
			$data['properties_list'] = $this->properties_model->properties_with_brand_list();
			
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
				
				'template_name' => $template_name,
				'property_id' => $property_id,
				'published' => $published,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("audit_templates", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new template.');
			redirect($this->router->fetch_class());			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('audits_model');
		$data['entries'] = $this->audits_model->get_template_by_id($the_id);

		if ($data['entries']){
			$this->load->model('properties_model');
			$data['properties_list'] = $this->properties_model->properties_with_brand_list();

			$data['view_page'] = "audits/audit_templates_form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');			
			
			$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');		
			$this->form_validation->set_rules('template_name', 'Template Name', 'required|xss_clean');
			$this->form_validation->set_rules('plubished', 'Published', 'xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {				
				$update_data = array(
						'property_id' => $property_id,
		               'template_name' => $template_name,
		               'published' => $published		               
		            );
				
				$this->common->update('audit_templates', 'id', $id, $update_data);
				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}		
	}

	function manage(){
		$the_id = $this->uri->segment(3);
		$this->load->model('audits_model');
		$data['template_details'] = $this->audits_model->get_template_by_id($the_id);

		$data['entries'] = $this->audits_model->Get_All_Touchpoints_of_a_Template_in_manage_view($the_id);
		
		$this->load->model('touchpoints_model');
		$data['cat_list'] = $this->touchpoints_model->get_categories_list();
 		 //$tp_list = $this->touchpoints_model->get_all_touchpoints();

		//if ($data['entries']){

			$data['view_page'] = "audits/audit_templates_manage_form";
			$data['page_title'] = "Manage Template - ".$data['template_details']->template_name;
			$data['bread_crumbs'] = array(
									'Audit Templates|'.base_url().$this->router->fetch_class(),
									$data['template_details']->template_name.'|'
									);
			$data['action'] = "edit";
			
			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		//}
		//else
			//redirect($this->router->fetch_class());
			
	}

	public function edit_tp()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('audit_template_touchpoints', 'id', $the_id);


		if ($data['entries']){

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Audit Template - Edit Touchpoint";
			$data['action'] = "edit";
			$data['return_url'] = "manage/".$data['entries']->template_id;
			$data['update_url'] = "/update_tp/";
			$data['the_fields'] = array('1' => 'touch_point_name|');	

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());			
	}

	public function update_tp(){
		extract($_POST);
		
		$this->load->model('common');			
		
		$this->form_validation->set_rules('touch_point_name', 'Touch Point Name', 'required|xss_clean');
				
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('required_error', validation_errors());			
			if ($id)
				redirect($this->router->fetch_class().'/edit_tp/'.$id);
			else
				redirect($this->router->fetch_class());
		}
		else {				
			$update_data = array(
	               'touch_point_name' => $touch_point_name,
	               'modified_by' => $this->session->userdata('user_id')		               
	            );
			
			$this->common->update('audit_template_touchpoints', 'id', $id, $update_data);
			$this->session->set_flashdata('success_notification', 'Updated Successfully');				
		
			redirect($this->router->fetch_class().'/'.$return_url);
		}
	}

	/* AJAX Functions */
	function ajax_delete_template(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($template_id)){
			$this->load->model('common');
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('audit_templates', 'id', $template_id, $update_data);
		}
		else
			redirect($this->router->fetch_class());
			
	}

	function update_app_sortorder(){
		$updateRecordsArray = $_POST['tr'];
		
		$listingCounter = 1;
		foreach ($updateRecordsArray as $recordIDValue) {			
			
			$query = "UPDATE applications SET position = " . $listingCounter . " WHERE id = " . $recordIDValue;
			mysql_query($query) or die(mysql_error());
			
			$listingCounter = $listingCounter + 1;			
		}		
	}

	function editor_loader(){
		$this->load->model('touchpoints_model');
		$this->load->view('audits/editors/touch-point');
	}

	function load_touchpoint_selection(){
		$this->load->model('touchpoints_model');
		$touch_point_list = $this->touchpoints_model->get_touchpoints_by_category($_POST['the_category']);
		
		if ($touch_point_list){
			echo " <select id='touch_point_id' name='touch_point_id'>";
			echo "<option value=''>Select a Touch Point</option>";
			foreach ($touch_point_list as $tp_list){
				echo "<option value='$tp_list->id'>$tp_list->touch_point</option>";
			}
			echo "</select>";
			//echo ' <button id="add_touch_point" type="submit" class="btn btn-primary">Add Touch Point</button>';
			//echo "<div><button type='submit' class='btn btn-primary'>Save</button><button id='cancel-touch-point' type='button' class='btn'>Cancel</button></div>";
		}
		
	}

	function add_touchPoint_in_template(){
		extract($_POST);
			
		$this->load->model('common');
			
		$insert_data = array(
			
			'template_id' => $template_id,
			'touchpoint_category' => $touchpoint_id,
			'touch_point_name' => $touch_point_name,
			'touch_point_id' => $touch_point_id,
			'status' => '1',
			'created_by' => $this->session->userdata('user_id')
            );
		
		$this->common->insert("audit_template_touchpoints", $insert_data);
		
		$this->session->set_flashdata('success_notification', 'You have successfully added a touch point in the template.');
		redirect($this->router->fetch_class().'/manage/'.$template_id);


		
	}

	/* Touch Point Manangement */
	function manage_tp(){
		/* 
		This is for Audit template editors to manage the touchpoints of a certain template
		*/
		$the_id = $this->uri->segment(3);
		
		$this->load->model('audits_model');
		$data['tp_details'] = $this->audits_model->Get_Touchpoint_of_a_Template_by_id($the_id);
		
		//Get the added Elements
		$data['elements'] = $this->audits_model->Get_All_Elements_from_a_Touchpoint($data['tp_details']->template_id,$the_id);
		/*
		$data['entries'] = $this->audits_model->Get_All_Touchpoints_of_a_Template($the_id);
		
		$this->load->model('touchpoints_model');
		$data['cat_list'] = $this->touchpoints_model->get_categories_list();
 		*/ //$tp_list = $this->touchpoints_model->get_all_touchpoints();

		//if ($data['entries']){

			$data['view_page'] = "audits/audit_templates_manage_tp_form";
			$data['page_title'] = "Manage Template - Touch Point: ".$data['tp_details']->touch_point_name;
			
			$data['bread_crumbs'] = array(
									'Audit Templates|'.base_url().$this->router->fetch_class(),
									$data['tp_details']->template_name.'|'.base_url().$this->router->fetch_class().'/manage/'.$data['tp_details']->template_id,
									$data['tp_details']->touch_point_name.'|'
									);
			//$data['action'] = "edit";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		//}
		//else
			//redirect($this->router->fetch_class());
	}

	function ajax_duplicate_tp(){
		extract($_POST);
		
		$created_by = $this->session->userdata('user_id');

		//duplicating Touchpoint
		$tp_sql = "INSERT INTO audit_template_touchpoints (template_id,touchpoint_category,touch_point_id,touch_point_name,status,created_by,date_created)".
							" SELECT template_id,touchpoint_category,touch_point_id,'$new_tp_name',status,$created_by,NOW()".
							" FROM audit_template_touchpoints WHERE id = $tp_id";
		$this->db->query($tp_sql);
		$latest_tp_id = $this->db->insert_id();

		//duplicating Touchpoint's elements		
		$e_sql = 	"SELECT *".
					" FROM audit_touchpoint_elements".
					" WHERE touchpoint_id = $tp_id".
					" AND status = 1 AND deleted = 0";
		$e_query = $this->db->query($e_sql);
		

		//return $e_sql;
		foreach ($e_query->result() as $row)
		{			
		    $e_sql2 = "INSERT INTO audit_touchpoint_elements (template_id,touchpoint_id,element_type,caption,status,position,date_created,created_by)".
							" SELECT template_id,$latest_tp_id,element_type,caption,status,position,NOW(),$created_by".
							" FROM audit_touchpoint_elements WHERE id = $row->id";
			$this->db->query($e_sql2);
			$latest_element_id = $this->db->insert_id();

			//duplicating element's entries
			$entries_sql = "INSERT INTO audit_touchpoint_entries (element_id,field_name,entry_type,status,dropdown_list,position,date_created,created_by)".
							" SELECT $latest_element_id,field_name,entry_type,status,dropdown_list,position,NOW(),$created_by".
							" FROM audit_touchpoint_entries WHERE element_id = $row->id AND deleted = 0";
			$this->db->query($entries_sql);
		}

		//return $test;
		
	}

	function ajax_delete_tp(){
		extract($_POST);		
		$update_data = array(
				'deleted' => 1
	            );		
		$this->load->model('common');
		//set the delete to 1 in the entry		
		$this->common->update('audit_template_touchpoints', 'id', $the_id, $update_data);
	}

	// Elements Level
	function Show_Elements(){
		extract($_POST);
		$this->load->view('audits/templates/'.$element_type);
		
	}

	function ajax_delete_elements(){
		extract($_POST);
		
		$delete_data = array(
				'id' => $element_id
	            );
		$delete_data2 = array(
				'element_id' => $element_id
	            );
		$this->load->model('common');
		//deleting the element
		$this->common->delete('audit_touchpoint_elements',$delete_data);
		//deleting the element's touchpoints
		$this->common->delete('audit_touchpoint_entries',$delete_data2);
		//return 'deleted';
	}

	function ajax_delete_entry(){
		extract($_POST);		
		$update_data = array(
				'deleted' => 1
	            );		
		$this->load->model('common');
		//set the delete to 1 in the entry		
		$this->common->update('audit_touchpoint_entries', 'id', $the_id, $update_data);
	}

	function add_tp_element_in_template(){
		extract($_POST);
		$caption = ucwords(str_replace("_"," ",$caption));
		
		$this->load->model('audits_model');
		if ($this->audits_model->Check_If_Element_Is_Added($touchpoint_id, $element_type)){
			//it exists, so it's not allowed to add the element
			$this->session->set_flashdata('notification_error', 'Element "'.$caption.'" is already added.');
			redirect($this->router->fetch_class().'/manage_tp/'.$touchpoint_id.'/');
		}
		else{
			//not exists, so create the Element
			$insert_data = array(
			
			'template_id' => $template_id,
			'touchpoint_id' => $touchpoint_id,
			'caption' => $caption,
			'element_type' => $element_type,
			'status' => '1',
			'deleted' => '0',
			'created_by' => $this->session->userdata('user_id')
            );
		
			$this->load->model('common');
			$this->common->insert("audit_touchpoint_elements", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a touch point element.');
			redirect($this->router->fetch_class().'/manage_tp/'.$touchpoint_id.'/#content'.$this->db->insert_id());
		}
		
		
	}

	/*function update_tp_element_in_template(){
		extract($_POST);
			
		if ($element_type == "interaction_overview"){
			
			$combined_answers = $text1.'|?|'.$text2.'|?|'.$text3;

			$insert_data = array(
			
			'element_id' => $element_id,
			'answer_date' => $answer_date,
			'answer_text' => $combined_answers,
			'answer_full' => $element_type,
			//'status' => '1',
			//'deleted' => '0',
			'created_by' => $this->session->userdata('user_id')
            );
		}
		else {

		}

		
		
		$this->common->insert("audit_touchpoint_elements", $insert_data);
		
		$this->session->set_flashdata('success_notification', 'You have successfully added a touch point element.');
		redirect($this->router->fetch_class().'/manage_tp/'.$touchpoint_id);
	}*/
	function update_tp_element_in_template(){
		extract($_POST);
			
			$update_data = array(
			
			'caption' => $caption,
			'modified_by' => $this->session->userdata('user_id')
            );

		$this->common->update('audit_touchpoint_elements', 'id', $element_id, $update_data);
		
		
		$this->session->set_flashdata('success_notification', 'You have successfully added a touch point element.');
		
		redirect($this->router->fetch_class().'/manage_tp/'.$touchpoint_id.'#content'.$element_id);
	}

	function add_tp_element_entries(){
		extract($_POST);
			
		
			$insert_data = array(
			
			'element_id' => $element_id,
			'field_name' => $field_name,
			'entry_type' => ($entry_type)?$entry_type:'',
			'answer_full_text' => ($answer_full_text)?$answer_full_text:'',
			'dropdown_list' => ($dropdown_list)?$dropdown_list:'',
			'status' => '1',
			//'deleted' => '0',
			'created_by' => $this->session->userdata('user_id')
            );
		
		$this->common->insert("audit_touchpoint_entries", $insert_data);
		
		//$this->session->set_flashdata('success_notification', 'You have successfully added a touch point element.');
		redirect($this->router->fetch_class().'/manage_tp/'.$touchpoint_id.'#content'.$element_id);
	}

	/* SORTING FUNCTIONS */
	function update_sortorder_entries(){
		$updateRecordsArray = $_POST['entry'];
		
		$listingCounter = 1;
		foreach ($updateRecordsArray as $recordIDValue) {			
			
			$query = "UPDATE audit_touchpoint_entries SET position = " . $listingCounter . " WHERE id = " . $recordIDValue;
			mysql_query($query) or die(mysql_error());
			
			$listingCounter = $listingCounter + 1;			
		}		
	}

	function update_sortorder_elements(){
		// This is called via AJAX when user is doing a drag sort in elements
		$updateRecordsArray = $_POST['sortelement'];
		
		$listingCounter = 1;
		foreach ($updateRecordsArray as $recordIDValue) {			
			
			$query = "UPDATE audit_touchpoint_elements SET position = " . $listingCounter . " WHERE id = " . $recordIDValue;
			mysql_query($query) or die(mysql_error());
			
			$listingCounter = $listingCounter + 1;			
		}		
	}

	function update_entries(){
		extract($_POST);		
		$update_data = array(
				'field_name' => $field_name,				
				'entry_type' => ($entry_type)?$entry_type:'',
				'answer_full_text' => ($answer_full_text)?$answer_full_text:'',
				'dropdown_list' => ($dropdown_list)?$dropdown_list:'',				
				'modified_by' => $this->session->userdata('user_id')
	            );
		$this->load->model('common');
		//set the delete to 1 in the entry		
		$this->common->update('audit_touchpoint_entries', 'id', $entry_id, $update_data);
		redirect($this->router->fetch_class().'/manage_tp/'.$touchpoint_id.'#content'.$element_id);
	}
}

/* End of file audit_templates.php */
/* Location: ./application/controllers/audit_templates.php */