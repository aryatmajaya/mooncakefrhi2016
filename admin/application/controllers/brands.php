<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* updates
02-04-2013 - added logo image upload
*/

class Brands extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		
		

		$this->load->model('brands_model');
		$data['entries'] = $this->brands_model->get_all_brands();

		$data['view_page'] = "brands/brands";
		$data['page_title'] = $this->current_app_id->app_name;


		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	

	public function add()
	{
		$this->form_validation->set_rules('client_id', 'Client', 'required|xss_clean');
		$this->form_validation->set_rules('brand_name', 'Brand', 'required|xss_clean|is_unique[brands.brand_name]');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['view_page'] = "brands/brands-form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "add";

			$this->load->model('common');
			$data['clients_list'] = $this->common->get_table_list('clients','client_name','ASC');

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');
			extract($_POST);

			$new_filename = '';

			if ($_FILES['logo_image']['name'] != "")
				$new_filename = mktime().'_'.$_FILES['logo_image']['name'];
			
			$insert_data = array(
				'client_id' => $client_id,
				'brand_name' => $brand_name,
				'logo_filename' => $new_filename,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("brands", $insert_data);
			$latest_id = $this->db->insert_id();

			//UPLOADING LOGO IMAGE
			$path = "../uploads/clients/$latest_id/";
			if(!file_exists($path))
			{
			   mkdir($path);
			}

			if ($_FILES['logo_image']['name'] != "") {
				
				$config['upload_path'] = $path;
				$config['file_name'] = $new_filename;
				$config['allowed_types'] = 'jpg|gif|png';
				$config['overwrite'] = TRUE;
				
				$this->load->library('upload', $config);
								
				$this->upload->do_upload('logo_image');
				
			}
			// End Upload Logo Image
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new brand.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('brands','brand_id',$the_id);

		if ($data['entries']){

			
			$data['clients_list'] = $this->common->get_table_list('clients','client_name','ASC');

			$data['view_page'] = "brands/brands-form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";			

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			
			$this->form_validation->set_rules('brand_name', 'Brand Name', 'required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{

				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {
				$new_filename = '';

				if ($_FILES['logo_image']['name'] != "")
					$new_filename = mktime().'_'.$_FILES['logo_image']['name'];
				
				if ($old_logo_file == "")
					$old_logo_file = "";
				
				$update_data = array(
						'client_id' => $client_id,
		               'brand_name' => $brand_name,
		               'status' => $status,
		               'modified_by' => $this->session->userdata('user_id'),
		               'logo_filename' => ($new_filename)?$new_filename:$old_logo_file
		            );
				
				$this->common->update('brands', 'brand_id', $id, $update_data);

				//UPLOADING LOGO IMAGE
				$path = "../uploads/clients/$id/";
				if(!file_exists($path))
				{
				   mkdir($path);
				}

				if ($_FILES['logo_image']['name'] != "") {
					$config['upload_path'] = $path;
					$config['file_name'] = $new_filename;
					$config['allowed_types'] = 'jpg|gif|png';
					$config['overwrite'] = TRUE;
					
					$this->load->library('upload', $config);
									
					$this->upload->do_upload('logo_image');					
				}

				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($brand_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('brands', 'brand_id', $brand_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

}

/* End of file brands.php */
/* Location: ./application/controllers/brands.php */