<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		$this->load->model('common');
		$data['entries'] = $this->common->get_all_records2('clients','client_name','ASC');

		$data['view_page'] = "generic/generic";
		$data['page_title'] = "Clients";

		$data['add_caption'] = " Add Client";
		$data['the_fields'] = array('1' => 'client_name', '2' => 'status');

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function add()
	{
			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Clients";
			$data['action'] = "add";

			$data['the_fields'] = array('1' => 'client_name|', '2' => 'status|');

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');			
	}

	public function insert()
	{
		$this->form_validation->set_rules('client_name', 'Client Name', 'required|xss_clean|is_unique[clients.client_name]');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('required_error', validation_errors());
			redirect($this->router->fetch_class().'/add/');
			//$this->add();
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
				
				'client_name' => $client_name,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("clients", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new client.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('clients','id',$the_id);

		if ($data['entries']){	

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "Clients";
			$data['action'] = "edit";			

			$data['the_fields'] = array('1' => 'client_name|', '2' => 'status|');

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');			
			
			$this->form_validation->set_rules('client_name', 'Client Name', 'required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('required_error', validation_errors());
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {			
				$update_data = array(
		               'client_name' => $client_name,
		               'status' => $status,
		               'modified_by' => $this->session->userdata('user_id')
		            );
				$this->common->update('clients', 'id', $id, $update_data);
				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('clients', 'id', $the_id, $update_data);
		}
		else
			redirect($this->router->fetch_class());			
	}

}

/* End of file clients.php */
/* Location: ./application/controllers/clients.php */