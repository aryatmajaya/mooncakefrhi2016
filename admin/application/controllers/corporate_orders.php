<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Corporate_orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('Check_permission');

		$this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
		$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
		
		//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
		if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
			redirect($this->router->fetch_class());
		} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
			redirect($this->router->fetch_class());
		} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
			redirect($this->router->fetch_class());
			
		} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
			redirect($this->router->fetch_class());
		}


		$this->load->library('Mc_utilities');
                $this->load->library('cart');

	}
	
	public function index($view_type = NULL, $page_number = NULL, $option1 = NULL) {
            $this->load->model('orders_model');
            $this->load->model('mc_transactions_model');
            
            $data['tool_type'] = null;
        
            if (!empty($option1)) {
                $the_option = explode('_', $option1);
                $data['option1'] = $option1;

                if ($the_option[0] == "totalsales") {
                    $data['total_sales'] = 1;
                    $data['from_date'] = $the_option[1];
                    $data['to_date'] = $the_option[2];
                }

                $data['tool_type'] = $the_option[0]; //assign what kind of tool being used
            } else {
                $data['from_date'] = NULL;
                $data['to_date'] = NULL;
                
            }
            
                if (isset($view_type) && $view_type != "all"){             
                    $get_code = Mc_utilities::order_status_id($view_type);
                    $status = $get_code[0];                
                }
                else {
                    $view_type = "all";
                    $status = 99;
                }
                
                $data['view_type'] = $view_type;

		
		
		$corporate_orders = orders_model::get_all_corporate_transactions2($status, NULL, NULL, NULL, NULL, $data['from_date'], $data['to_date']);
                
		if($corporate_orders){
			foreach($corporate_orders as $key=>$transaction){
				$corporate_orders[$key]->total_amount = $this->mc_transactions_model->Get_Total_Amount($transaction->id);
				$corporate_orders[$key]->raw_total = $this->mc_transactions_model->Get_Raw_Total_Qty($transaction->id);
				$corporate_orders[$key]->total_qty = $this->mc_transactions_model->Get_Total_Qty($transaction->id);
			}
		}else{
			$corporate_orders = array();
		}
		$data['transactions'] = $corporate_orders;
		$data['view_page'] = "corporate_orders/corporate_orders";
		$data['page_title'] = $this->current_app_id->app_name;
		
		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}
	public function index_test($view_type = NULL, $page_number = NULL, $option1 = NULL) {
            $this->load->model('orders_model');
            $this->load->model('mc_transactions_model');
            
                if (isset($view_type) && $view_type != "all"){             
                    $get_code = Mc_utilities::order_status_id($view_type);
                    $status = $get_code[0];                
                }
                else {
                    $view_type = "all";
                    $status = 99;
                }
                
                $data['view_type'] = $view_type;

		
		$corporate_orders = orders_model::get_all_corporate_transactions2($status, NULL, NULL, NULL, NULL, NULL, NULL);
                //var_dump($corporate_orders);die();
		if($corporate_orders){
			foreach($corporate_orders as $key=>$transaction){
				$corporate_orders[$key]->total_amount = $this->mc_transactions_model->Get_Total_Amount($transaction->id);
				$corporate_orders[$key]->raw_total = $this->mc_transactions_model->Get_Raw_Total_Qty($transaction->id);
				$corporate_orders[$key]->total_qty = $this->mc_transactions_model->Get_Total_Qty($transaction->id);
			}
		}else{
			$corporate_orders = array();
		}
		$data['transactions'] = $corporate_orders;
		$data['view_page'] = "corporate_orders/corporate_orders";
		$data['page_title'] = $this->current_app_id->app_name;
		
		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}
	
    function add_v1() {

            $this->load->model('corporate_orders_model');
            $data['corporate_customers'] = $this->corporate_orders_model->Get_Customers('corporate');
            
            $this->form_validation->set_rules('customer_type', 'Customer Type', 'xss_clean');
        
        if ($this->input->post('customer_type') == "new") { 
            $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean'); 
            $this->form_validation->set_rules('mobile', 'Contact No', 'required|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
        } else {
            $this->form_validation->set_rules('search_customer', 'Search Customer', 'xss_clean');
        }
		$this->load->model('users_model');
		
		if ($this->form_validation->run() == FALSE) {
			$customer = common::get_record_by_id('mc_customers', 'email', $this->session->userdata('suser_username'));//$data['corporate_customers'] = common::dynamic_query("SELECT * FROM mc_customers WHERE password='corporate-order-only'");
			$data['products'] = common::get_all_records3('mc_products', 'id', 'ASC');
			
			$data['user_data'] = (object) array(
				'first_name' => $this->session->userdata('user_firstname'),
				'last_name' => $this->session->userdata('user_firstname'),
			 	'email' => $this->session->userdata('suser_username')
			);

			if($customer){
				$data['user_data'] = $customer;
			}
			
			$data['action'] = "add";

			$data['view_page'] = "corporate_orders/order_form";
			$data['page_title'] = $this->current_app_id->app_name;

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		} else {
			extract($_POST);
			if ($customer_type == "existing") {
                            $customer_id = $search_customer;
                        } else {
                            //SAVING CUSTOMER DETAILS
                            $customer_data = array(
                                    'password' => '*corporate-order-only*',
                                    'first_name' => $first_name,
                                    'last_name' => $last_name,
                                    'company_name' => ($company_name)?$company_name:'',
                                    'address' => ($address)?$address:'',
                                    'postal_code' => ($postal_code)?$postal_code:'',
                                    'mobile' => $mobile,
                                    'email' => $email,
                                    'status' => 1,
                                    'corporate' => 1,
                                    'created_by' => $this->session->userdata('user_id')
                                                );
                            common::insert("mc_customers", $customer_data);
                            $customer_id = $this->db->insert_id();	
                            //END
			}
			
			//CREATE ORDER
			$this->load->model('orders_model');
			$the_sequence_id = Orders_model::GetMaxID();
			$order_ref = 'FC'.sprintf("%06d",$the_sequence_id+1); //create order_ref
		   
			$order_data = array(
				'order_ref' =>  $order_ref,
				'customer_id' => $customer_id,
				//'corporate_discount' => $total_discount * 100,
				'corporate_discount' => 0,
				'total_amount' => $total_price,
				//'discounted_amount' => $total_price * $total_discount,
				'discounted_amount' => 0,
				'status' => 10, //offline incomplete
				'ordering_method' => 'corporate',
				'admin_notes' => '',
				'admin_ordered_by' => $this->session->userdata('user_id')
			);

			Orders_model::add_order($order_data);
			$order_id = $this->db->insert_id();
			//END

			for ($i = 0; $i < count($product_qty); ++$i) {
				if ($product_qty[$i] > 0) {
					
					$items_data = array(
						'order_id' => $order_id,
						'product_id' => $product_id[$i],
						'product_name' => $product_name[$i],
						'qty' => $product_qty[$i],
						'price' => $product_price[$i]
					);
					common::insert("mc_orders_items", $items_data);
				}
			}
                        
                        /*
                         * ORDER SECTION
                         */
                        $order_section = $this->input->post('order_section');
                        $hot_stamping = $this->input->post('hot_stamping');
                        $logo_id = $this->input->post('logo_id');
                        $sales_manager = $this->input->post('sales_manager');
                        $customization = $this->input->post('customization_charge');
                        $payment_mode = $this->input->post('payment_mode');
                        
                        $info2_data = array(
                            'order_id' => $order_id,
                            'notes1' => (!empty($notes1))?$notes1:' ',
                            'notes2' => (!empty($notes2))?$notes2:' ',
                            'order_section' => ($order_section)?$order_section:' ',
                            'hot_stamping' => ($hot_stamping)?$hot_stamping:0,
                            'logo_id' => ($logo_id)?$logo_id:' ',
                            'sales_manager' => ($sales_manager)?$sales_manager:' ',
                            'customization' => ($customization)?$customization:0,
                            'payment_mode' => ($payment_mode)?$payment_mode:0
                            
                        );
                        common::insert("mc_orders_corporate", $info2_data);
                        
                        
			$this->session->set_flashdata('success_notification', "Order has been saved.");
			redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
		}
	}
        
    function add() {
        /*
         * Version 2 by James Castaneros - 24 July 2014
         * Change from saving the orders right away in database , to session first
         * and once order complete  then save to database
         */
        $this->session->unset_userdata('customer_id');
        $this->session->unset_userdata('co_customer');
        $this->session->unset_userdata('co_order');
        $this->session->unset_userdata('co_cart');
        $this->session->unset_userdata('co_fulfillment');
        $this->session->unset_userdata('co_orderinfo2');
        
            $this->load->model('corporate_orders_model');
            $data['corporate_customers'] = $this->corporate_orders_model->Get_Customers('corporate');
            
            $data['sales_managers'] = corporate_orders_model::Get_SalesManagers();
            
            $this->form_validation->set_rules('customer_type', 'Customer Type', 'xss_clean');
        
        if ($this->input->post('customer_type') == "new") { 
            $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean'); 
            $this->form_validation->set_rules('mobile', 'Contact No', 'required|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
        } else {
            $this->form_validation->set_rules('search_customer', 'Search Customer', 'xss_clean');
        }
		$this->load->model('users_model');
		
		if ($this->form_validation->run() == FALSE) {
			//$customer = common::get_record_by_id('mc_customers', 'email', $this->session->userdata('suser_username'));//$data['corporate_customers'] = common::dynamic_query("SELECT * FROM mc_customers WHERE password='corporate-order-only'");
			$data['products'] = common::get_all_records3('mc_products', 'id', 'ASC');
			
			/*$data['user_data'] = (object) array(
				'first_name' => $this->session->userdata('user_firstname'),
				'last_name' => $this->session->userdata('user_firstname'),
			 	'email' => $this->session->userdata('suser_username')
			);

			if($customer){
				$data['user_data'] = $customer;
			}
                         * 
                         */
                        
                        
                        //Order Section
                        $data['co_orderinfo2'] = $this->session->userdata('co_orderinfo2');
                        
                        
                        
			
			$data['action'] = "add";

			$data['view_page'] = "corporate_orders/order_form";
			$data['page_title'] = $this->current_app_id->app_name;

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		} else {
			extract($_POST);
			if ($customer_type == "existing") {
                            $customer_id = $search_customer;
                            $this->session->set_userdata('customer_type', 'existing');
                            $this->session->set_userdata('customer_id', $customer_id);
                        } else {
                            $this->session->set_userdata('customer_type', 'new');
                            //SAVING CUSTOMER DETAILS TO SESSION
                            $customer_data = array(
                                    'password' => '*corporate-order-only*',
                                    'first_name' => $first_name,
                                    'last_name' => $last_name,
                                    'company_name' => ($company_name)?$company_name:'',
                                    'address' => ($address)?$address:'',
                                    'postal_code' => ($postal_code)?$postal_code:'',
                                    'mobile' => $mobile,
                                    'email' => $email,
                                    'status' => 1,
                                    'corporate' => 1,
                                    'created_by' => $this->session->userdata('user_id')
                                                );
                            //common::insert("mc_customers", $customer_data);
                            $this->session->set_userdata('co_customer', $customer_data);	
                            //$customer_id = $this->db->insert_id();	
                            //END
			}
                        //$cool = $this->session->userdata('co_customer');
                        
                        
                        //SAVING ORDER TO SESSION
                        $order_data = array(
                            'corporate_discount' => (!empty($corporate_discount))?$corporate_discount:0
                        );
                        $this->session->set_userdata('co_order', $order_data);
                        
                        
                        //SAVING ORDER ITEMS TO CART
                        //$this->cart->destroy();
                        $this->session->unset_userdata('co_cart');
                        for ($i = 0; $i < count($product_qty); ++$i) {
				
				if ($product_qty[$i] > 0):	
					                                        
					$this->cart->insert($cart_data);

                                        $cart[$i]['id'] = $product_id[$i];
                                        $cart[$i]['name'] = $product_name[$i];
                                        $cart[$i]['qty'] = $product_qty[$i];
                                        $cart[$i]['price'] = $product_price[$i];
                                        

                                        $this->session->set_userdata('co_cart',$cart);
                    
                                        
                                        
                                        
                                endif;
				
			}
                        
                        //echo $this->cart->total();
                        //var_dump($this->cart->contents()); die();
                        
			
			

			
                        
                        /*
                         * ORDER SECTION
                         */
                        $order_section = $this->input->post('order_section');
                        $hot_stamping = $this->input->post('hot_stamping');
                        $logo_id = $this->input->post('logo_id');
                        $sales_manager = $this->input->post('sales_manager');
                        $customization = $this->input->post('customization_charge');
                        $payment_mode = $this->input->post('payment_mode');
                        
                        $info2_data = array(
                            'order_id' => $order_id,
                            'notes1' => (!empty($notes1))?$notes1:' ',
                            'notes2' => (!empty($notes2))?$notes2:' ',
                            'order_section' => ($order_section)?$order_section:' ',
                            'hot_stamping' => ($hot_stamping)?$hot_stamping:0,
                            'logo_id' => ($logo_id)?$logo_id:' ',
                            'sales_manager' => ($sales_manager)?$sales_manager:' ',
                            'customization' => ($customization)?$customization:0,
                            'payment_mode' => ($payment_mode)?$payment_mode:0
                            
                        );
                        
                        $this->session->set_userdata('co_orderinfo2', $info2_data);                        
			
			redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
		}
	}


    function edit($order_id) {
        $this->load->model('corporate_orders_model');
        $this->load->model('orders_model');
        $this->load->model('common');

        $data['order_details'] = corporate_orders_model::Order_Details($order_id);
        $data['products'] = common::get_all_records3('mc_products', 'id', 'ASC');

        $data['items'] = $this->orders_model->Get_Ordered_Items($order_id);
        $data['delivery_options'] = $this->orders_model->Get_Delivery_Option($data['order_details']->order_ref);
        
        $data['sales_managers'] = corporate_orders_model::Get_SalesManagers();
        
        $data['bc'] = $this->orders_model->get_all_block_dates('collection'); //block collection dates
        $data['bd'] = $this->orders_model->get_all_block_dates('delivery'); //block delivery dates

        $data['redirect_url'] = "edit";
        $data['action'] = "add";
        
        $data['view_page'] = "corporate_orders/order_form_edit";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
	
	function fulfillment_options($order_id = NULL) {
            $this->load->model('orders_model');
            if ($order_id):
		
		$data['order_details'] = orders_model::Order_Summary_byID($order_id);
		//$data['items'] = $this->session->userdata('sess_co_cart');
		$data['items'] = orders_model::Get_Ordered_Items($order_id);
		
		//Get delivery options to display it
		$data['delivery_options'] = orders_model::Get_Delivery_Option($data['order_details']->order_ref);
		//$data['total_delivery'] = orders_model::Get_Total_Delivery($order_details->order_ref);
		$data['bc'] = orders_model::get_all_block_dates('collection'); //block collection dates
		$data['bd'] = orders_model::get_all_block_dates('delivery'); //block delivery dates

		$data['action'] = "add";

		$data['view_page'] = "corporate_orders/fulfillment_options_form";
		$data['page_title'] = $this->current_app_id->app_name;

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
            else:
                
                $co_order = $this->session->userdata('co_order');
                $data['corporate_discount'] = (!empty($co_order['corporate_discount']))?$co_order['corporate_discount']:0;
                //var_dump($co_order);die();
                $data['delivery_charge'] = $this->session->userdata('delivery_charge');
                
                $data['items'] = $this->session->userdata('co_cart');
                //var_dump($data['items']);die();
                
                $data['fulfillment_options'] = $this->session->userdata('co_fulfillment');
                //var_dump($data['fulfillment_options']); die();
            
                $data['bc'] = orders_model::get_all_block_dates('collection'); //block collection dates
		$data['bd'] = orders_model::get_all_block_dates('delivery'); //block delivery dates
                
                $data['action'] = "add";

		$data['view_page'] = "corporate_orders/fulfillment_options_form";
		$data['page_title'] = $this->current_app_id->app_name;
                
                $this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
            endif;
	}
	
	function add_fulfillment_v1() {
		$this->load->model('orders_model');
		$this->form_validation->set_rules('delivery_date_field', 'Delivery Date', 'required|xss_clean'); 
		//die(print_r($_SERVER));
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('fulfillment_required_error', 'Date is required. Please fillup the date from Fulfillment Options.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$order_id = $this->input->post('order_id');
		$order_ref = $this->input->post('order_ref');
		$customer_id = $this->input->post('customer_id');
		
		if (isset($order_ref)) {
			if ($this->input->post('service_type') == "self collection"){
				$insert_data = array(
					'order_ref' => $order_ref,
					'customer_id' => $customer_id,
					'service_type' => $this->input->post('service_type'),
					'service_date' => $this->input->post('delivery_date_field')
				);
			}else{ //delivery
				$insert_data = array(
					'order_ref' => $order_ref,
					'customer_id' => $customer_id,
					'service_type' => $this->input->post('service_type'),
					'service_date' => $this->input->post('delivery_date_field'),
					'time_slot' => $this->input->post('time_slot'),
					'address' => $this->input->post('address'),
					'postal_code' => $this->input->post('postal_code'),
					'delivered_to' => $this->input->post('delivered_to'),
					'delivered_contact' => $this->input->post('delivered_contact')
				);            
			}
			orders_model::add_delivery($insert_data);
			$delivery_id = $this->db->insert_id();
		}
		
		
		for ($i = 0; $i < count($_POST['product_qty']); ++$i) {
			if ($_POST['product_qty'][$i] > 0) {
				//echo $_POST['product_name'][$i].' '.$_POST['product_qty'][$i].'<br />';
				$items_data = array(
					'delivery_id' => $delivery_id,
					'order_ref' => $order_ref,
					'product_id' => $_POST['product_id'][$i],
					'product_name' => $_POST['product_name'][$i],
					'qty' => $_POST['product_qty'][$i]
				);
				orders_model::add_delivery_item($items_data);
			}
		}
		redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
		
	}
        
	function add_fulfillment() {
            /* version 2 by James Castaneros - 24 July 2014
             * This is in session mode
             */
		$this->form_validation->set_rules('delivery_date_field', 'Delivery Date', 'required|xss_clean'); 
		//die(print_r($_SERVER));
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('fulfillment_required_error', 'Date is required. Please fillup the date from Fulfillment Options.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
			if ($this->input->post('service_type') == "self collection"){
				$fulfillment_data = array(
					'service_type' => $this->input->post('service_type'),
					'service_date' => $this->input->post('delivery_date_field')
				);
			}else{ //delivery
				$fulfillment_data = array(
					'service_type' => $this->input->post('service_type'),
					'service_date' => $this->input->post('delivery_date_field'),
					'time_slot' => $this->input->post('time_slot'),
					'address' => $this->input->post('address'),
					'postal_code' => $this->input->post('postal_code'),
					'delivered_to' => $this->input->post('delivered_to'),
					'delivered_contact' => $this->input->post('delivered_contact')
				);
			}
			$this->session->set_userdata('co_fulfillment', $fulfillment_data);
                        
                        $delivery_charge = $this->input->post('delivery_charge');
			$this->session->set_userdata('delivery_charge', (empty($delivery_charge))?0:$delivery_charge);              					
			
		
		redirect($this->router->fetch_class().'/fulfillment_options/');
		
	}
	
	function delete_option_v1($delivery_id, $order_id, $order_ref){
		$this->load->model('orders_model');
		if (isset($delivery_id) && is_numeric($delivery_id)){
			orders_model::Delete_Delivery($delivery_id, $order_ref);
		}
		redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
	}
	function delete_option(){
		$this->session->unset_userdata('co_fulfillment');
		redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
	}
	
	function print_report_PDF($from_date = NULL, $to_date = NULL) {
		$status = 8;
		$this->load->model('Mc_delivery_report_model');
		
		$status = 8;
		$this->load->model('Mc_delivery_report_model');
		$data['transactions'] = Mc_delivery_report_model::get_all_deliveries($status, (isset($from_date))?$from_date:null, (isset($to_date))?$to_date:null);
		//var_dump($data['transactions']);
		$data['print_mode'] = 1;
		
			//$the_html = $this->load->view('mc_delivery_report/pdf_report', $data, (!isset($debug))?true:false);
			$the_html = $this->load->view('mc_delivery_report/pdf_report', $data, true);
		
			
			
			// Export to PDF using mPDF
			$this->load->library('mpdf56');
			$pdf = $this->mpdf56->load();
			
			ini_set("memory_limit","2064M");
			
			//CONFIGURATIONS
			//$pdf->showStats = true;
			$pdf->showImageErrors = false;
			$pdf->shrink_tables_to_fit=1;
			
			 //APPLYING CSS
			$cssFilePath = FCPATH."/assets/css/";
			$stylesheet = file_get_contents($cssFilePath."report_print.css");
			$pdf->WriteHTML($stylesheet,1);
			$pdf->use_kwt = true;
			
			$pdf->WriteHTML($the_html,2);
			$pdf->Output('report.pdf', 'I');
		 
	}
	
	function print_deliverynotes_PDF($delivery_ids,$debug = NULL) {
		$this->load->model('mc_delivery_report_model');
		$this->load->model('mc_transactions_model');

		$the_ids = explode('-', $delivery_ids);
		$the_notes = "";
		foreach ($the_ids as $ids) {
			$data['delivery_id'] = $ids;
			$data['order'] = mc_delivery_report_model::Get_OrderDetails_by_deliveryID($ids);
			$data['items'] = mc_delivery_report_model::Get_OrderItems_by_deliveryID($ids);
			
			
			$the_notes .= $this->load->view('mc_delivery_report/delivery_notes_table', $data, (!isset($debug))?true:false);
			
		}
		// Export to PDF using mPDF
		$this->load->library('mpdf56');
		$pdf = $this->mpdf56->load();
		
		ini_set("memory_limit","2064M");
		
		//CONFIGURATIONS
		//$pdf->showStats = true;
		$pdf->showImageErrors = false;
		$pdf->shrink_tables_to_fit=1;
		
		 //APPLYING CSS
		$cssFilePath = FCPATH."/assets/css/";
		$stylesheet = file_get_contents($cssFilePath."report_print.css");
		$pdf->WriteHTML($stylesheet,1);
			
		//SETTING THE FOOTER
		$the_footer = $this->load->view('mc_delivery_report/delivery_notes_footer', $data, (!isset($mode))?true:false);
		$pdf->SetHTMLFooter($the_footer);
		
		$pdf->use_kwt = true;
			
		if (!isset($debug)){
			$pdf->WriteHTML($the_notes,3);
			$pdf->Output('delivery_notes.pdf', 'I');
		}
		 
	}
	
	function test_pdf() {
		$this->load->library('mpdf56');
		$mpdf=$this->mpdf56->load();
		$mpdf->WriteHTML('<columns column-count="3" vAlign="J" column-gap="7" />');
		$mpdf->WriteHTML('<p>Some text...</p>');
		$mpdf->WriteHTML('<columnbreak />');
		$mpdf->WriteHTML('<p>Next column...</p>');
		$mpdf->Output("testmpdf.pdf","I");
	}
	
	function total_sales() {
		extract($_POST);
		redirect(BASE_URL.$this->router->fetch_class().'/index/'.$view_type.'/0/totalsales_'.$from_date.'_'.$to_date);
	}

	public function view($the_id){
		$this->load->model('mc_transactions_model');
		$data['entry'] = mc_transactions_model::Get_Order_Details($the_id);
		$data['total'] = mc_transactions_model::Get_Total_Amount($the_id);
		$data['items'] = mc_transactions_model::Get_Ordered_Items($the_id);
		$data['status_list'] = mc_utilities::order_status();

		if ($data['entry']){
			$data['fo'] = Mc_transactions_model::Get_All_Fulfillment_Options($data['entry']->order_ref);

			//This is to get the number of deliveries to be used for counting of number of deliveries
			$data['deliveries'] = Mc_transactions_model::Get_Total_Delivery_Qty($data['entry']->order_ref);

			$data['view_page'] = "mc_transactions/mc_transactions_view";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
	}

    public function update(){
        $this->load->model('mc_transactions_model');
        extract($_POST);
        
        // UPDATE ORDER DETAILS
        $update_data = array(
            //'corporate_discount' => $total_discount * 100,
            //'total_amount' => $total_price,
            //'discounted_amount' => $total_discount * $total_price,
            'date_modified' => date('Y-m-d H:i:s')
        );

        common::update('mc_orders', 'id', $order_id, $update_data);
        
        $update_data2 = array(
            'notes1' => (!empty($notes1))?$notes1:'',
            'notes2' => (!empty($notes2))?$notes2:'',
            'order_section' => $order_section,
            'hot_stamping' => (!empty($hot_stamping))?$hot_stamping:0,
            'logo_id' => (!empty($logo_id))?$logo_id:'',
            'sales_manager' => (!empty($sales_manager))?$sales_manager:'',
            'customization' => (!empty($customization_charge))?$customization_charge:0,
            'payment_mode' => (!empty($payment_mode))?$payment_mode:''
        );
        common::update('mc_orders_corporate', 'order_id', $order_id, $update_data2);
        

        //UPDATE ORDER ITEMS
        $delete_data = array(
            'order_id' => $order_id
        );
        common::delete('mc_orders_items', $delete_data);
        for ($i = 0; $i < count($_POST['product_qty']); ++$i) {
                if ($_POST['product_qty'][$i] > 0) {
                    //echo $_POST['product_name'][$i].'<br />';
                    $items_data = array(
                        'order_id' => $order_id,
                        'product_id' => $_POST['product_id'][$i],
                        'product_name' => $_POST['product_name'][$i],
                        'qty' => $_POST['product_qty'][$i],
                        'price' => $_POST['product_price'][$i]
                    );
                    common::insert("mc_orders_items", $items_data);
                }
        }
        
        $this->session->set_flashdata('success_notification', 'Updated Successfully');
        redirect($this->router->fetch_class().'/corporate_orders/');
        
    }
    
    function save_order() {
        
        $customer_type = $this->session->userdata('customer_type');
        
        if ($customer_type == "existing"):
            $customer_id = $this->session->userdata('customer_id');
            
        else:
            //SAVING CUSTOMER DETAILS
            $co_customer = $this->session->userdata('co_customer');
            $customer_data = array(
                'password' => '*corporate-order-only*',
                'first_name' => $co_customer['first_name'],
                'last_name' => $co_customer['last_name'],
                'company_name' => ($co_customer['company_name']) ? $co_customer['company_name'] : '',
                'address' => ($co_customer['address']) ? $co_customer['address'] : '',
                'postal_code' => ($co_customer['postal_code']) ? $co_customer['postal_code'] : '',
                'mobile' => $co_customer['mobile'],
                'email' => $co_customer['email'],
                'status' => 1,
                'corporate' => 1,
                'created_by' => $this->session->userdata('user_id')
            );
            common::insert("mc_customers", $customer_data);
            $customer_id = $this->db->insert_id();
            
            //END
        endif;
        
        
        
        
        
        //SAVING ORDER TO DATABASE
        $co_order = $this->session->userdata('co_order');
        
        $this->load->model('orders_model');
        $the_sequence_id = Orders_model::GetMaxID();
        $order_ref = 'FC'.sprintf("%06d",$the_sequence_id+1); //create order_ref
        
        $subtotal_amount = $this->input->post('subtotal_amount');
        $discount_amount = $this->input->post('discount_amount');
        
        $delivery_charge = $this->session->userdata('delivery_charge');

        $order_data = array(
                'order_ref' =>  $order_ref,
                'customer_id' => $customer_id,
                //'corporate_discount' => $total_discount * 100,
                'corporate_discount' => (!empty($co_order['corporate_discount']))?$co_order['corporate_discount']:0,
                'total_amount' => $subtotal_amount,
                //'discounted_amount' => $total_price * $total_discount,
                'delivery_charge' => (!empty($delivery_charge))?$delivery_charge:0,
                'discounted_amount' => $discount_amount,
                'status' => 10, //offline incomplete
                'ordering_method' => 'corporate',
                'admin_notes' => '',
                'admin_ordered_by' => $this->session->userdata('user_id')
        );

        Orders_model::add_order($order_data);
        $order_id = $this->db->insert_id();
        
        //END
        
        //SAVING ORDER ITEMS TO DATABASE
        $items = $this->session->userdata('co_cart');        
        foreach($items as $key=>$item) {
            $items_data = array(
                    'order_id' => $order_id,
                    'product_id' => $item['id'],
                    'product_name' => $item['name'],
                    'qty' => $item['qty'],
                    'price' => $item['price']
            );
            common::insert("mc_orders_items", $items_data);
        }
        
        //END
        
        //SAVING FULFILLMENT OPTIONS TO DATABASE
        $co_fulfillment = $this->session->userdata('co_fulfillment');
        $fulfilment_data = array(
            'order_ref' => $order_ref,
            'customer_id' => $customer_id,
            'service_type' => $co_fulfillment['service_type'],
            'service_date' => $co_fulfillment['service_date'],
            'time_slot' => $co_fulfillment['time_slot'],
            'address' => $co_fulfillment['address'],
            'postal_code' => $co_fulfillment['postal_code'],
            'delivered_to' => $co_fulfillment['delivered_to'],
            'delivered_contact' => $co_fulfillment['delivered_contact']
        );            
        orders_model::add_delivery($fulfilment_data);
        $delivery_id = $this->db->insert_id();
        
        
        //SAVING FULFILLMENT ITEMS TO DATABASE
        
        foreach($items as $key=>$item) {
            $items_data = array(
                    'delivery_id' => $delivery_id,
                    'order_ref' => $order_ref,
                    'product_id' => $item['id'],
                    'product_name' => $item['name'],
                    'qty' => $item['qty']
            );
            orders_model::add_delivery_item($items_data);
        }
        
        //END
        
        //SAVING COPORATE ORDER INFO TO DATABASE
        $co_orderinfo2 = $this->session->userdata('co_orderinfo2');
        
        $info2_data = array(
            'order_id' => $order_id,
            'notes1' => (!empty($co_orderinfo2['notes1']))?$co_orderinfo2['notes1']:' ',
            'notes2' => (!empty($co_orderinfo2['notes2']))?$co_orderinfo2['notes2']:' ',
            'order_section' => $co_orderinfo2['order_section'],
            'hot_stamping' => $co_orderinfo2['hot_stamping'],
            'logo_id' => $co_orderinfo2['logo_id'],
            'sales_manager' => $co_orderinfo2['sales_manager'],
            'customization' => $co_orderinfo2['customization'],
            'payment_mode' => $co_orderinfo2['payment_mode']
        );
        common::insert("mc_orders_corporate", $info2_data);
        
        //END
        
        //Now send a mail notification
        $this->send_confirmation($order_ref);
        
        $this->session->unset_userdata('customer_id');
        $this->session->unset_userdata('co_customer');
        $this->session->unset_userdata('co_order');
        $this->session->unset_userdata('co_cart');
        $this->session->unset_userdata('co_fulfillment');
        $this->session->unset_userdata('co_orderinfo2');
                
        redirect($this->router->fetch_class().'/corporate_orders/');
    }
    
    function print_slips_PDF($delivery_ids, $debug = NULL) {
        $this->load->model('corporate_orders_model');
        $this->load->model('mc_transactions_model');
        $this->load->model('mc_delivery_report_model');
        
        
        $data['print'] = 1;
        $data['service_type'] = 'corporate';
        $data['debug'] = $debug;
        
        $the_ids = explode('-', $delivery_ids);
       // $the_notes = "";
        
        //$data['products'] = common::get_all_records3('mc_products', 'receipt_sort_order', 'ASC');
        //var_dump($data['products']);
        
        
            
        // Export to PDF using mPDF
        $this->load->library("html2pdf/pdf");
		$this->pdf->setDefaultFont('kozgopromedium');
		
		//$this->load->library('mpdf56');
        //$pdf = $this->mpdf56->load();

        //ini_set("memory_limit","2064M");

        //CONFIGURATIONS
        //$pdf->showStats = true;
      //  $pdf->showImageErrors = false;
        //$pdf->shrink_tables_to_fit=1;

        //APPLYING CSS
       // $cssFilePath = FCPATH."/assets/css/";
       // $stylesheet = file_get_contents($cssFilePath."report_print.css");
       // $pdf->WriteHTML($stylesheet,1);
        
		
		
        foreach ($the_ids as $ids) {
            $data['delivery_id'] = $ids;
            $data['order'] = corporate_orders_model::Get_OrderDetails_by_TransactionID($ids);
            $data['items'] = corporate_orders_model::Get_OrderItems_by_OrderID($ids);
			
			//echo $ids. " ";
            //var_dump($data['items']);
			
			$content = "";
            
			$content .= "<page backtop='120px' backbottom='100px'>";
			$content .= "<page_header>";
			$content .= $this->load->view('mc_delivery_report/delivery_notes_header', $data, true);
			$content .= "</page_header>";
			
            $content .= $this->load->view('mc_delivery_report/delivery_notes_table', $data, true);
            //$pdf->AddPage();
           // $pdf->WriteHTML($the_notes);
		   
		   	$content .= '<page_footer>';
        	$content .= $this->load->view('mc_delivery_report/delivery_notes_footer', $data, true);
       	 	$content .= '</page_footer>';
            
            //SETTING THE FOOTER
       		//$the_footer = $this->load->view('mc_delivery_report/delivery_notes_footer', $data, (!isset($mode))?true:false);
        	//$pdf->SetHTMLFooter($the_footer);
			
			$content .= "</page>";
			
			$this->pdf->WriteHTML($content);
        }

        

        
        $this->pdf->Output($data['service_type'] .'-notes.pdf');
        //$pdf->autoPageBreak = true;
        //$pdf->use_kwt = true;
            
        //if (!isset($debug)){
            //$pdf->WriteHTML($the_notes,3);
            //$pdf->writeBarcode('978-1234-567-890');
          //  $pdf->Output($service_type.'-notes.pdf', 'I');
        //}
    }
    
    public function totals234($order_ref) {
        
        common::compute_total_amount($order_ref);
        //var_dump($ans);
        //echo $order_ref;
       
		
		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1');
		$this->load->view('elements/footer');
        
    }
	
	
	/* AJAX Functions */
	
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($property_id)){
			$this->load->model('common');
			
			$update_data = array(
			   'deleted' => 1		            
			);
			
			$this->common->update('properties', 'property_id', $property_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

	function ajax_corporate_discount(){
		echo json_encode(array(
			array('min'=>1, 'max'=>100, 'discount'=>.2),
			array('min'=>101, 'max'=>300, 'discount'=>.25),
			array('min'=>301, 'max'=>500, 'discount'=>.28),
			array('min'=>500, 'max'=>1000000, 'discount'=>.30),
		));
	}
		
	function test_ajax4pdf($delivery_ids) {
		$this->load->library('mpdf56');
		$mpdf=$this->mpdf56->load();
		$mpdf->WriteHTML('<columns column-count="3" vAlign="J" column-gap="7" />');
		
		$the_ids = explode('-', $delivery_ids);
		
		foreach ($the_ids as $ids) {
			$mpdf->WriteHTML($ids);
			
		}
		
		
		$mpdf->WriteHTML('<columnbreak />');
		$mpdf->WriteHTML('<p>Next column...</p>');
		$mpdf->Output("testmpdf.pdf","I");
	}
        
        function test123() {
            echo 'test';
        }
        
	public function get_customer_details() {
        $customer_id = $this->input->post('customer_id');
        
        $customer_details = common::get_record_by_id('mc_customers', 'id', $customer_id);
        echo json_encode($customer_details);
        
    }
    
    function display_customer($customer_id) {
       $data['customer']  = common::get_record_by_id('mc_customers', 'id', $customer_id);
       $this->load->view('corporate_orders/iframe_customer_info', $data);
		
       
    }
    
     function send_confirmation($order_ref = null){
        /* 
         * Created by James Castaneros - 05 August 2014
         * Notes: This method is a tool to send confirmation
         */
        if ($order_ref){
            Mc_utilities::Send_Confirmation($order_ref);
        }
    }
    
    function manual_send_confirmation($order_ref = null){
        /* 
         * Created by James Castaneros - 05 August 2014
         * Notes: This method is a tool to send confirmation manually by CPR Vision
         */
        if ($order_ref){
            Mc_utilities::Send_Confirmation($order_ref);
        }
    }
    
    function view_notification($order_ref = null) {
        $data = Mc_utilities::Send_Confirmation($order_ref, NULL, 'view');
        var_dump($data);
        
    }
	
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */