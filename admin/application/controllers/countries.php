<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countries extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
	
	public function index()
	{
		$this->load->model('countries_model');

		$data['view_page'] = "countries/countries";
		$data['page_title'] = "Countries";

		$data['entries'] = $this->countries_model->get_all_countries();
		

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function add()
	{
		
			$this->load->model('regions_model');
			$data['regions_list'] = $this->regions_model->get_regions_list();

			$data['view_page'] = "countries/countries-form";
			$data['page_title'] = "Countries";
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		
	}

	public function insert()
	{
		//$hdata['restricted'] = 1;
		
		//echo count($_POST['song_title']);
		/*
		foreach($_POST['song_title'] as $song_info){
			echo 'song: '.$song_title.'<br />';
		}*/
		
		
		$this->form_validation->set_rules('region_id', 'Region', 'required|xss_clean');
		$this->form_validation->set_rules('country', 'Country', 'required|xss_clean|is_unique[countries.country]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('required_error', validation_errors());
			redirect('/countries/add/');
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
				'region_id' => $region_id,
				'country' => $country,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("countries", $insert_data);
			
			$this->session->set_flashdata('success', 'You have successfully added a new country.');
			redirect('/countries/');
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		
		$this->load->model('regions_model');
		$data['regions_list'] = $this->regions_model->get_regions_list();

		$this->load->model('countries_model');
		$data['entries'] = $this->countries_model->get_record($the_id);
		
			
		if ($data['entries']){
			$data['view_page'] = "countries/countries-form";
			$data['page_title'] = "Countries";
			$data['action'] = "edit";
			
			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
		
	}

	public function update(){
		$this->load->model('common');
		extract($_POST);
		
		$this->form_validation->set_rules('country', 'Country', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{

			$this->session->set_flashdata('required_error', validation_errors());
			redirect('/countries/edit/'.$id);
		}
		else {
		
			$update_data = array(
	               'region_id' => $region_id,
	               'country' => $country,
	               'status' => $status
	            );
			
			$this->common->update('countries', 'country_id', $id, $update_data);
			$this->session->set_flashdata('notification_status', 'Updated Successfully');
			
			//redirect('/regions/edit/'.$id);
			redirect($this->router->fetch_class());
		}
	}

	/* AJAX Functions */
	

	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($country_id)){
			$this->load->model('common');	
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('countries', 'country_id', $country_id, $update_data);
		}
		else
			redirect($this->router->fetch_class());			
	}
	
}

/* End of file countries.php */
/* Location: ./application/controllers/countries.php */