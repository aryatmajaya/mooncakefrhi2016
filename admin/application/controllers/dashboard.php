<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	
	public function index()
	{

		$data['view_page'] = "dashboard/dashboard";
		$data['page_title'] = "Dashboard";

		$this->load->view('elements/header');
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */