<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discount_module extends CI_Controller {

	public function __construct() {
        parent::__construct();
		
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        $this->load->model('discount_module_model');
    }

    public function index()
    {
        $data['entries'] = $this->discount_module_model->get_all();
        
        $data['view_page'] = "discount_module/discount_module";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }

    function edit($id){
		$this->form_validation->set_rules('mid', 'MID', 'xss_clean|required');
 		
        $this->load->model("discount_module_model");

       	if($this->input->post("submit_form") && $this->form_validation->run()){
			$this->discount_module_model->update($id);
			$this->session->set_flashdata('success_notification', 'Data updated successfully.');
			redirect($this->router->fetch_class());
		}
		
		$this->getForm($id);
    }

     private function getForm($id){
		
		if($id){
			$discount_module_info = $this->discount_module_model->get($id);
			if($discount_module_info){
				$data['view_page'] = "discount_module/discount_module_form";
        		$data['page_title'] = $this->current_app_id->app_name;
				
				$value = array();
		
				$value['title'] = $discount_module_info->title;
				
				if(isset($_POST['mid'])){
					$value['mid'] = $this->input->post("mid");
				}else{
					$value['mid'] = $discount_module_info->mid;
				}
				
				if(isset($_POST['discount'])){
					$value['discount'] = $this->input->post("discount");
				}else{
					$value['discount'] = $discount_module_info->discount;
				}
				
				if(isset($_POST['status'])){
					$value['status'] = $this->input->post("status");
				}else{
					$value['status'] = $discount_module_info->status;
				}
				
				if(isset($_POST['sort_order'])){
					$value['sort_order'] = $this->input->post("sort_order");
				}else{
					$value['sort_order'] = $discount_module_info->sort_order;
				}
				
				if(isset($_POST['default'])){
					$value['default'] = $this->input->post("default");
				}else{
					$value['default'] = $discount_module_info->default;
				}
				
				$data['value'] = $value;
				
				$this->load->view('elements/header', $this->header_data);
				$this->load->view('elements/template1', $data);
				$this->load->view('elements/footer');
				
			}else{
				die("data not found");	
			}
		}else{
			die("data not found");		
		}
		
    }
}