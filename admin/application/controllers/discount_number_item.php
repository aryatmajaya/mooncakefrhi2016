<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discount_number_item extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        $this->load->model('discount_number_item_model');
    }

    public function index()
    {
        $data['entries'] = $this->discount_number_item_model->get_all();
        
        $data['view_page'] = "discount_number_item/discount_number_item";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }

    function edit($id){
		$this->form_validation->set_rules('start_date', 'Start date', 'xss_clean|required');
        $this->form_validation->set_rules('end_date', 'End date', 'xss_clean|required');
		$this->form_validation->set_rules('total_item', 'Total item', 'required|xss_clean');
        $this->form_validation->set_rules('discount', 'Discount', 'xss_clean|required');
		
        $this->load->model("discount_number_item_model");

       	if($this->input->post("submit_form") && $this->form_validation->run()){
			$this->discount_number_item_model->update($id);
			$this->session->set_flashdata('success_notification', 'You have successfully updated data.');
			redirect($this->router->fetch_class());
		}
		
		$this->getForm($id);
    }

    function add(){
		
        $this->form_validation->set_rules('start_date', 'Start date', 'xss_clean|required');
        $this->form_validation->set_rules('end_date', 'End date', 'xss_clean|required');
		$this->form_validation->set_rules('total_item', 'Total item', 'required|xss_clean');
        $this->form_validation->set_rules('discount', 'Discount', 'xss_clean|required');
		
		$this->load->model("discount_number_item_model");
		
		if($this->input->post("submit_form") && $this->form_validation->run()){
			$this->discount_number_item_model->insert();
			$this->session->set_flashdata('success_notification', 'You have successfully added a new discount.');
			redirect($this->router->fetch_class());
		}
        
		$this->getForm();
	}

    function delete($id){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->load->model('discount_number_item_model');
            $verified = $this->discount_number_item_model->get($id);
            if($verified){
                $this->load->model('common');
                $this->common->delete('discount_number_item', array('discount_id'=>$id));
				$this->session->set_flashdata('success_notification', 'Discount deleted.');
                redirect($this->router->fetch_class());
            }
        }
    }

    private function getForm($id=""){
		
		$this->load->model("mc_products_model");
		
		$data['products'] = $this->mc_products_model->get_all_products();
        $data['view_page'] = "discount_number_item/discount_number_item_form";
        $data['page_title'] = $this->current_app_id->app_name;
		
		$discount_info = array();
		$discount_products=array();
		if($id){
			$discount_info = $this->discount_number_item_model->get($id);
		}
		
		$value = array();
		
		if(isset($_POST['start_date'])){
			$value['start_date'] = $this->input->post("start_date");
		}elseif($discount_info){
			$value['start_date'] = date("d F Y",strtotime($discount_info->start_date));
		}else{
			$value['start_date'] = "";
		}
		
		if(isset($_POST['end_date'])){
			$value['end_date'] = $this->input->post("end_date");
		}elseif($discount_info){
			$value['end_date'] = date("d F Y",strtotime($discount_info->end_date));
		}else{
			$value['end_date'] = "";
		}
		
		if(isset($_POST['total_item'])){
			$value['total_item'] = $this->input->post("total_item");
		}elseif($discount_info){
			$value['total_item'] = $discount_info->total_item;
		}else{
			$value['total_item'] = "";
		}
		
		if(isset($_POST['discount'])){
			$value['discount'] = $this->input->post("discount");
		}elseif($discount_info){
			$value['discount'] = $discount_info->discount;
		}else{
			$value['discount'] = "";
		}
		
		$data['value'] = $value;
		
        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
}