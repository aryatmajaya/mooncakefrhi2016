<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discount_number_item_products extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
    }

    public function index()
    {
		$this->load->model("mc_products_model");
		$this->load->model("discount_number_item_products_model");
		
		if($this->input->post("submit_form")){
			$this->discount_number_item_products_model->update();
			$this->session->set_flashdata("message_success","Update Quantity Discount Products success");
			redirect("discount_number_item_products");
		}
		
		$data['products'] = $this->mc_products_model->get_all_products_active();
        $data['view_page'] = "discount_number_item_products/discount_number_item_products_form";
        $data['page_title'] = $this->current_app_id->app_name;
		
		$data['discount_products'] = $this->discount_number_item_products_model->getProductsChecked();
		
        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
}