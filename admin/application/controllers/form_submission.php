<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_submission extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		$this->load->model('audits_model');
		$data['entries'] = $this->audits_model->Get_All_Templates_for_Auditors();

		$data['view_page'] = "form-creation/form_submission";
		$data['page_title'] = $this->current_app_id->app_name;


		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function form_tp()
	{
		$template_id = $this->uri->segment(3);

		$this->load->model('audits_model');

		$data['template_details'] = $this->audits_model->get_template_by_id($template_id);
		$data['touchpoints'] = $this->audits_model->Get_All_Touchpoints_of_a_Template($template_id);

		$data['bread_crumbs'] = array(
									$this->current_app_id->app_name.'|'.base_url().$this->router->fetch_class(),
									$data['template_details']->template_name.'|'
									);

		$data['view_page'] = "form-creation/form_tp";
		$data['page_title'] = $this->current_app_id->app_name." - ".$data['template_details']->template_name;
		$data['action'] = "add";

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');	
	}

	//FOR AUDITOR'S AUDIT FORM
	public function form_tp_form()
	{
		$touchpoint_id = $this->uri->segment(3);
		$this->load->model('audits_model');

		$data['tp_details'] = $this->audits_model->Get_Touchpoint_of_a_Template_by_id($touchpoint_id);

		$data['elements'] = $this->audits_model->Get_All_Elements_from_a_Touchpoint($data['tp_details']->template_id, $touchpoint_id);

		$data['bread_crumbs'] = array(
									$this->current_app_id->app_name.'|'.base_url().$this->router->fetch_class(),
									$data['tp_details']->template_name.'|'.base_url().$this->router->fetch_class().'/form_tp/'.$data['tp_details']->template_id,
									$data['tp_details']->touch_point_name.'|'
									);

		$data['view_page'] = "form-creation/form_tp_form";
		$data['page_title'] = $this->current_app_id->app_name." - ".$data['tp_details']->touch_point_name;
		$data['action'] = "add";

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');	
	}

	

	/* FOR AUDITORS */
	function save_form_answers(){
		extract($_POST);
		//var_dump($_POST);
		foreach ($_POST as $key => $value){
			if ($key != "element_id" && $key != "template_id" && $key != "touchpoint_id"){
				//echo $key.':'.$value.'</br>';
				$the_params = explode("-",$key); //1-entry_id; 2-answer field type
				//var_dump($the_params); die();
				$the_entry_id = $the_params[1];
				$the_field = $the_params[2];
				//echo $key.'<br>';
				
				if ($the_params[2] == 'answer_date'){
					$update_data = array(
						$the_field => ($value)?$value:'0000-00-00'
					);
				}
				else{
					$update_data = array(
						$the_field => ($value)?$value:''
					);							
				}
					

				$this->common->update('audit_touchpoint_entries', 'id', $the_entry_id, $update_data);
			}
			
		}

		//PROPERTY PHOTOS UPLOADING
		//$path = "../uploads/audits/3/";
		
		// var_dump($_FILES);
		$the_files = $_FILES;
		$path = "uploads/forms/$template_id/";
		if(!file_exists($path))
		{
		   mkdir($path);
		}

		foreach ($the_files as $key => $value){
			$the_params = explode("-",$key); //1-entry_id;
			$the_entry_id = $the_params[1];

			//echo $key.'-'.$value['name'].'-'.$value['tmp_name'].'<br>';
			if ($value['name'] != ""){
				$new_filename = mktime().'_'.$the_entry_id.'_'.$value['name'];						
							
				$config['upload_path'] = $path;
				$config['file_name'] = $new_filename;
				$config['remove_spaces'] = FALSE;
				$config['allowed_types'] = 'jpg|gif|png';
				$config['overwrite'] = TRUE;								
				$this->load->library('upload', $config);

				$this->upload->initialize($config);											
				$this->upload->do_upload($key);

				$update_data = array(
						'answer_text' => $new_filename
					);								

				$this->common->update('audit_touchpoint_entries', 'id', $the_entry_id, $update_data);					
			}
		}
		
		$this->session->set_flashdata('success_notification', 'Audit Saved Successfully');	
		
		redirect($this->router->fetch_class().'/form_tp_form/'.$touchpoint_id);
	}

	/* AJAX Functions */
	

}

/* End of file form_submission.php */
/* Location: ./application/controllers/form_submission.php */