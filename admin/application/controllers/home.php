<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	
	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('home/home');
		}
		else
		{
			$this->load->model('users_model');
			extract($_POST); 
			$user_id = $this->users_model->check_login($username, $password);
			
			if ($user_id == 'inactive'){
				//login failed
				$this->session->set_flashdata('inactive', TRUE);
				redirect('/home/');
			}
			elseif (!$user_id){
				//login failed
				$this->session->set_flashdata('login_error', TRUE);
				redirect('/home/');
			}
			else{
				//login successful
				//$this->load->model('common');
				//$this->session->set_userdata('lra_menu_configurations',$this->common->Allowed_Apps_Query($this->session->userdata('sess_user_account_type')));

				redirect('welcome');
			}
		}
	}
	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */