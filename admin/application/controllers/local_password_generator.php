<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Local_password_generator extends CI_Controller {
	
	public function index()
	{
            extract($_POST);
            $data = null;
            
            $this->load->library('encrypt');
            
            if (isset($password))
                $data['encrypted_password'] = $this->encrypt->encode($password);
            
            $this->load->view('local_password_generator/local_password_generator', $data);
	}
        
        public function decode() {
            extract($_POST);
            $data = null;
            
            $this->load->library('encrypt');
            
            if (isset($password))
                $data['decoded_password'] = $this->encrypt->decode($password);
            
            $this->load->view('local_password_generator/local_password_decoder', $data);
        }
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */