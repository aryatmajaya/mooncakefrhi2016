<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_card_discount extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        $this->load->model('mc_ccard_promo_model');
    }

    public function index()
    {
        $data['entries'] = $this->mc_ccard_promo_model->get_all();
        
        $data['view_page'] = "mc_ccard_promo/mc_ccard_promo";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->render($data);
    }

    function edit($id){
        $this->load->model('mc_creditcards_model');

        $data['card_discount'] = $this->mc_ccard_promo_model->get($id);
        $data['view_page'] = "mc_ccard_promo/mc_ccard_promo_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['creditcards'] = $this->mc_creditcards_model->get_all();

        $data['action'] = 'edit';
        $this->render($data);
    }

    function add(){
        $this->set_validations();
        $id = $this->input->post('card_discount_id');
        $data = $this->input->post('form');
        if ($this->form_validation->run())
        {
            $this->load->model('common');
            
            $data['start_date'] = date('Y-m-d H:i:s', strtotime($data['start_date']));
            $data['end_date'] = date('Y-m-d H:i:s', strtotime($data['end_date']));
            $data['created_by'] = $this->session->userdata('user_id');
            $this->common->insert("mc_ccard_promo", $data);
            
            $this->session->set_flashdata('success_notification', 'You have successfully added a new caredit card.');
            redirect($this->router->fetch_class());
        }
        if($data){
            $data['id'] = $id;
            $data['card_discount'] = (object) $data;    
        }
        
       
        $this->load->model('mc_creditcards_model');
        $data['creditcards'] = $this->mc_creditcards_model->get_all();

        $data['view_page'] = "mc_ccard_promo/mc_ccard_promo_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['action'] = "add";

        $this->render($data);
    }

    function update(){
        $this->set_validations();
        
        $id = $this->input->post('card_discount_id');
        $data = $this->input->post('form');

        if ($this->form_validation->run())
        {
            $this->load->model('common');
            $data['start_date'] = date('Y-m-d H:i:s', strtotime($data['start_date']));
            $data['end_date'] = date('Y-m-d H:i:s', strtotime($data['end_date']));
            $data['modified_by'] = $this->session->userdata('user_id');
            
            $this->common->update("mc_ccard_promo", 'id', $id, $data);
            
            $this->session->set_flashdata('success_notification', 'You have successfully update a card discount.');
            redirect($this->router->fetch_class());
        }
       
        $this->load->model('mc_creditcards_model');
        $data['creditcards'] = $this->mc_creditcards_model->get_all();

        $data['id'] = $id;
        $data['card_discount'] = (object) $data;
        
        $data['view_page'] = "mc_ccard_promo/mc_ccard_promo_form";
        $data['page_title'] = $this->current_app_id->app_name;
        
        $data['action'] = 'edit';
        $this->render($data);

    }

    function delete($id){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->load->model('mc_ccard_promo_model');
            $verified = $this->mc_ccard_promo_model->get($id);

            if($verified){
                $this->load->model('common');
                if($this->common->delete('mc_ccard_promo', array('id'=>$id))){
                    $this->session->set_flashdata('success_notification', 'Card discount deleted.');
                }

                redirect($this->router->fetch_class());

            }
        }
    }

    private function set_validations(){
        $this->form_validation->set_rules('form[promo_name]', 'Promo name', 'required|xss_clean');
        //$this->form_validation->set_rules('form[credit_card_id]', 'Credit card', 'xss_clean|required');
		$this->form_validation->set_rules('form[discount_code]', 'Discount Code', 'xss_clean|required');
        $this->form_validation->set_rules('form[start_date]', 'Credit card', 'xss_clean|required');
        $this->form_validation->set_rules('form[end_date]', 'Status', 'xss_clean|required');
        $this->form_validation->set_rules('form[status]', 'Status', 'xss_clean');
    }

    private function render($data){
        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
}