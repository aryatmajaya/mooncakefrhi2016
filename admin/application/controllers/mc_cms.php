<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_cms extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        $this->load->model('mc_cms_model');

    }

    public function index()
    {
        $data['entries'] = $this->mc_cms_model->get_all();
        
        $data['view_page'] = "mc_cms/mc_cms";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->render($data);
    }

     public function edit($id)
    {

        $data['cms'] = $this->mc_cms_model->get($id);
        
        $data['view_page'] = "mc_cms/mc_cms_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['action'] = 'edit';
        $data['categories'] = $this->mc_cms_model->get_categories();
        $this->render($data);
    }

    public function add(){
        $this->set_validations();
        $data = $this->input->post('cms');
        if(isset($data['id'])) unset($data['id']); 
        if ($this->form_validation->run())
        {
            $this->load->model('common');
            
            $data['created_by'] = $this->session->userdata('user_id');
            $this->common->insert("mc_cms", $data);
            
            $this->session->set_flashdata('success_notification', 'You have successfully added a new content.');
            redirect($this->router->fetch_class());
        }

        if($data){
            $data['cms'] = (object) $data;    
        }

        $data['view_page'] = "mc_cms/mc_cms_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['action'] = "add";
        $data['categories'] = $this->mc_cms_model->get_categories();

        $this->render($data);
    }

    public function update(){
    	$this->set_validations('edit');
    	$data = $this->input->post('cms');
    	//die(print_r($data));
    	if($this->form_validation->run()){
    		$this->load->model('common');

            $data['modified_by'] = $this->session->userdata('user_id');
            //unset id to prevent issue
            $cms_id = $data['id'];
            unset($data['id']);

            $this->common->update('mc_cms', 'id', $cms_id, $data);
            $this->session->set_flashdata('success_notification', 'Content updated');
            redirect($this->router->fetch_class());
    	}
    }

    public function delete($id){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->load->model('mc_cms_model');
            $verified = $this->mc_cms_model->get($id);

            if($verified){
                $this->load->model('common');
                if($this->common->delete('mc_cms', array('id'=>$id))){
                    $this->session->set_flashdata('success_notification', 'Content deleted.');
                }

                redirect($this->router->fetch_class());

            }
        }
    }

    private function set_validations($state= 'add'){

        $this->form_validation->set_rules('cms[title]', 'Title', 'required|xss_clean' . (($state == 'add') ? 'is_unique[mc_cms.slug]' : ''))	;
        $this->form_validation->set_rules('cms[slug]', 'Slug Url', 'xss_clean|required');
        $this->form_validation->set_rules('cms[description]', 'Description', 'xss_clean');
        $this->form_validation->set_rules('cms[html_content]', 'HTML Content', 'xss_clean|required');
        $this->form_validation->set_rules('cms[published]', 'Published', 'xss_clean|required');
        $this->form_validation->set_rules('cms[published_date]', 'Date Published', 'xss_clean|required');


    }

    private function render($data){
        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
}