<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_collection_report extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        
        //$this->order_status = 9; //process
		$this->order_status = 99;

    }
	
    public function index() {
        $data['tool_type'] = $this->input->post('tool_type');
        
        $data['from_date'] = $from_date = $this->input->post('from_date');
        $data['to_date'] = $to_date = $this->input->post('to_date');
        
        
        
        /*if (!empty($data['from_date_ordered']))
            echo $param;
         * 
         */
        
        //echo $from_date.'-'.$to_date;

        $data['service_type'] = "self_collection";

        $this->load->model('Mc_delivery_report_model');
        $this->load->model('Mc_transactions_model');
              
        $data['transactions'] = Mc_delivery_report_model::get_all_deliveries2(
			$this->order_status,
			(isset($from_date))?$from_date:null,
			(isset($to_date))?$to_date:null,
			$data['service_type'],
			$data['tool_type'],
			$this->input->get("sort")?$this->input->get("sort"):"service_date",
			$this->input->get("order")?$this->input->get("order"):"desc"
		);
        //var_dump($data['transactions']); die();
        
        $data['view_page'] = "mc_delivery_report/mc_delivery_report";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
    
    
        
   
    
    /* AJAX Functions */
	
	
}

/* End of file mc_collection_report.php */
/* Location: ./application/controllers/mc_collection_report.php */