<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_collectiondeliveryreports extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');
        $this->load->model('mooncakes_model');
        

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');

    }
	
    public function index($view_type = NULL, $page_number = NULL, $option1 = NULL) {
        /* Notes:
         * 
         * $this->uri->segment(1); - mc_transactions
         * $option1 = $value1|$value2|$value3
         * 
         */
        
        if (!empty($option1)) {
            $the_option = explode('_', $option1);
            
            if ($the_option[0] == "totalsales") {
                $data['total_sales'] = 1;
                $from_date = $the_option[1];
                $to_date = $the_option[2];
            }
        }
        
        $this->form_validation->set_rules('search', 'Search Field is required.', 'xss_clean');
        //Search
        $search_field = $this->input->post('search_field');
        $search = $this->input->post('search');

        /*if (isset($view_type) && $view_type != "all"){             
            $get_code = Mc_utilities::order_status_id($view_type);
            $status = $get_code[0];                
        }
        else {
            $view_type = "all";
            $status = 99;
        }
         * 
         */

        //$this->load->model('mc_transactions_model');
        $data['view_type'] = $view_type;
        
        
        if (empty($option1[0])) {
            /* Pagination */
            $this->load->library('pagination');
            $this->config->load('raffles_mooncakes_config');

            $config["base_url"] = BASE_URL.$this->router->fetch_class()."/index/".$view_type;
            $config["total_rows"] = 100;
            $config["per_page"] = 100;
            $config["uri_segment"] = 4;
            $config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
            $config['full_tag_close'] = '</ul></div><!--pagination-->';

            $config['first_link'] = '&laquo; First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last &raquo;';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';

            $config['next_link'] = 'Next &rarr;';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = '&larr; Previous';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';


            $this->pagination->initialize($config);
            $data["pagination_links"] = $this->pagination->create_links();
            /* End Pagination */
        }
        
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        //$data['transactions'] = mc_transactions_model::get_all_transactions($status, $search_field, $search, (isset($config["per_page"]))?$config["per_page"]:null, $page, (isset($from_date))?$from_date:null, (isset($to_date))?$to_date:null);
        $data['entries'] = mooncakes_model::get_all_collectiondelivery();
        

        $data['view_page'] = "mc_collectiondeliveryreports/mc_collectiondeliveryreports";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
    
    function total_sales() {
        extract($_POST);
        redirect(BASE_URL.$this->router->fetch_class().'/index/'.$view_type.'/0/totalsales_'.$from_date.'_'.$to_date);
    }

    public function view($delivery_id){
        $this->load->model('mc_transactions_model');
        
        //Get the delivery details
        $data['delivery_details'] = mooncakes_model::DeliveryCollectionDetails($delivery_id);

        if ($data['delivery_details']){
            
            //Get Order Details based on delivery's order reference
            $data['entry'] = mooncakes_model::Get_Order_Details_by_RefID($data['delivery_details']->order_ref);
            
            //Get the delivery items
            $data['delivery_items'] = mooncakes_model::DeliveryCollectionItems($delivery_id);
            var_dump($data['delivery_items']);

            $data['view_page'] = "mc_collectiondeliveryreports/mc_collectiondeliveryreports_view";
            $data['page_title'] = $this->current_app_id->app_name;
            $data['action'] = "edit";

            $this->load->view('elements/header', $this->header_data);
            $this->load->view('elements/template1', $data);
            $this->load->view('elements/footer');
        }
        else
            redirect($this->router->fetch_class());
    }

    

	/* AJAX Functions */
	
	
	
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */