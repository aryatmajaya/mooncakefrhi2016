<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_creditcards extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        $this->load->model('mc_creditcards_model');
    }

    public function index()
    {
        $data['entries'] = $this->mc_creditcards_model->get_all();
        
        $data['view_page'] = "mc_creditcards/mc_creditcards";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->render($data);
    }

    function edit($id){
        $this->load->model('mc_paymentgateways_model');

        $data['creditcard'] = $this->mc_creditcards_model->get($id);
        $data['view_page'] = "mc_creditcards/mc_creditcard_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['payment_gateways'] = $this->mc_paymentgateways_model->get_all();

        $data['action'] = 'edit';
        $this->render($data);
    }

    function add(){
        $this->set_validation();
        
        if ($this->form_validation->run())
        {
            $this->load->model('common');
            $data = $this->input->post('form');
            $data['created_by'] = $this->session->userdata('user_id');
            $this->common->insert("mc_credit_cards", $data);
            
            $this->session->set_flashdata('success_notification', 'You have successfully added a new caredit card.');
            redirect($this->router->fetch_class());
        }

        $this->load->model('mc_paymentgateways_model');
        $data['payment_gateways'] = $this->mc_paymentgateways_model->get_all();
        $data['view_page'] = "mc_creditcards/mc_creditcard_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['action'] = "add";

        $this->render($data);
    }

    function update(){
        $this->form_validation->set_rules('form[name]', 'Name', 'required|xss_clean');
        $this->form_validation->set_rules('form[label]', 'Label', 'required|xss_clean');
        $this->form_validation->set_rules('form[payment_gateway_id]', 'Payment gateway', 'xss_clean|required');
        $this->form_validation->set_rules('form[mid]', 'Merchant ID', 'required|xss_clean[mc_credit_cards.mid]|min_length[11]|numeric');
        $this->form_validation->set_rules('form[issued_by]', 'Issued by', 'xss_clean|alpha_dash');
        $this->form_validation->set_rules('form[status]', 'Status', 'xss_clean');
        
        $id = $this->input->post('creditcard_id');
        $data = $this->input->post('form');

        if ($this->form_validation->run())
        {
            $this->load->model('common');
            
            $data['modified_by'] = $this->session->userdata('user_id');
            $this->common->update("mc_credit_cards", 'id', $id, $data);
            
            $this->session->set_flashdata('success_notification', 'You have successfully update a credit card.');
            redirect($this->router->fetch_class());
        }
       
        $this->load->model('mc_paymentgateways_model');
        $data['id'] = $id;
        $data['creditcard'] = (object) $data;
        
        $data['view_page'] = "mc_creditcards/mc_creditcard_form";
        $data['page_title'] = $this->current_app_id->app_name;
        $data['payment_gateways'] = $this->mc_paymentgateways_model->get_all();
        
        $data['action'] = 'edit';
        $this->render($data);

        
    }

    function delete($id){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->load->model('mc_creditcards_model');
            $verified = $this->mc_creditcards_model->get($id);

            if($verified){
                $this->load->model('common');
                if($this->common->delete('mc_credit_cards', array('id'=>$id))){
                    $this->session->set_flashdata('success_notification', 'Credit Card deleted.');
                }

                redirect($this->router->fetch_class());

            }
        }
    }


    private function set_validation(){
        $this->form_validation->set_rules('form[name]', 'Name', 'required|xss_clean|is_unique[mc_credit_cards.name]');
        $this->form_validation->set_rules('form[label]', 'Label', 'required|xss_clean|is_unique[mc_credit_cards.label]');
        $this->form_validation->set_rules('form[payment_gateway_id]', 'Payment gateway', 'xss_clean|required');
        $this->form_validation->set_rules('form[mid]', 'Merchant ID', 'required|xss_clean|is_unique[mc_credit_cards.mid]|min_length[11]|numeric');
        $this->form_validation->set_rules('form[issued_by]', 'Issued by', 'xss_clean|alpha_dash');
        $this->form_validation->set_rules('form[status]', 'Status', 'xss_clean');
    }

    private function render($data){
        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
}