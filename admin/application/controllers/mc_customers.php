<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_customers extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');

    }
	
    public function index()
    {
        $this->form_validation->set_rules('search', 'Search Field is required.', 'xss_clean');
        //Search
        $search_field = $this->input->post('search_field');
        $search = $this->input->post('search');
  

        $this->load->model('mc_customers_model');        

        $data['entries'] = mc_customers_model::Get_All_Customers($search_field, $search);

        $data['view_page'] = "mc_customers/mc_customers";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }

	/*public function add()
	{
		
			

		
	}*/

	

    public function edit($the_id) {
        $this->load->library('encrypt');
        $this->load->model('mc_customers_model');
        
        $data['entry'] = mc_customers_model::Get_Customer_Details($the_id);

        if ($data['entry']){
            $data['view_page'] = "mc_customers/mc_customers_crud";
            $data['page_title'] = $this->current_app_id->app_name;
            $data['action'] = "edit";
            
            //getting the customer's order history
            $status = 99;
            $this->load->model('mc_transactions_model');
            //$data['transactions'] = mc_transactions_model::get_all_transactions($status, 'customer_id', $the_id);
            $data['transactions'] = mc_transactions_model::get_order_history($status, 'customer_id', $the_id);
            
            //$data['transactions'] = NULL;

            $this->load->view('elements/header', $this->header_data);
            $this->load->view('elements/template1', $data);
            $this->load->view('elements/footer');
        }
        else
            redirect($this->router->fetch_class());
    }

    public function update(){
        
        $this->load->model('mc_customers_model');
        extract($_POST);

        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|xss_clean');
        $this->form_validation->set_rules('alt_contact', 'Alt Contact No.', 'xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'xss_clean|matches[password2]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('required_error', validation_errors());
            redirect($this->router->fetch_class().'/edit/'.$id);
        }
        else {
            $this->load->library('encrypt');
            //updating order details
            if ($password == ""){
                $update_data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'mobile' => $mobile,
                    'alt_contact' => $alt_contact,
                    'email' => $email               
                );
            } else {
                $update_data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'mobile' => $mobile,
                    'alt_contact' => $alt_contact,
                    'email' => $email,
                    'password' => $this->encrypt->encode($password)
                );
            }
            
            mc_customers_model::Update_Customer($id, $update_data);
            $this->session->set_flashdata('notification_status', 'Updated Successfully');
            redirect($this->router->fetch_class());
        }
    }

	/* AJAX Functions */
	
    function ajax_delete_customer(){
        extract($_POST);
        if ($this->current_app_info->delete_role && isset($the_id)){
            $this->load->model('common');
            $update_data = array(
                   'deleted' => 1		            
                );
            $this->common->update('mc_customers', 'id', $the_id, $update_data);
        }
        else
            redirect($this->router->fetch_class());			
    }
	
}

/* End of file mc_customers.php */
/* Location: ./application/controllers/mc_customers.php */