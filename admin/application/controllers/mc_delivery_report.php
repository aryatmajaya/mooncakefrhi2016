<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_delivery_report extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');
        
        //$this->order_status = 9; //process
		$this->order_status = 99;

    }
	
    public function index() {
        $data['tool_type'] = $this->input->post('tool_type');
        
        $data['from_date'] = $from_date = $this->input->post('from_date');
        $data['to_date'] = $to_date = $this->input->post('to_date');
        
        //echo $from_date.'-'.$to_date;

        $data['service_type'] = 'delivery';

        $this->load->model('Mc_delivery_report_model');
		$this->load->model('Mc_transactions_model');
              
        //$data['transactions'] = Mc_delivery_report_model::get_all_deliveries($this->order_status, (isset($from_date))?$from_date:null, (isset($to_date))?$to_date:null, $data['service_type'], $data['tool_type']);
		
		$data['transactions'] = Mc_delivery_report_model::get_all_deliveries2(
			$this->order_status,
			(isset($from_date))?$from_date:null,
			(isset($to_date))?$to_date:null,
			$data['service_type'],
			$data['tool_type'],
			$this->input->get("sort")?$this->input->get("sort"):"service_date",
			$this->input->get("order")?$this->input->get("order"):"desc"
		);
		
		
        
        $data['view_page'] = "mc_delivery_report/mc_delivery_report";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
    
    function print_report_PDF($service_type, $from_date = NULL, $to_date = NULL, $param = NULL) {
        $this->load->model('Mc_delivery_report_model');
        $this->load->model('Mc_transactions_model');
        
        $data['service_type'] = $service_type;
        
        $data['transactions'] = Mc_delivery_report_model::get_all_deliveries2($this->order_status, (isset($from_date))?$from_date:null, (isset($to_date))?$to_date:null, $data['service_type'], $param);
        //var_dump($data['transactions']);
        $data['print_mode'] = 1;
        
        //$the_html = $this->load->view('mc_delivery_report/pdf_report', $data, (!isset($debug))?true:false);
        $the_html = $this->load->view('mc_delivery_report/pdf_report', $data, true);
            
        // Export to PDF using mPDF
        $this->load->library('mpdf56');
        $pdf = $this->mpdf56->load();

        ini_set("memory_limit","2064M");

        //CONFIGURATIONS
        //$pdf->showStats = true;
        $pdf->showImageErrors = false;
        //$pdf->shrink_tables_to_fit=1;

         //APPLYING CSS
        $cssFilePath = FCPATH."/assets/css/";
        $stylesheet = file_get_contents($cssFilePath."report_print.css");
        $pdf->WriteHTML($stylesheet,1);
            
        //SETTING THE FOOTER
        //$the_footer = $this->load->view('audit_system/reports/footer', $data, (!isset($mode))?true:false);
        //$pdf->SetHTMLFooter($the_footer);
        
        //$pdf->autoPageBreak = true;
        $pdf->use_kwt = true;
            
        //if (!isset($debug)){
            $pdf->WriteHTML($the_html,2);
            $pdf->Output('report.pdf', 'I');
        //}
    }
    
    function print_deliverynotes_PDF($service_type, $delivery_ids,$debug = NULL) {
		
		$this->load->library("html2pdf/pdf");
		$this->pdf->setDefaultFont('kozgopromedium');
		
		//$this->pdf->setDefaultFont('javiergb');
		
		//$this->pdf = new FPDF();
//		$this->pdf->AddPage();
//		$this->pdf->SetFont('Arial','B',16);
		//$this->pdf->Cell(40,10,'Hello World!');
		
		
		//$this->pdf->MultiCell(200, 10, "testingsdf dsfds",0, "L", true);
		
		//$this->pdf->Output();
		
		
		
       	$this->load->model('mc_delivery_report_model');
        $this->load->model('mc_transactions_model');
        
		
        $data['print'] = 1;
        $data['service_type'] = $service_type;
        $data['debug'] = $debug;
        
        $the_ids = explode('-', $delivery_ids);
        $the_notes = "";
        
        //$data['products'] = common::get_all_records3('mc_products', 'receipt_sort_order', 'ASC');
            
        // Export to PDF using mPDF
       // $this->load->library('mpdf56');
        //$pdf = $this->mpdf56->load();
		
        //ini_set("memory_limit","2064M");
		
        //CONFIGURATIONS
        //$pdf->showStats = true;
       // $pdf->showImageErrors = false;
        //$pdf->shrink_tables_to_fit=1;
		
        //APPLYING CSS
        //$cssFilePath = FCPATH."/assets/css/";
        //$stylesheet = file_get_contents($cssFilePath."report_print.css");
		
		//echo "testing here";
		
        //$pdf->WriteHTML($stylesheet,1);
		$the_notes = "";
		
		
		
        foreach ($the_ids as $ids) {
			$content = "";
			
			$data['delivery_id'] = $ids;
            $data['order'] = mc_delivery_report_model::Get_OrderDetails_by_deliveryID($ids);
            //$data['items'] = mc_delivery_report_model::Get_OrderItems_by_deliveryID($ids);
			$data['items'] = $this->mc_delivery_report_model->getProductsByDeliveryId($ids);
			
			$content .= "<page backtop='120px' backbottom='100px'>";
			$content .= "<page_header>";
			$content .= $this->load->view('mc_delivery_report/delivery_notes_header', $data, true);
			$content .= "</page_header>";
           	$content .= $this->load->view('mc_delivery_report/delivery_notes_table', $data, true);
			$content .= '<page_footer>';
        	$content .= $this->load->view('mc_delivery_report/delivery_notes_footer', $data, true);
       	 	$content .= '</page_footer>';
			
			$content .= "</page>";
			
			//echo $content;

			$this->pdf->WriteHTML($content);
        }
		
		
		$this->pdf->Output($service_type.'-notes.pdf');
    }
        
    public function view_delivery_note($delivery_id){
        $this->load->model('mc_delivery_report_model');
        $this->load->model('mc_transactions_model');
        
        $data['delivery_id'] = $delivery_id;
        $data['order'] = mc_delivery_report_model::Get_OrderDetails_by_deliveryID($delivery_id);
        $data['items'] = mc_delivery_report_model::Get_OrderItems_by_deliveryID($delivery_id);
        
        $data['print'] = NULL;
            
        $data['view_page'] = "mc_delivery_report/view_delivery_note";
        $data['page_title'] = $this->current_app_id->app_name.' - Delivery Note';
        $data['action'] = "edit";

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
    
    /* AJAX Functions */
	
	
}

/* End of file mc_delivery_report.php */
/* Location: ./application/controllers/mc_delivery_report.php */