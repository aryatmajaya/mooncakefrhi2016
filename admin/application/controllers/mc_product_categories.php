<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* updates
02-04-2013 - added logo image upload
*/

class Mc_product_categories extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('Check_permission');

		$this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');

		$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));

		//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
		if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
			redirect($this->router->fetch_class());
		} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
			redirect($this->router->fetch_class());
		} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
			redirect($this->router->fetch_class());
			
		} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
			redirect($this->router->fetch_class());
		}

	}

	public function index(){
		$data['view_page'] = "mc_categories/mc_categories";
		$data['page_title'] = $this->current_app_id->app_name;

		$this->load->model('mc_product_categories_model');

		$data['entries'] = Mc_product_categories_model::get_all();

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function add()
	{

		$this->form_validation->set_rules('form[name]', 'Name', 'required|xss_clean|is_unique[mc_product_categories.name]');
		$this->form_validation->set_rules('form[china_name]', 'China Name', 'required|xss_clean');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['view_page'] = "mc_categories/categories-form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "add";

			$this->load->model('mc_product_categories_model');
			$data['parent_categories'] = Mc_product_categories_model::get_parent_categories();

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');

			extract($_POST);
			
			$form['created_by'] = $this->session->userdata('user_id');
			
			$this->common->insert("mc_product_categories", $form);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new brand.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$category_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('mc_product_categories','id',$category_id);

		if ($data['entries']){
			$this->load->model('mc_product_categories_model');
			$data['parent_categories'] = Mc_product_categories_model::get_parent_categories();

			$data['view_page'] = "mc_categories/categories-form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";			

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else{
			redirect($this->router->fetch_class());
		}
	}

	function update(){
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->form_validation->set_rules('form[name]', 'Name', 'required|xss_clean');

			if ($this->form_validation->run()){
				$this->load->model('common');

				extract($_POST);				
				$form['modified_by'] = $this->session->userdata('user_id');

				$this->common->update("mc_product_categories", 'id', $category_id, $form);
				$this->session->set_flashdata('success_notification', 'You have successfully added a new brand.');
			}else{
				$this->session->set_flashdata('required_error', 'Invalid inputs. Please remove invalid characters from the inputs.');
			}
			
		}else{
			$this->session->set_flashdata('required_error', 'Invalid request method.');
		}

		redirect($this->router->fetch_class());
	}

	function ajax_delete_category(){

		extract($_POST);
		if ($this->current_app_info->delete_role && isset($category_id)){
			$this->load->model('common');
			
			$category = $this->common->get_record_by_id('mc_product_categories','id',$category_id);
			if($category){
				$update_data = array(
		               'deleted' => 1        
		            );
				$update_data['modified_by'] = $this->session->userdata('user_id');
					
				$this->common->update('mc_product_categories', 'id', $category_id, $update_data);	
				$this->session->set_flashdata('success_notification', 
											'You have successfully deleted the category named.' . $category->name);
			}
		}
		else
			redirect($this->router->fetch_class());
			
	}

	function ajax_close_category(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
                    $this->load->model('common');
			
                    $update_data = array(
                        'status' => 0
                    );
				
                    $this->common->update('mc_product_categories', 'id', $the_id, $update_data);
		}
		else
                    redirect($this->router->fetch_class());			
	}
        
	function ajax_open_category(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
                    $this->load->model('common');
			
                    $update_data = array(
                        'status' => 1
                    );
				
                    $this->common->update('mc_product_categories', 'id', $the_id, $update_data);
		}
		else
                    redirect($this->router->fetch_class());			
	}



}