<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* updates
02-04-2013 - added logo image upload
*/

class Mc_products extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());	
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
    }
    
	public function index()
	{
		$this->load->model('mc_products_model');
		$data['entries'] = Mc_products_model::get_all_products();

		$data['view_page'] = "mc_products/mc_products";
		$data['page_title'] = $this->current_app_id->app_name;

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	/* Product image dimension 380x228px */
	public function add()
	{
		$this->form_validation->set_rules('product[product_name]', 'Product Name', 'required|xss_clean');
		$this->form_validation->set_rules('product[description]', 'Description', 'xss_clean');
		$this->form_validation->set_rules('product[short_description]', 'Short Description', 'xss_clean');
		$this->form_validation->set_rules('product[category_id]', 'Category', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('required_error', validation_errors());
			$data['view_page'] = "mc_products/mc_products_form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "add";
			$data['product'] = extract($_POST);

			$this->load->model('common');
			$data['categories'] = $this->common->get_table_list('mc_product_categories','name','ASC');

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
	            'category_id'=>$product['category_id'],
               	'product_name' => $product['product_name'],
				'product_name_china' => $product['product_name_china'],
               	'description' => (isset($product['description']) ? $product['description'] : ''),
               	'short_description' => (isset($product['short_description']) ? $product['short_description'] : ''),
               	'status' => $product['status'],
				'show_in_front' => $product['show_in_front'],
               	'price' => $product['price'],
               	'pc_per_box' => $product['pc_per_box'],
               	'is_new' => $product['is_new'],
				'created_by' => $this->session->userdata('user_id')
	            );

			if(!empty($product['product_images'])){
				$array_of_images = array();
				foreach ($product['product_images'] as $key => $image) {
					$array_of_images[] = $image;
				}

				$insert_data['images'] = json_encode($array_of_images);
			}
			
			$this->common->insert("mc_products", $insert_data);
			$latest_id = $this->db->insert_id();
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new brand.');
			redirect($this->router->fetch_class().'/edit/'.$latest_id);
			
		}
	}

	public function edit()
	{
		$product_id = $this->uri->segment(3);
		$this->load->model('Mc_products_model');
		$data['product'] = $this->Mc_products_model->get_product($product_id);
		//echo '<pre>';
		//print_r($data['product']);
		if ($data['product']){
			
			$data['categories'] = $this->common->get_table_list('mc_product_categories','name','ASC');

			$data['view_page'] = "mc_products/mc_products_form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";			

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			$this->form_validation->set_rules('product[product_name]', 'Product Name', 'required|xss_clean');
			$this->form_validation->set_rules('product[description]', 'Description', 'xss_clean');
			$this->form_validation->set_rules('product[short_description]', 'Short Description', 'xss_clean');
			$this->form_validation->set_rules('product[category_id]', 'Category', 'required|xss_clean');
			if ($this->form_validation->run() == FALSE)
			{

				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {
				$update_data = array(
					'category_id'=>$product['category_id'],
	               	'product_name' => $product['product_name'],
					'product_name_china' => $product['product_name_china'],
	               	'description' => (isset($product['description']) ? $product['description'] : ''),
	               	'short_description' => (isset($product['short_description']) ? $product['short_description'] : ''),
	               	'status' => $product['status'],
	               	'price' => $product['price'],
					'show_in_front' => $product['show_in_front'],
	               	'pc_per_box' => $product['pc_per_box'],
	               	'is_new' => $product['is_new'],
	               	'modified_by' => $this->session->userdata('user_id'),
	               	'images' => (isset($product['images']) ? $product['images'] : '')
	            );
				
				if(!empty($product['product_images'])){
					$array_of_images = array();
					foreach ($product['product_images'] as $key => $image) {
						$array_of_images[] = $image;
					}

					$update_data['images'] = json_encode($array_of_images);
				}

				//die(print_r($update_data));

				$this->common->update('mc_products', 'id', $id, $update_data);

				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class().'/edit/'.$id);
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($brand_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('brands', 'brand_id', $brand_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}
        
	function ajax_close_product(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
                    $this->load->model('common');
			
                    $update_data = array(
                        'status' => 0
                    );
				
                    $this->common->update('mc_products', 'id', $the_id, $update_data);
		}
		else
                    redirect($this->router->fetch_class());			
	}
        
	function ajax_open_product(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
                    $this->load->model('common');
			
                    $update_data = array(
                        'status' => 1
                    );
				
                    $this->common->update('mc_products', 'id', $the_id, $update_data);
		}
		else
                    redirect($this->router->fetch_class());			
	}
	
	
	function ajax_hide_in_front(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');
			$update_data = array(
				'show_in_front' => 0
			);
            $this->common->update('mc_products', 'id', $the_id, $update_data);
		}
		else{
        	redirect($this->router->fetch_class());
		}
	}
        
	function ajax_show_in_front(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');		
			$update_data = array(
				'show_in_front' => 1
			);
			$this->common->update('mc_products', 'id', $the_id, $update_data);
		}
		else{
            redirect($this->router->fetch_class());
		}
	}
	
	

	function ajax_upload(){
		
		
		error_reporting(E_ALL | E_STRICT);
		$this->load->library('UploadHandler');
		
	}

	function update_app_sortorder(){
		$updateRecordsArray = $_POST['tr'];
		
		$listingCounter = 1;
		foreach ($updateRecordsArray as $recordIDValue) {			
			$query = "UPDATE mc_products SET sort_order = " . $listingCounter . " WHERE id = " . $recordIDValue;
			mysql_query($query) or die(mysql_error());
			
			$listingCounter = $listingCounter + 1;			
		}		
	}
}

/* End of file mc_products.php */
/* Location: ./application/controllers/mc_products.php */