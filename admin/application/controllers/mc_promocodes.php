<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_promocodes extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        
        $this->load->library('Mc_utilities');

    }

    private function set_validation($action = 'add'){
        $this->form_validation->set_rules('promocode[promocode]', 'Promo code', 'required|xss_clean'); 
        $this->form_validation->set_rules('promocode[credit_card_id]', 'Promo code', 'required|xss_clean'); 
        $this->form_validation->set_rules('promocode[discount]', 'Discount', 'required|decimal|xss_clean');
        $this->form_validation->set_rules('promocode[start_date]', 'Start Date', 'required|xss_clean');
        $this->form_validation->set_rules('promocode[end_date]', 'End Date', 'required|xss_clean');
    }

    private function render_view($data){
        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }

    public function index()
    {
        $this->load->model('mc_promocode_model');
        $this->load->model('mc_paymentgateways_model');
        
        $data['promocodes'] = $this->mc_promocode_model->get_all();
        $data['view_page'] = "mc_promotions/mc_promotions";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->render_view($data);
    }

    public function add(){
       
        $this->set_validation();
        
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            if ($this->form_validation->run()) {
                $this->load->model('common');
                $promocode = $this->input->post('promocode');
                $promocode['created_by'] = $this->session->userdata('user_id');
                $promocode['start_date'] = date('Y-m-d H:i:s', strtotime($promocode['start_date']));
                $promocode['end_date'] = date('Y-m-d H:i:s', strtotime($promocode['end_date']));
                $promocode['status'] = 1;
                $this->common->insert('mc_far_promocodes', $promocode);
                $this->session->set_flashdata('success_notification', 'Promo code Added');
                redirect($this->router->fetch_class());
            }
        }
        if(isset($promocode)){
            $data['promocode'] = (object) $promocode;
        }
        $this->load->model('mc_creditcards_model');
        $data['creditcards'] = $this->mc_creditcards_model->get_all();

        $data['view_page'] = "mc_promotions/mc_promotions_form";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->render_view($data);
    }

    public function edit($id){
        $this->set_validation('edit');
        
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $promocode = $this->input->post('promocode');
            if ($this->form_validation->run()) {
                $this->load->model('common');
                
                $promocode['date_modified'] = date('Y-m-d H:i:s', time());
                $promocode['modified_by'] = $this->session->userdata('user_id');
                $promocode['start_date'] = date('Y-m-d H:i:s', strtotime($promocode['start_date']));
                $promocode['end_date'] = date('Y-m-d H:i:s', strtotime($promocode['end_date']));
                
                //unset id to prevent issue
                $promo_id = $promocode['id'];
                unset($promocode['id']);

                $this->common->update('mc_far_promocodes', 'id', $promo_id, $promocode);
                $this->session->set_flashdata('success_notification', 'Promo code Updated');
                redirect($this->router->fetch_class());
            }
        }
        $this->load->model('mc_promocode_model');
        $this->load->model('mc_creditcards_model');

        if(isset($promocode)){
            $data['promocode'] = (object) $promocode;
        }else{
            $data['promocode'] = $this->mc_promocode_model->get($id);
        }
        
        $data['creditcards'] = $this->mc_creditcards_model->get_all();

        $data['view_page'] = "mc_promotions/mc_promotions_form";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->render_view($data);
    }

    function delete($id){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->load->model('mc_promocode_model');
            $verified = $this->mc_promocode_model->get($id);
            if($verified){
                $this->load->model('common');
                if($this->common->delete('mc_far_promocodes', array('id'=>$id))){
                    $this->session->set_flashdata('success_notification', 'Promo code deleted.');
                }

                redirect($this->router->fetch_class());

            }
        }
    }
}