<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_sales_report extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');
        //echo $this->router->fetch_class();
        $this->current_app_id = $this->Thechecker_model->Get_App_ID('mc_sales_report/product/'); //var_dump($this->current_app_id);die();
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');

    }
	
    public function index() {
        
    }
    
    
    
    function product() {
       
        //GET post variables
        $view_type = $this->input->post('view_type');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $order_type = $this->input->post('order_type');
        $order_status = $this->input->post('order_status');
        
        $data['view_type'] = $this->header_data['view_type'] = ($view_type)?$view_type:'html';
        $dt = date("Y-m-d");       
        $data['from_date'] = ($from_date)?$from_date:date('Y-m-d', strtotime( "$dt -7 day" ));
        $data['to_date'] = ($to_date)?$to_date:date('Y-m-d');
        $data['order_type'] = ($order_type)?$order_type:array('online','corporate');
        $data['order_status'] = ($order_status)?$order_status:array(2,3,8,9,10);

        $this->load->model('mc_sales_report_model');
        $data['products'] = mc_sales_report_model::GetProducts();
        //$data['reports'] = NULL;//mc_sales_report_model::By_Product();
        
        //For Status Dropdown
        $data['status_list'] = mc_utilities::order_status();
        
        $this->header_data['xls_filename'] = 'sales_report_by_product';
        $data['view_page'] = "mc_sales_report/product";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header-report', $this->header_data);
        $this->load->view('mc_sales_report/product', $data);
        $this->load->view('elements/footer-report');
        
    }
	
	function product_delivery() {
		
		$this->load->model('mc_sales_report_model');
		$this->load->model("mc_products_model");
		
		$products = $this->mc_products_model->getProductsOrder();
		
		if($this->input->post('from_date')){
			$from_date = $this->input->post('from_date');	
		}else{
			$dt = date("Y-m-d");
			$from_date = date('Y-m-d',strtotime( "$dt -7 day"));	
		}
		
		if($this->input->post('to_date')){
			$to_date = $this->input->post('to_date');
		}else{
			$to_date = date('Y-m-d');	
		}
		
		if($this->input->post("view_type")){
			$view_type = $this->input->post("view_type");
		}else{
			$view_type = "html";
		}
		
		if($this->input->post("order_type")){
			$order_type = $this->input->post("order_type");
		}else{
			$order_type = array('online','corporate');
		}
		
		if($this->input->post("order_status")){
			$order_status = $this->input->post("order_status");
		}else{
			$order_status = array(2,3,8,9,10);
		}
		
		$data_deliveries = $this->mc_sales_report_model->getDeliveries($from_date, $to_date, $order_status, $order_type, "delivery");
		$deliveries = array();
		foreach($data_deliveries as $dt_delivery){
			$deliveries[$dt_delivery->product_id][strtotime($dt_delivery->service_date)] = $dt_delivery->total;
		}
		

		$data['products'] = $products;
		$data['deliveries'] = $deliveries;
        $data['view_type'] = $view_type;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
        $data['order_status'] = $order_status;
		$data['order_type'] = $order_type;
		
        //For Status Dropdown
        $data['status_list'] = $this->mc_utilities->order_status();
        
        $this->header_data['xls_filename'] = 'sales_report_by_product';
		$this->header_data['view_type'] = $view_type;
        //$data['view_page'] = "mc_sales_report/product";
        $data['page_title'] = $this->current_app_id->app_name;
		$data['action'] = BASE_URL."mc_sales_report/product_delivery";
		$data['title'] = "Delivery";

        $this->load->view('elements/header-report', $this->header_data);
        $this->load->view('mc_sales_report/product_delivery', $data);
        $this->load->view('elements/footer-report');
        
    }
	
	function product_collection() {

        $this->load->model('mc_sales_report_model');
		$this->load->model("mc_products_model");
		
		$products = $this->mc_products_model->getProductsOrder();
		
		if($this->input->post('from_date')){
			$from_date = $this->input->post('from_date');	
		}else{
			$dt = date("Y-m-d");
			$from_date = date('Y-m-d',strtotime( "$dt -7 day"));	
		}
		
		if($this->input->post('to_date')){
			$to_date = $this->input->post('to_date');
		}else{
			$to_date = date('Y-m-d');	
		}
		
		if($this->input->post("view_type")){
			$view_type = $this->input->post("view_type");
		}else{
			$view_type = "html";
		}
		
		if($this->input->post("order_type")){
			$order_type = $this->input->post("order_type");
		}else{
			$order_type = array('online','corporate');
		}
		
		if($this->input->post("order_status")){
			$order_status = $this->input->post("order_status");
		}else{
			$order_status = array(2,3,8,9,10);
		}
		
		$data_deliveries = $this->mc_sales_report_model->getDeliveries($from_date, $to_date, $order_status, $order_type);
		$deliveries = array();
		foreach($data_deliveries as $dt_delivery){
			$deliveries[$dt_delivery->product_id][strtotime($dt_delivery->service_date)] = $dt_delivery->total;
		}
		

		$data['products'] = $products;
		$data['deliveries'] = $deliveries;
        $data['view_type'] = $view_type;
		$data['from_date'] = $from_date;
		$data['to_date'] = $to_date;
        $data['order_status'] = $order_status;
		$data['order_type'] = $order_type;
		
        //For Status Dropdown
        $data['status_list'] = $this->mc_utilities->order_status();
        
        $this->header_data['xls_filename'] = 'sales_report_by_product';
		$this->header_data['view_type'] = $view_type;
        //$data['view_page'] = "mc_sales_report/product";
        $data['page_title'] = $this->current_app_id->app_name;
		$data['action'] = BASE_URL."mc_sales_report/product_collection";
		$data['title'] = "Collection";

        $this->load->view('elements/header-report', $this->header_data);
        $this->load->view('mc_sales_report/product_delivery', $data);
        $this->load->view('elements/footer-report');
        
    }
    
    function sale_manager() {
        
        //GET post variables
        $view_type = $this->input->post('view_type');
        //echo $view_type; die();
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $order_type = $this->input->post('order_type');
        $order_status = $this->input->post('order_status');
        $sales_manager = $this->input->post('sales_manager');
        
        $data['view_type'] = $this->header_data['view_type'] = ($view_type)?$view_type:'html';
        $dt = date("Y-m-d");       
        $data['from_date'] = ($from_date)?$from_date:date('d-M-Y', strtotime( "$dt -7 day" ));
        $data['to_date'] = ($to_date)?$to_date:date('d-M-Y');
        //$data['order_type'] = ($order_type)?$order_type:'online';
        $data['order_status'] = ($order_status)?$order_status:array(2,3,8,9,10); //collected
        $data['sales_manager'] = ($sales_manager)?$sales_manager:'View All';

        $this->load->model('mc_sales_report_model');
        
        $param = array(
            //'view_type' => $data['view_type'],
            'from_date' => $data['from_date'],
            'to_date' => $data['to_date'],
            //'order_type' => $data['order_type'],
            'order_status' => $data['order_status'],
            'sales_manager' => $data['sales_manager']
        );
        
        $data['reports'] = mc_sales_report_model::By_SalesManager($param);
        
        //For Status Dropdown
        $data['status_list'] = mc_utilities::order_status();
        
        //For Sales Manager
        $data['managers'] = mc_sales_report_model::Get_SalesManagers();
        
        $this->header_data['xls_filename'] = 'sales_report_by_sales_manager';
        
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header-report', $this->header_data);
        $this->load->view('mc_sales_report/sale_manager', $data);
        $this->load->view('elements/footer-report');
        
    }
    
    
  

    
	
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */