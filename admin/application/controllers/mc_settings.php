<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mc_settings extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		$this->load->model('mc_settings_model');
		$data['bc'] = Mc_settings_model::get_all_block_dates('collection'); //for block collection
		$data['bd'] = Mc_settings_model::get_all_block_dates('delivery'); //for delivery collection
        $data['pr'] = Mc_settings_model::get_all_promotion_rules('delivery'); //for promotion rules

		$data['view_page'] = "mc_settings/mc_settings";
		$data['page_title'] = $this->current_app_id->app_name;
                
		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	

	public function add_bc()
	{
		$this->form_validation->set_rules('bc_date', 'Block Collection Date', 'required|xss_clean');
		
		
		if ($this->form_validation->run() == FALSE)
		{
                    $this->session->set_flashdata('error_notification', validation_errors());
                    redirect($this->router->fetch_class());
		}
		else
		{
			$this->load->model('common');
			extract($_POST);

			
			$insert_data = array(
				'block_type' => 'collection',
				'block_date' => date("Y-m-d", strtotime($bc_date)),				
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("mc_block_dates", $insert_data);			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new block collection date.');
			redirect($this->router->fetch_class());			
		}
	}
	public function add_bd()
	{
            $this->form_validation->set_rules('bd_date', 'Block Collection Date', 'required|xss_clean');	

            if ($this->form_validation->run() == FALSE)
            {
                $this->session->set_flashdata('error_notification', validation_errors());
                redirect($this->router->fetch_class());
            }
            else
            {
                $this->load->model('common');
                extract($_POST);

                $insert_data = array(
                    'block_type' => 'delivery',
                    'block_date' => date("Y-m-d", strtotime($bd_date)),				
                    'created_by' => $this->session->userdata('user_id')
                );

                $this->common->insert("mc_block_dates", $insert_data);			
                $this->session->set_flashdata('success_notification', 'You have successfully added a new block delivery date.');
                redirect($this->router->fetch_class());			
            }
	}
        
        function update_promotion_rules(){
            
            $total_post = count($this->input->post('promo'));            
            for($a = 1; $a<=$total_post; $a++ ) {
               $update_data = array(
                    'date_start' => date("Y-m-d", strtotime($_POST['date_start'][$a])),
                    'date_end' => date("Y-m-d", strtotime($_POST['date_end'][$a])),
                    'discount' => ($_POST['discount'][$a])?$_POST['discount'][$a]:0
                );                
                $this->common->update("mc_promotion_rules", 'id', $a, $update_data);                
            }
            $this->session->set_flashdata('success_notification', 'You have successfully updated the promotion rules.');
            redirect($this->router->fetch_class());
        }
	

	

    /* AJAX Functions */
    function ajax_delete_date(){
        extract($_POST);
        if ($this->current_app_info->delete_role && isset($the_id)){
            $this->load->model('common');			
            $delete_data = array(
                   'id' => $the_id
                );				
            $this->common->delete('mc_block_dates', $delete_data);
        }
        else
            redirect($this->router->fetch_class());		
    }
        
	
        
    
}

/* End of file mc_settings.php */
/* Location: ./application/controllers/mc_settings.php */