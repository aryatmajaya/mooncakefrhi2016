<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mc_transactions extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
        $this->load->library('Mc_utilities');

    }
	
    public function index($view_type = NULL, $page_number = NULL, $option1 = NULL) {
        /* Notes:
         * 
         * $this->uri->segment(1); - mc_transactions
         * $option1 = $value1|$value2|$value3
         * 
         */
        $data['tool_type'] = null;
        
        if (!empty($option1)) {
            $the_option = explode('_', $option1);
            $data['option1'] = $option1;
            
            if ($the_option[0] == "totalsales") {
                $data['total_sales'] = 1;
                $data['from_date'] = $the_option[1];
                $data['to_date'] = $the_option[2];
            }
            
            $data['tool_type'] = $the_option[0]; //assign what kind of tool being used
        }
        
        $this->form_validation->set_rules('search', 'Search Field is required.', 'xss_clean');
        //Search
        $search_field = $this->input->post('search_field');
        $search = $this->input->post('search');

        if (isset($view_type) && $view_type != "all"){             
            $get_code = Mc_utilities::order_status_id($view_type);
            $status = $get_code[0];                
        }
        else {
            $view_type = "all";
            $status = 99;
        }

        $this->load->model('mc_transactions_model');
        $data['view_type'] = $view_type;
        
        
        if (empty($option1[0])) {
            /* Pagination */
            $this->load->library('pagination');
            $this->config->load('raffles_mooncakes_config');
			
			
		
			
			
			
			$url = site_url($this->router->fetch_class()."/index/".$view_type);
			
			$config["base_url"] = $url;
            $config["total_rows"] = mc_transactions_model::transaction_count($status);
            $config["per_page"] = 100;
            $config["uri_segment"] = 4;
            $config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
            $config['full_tag_close'] = '</ul></div><!--pagination-->';

            $config['first_link'] = '&laquo; First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last &raquo;';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';

            $config['next_link'] = 'Next &rarr;';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = '&larr; Previous';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';
			
			
			
			$query_array = array();
			if($this->input->get("sort")){
				$query_array["sort"]=$this->input->get("sort");
			}
			if($this->input->get("order")){
				$query_array["order"]=$this->input->get("order");
			}
			
			$parse_query = parse_url($url, PHP_URL_QUERY);
			
			$new_query = "";
			
			if($query_array){
				if($parse_query){
					$new_query = '&'.http_build_query($query_array);
				}else{
					$new_query = '?'.http_build_query($query_array);
				}
			}
			
			$config['first_url'] = $url.$new_query;
			$config['suffix'] = $new_query;
			

            $this->pagination->initialize($config);
            $data["pagination_links"] = $this->pagination->create_links();
            /* End Pagination */
        }
        
        $page = ($page_number && is_numeric($page_number)) ? $page_number : 0;
        
        $data['page_number'] = $page;
		
        $data['transactions'] = mc_transactions_model::get_all_transactions(
			$status,
			$search_field,
			$search,
			(isset($config["per_page"]))?$config["per_page"]:null,
			$page,
			(isset($data['from_date']))?$data['from_date']:null,
			(isset($data['to_date']))?$data['to_date']:null,
			$this->input->get("sort")?$this->input->get("sort"):"id",
			$this->input->get("order")?$this->input->get("order"):"desc"
		);
        

        $data['view_page'] = "mc_transactions/mc_transactions";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
    
    function total_sales() {
        extract($_POST);
        
        redirect(site_url($this->router->fetch_class().'/index/'.$status_type.'/0/totalsales_'.$from_date.'_'.$to_date));
    }

    public function view($the_id){
        $this->load->model('mc_transactions_model');
        $data['entry'] = mc_transactions_model::Get_Order_Details($the_id);
		$data['order_corporate'] = mc_transactions_model::get_order_corporate($the_id);
        $data['total'] = mc_transactions_model::Get_Total_Amount($the_id);
        $data['items'] = mc_transactions_model::Get_Ordered_Items($the_id);
        $data['status_list'] = mc_utilities::order_status();


        if ($data['entry']){
            $data['fo'] = Mc_transactions_model::Get_All_Fulfillment_Options2($data['entry']->order_ref);

            //This is to get the number of deliveries to be used for counting of number of deliveries
            $data['deliveries'] = Mc_transactions_model::Get_Total_Delivery_Qty($data['entry']->order_ref);

            $data['view_page'] = "mc_transactions/mc_transactions_view";
            $data['page_title'] = $this->current_app_id->app_name;
            $data['action'] = "edit";

            $this->load->view('elements/header', $this->header_data);
            $this->load->view('elements/template1', $data);
            $this->load->view('elements/footer');
        }
        else
            redirect($this->router->fetch_class());
    }

    public function update(){
        $this->load->model('mc_transactions_model');
        extract($_POST);

        $this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
        $this->form_validation->set_rules('payment_mode', 'Payment Mode', 'xss_clean');
        $this->form_validation->set_rules('admin_notes', 'Remarks/Notes', 'xss_clean');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');

        $this->form_validation->set_rules('mobile', 'Mobile#', 'required|xss_clean');
        $this->form_validation->set_rules('alt_contact', 'Alt Contact #', 'xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');


        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('required_error', validation_errors());
            redirect($this->router->fetch_class().'/view/'.$order_id);
        }
        else {
            //updating order details
            $order_data = array(
            	'payment_mode' => $payment_mode,
               	'status' => $status,
               	'admin_notes' => $admin_notes
            );
					
            mc_transactions_model::Update_Order($order_id, $order_data);
			
            //updating customer details
            $customer_data = array(
                'email' => $email,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'mobile' => $mobile,
                'alt_contact' => ($alt_contact)?$alt_contact:''
            );			
            mc_transactions_model::Update_Customer($customer_id, $customer_data);

            //check if Notify Customer is Yes, then send email notification
            if ($status == 8 && $notify == "yes"){
                //send mail here
                //Mc_utilities::Send_Confirmation($email, $order_ref);
                redirect(CONFIRMATION_MAILER.$order_ref.'/returnadmin/');
            }else{
                $this->session->set_flashdata('notification_status', 'Updated Successfully');
                redirect($this->router->fetch_class());
            }
        }
    }
    
    public function update_fulfillmentdate(){
        extract($_POST);

        
            //updating mc_delivery
            $update_data = array(
                'service_date' => $delivery_date_field
                );			
            common::update('mc_delivery', 'order_ref', $order_ref, $update_data);
            
            redirect($this->router->fetch_class().'/view/'.$order_id);
            
        
    }
    
	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($property_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('properties', 'property_id', $property_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}
    
    function print_report_PDF($view_type = NULL, $page_number = NULL, $option1 = NULL) {

        //$this->load->model('Mc_delivery_report_model');
        
        if (!empty($option1)) {
            $the_option = explode('_', $option1);
            
            if ($the_option[0] == "totalsales") {
                $data['total_sales'] = 1;
                $from_date = $the_option[1];
                $to_date = $the_option[2];
            }
        }
        
        //Search
        $search_field = null;
        $search = null;

        if (isset($view_type) && $view_type != "all"){             
            $get_code = Mc_utilities::order_status_id($view_type);
            $status = $get_code[0];                
        }
        else {
            $view_type = "all";
            $status = 99;
        }

        $this->load->model('mc_transactions_model');
        $this->load->model('Mc_delivery_report_model');
        
        $data['view_type'] = $view_type;
        
        $data['allowed_collection_date'] = array('in-process', 'completed');
        
        
        
        $data['transactions'] = mc_transactions_model::get_all_transactions($status, $search_field, $search, (isset($config["per_page"]))?$config["per_page"]:null, $page_number, (isset($from_date))?$from_date:null, (isset($to_date))?$to_date:null);
        //var_dump($data['transactions']);
        $data['print_mode'] = 1;
        
        $the_html = $this->load->view('mc_transactions/pdf_report', $data, true);
        
            
        // Export to PDF using mPDF
        $this->load->library('mpdf56');
        $pdf = $this->mpdf56->load();

        ini_set("memory_limit","2064M");

        //CONFIGURATIONS
        //$pdf->showStats = true;
        $pdf->showImageErrors = false;
        $pdf->shrink_tables_to_fit=1;

         //APPLYING CSS
        $cssFilePath = FCPATH."/assets/css/";
        $stylesheet = file_get_contents($cssFilePath."report_print.css");
        $pdf->WriteHTML($stylesheet,1);
            
        //SETTING THE FOOTER
        //$the_footer = $this->load->view('audit_system/reports/footer', $data, (!isset($mode))?true:false);
        //$pdf->SetHTMLFooter($the_footer);
        
        
        //$pdf->autoPageBreak = true;
        $pdf->use_kwt = true;
            
        //if (!isset($debug)){
            $pdf->WriteHTML($the_html,2);
            $pdf->Output('transaction_report.pdf', 'I');
        //}
    }
    
    public function boxview($the_id){
        /* Added 18 June 2014 by James Castaneros */
        $this->load->model('mc_transactions_model');
        $data['entry'] = mc_transactions_model::Get_Order_Details($the_id);
        $data['total'] = mc_transactions_model::Get_Total_Amount($the_id);
        $data['items'] = mc_transactions_model::Get_Ordered_Items($the_id);
        $data['status_list'] = mc_utilities::order_status();


        if ($data['entry']){
            $data['fo'] = Mc_transactions_model::Get_All_Fulfillment_Options2($data['entry']->order_ref);

            //This is to get the number of deliveries to be used for counting of number of deliveries
            $data['deliveries'] = Mc_transactions_model::Get_Total_Delivery_Qty($data['entry']->order_ref);

            $data['view_page'] = "mc_transactions/mc_transactions_view";
            $data['page_title'] = $this->current_app_id->app_name;
            $data['action'] = "edit_status";

            $this->load->view('elements/box-template', $data);
        }
        else
            redirect($this->router->fetch_class());
    }
	
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */