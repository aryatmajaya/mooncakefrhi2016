<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        

        //$this->load->library('Check_permission');

        //$this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	//$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	/*
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}
*/

    }
    
	


	

	public function index()
	{
		$the_id = $this->session->userdata('user_id');
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('users','id',$the_id);

		if ($data['entries']){	

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = "My Account";
			$data['action'] = "edit";

			$data['the_fields'] = array('1' => 'username|', '2' => 'password|password', '3' => 'first_name|', '4' => 'last_name|', '5' => 'email|', '6' => 'company_name|');
	

			$this->load->view('elements/header');
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			
			if ($username == $this->session->userdata('suser_username'))
				$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
			else
				$this->form_validation->set_rules('username', 'Username', 'required|xss_clean|is_unique[users.username]');

			//$this->form_validation->set_rules('password', 'Username', 'required|xss_clean');
			$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
			$this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('required_error', validation_errors());
				
				redirect($this->router->fetch_class());
				
			}
			else {
			
				
				
				if ($password == ""){
				$update_data = array(
		               'username' => $username,
		               'first_name' => $first_name,
		               'last_name' => $last_name,
		               'email' => $email,
		               'company_name' => $company_name
		            );
				}
				else{
					$update_data = array(
			               'username' => $username,
			               'password' => sha1($password),
			               'first_name' => $first_name,
			               'last_name' => $last_name,
			               'email' => $email,
			               'company_name' => $company_name
			            );	
				}
				
				$this->common->update('users', 'id', $this->session->userdata('user_id'), $update_data);
				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($brand_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('brands', 'brand_id', $brand_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

}

/* End of file brands.php */
/* Location: ./application/controllers/brands.php */