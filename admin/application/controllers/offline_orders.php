<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offline_orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('Check_permission');

		$this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
		$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
		
		//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
		if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
			redirect($this->router->fetch_class());
		} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
			redirect($this->router->fetch_class());
		} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
			redirect($this->router->fetch_class());
			
		} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
			redirect($this->router->fetch_class());
		}


		$this->load->library('Mc_utilities');

	}
	
	public function index() {

		$this->load->model('orders_model');
		$this->load->model('mc_transactions_model');
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$offline_transactions = $this->orders_model->get_all_offline_transactions();
		if($offline_transactions){	
			foreach($offline_transactions as $key=>$transaction){
				$offline_transactions[$key]->total_amount = $this->mc_transactions_model->Get_Total_Amount($transaction->id);
				$offline_transactions[$key]->raw_total = $this->mc_transactions_model->Get_Raw_Total_Qty($transaction->id);
				$offline_transactions[$key]->total_qty = $this->mc_transactions_model->Get_Total_Qty($transaction->id);
			}
		}else{
			$offline_transactions = array();
		}
		
		$data['transactions'] = $offline_transactions;

		$data['view_page'] = "corporate_orders/corporate_orders";
		$data['page_title'] = $this->current_app_id->app_name;
		
		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}
	
	function add() {

		$this->load->model('corporate_orders_model');
        $data['corporate_customers'] = $this->corporate_orders_model->Get_Customers();
        
                    
        if ($this->input->post('customer_type') == "new") { 
            $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean'); 
            $this->form_validation->set_rules('mobile', 'Contact No', 'required|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean');
        } else {
            $this->form_validation->set_rules('search_customer', 'Search Customer', 'xss_clean');
        }
		$this->load->model('users_model');
		
		if ($this->form_validation->run() == FALSE) {
			$customer = common::get_record_by_id('mc_customers', 'email', $this->session->userdata('suser_username'));
			$data['products'] = common::get_all_records3('mc_products', 'id', 'ASC');
			
			$data['user_data'] = (object) array(
				'first_name' => $this->session->userdata('user_firstname'),
				'last_name' => $this->session->userdata('user_firstname'),
			 	'email' => $this->session->userdata('suser_username')
			 	);

			if($customer){
				$data['user_data'] = $customer;
			}
			
			$data['action'] = "add";

			$data['view_page'] = "corporate_orders/order_form";
			$data['page_title'] = $this->current_app_id->app_name;

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		} else {
			extract($_POST);
			if ($customer_type == "existing") {
                $customer_id = $search_customer;
            } else {
				//SAVING CUSTOMER DETAILS
				$customer_data = array(
					'password' => '*offline-order-only*',
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'company_name' => ($company_name)?$company_name:'',
                    'address' => ($address)?$address:'',
                    'postal_code' => ($postal_code)?$postal_code:'',
                    'mobile' => $mobile,
                    'status' => 1,
                    'created_by' => $this->session->userdata('user_id')
				);
				common::insert("mc_customers", $customer_data);
				$customer_id = $this->db->insert_id();	
				//END
			}
			
			//CREATE ORDER
			$this->load->model('orders_model');
			$the_sequence_id = Orders_model::GetMaxID();
			$order_ref = date("ymdHi").sprintf("%05d",$the_sequence_id+1); //create order_ref
		   
			$order_data = array(
				'order_ref' =>  $order_ref,
				'customer_id' => $customer_id,
				'corporate_discount' => $total_discount * 100,
				'total_amount' => $total_price,
				'discounted_amount' => $total_price * $total_discount,
				'status' => 10, //offline incomplete
				'ordering_method' => 'offline',
				'admin_notes' => '',
				'admin_ordered_by' => $this->session->userdata('user_id')
			);

			Orders_model::add_order($order_data);
			$order_id = $this->db->insert_id();
			//END

			for ($i = 0; $i < count($product_qty); ++$i) {
				if ($product_qty[$i] > 0) {
					
					$items_data = array(
						'order_id' => $order_id,
						'product_id' => $product_id[$i],
						'product_name' => $product_name[$i],
						'qty' => $product_qty[$i],
						'price' => $product_price[$i]
					);
					common::insert("mc_orders_items", $items_data);
				}
			}
			$this->session->set_flashdata('success_notification', "Order has been saved.");
			redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
		}
	}

	function edit($order_id) {
        $this->load->model('corporate_orders_model');
        $this->load->model('orders_model');
        $this->load->model('common');

        $data['order_details'] = $this->corporate_orders_model->Order_Details($order_id);
        $data['products'] = $this->common->get_all_records3('mc_products', 'id', 'ASC');

        $data['items'] = $this->orders_model->Get_Ordered_Items($order_id);
        $data['delivery_options'] = $this->orders_model->Get_Delivery_Option($data['order_details']->order_ref);
        
        $data['bc'] = $this->orders_model->get_all_block_dates('collection'); //block collection dates
        $data['bd'] = $this->orders_model->get_all_block_dates('delivery'); //block delivery dates

        $data['redirect_url'] = "edit";
        $data['action'] = "add";
        
        $data['view_page'] = "corporate_orders/order_form_edit";
        $data['page_title'] = $this->current_app_id->app_name;

        $this->load->view('elements/header', $this->header_data);
        $this->load->view('elements/template1', $data);
        $this->load->view('elements/footer');
    }
	
	function fulfillment_options($order_id) {
		$this->load->model('orders_model');
		$data['order_details'] = orders_model::Order_Summary_byID($order_id);
		//$data['items'] = $this->session->userdata('sess_co_cart');
		$data['items'] = orders_model::Get_Ordered_Items($order_id);
		
		//Get delivery options to display it
		$data['delivery_options'] = orders_model::Get_Delivery_Option($data['order_details']->order_ref);
		//$data['total_delivery'] = orders_model::Get_Total_Delivery($order_details->order_ref);
		$data['bc'] = orders_model::get_all_block_dates('collection'); //block collection dates
		$data['bd'] = orders_model::get_all_block_dates('delivery'); //block delivery dates

		$data['action'] = "add";

		$data['view_page'] = "corporate_orders/fulfillment_options_form";
		$data['page_title'] = $this->current_app_id->app_name;

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}
	
	function add_fulfillment() {
		$this->load->model('orders_model');
		$this->form_validation->set_rules('delivery_date_field', 'Delivery Date', 'required|xss_clean'); 
		//die(print_r($_SERVER));
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('fulfillment_required_error', 'Date is required. Please fillup the date from Fulfillment Options.');
			redirect($_SERVER['HTTP_REFERER']);
		}
		$order_id = $this->input->post('order_id');
		$order_ref = $this->input->post('order_ref');
		$customer_id = $this->input->post('customer_id');
		
		if (isset($order_ref)) {
			if($this->input->post('service_type') == "self_collection"){
				$insert_data = array(
					'order_ref' => $order_ref,
					'customer_id' => $customer_id,
					'service_type' => $this->input->post('service_type'),
					'service_date' => $this->input->post('delivery_date_field')
				);
			}else{ //delivery
				$insert_data = array(
					'order_ref' => $order_ref,
					'customer_id' => $customer_id,
					'service_type' => $this->input->post('service_type'),
					'service_date' => $this->input->post('delivery_date_field'),
					'time_slot' => $this->input->post('time_slot'),
					'address' => $this->input->post('address'),
					'postal_code' => $this->input->post('postal_code'),
					'delivered_to' => $this->input->post('delivered_to'),
					'delivered_contact' => $this->input->post('delivered_contact')
				);            
			}
			orders_model::add_delivery($insert_data);
			$delivery_id = $this->db->insert_id();
		}
		
		
		for ($i = 0; $i < count($_POST['product_qty']); ++$i) {
			if ($_POST['product_qty'][$i] > 0) {
				//echo $_POST['product_name'][$i].' '.$_POST['product_qty'][$i].'<br />';
				$items_data = array(
					'delivery_id' => $delivery_id,
					'order_ref' => $order_ref,
					'product_id' => $_POST['product_id'][$i],
					'product_name' => $_POST['product_name'][$i],
					'qty' => $_POST['product_qty'][$i]
				);
				orders_model::add_delivery_item($items_data);
			}
		}
		redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
		
	}
	
	function delete_option($delivery_id, $order_id, $order_ref){
		$this->load->model('orders_model');
		if (isset($delivery_id) && is_numeric($delivery_id)){
			orders_model::Delete_Delivery($delivery_id, $order_ref);
		}
		redirect($this->router->fetch_class().'/fulfillment_options/'.$order_id);
	}
	
	function print_report_PDF($from_date = NULL, $to_date = NULL) {
		$status = 8;
		$this->load->model('Mc_delivery_report_model');
		
		$status = 8;
		$this->load->model('Mc_delivery_report_model');
		$data['transactions'] = Mc_delivery_report_model::get_all_deliveries($status, (isset($from_date))?$from_date:null, (isset($to_date))?$to_date:null);
		//var_dump($data['transactions']);
		$data['print_mode'] = 1;
		
			//$the_html = $this->load->view('mc_delivery_report/pdf_report', $data, (!isset($debug))?true:false);
			$the_html = $this->load->view('mc_delivery_report/pdf_report', $data, true);
		
			
			
			// Export to PDF using mPDF
			$this->load->library('mpdf56');
			$pdf = $this->mpdf56->load();
			
			ini_set("memory_limit","2064M");
			
			//CONFIGURATIONS
			//$pdf->showStats = true;
			$pdf->showImageErrors = false;
			$pdf->shrink_tables_to_fit=1;
			
			 //APPLYING CSS
			$cssFilePath = FCPATH."/assets/css/";
			$stylesheet = file_get_contents($cssFilePath."report_print.css");
			$pdf->WriteHTML($stylesheet,1);
			$pdf->use_kwt = true;
			
			$pdf->WriteHTML($the_html,2);
			$pdf->Output('report.pdf', 'I');
		 
	}
	
	function print_deliverynotes_PDF($delivery_ids,$debug = NULL) {
		$this->load->model('mc_delivery_report_model');
		$this->load->model('mc_transactions_model');

		$the_ids = explode('-', $delivery_ids);
		$the_notes = "";
		foreach ($the_ids as $ids) {
			$data['delivery_id'] = $ids;
			$data['order'] = mc_delivery_report_model::Get_OrderDetails_by_deliveryID($ids);
			$data['items'] = mc_delivery_report_model::Get_OrderItems_by_deliveryID($ids);
			
			
			$the_notes .= $this->load->view('mc_delivery_report/delivery_notes_table', $data, (!isset($debug))?true:false);
			
		}
		// Export to PDF using mPDF
		$this->load->library('mpdf56');
		$pdf = $this->mpdf56->load();
		
		ini_set("memory_limit","2064M");
		
		//CONFIGURATIONS
		//$pdf->showStats = true;
		$pdf->showImageErrors = false;
		$pdf->shrink_tables_to_fit=1;
		
		 //APPLYING CSS
		$cssFilePath = FCPATH."/assets/css/";
		$stylesheet = file_get_contents($cssFilePath."report_print.css");
		$pdf->WriteHTML($stylesheet,1);
			
		//SETTING THE FOOTER
		$the_footer = $this->load->view('mc_delivery_report/delivery_notes_footer', $data, (!isset($mode))?true:false);
		$pdf->SetHTMLFooter($the_footer);
		
		$pdf->use_kwt = true;
			
		if (!isset($debug)){
			$pdf->WriteHTML($the_notes,3);
			$pdf->Output('delivery_notes.pdf', 'I');
		}
		 
	}
	
	function test_pdf() {
		$this->load->library('mpdf56');
		$mpdf=$this->mpdf56->load();
		$mpdf->WriteHTML('<columns column-count="3" vAlign="J" column-gap="7" />');
		$mpdf->WriteHTML('<p>Some text...</p>');
		$mpdf->WriteHTML('<columnbreak />');
		$mpdf->WriteHTML('<p>Next column...</p>');
		$mpdf->Output("testmpdf.pdf","I");
	}
	
	function total_sales() {
		extract($_POST);
		redirect(BASE_URL.$this->router->fetch_class().'/index/'.$view_type.'/0/totalsales_'.$from_date.'_'.$to_date);
	}

	public function view($the_id){
		$this->load->model('mc_transactions_model');
		$data['entry'] = mc_transactions_model::Get_Order_Details($the_id);
		$data['total'] = mc_transactions_model::Get_Total_Amount($the_id);
		$data['items'] = mc_transactions_model::Get_Ordered_Items($the_id);
		$data['status_list'] = mc_utilities::order_status();

		if ($data['entry']){
			$data['fo'] = Mc_transactions_model::Get_All_Fulfillment_Options($data['entry']->order_ref);

			//This is to get the number of deliveries to be used for counting of number of deliveries
			$data['deliveries'] = Mc_transactions_model::Get_Total_Delivery_Qty($data['entry']->order_ref);

			$data['view_page'] = "mc_transactions/mc_transactions_view";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
	}

	public function update(){
        $this->load->model('mc_transactions_model');
        extract($_POST);
        
        // UPDATE ORDER DETAILS
        $update_data = array(
            'corporate_discount' => $total_discount * 100,
            'total_amount' => $total_price,
            'discounted_amount' => $total_discount * $total_price,
            'date_modified' => date('Y-m-d H:i:s')
        );

        common::update('mc_orders', 'id', $order_id, $update_data);
        
        $update_data2 = array(
            'notes1' => ($notes1)?$notes1:'',
            'notes2' => ($notes2)?$notes2:''
        );
        common::update('mc_orders_corporate', 'order_id', $order_id, $update_data2);
        

        //UPDATE ORDER ITEMS
        $delete_data = array(
            'order_id' => $order_id
        );
        common::delete('mc_orders_items', $delete_data);
        for ($i = 0; $i < count($_POST['product_qty']); ++$i) {
                if ($_POST['product_qty'][$i] > 0) {
                    //echo $_POST['product_name'][$i].'<br />';
                    $items_data = array(
                        'order_id' => $order_id,
                        'product_id' => $_POST['product_id'][$i],
                        'product_name' => $_POST['product_name'][$i],
                        'qty' => $_POST['product_qty'][$i],
                        'price' => $_POST['product_price'][$i]
                    );
                    common::insert("mc_orders_items", $items_data);
                }
        }
        
        $this->session->set_flashdata('success_notification', 'Updated Successfully');
        redirect($this->router->fetch_class().'/corporate_orders/');
        
    }
	
	
	/* AJAX Functions */
	
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($property_id)){
			$this->load->model('common');
			
			$update_data = array(
			   'deleted' => 1		            
			);
			
			$this->common->update('properties', 'property_id', $property_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

	function ajax_corporate_discount(){
		echo json_encode(array(
			array('min'=>1, 'max'=>100, 'discount'=>.2),
			array('min'=>101, 'max'=>300, 'discount'=>.25),
			array('min'=>301, 'max'=>500, 'discount'=>.28),
			array('min'=>500, 'max'=>1000000, 'discount'=>.30),
		));
	}
		
	function test_ajax4pdf($delivery_ids) {
		$this->load->library('mpdf56');
		$mpdf=$this->mpdf56->load();
		$mpdf->WriteHTML('<columns column-count="3" vAlign="J" column-gap="7" />');
		
		$the_ids = explode('-', $delivery_ids);
		
		foreach ($the_ids as $ids) {
			$mpdf->WriteHTML($ids);
			
		}
		
		
		$mpdf->WriteHTML('<columnbreak />');
		$mpdf->WriteHTML('<p>Next column...</p>');
		$mpdf->Output("testmpdf.pdf","I");
	}

	function ajax_get_customer_details() {
        $customer_id = $this->input->post('customer_id');
        
        $customer_details = common::get_record_by_id('mc_customers', 'id', $customer_id);
        echo json_encode($customer_details);
        
    }
	
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */