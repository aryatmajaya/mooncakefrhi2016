<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Properties extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
	
	public function index()
	{
		$this->load->model('properties_model');

		$data['entries'] = $this->properties_model->get_all_properties();

		$data['view_page'] = "properties/properties";
		$data['page_title'] = "Properties";

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	/*public function add()
	{
		
			

		
	}*/

	public function add()
	{		
		
		$this->form_validation->set_rules('brand_id', 'Brand', 'required|xss_clean');
		$this->form_validation->set_rules('country_id', 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('other_id', 'Other', 'xss_clean');
		$this->form_validation->set_rules('property_code', 'Property Code', 'xss_clean');
		$this->form_validation->set_rules('hotel_name', 'Hotel', 'required|xss_clean|is_unique[properties.hotel_name]');

		$this->form_validation->set_rules('address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('city', 'City', 'xss_clean');
		$this->form_validation->set_rules('state', 'State', 'xss_clean');
		$this->form_validation->set_rules('zip_code', 'Zip Code', 'xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'xss_clean');
		//$this->form_validation->set_rules('logo_image', 'Logo Image', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			
			
			$this->load->model('brands_model');
			$data['brands_list'] = $this->brands_model->get_brands_list();

			$this->load->model('countries_model');
			$data['countries_list'] = $this->countries_model->get_countries_list();

			$this->load->model('common');
			$data['others_list'] = $this->common->get_table_list('others','caption','ASC');
			$data['regions_list'] = $this->common->get_table_list('regions','region','ASC');

			$data['countries_list'] = "";

			$data['view_page'] = "properties/properties-form";
			$data['page_title'] = "Properties";
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');	

		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			/*$new_filename = '';

			if ($_FILES['logo_image']['name'] != "")
				$new_filename = mktime().'_'.$_FILES['logo_image']['name'];*/

			$insert_data = array(
				'brand_id' => $brand_id,
				'region_id' => $region_id,
				'country_id' => $country_id,
				'other_id' => ($other_id)?$other_id:0,
				'property_code' => $property_code,
				'hotel_name' => $hotel_name,
				'address' => $address,
               'city' => $city,
               'state' => $state,
               'zip_code' => $zip_code,
				'logo_filename' => '',
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("properties", $insert_data);
			$latest_id = $this->db->insert_id();
			
			/*//UPLOADING LOGO IMAGE
			$path = "../uploads/clients/$latest_id/";
			if(!file_exists($path))
			{
			   mkdir($path);
			}

			if ($_FILES['logo_image']['name'] != "") {
				

				$config['upload_path'] = $path;
				$config['file_name'] = $new_filename;
				$config['allowed_types'] = 'jpg|gif|png';
				$config['overwrite'] = TRUE;
				
				$this->load->library('upload', $config);
								
				$this->upload->do_upload('logo_image');
				
			}*/
			
			$this->session->set_flashdata('success', 'You have successfully added a new property.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);

		$this->load->model('brands_model');
		$data['brands_list'] = $this->brands_model->get_brands_list();

		
		$this->load->model('common');
		$data['others_list'] = $this->common->get_table_list('others','caption','ASC');
		$data['regions_list'] = $this->common->get_table_list('regions','region','ASC');

		$this->load->model('properties_model');
		$data['entries'] = $this->properties_model->get_record($the_id);

		$this->load->model('countries_model');
		$data['countries_list'] = $this->countries_model->get_countries_list_by_region_id($data['entries']->region_id);


		if ($data['entries']){		


			$data['view_page'] = "properties/properties-form";
			$data['page_title'] = "Properties";
			$data['action'] = "edit";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
	}

	public function update(){
		$this->load->model('common');
		extract($_POST);
		
		$this->form_validation->set_rules('brand_id', 'Brand', 'required|xss_clean');
		$this->form_validation->set_rules('region_id', 'Region', 'required|xss_clean');
		$this->form_validation->set_rules('country_id', 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('hotel_name', 'Hotel', 'required|xss_clean');
		$this->form_validation->set_rules('other_id', 'Other', 'xss_clean');
				
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('required_error', validation_errors());
			redirect($this->router->fetch_class().'/edit/'.$id);
		}
		else {
		
			/*$new_filename = '';

			if ($_FILES['logo_image']['name'] != "")
				$new_filename = mktime().'_'.$_FILES['logo_image']['name'];*/
			
			$update_data = array(
	               'brand_id' => $brand_id,
	               'region_id' => $region_id,
	               'country_id' => $country_id,
	               'other_id' => ($other_id)?$other_id:0,
	               'property_code' => $property_code,
	               'hotel_name' => $hotel_name,
	               'address' => $address,
	               'city' => $city,
	               'state' => $state,
	               'zip_code' => $zip_code,
	               'status' => $status
	               //'logo_filename' => ($new_filename)?$new_filename:$old_logo_file
	            );
			
			$this->common->update('properties', 'property_id', $id, $update_data);

			//UPLOADING LOGO IMAGE
			/*$path = "../uploads/clients/$id/";
			if(!file_exists($path))
			{
			   mkdir($path);
			}

			if ($_FILES['logo_image']['name'] != "") {
				

				$config['upload_path'] = $path;
				$config['file_name'] = $new_filename;
				$config['allowed_types'] = 'jpg|gif|png';
				$config['overwrite'] = TRUE;
				
				$this->load->library('upload', $config);
								
				$this->upload->do_upload('logo_image');
				
			}*/


			$this->session->set_flashdata('notification_status', 'Updated Successfully');
			
			//redirect('/regions/edit/'.$id);
			redirect($this->router->fetch_class());
		}
	}

	/* AJAX Functions */
	
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($property_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('properties', 'property_id', $property_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}
	
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */