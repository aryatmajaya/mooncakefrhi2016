<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regions extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }

	public function index()
	{
		$this->load->model('regions_model');

		$data['view_page'] = "regions/regions";
		$data['page_title'] = "Regions";

		$data['entries'] = $this->regions_model->get_all_regions();
		

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	/*public function add()
	{
			$data['view_page'] = "regions/regions-form";
			$data['page_title'] = "Regions";
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');		
	}
	*/

	public function add() //add and insert
	{
		
		$this->form_validation->set_rules('region', 'Region', 'required|xss_clean|is_unique[regions.region]');
		$this->form_validation->set_rules('status', 'Status', 'xss_clean');
		
		extract($_POST);
		if (isset($_POST['save'])){
			$insert_data = array(
				'region' => $region,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );	
		}
		

		if ($this->form_validation->run() == FALSE)
		{
			//$this->session->set_flashdata('required_error', validation_errors());
			/*
			$this->session->set_flashdata('field_values', $insert_data);
			redirect('/regions/add/');
			*/
			$data['view_page'] = "regions/regions-form";
			$data['page_title'] = "Regions";
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');
			$this->common->insert("regions", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new region.');
			redirect($this->router->fetch_class());			
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('regions','region_id',$the_id);
			
		if ($data['entries']){	
			

			$data['view_page'] = "regions/regions-form";
			$data['page_title'] = "Regions";
			$data['action'] = "edit";
			

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
	}

	public function update(){
		$this->load->model('common');
		extract($_POST);
		
		$this->form_validation->set_rules('region', 'Region', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{

			$this->session->set_flashdata('required_error', validation_errors());
			redirect('/regions/edit/'.$id);
		}
		else {
		
			
			
			$update_data = array(
	               'region' => $region,
	               'status' => $status
	            );
			
			$this->common->update('regions', 'region_id', $id, $update_data);
			$this->session->set_flashdata('notification_status', 'Updated Successfully');
			
			redirect($this->router->fetch_class());
		}
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('regions', 'region_id', $the_id, $update_data);
		}
		else
			redirect($this->router->fetch_class());	
			
			
	}
	
}

/* End of file regions.php */
/* Location: ./application/controllers/regions.php */