<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {
	//private $current_app_id;

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		
		

		$this->load->model('audits_model');
		$data['entries'] = $this->audits_model->Get_All_Templates_for_Auditors();

		$data['view_page'] = "reports/reports";
		$data['page_title'] = "Reports (WIP)";


		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function view_report()
	{
		$template_id = $this->uri->segment(3);

		$this->load->model('audits_model');

		$data['template_details'] = $this->audits_model->get_template_by_id($template_id);
		$data['touchpoints'] = $this->audits_model->Get_All_Touchpoints_of_a_Template($template_id);

		$data['view_page'] = "reports/view_report";
		$data['page_title'] = "Audits - ".$data['template_details']->template_name;
		$data['bread_crumbs'] = 'none';
		$data['action'] = "add";

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');	
	}

	//FOR AUDITOR'S AUDIT FORM
	public function audit_tp_form()
	{
		$touchpoint_id = $this->uri->segment(3);
		$this->load->model('audits_model');

		$data['tp_details'] = $this->audits_model->Get_Touchpoint_of_a_Template_by_id($touchpoint_id);

		$data['elements'] = $this->audits_model->Get_All_Elements_from_a_Touchpoint($data['tp_details']->template_id, $touchpoint_id);

		$data['view_page'] = "audits/audit_tp_form";
		$data['page_title'] = "Audits - ".$data['tp_details']->touch_point_name;
		$data['action'] = "add";

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');	
	}

	
	
	
	

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($brand_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('brands', 'brand_id', $brand_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */