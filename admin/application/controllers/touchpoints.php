<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Touchpoints extends CI_Controller {
	//private $current_app_id;
	private $the_fields = array('1' => 'category_id|dropdown', '2' => 'touch_point|', '3' => 'status|');
	
	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }
    
	public function index()
	{
		
		

		$this->load->model('touchpoints_model');
		$data['entries'] = $this->touchpoints_model->get_all_touchpoints();

		$data['view_page'] = "generic/generic";
		$data['page_title'] = $this->current_app_id->app_name;
		$data['add_caption'] = " Add";
		$data['the_fields'] = array('1' => 'touch_point', '2' => 'category_name', '3' => 'status');

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	public function add()
	{
		$this->load->model('categories_model');
		$data['drop_list1'] = $this->categories_model->get_categories_list();		

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['the_fields'] = $this->the_fields;
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
	}

	public function insert()
	{
		
		$this->form_validation->set_rules('category_id', 'Category', 'required|xss_clean');
		$this->form_validation->set_rules('touch_point', 'Touch Point', 'required|xss_clean|callback_touch_point_check');
		$this->form_validation->set_rules('status', 'Status', 'xss_clean');		
		
		if ($this->form_validation->run() == FALSE)
		{			
			$this->add();
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
			
			$insert_data = array(
				'category_id' => $category_id,
				'touch_point' => $touch_point,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("touch_points", $insert_data);
			
			$this->session->set_flashdata('success_notification', 'You have successfully added a new touch point.');
			redirect($this->router->fetch_class());			
		}
	}

	public function touch_point_check() {
		$cat = $this->input->post('category_id');
		$touch_point = $this->input->post('touch_point');
   		
		$this->load->model('touchpoints_model');
		//
		if ($this->touchpoints_model->Check_Touchpoint_Exists($cat, $touch_point)) {
			$this->form_validation->set_message('touch_point_check', 'Duplicate Touch Point in the same category is invalid.');
			return FALSE; //the form will have an error on touch point name
			//echo 'error'; die();
		} else {
			return TRUE; //no error on touch point name
			//echo 'no error'; die();
		}
	}

	public function edit()
	{
		$the_id = $this->uri->segment(3);
		$this->load->model('common');
		$data['entries'] = $this->common->get_record_by_id('touch_points','id',$the_id);

		$this->load->model('categories_model');
		$data['drop_list1'] = $this->categories_model->get_categories_list();

		if ($data['entries']){	

			$data['view_page'] = "generic/generic-form";
			$data['page_title'] = $this->current_app_id->app_name;
			$data['action'] = "edit";
			$data['the_fields'] = $this->the_fields;

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
			
	}

	public function update(){
			extract($_POST);
		
			$this->load->model('common');
			
			
			$this->form_validation->set_rules('category_id', 'Category', 'required|xss_clean');
			$this->form_validation->set_rules('touch_point', 'Touch Point', 'required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{

				$this->session->set_flashdata('required_error', validation_errors());
				
				if ($id)
					redirect($this->router->fetch_class().'/edit/'.$id);
				else
					redirect($this->router->fetch_class());
			}
			else {
			
				
				
				$update_data = array(
		               'category_id' => $category_id,
		               'touch_point' => $touch_point,
		               'status' => $status
		            );
				
				$this->common->update('touch_points', 'id', $id, $update_data);
				$this->session->set_flashdata('success_notification', 'Updated Successfully');				
			
				redirect($this->router->fetch_class());
			}
		
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($the_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('touch_points', 'id', $the_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

}

/* End of file touchpoints.php */
/* Location: ./application/controllers/touchpoints.php */