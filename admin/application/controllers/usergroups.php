<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usergroups extends CI_Controller {

	private $page_title = "User Groups";
	
	
	

	public function __construct() {
    	parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0 && $this->current_app_info->group_id != 1){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	//} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		//redirect($this->router->fetch_class());
    	}
    	
    }



	public function index(){
		//$data = $this->data;

		$this->load->model('users_model');

		$data['entries'] = $this->users_model->get_all_user_groups();

		$data['view_page'] = "users/user_groups";
		$data['page_title'] = $this->page_title;

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	

	public function add()
	{		
		
		//$this->form_validation->set_rules('group_name', 'Group Name', 'required|xss_clean|is_unique[user_groups.group_name]');
		$this->form_validation->set_rules('group_name', 'Group Name', 'required|xss_clean');
		$this->form_validation->set_rules('app_id[]', 'Application', 'xss_clean');
		$this->form_validation->set_rules('add_role[]', 'Add Role', 'xss_clean');
		$this->form_validation->set_rules('view_role[]', 'View Role', 'xss_clean');
		$this->form_validation->set_rules('edit_role[]', 'Edit Role', 'xss_clean');
		$this->form_validation->set_rules('delete_role[]', 'Delete Role', 'xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->model('applications_model');
			$data['applications_list'] = $this->applications_model->get_applications_list();

			
		

			$data['view_page'] = "users/user_groups_form";
			$data['page_title'] = $this->page_title;

			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
			$this->load->model('common');
			extract($_POST);
		
			$insert_data = array(
				'group_name' => $group_name,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("user_groups", $insert_data);
			$latest_id = $this->db->insert_id();

			//INSERTING APP PERMISSIONS
			//if (isset($_POST['app_id'])){
				for ($i = 0; $i < count($_POST['app_id']); ++$i) {

				
					$the_app_id = $_POST['app_id'][$i];

					if (isset($_POST['add_role'][$the_app_id]))
						$add_role = 1;
					else
						$add_role = 0;

					if (isset($_POST['view_role'][$the_app_id]))
						$view_role = 1;
					else
						$view_role = 0;

					if (isset($_POST['edit_role'][$the_app_id]))
						$edit_role = 1;
					else
						$edit_role = 0;


					if (isset($_POST['delete_role'][$the_app_id]))
						$delete_role = 1;
					else
						$delete_role = 0;

					$insert_apps_data = array(
						'group_id' => $latest_id,
						'app_id' => $_POST['app_id'][$i],
						'add_role' => $add_role,
						'view_role' => $view_role,
						'edit_role' => $edit_role,
						'delete_role' => $delete_role,						
						'created_by' => $this->session->userdata('user_id')
			            );
					
					$this->common->insert("user_group_app_permissions", $insert_apps_data);
				}
			//}
			//END INSERTING APP PERMISSIONS

			
	
			$this->session->set_flashdata('success_notification', 'You have successfully added a new user group.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
		if ($this->current_app_info->edit_role == 1)
			$can_access = 1;
		if ($this->current_app_info->group_id == 1)
			$can_access = 1;

		if ($can_access == 1){
			//$data = $this->data;

			$the_id = $this->uri->segment(3);

			$this->load->model('common');
			$this->load->model('applications_model');
			$data['applications_list'] = $this->applications_model->get_applications_list();

			/*$this->load->model('properties_model');
			$data['properties_list'] = $this->properties_model->get_properties_bybrand_list();*/

			$this->load->model('users_model');
			$data['entries'] = $this->users_model->get_user_group_record($the_id);

			$data['view_page'] = "users/user_groups_form";
			$data['page_title'] = $this->page_title;

			$data['action'] = "edit";

			if ($data['entries']){	

				$this->load->view('elements/header', $this->header_data);
				$this->load->view('elements/template1', $data);
				$this->load->view('elements/footer');
			}
			else
				redirect($this->router->fetch_class());
		}
		else
			redirect($this->router->fetch_class());
	}

	public function update(){
		
		$this->load->model('common');
		extract($_POST);
		
		$this->form_validation->set_rules('group_name', 'Group Name', 'required|xss_clean');
		/*
		$this->form_validation->set_rules('country_id', 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('hotel_name', 'Hotel', 'required|xss_clean');*/
				
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->session->set_flashdata('required_error', validation_errors());
			redirect($this->router->fetch_class().'/user_groups/edit/'.$id);
		}
		else {
			
			
			
			$update_data = array(
	               'group_name' => $group_name
	               
	            );
			
			$this->common->update('user_groups', 'group_id', $id, $update_data);

			//INSERTING APP PERMISSIONS
			$delete_data = array(
				'group_id' => $id
	            );
			$this->common->delete('user_group_app_permissions',$delete_data);

			for ($i = 0; $i < count($_POST['app_id']); ++$i) {

				
					$the_app_id = $_POST['app_id'][$i];

					if (isset($_POST['add_role'][$the_app_id]))
						$add_role = 1;
					else
						$add_role = 0;

					if (isset($_POST['view_role'][$the_app_id]))
						$view_role = 1;
					else
						$view_role = 0;

					if (isset($_POST['edit_role'][$the_app_id]))
						$edit_role = 1;
					else
						$edit_role = 0;


					if (isset($_POST['delete_role'][$the_app_id]))
						$delete_role = 1;
					else
						$delete_role = 0;

					$insert_apps_data = array(
						'group_id' => $id,
						'app_id' => $_POST['app_id'][$i],
						'add_role' => $add_role,
						'view_role' => $view_role,
						'edit_role' => $edit_role,
						'delete_role' => $delete_role,						
						'created_by' => $this->session->userdata('user_id')
			            );
					
					$this->common->insert("user_group_app_permissions", $insert_apps_data);
				
			}
			// END INSERTING APP PERMISSIONS

			$this->session->set_flashdata('notification_status', 'Updated Successfully');
			
			redirect($this->router->fetch_class());
		}
	}

	/* AJAX Functions */
	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($group_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('user_groups', 'group_id', $group_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */