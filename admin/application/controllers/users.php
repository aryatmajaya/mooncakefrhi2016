<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	private $page_title = "Users";

	public function __construct() {
        parent::__construct();
        $this->load->library('Check_permission');

        $this->current_app_id = $this->Thechecker_model->Get_App_ID($this->router->fetch_class().'/');
    	$this->header_data['current_app_info'] = $this->current_app_info = $this->Thechecker_model->Check_App_Permission($this->current_app_id->id, $this->session->userdata('sess_user_account_type'));
    	
    	//THIS IS TO CHECK USER'S PERMISSION TO ADD, EDIT
    	if ($this->router->fetch_method() == "add" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "edit" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	} elseif ($this->router->fetch_method() == "insert" && $this->current_app_info->add_role == 0){
    		redirect($this->router->fetch_class());
    		
    	} elseif ($this->router->fetch_method() == "update" && $this->current_app_info->edit_role == 0){
    		redirect($this->router->fetch_class());
    	}


    }



	public function index()
	{
		$this->load->model('applications_model');
		
		$this->load->model('users_model');

		$data['entries'] = $this->users_model->get_all_users();
                //var_dump($data['entries']);die();

		$data['view_page'] = "users/users";
		$data['page_title'] = $this->page_title;

		$this->load->view('elements/header', $this->header_data);
		$this->load->view('elements/template1', $data);
		$this->load->view('elements/footer');
	}

	

	public function add()
	{		
		
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('account_type', 'Account Type', 'required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'xss_clean');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->model('users_model');
			$data['user_groups_list'] = $this->users_model->get_all_user_groups();

			/*$this->load->model('properties_model');
			$data['properties_list'] = $this->properties_model->get_properties_bybrand_list();*/

			$this->load->model('common');
			$data['clients_list'] = $this->common->get_table_list('clients', 'client_name', 'ASC');

			$data['view_page'] = "users/users-form";
			$data['page_title'] = $this->page_title;
			$data['action'] = "add";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
		{
                    $this->load->library('encrypt');
			$this->load->model('common');
			extract($_POST);
			
			$new_filename = '';

			
			$insert_data = array(
				'username' => $username,
				'password' => $this->encrypt->encode($password),
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'company_name' => $company_name,
				'account_type' => $account_type,
				'status' => $status,
				'created_by' => $this->session->userdata('user_id')
	            );
			
			$this->common->insert("users", $insert_data);
			$latest_id = $this->db->insert_id();
			
			
			if (isset($_POST['property_id']) && count($_POST['property_id'])>0){
				//INSERTING PROPERTY PERMISSIONS
				$delete_properties_data = array(
					'user_id' => $latest_id
		            );
				$this->common->delete('user_property_permissions',$delete_properties_data);

				for ($i = 0; $i < count($_POST['property_id']); ++$i) {
						$insert_properties_data = array(
							'user_id' => $latest_id,
							'property_id' => $_POST['property_id'][$i],
							'created_by' => $this->session->userdata('user_id')
				            );
						
						$this->common->insert("user_property_permissions", $insert_properties_data);
				}
				// END INSERTING PROPERTY PERMISSIONS
			}
			
			
			$this->session->set_flashdata('success', 'You have successfully added a new user.');
			redirect($this->router->fetch_class());
			
		}
	}

	public function edit()
	{
            
            
            $this->load->library('encrypt');
		$the_id = $this->uri->segment(3);

		$this->load->model('users_model');
		$data['user_groups_list'] = $this->users_model->get_all_user_groups();
		$data['entries'] = $this->users_model->get_user_data($the_id);

		/*$this->load->model('properties_model');
		$data['properties_list'] = $this->properties_model->get_properties_bybrand_list();*/
                
                if ($this->session->userdata('sess_user_account_type') == 1)
                    $allow = true;
                else if ($this->session->userdata('sess_user_account_type') >= 2 && $data['entries']->account_type >= 2)
                    $allow = true;
                else
                    $allow = false;
                

		if ($allow){
			
			$this->load->model('common');
			$data['clients_list'] = $this->common->get_table_list('clients', 'client_name', 'ASC');

			$data['view_page'] = "users/users-form";
			$data['page_title'] = $this->page_title;
			$data['action'] = "edit";

			$this->load->view('elements/header', $this->header_data);
			$this->load->view('elements/template1', $data);
			$this->load->view('elements/footer');
		}
		else
			redirect($this->router->fetch_class());
	}

	public function update(){
            $this->load->library('encrypt');
            
		$this->load->model('common');
		extract($_POST);
		
		if ($username == $orig_username)
			$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		else
			$this->form_validation->set_rules('username', 'Username', 'required|xss_clean|is_unique[users.username]');

		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
				
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('required_error', validation_errors());
			redirect($this->router->fetch_class().'/edit/'.$id);
		}
		else {
		
			
			if ($password == ""){
				$update_data = array(
		               'username' => $username,
		               'first_name' => $first_name,
		               'last_name' => $last_name,
		               'email' => $email,
		               'company_name' => $company_name,
		               'account_type' => $account_type,
		               'status' => $status
		            );
			}
			else{
				$update_data = array(
		               'username' => $username,
                               'password' => $this->encrypt->encode($password),
		               'first_name' => $first_name,
		               'last_name' => $last_name,
		               'email' => $email,
		               'company_name' => $company_name,
		               'account_type' => $account_type,
		               'status' => $status
		            );	
			}

			$this->common->update('users', 'id', $id, $update_data);

			

			
			
			if (isset($_POST['property_id']) && count($_POST['property_id'])>0){
				
				//INSERTING PROPERTY PERMISSIONS
				$delete_properties_data = array(
					'user_id' => $id
		            );
				$this->common->delete('user_property_permissions',$delete_properties_data);

				for ($i = 0; $i < count($_POST['property_id']); ++$i) {		
						

						$insert_properties_data = array(
							'user_id' => $id,
							'property_id' => $_POST['property_id'][$i],
							'created_by' => $this->session->userdata('user_id')
				            );
						
						$this->common->insert("user_property_permissions", $insert_properties_data);
					
				}
				// END INSERTING PROPERTY PERMISSIONS
			}
			

			$this->session->set_flashdata('notification_status', 'Updated Successfully');
			
			redirect($this->router->fetch_class());
		}
	}

	/* AJAX Functions */
	

	function ajax_delete(){
		extract($_POST);
		if ($this->current_app_info->delete_role && isset($user_id)){
			$this->load->model('common');
			
			$update_data = array(
		               'deleted' => 1		            
		            );
				
			$this->common->update('users', 'id', $user_id, $update_data);

		}
		else
			redirect($this->router->fetch_class());
			
	}

	function dynamic_query(){
		extract($_POST);
		//echo $client_id;		

		$sql = 	"SELECT brand_id as list_id, brand_name as list_name".
				" FROM brands".
				" WHERE client_id=$client_id";

		$this->load->model('common');
		$the_list = $this->common->dynamic_query($sql);
		var_dump($the_list);
		
		if ($the_list) {
			echo "<option value='all'>All Brands</option>";
			foreach ($the_list as $the_list) {
				echo "<option>".$the_list->list_name."</option>\n";
			}	
		}
		
	}

	function ajax_display_properties($client_id = NULL){
		extract($_POST);
		//$action="add";
		if ($client_id){
			/*$sql = 	"SELECT brands.brand_id,brands.client_id,brands.brand_name,properties.property_id,properties.region_id,properties.hotel_name".
				" FROM brands".
				" LEFT JOIN properties ON brands.brand_id = properties.brand_id".
				" WHERE brands.client_id = $client_id".
				" AND brands.status = 1".
				" AND brands.deleted = 0".
				" AND brands.deleted = 0".
				" AND properties.status = 1".
				" AND properties.deleted = 0".
				" ORDER BY brands.brand_name, properties.region_id ASC";*/
			$sql = 	"SELECT brand_id, client_id, brand_name".
				" FROM brands".
				" WHERE brands.client_id = $client_id".
				" AND status = 1".
				" AND deleted = 0".
				" ORDER BY brand_name ASC";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0)
			   $brands_list = $query->result(); 



          $current_brand = "";
          $current_region = "none";
          $ctr = 1;
          $bctr = 1;
          $pctr = 1;
        	foreach ($brands_list as $brands_list){
        		echo "<div class='brand_container' id='brand$bctr'>\n";
	  			if ($current_brand != $brands_list->brand_name){          	
	              echo "<legend>$brands_list->brand_name";
	              echo " <input type='checkbox' class='check_all_regions' style='margin-bottom: 7px;'>\n";
	              echo "</legend>";
	              $current_brand = $brands_list->brand_name;
	            }

	            //Fetching Properties of a current brand
	            $psql = "SELECT properties.property_id, properties.region_id, properties.hotel_name, regions.region".
					" FROM properties".
					" LEFT JOIN regions ON properties.region_id = regions.region_id".
					" WHERE properties.brand_id = $brands_list->brand_id".
					" AND properties.status = 1".
					" AND properties.deleted = 0".
					" ORDER BY region_id ASC";
				$pquery = $this->db->query($psql);
				$count_properties = $pquery->num_rows();
				if($count_properties > 0)
				   $properties_list = $pquery->result(); 
	   			$current_region = "none";
		        
	   			if ($count_properties>0){
			        foreach ($properties_list as $properties_list){ 
			            // First property
			            if ($current_region == "none"){
			            	echo "<div class='span3' style='border:0px solid #F00;margin-bottom: 5px;' id='prop$pctr'>\n";
			            	$current_region = $properties_list->region_id;   
			            	echo "<strong>$properties_list->region</strong>";
			            	echo " <input type='checkbox' class='check_all_properties brand$bctr' style='margin-bottom: 5px;'>\n";
			            	$thecheck_classname = "prop$pctr";
			            }	        	
			            
			            // The rest of the properties
			            if ($current_region != $properties_list->region_id){
			           		echo "</div>\n";
				        	echo "<div class='span3' style='border:0px solid #000;margin-bottom: 5px;' id='prop$pctr'>\n";
				        	$current_region = $properties_list->region_id;
				        	echo "<strong>$properties_list->region</strong>";
				        	echo " <input type='checkbox' class='check_all_properties brand$bctr' style='margin-bottom: 5px;'>\n";
				        	$thecheck_classname = "prop$pctr";
			           	}	

			            echo "<label class='checkbox'><input name='property_id[]' value='$properties_list->property_id' class='$thecheck_classname brand$bctr' type='checkbox'";
			            if ($action == 'add'){
			              echo set_checkbox('property_id[]', $properties_list->property_id);
			            }
			            elseif ($action == 'edit'){
			              $property_permissions = $this->common->get_app_property_permissions($properties_list->property_id,$user_id);	              
			              if ($property_permissions && $property_permissions->property_id == $properties_list->property_id)
			                echo ' checked';
			            }
			            
			            echo "> $properties_list->hotel_name</label>\n";
			            $pctr++;
		           	}

	           		echo "</div>\n";
	           	}
       		echo "</div>\n"; //END of echo brand_container
       		$bctr++;
       		}
        }
        
	}

	
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */