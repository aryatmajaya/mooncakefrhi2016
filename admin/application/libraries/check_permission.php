<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Check_permission {

	function Check_Button_Add($caption, $allowed, $url){
		if ($allowed == 1){
			echo '<a href="'.$url.'/add/" class="btn btn-success">
  <i class="icon-plus icon-white"></i>';
  		echo $caption;
  		
  		echo '</a>';
		}
		
	}
        
        function Check_Button_Add_v2($caption, $allowed, $add_url){
		if ($allowed == 1){
		echo ' <a href="'.$add_url.'" class="btn btn-success"><i class="icon-plus icon-white"></i>'.$caption.'</a> ';
		}
	}

	function Check_Button_Edit($caption, $allowed, $edit_id, $url){
		if ($allowed == 1){
		echo ' <a href="'.$url.'/edit/'.$edit_id.'" class="btn btn-mini"><i class="icon-pencil"></i>'.$caption.'</a> ';
		}
	}

	function Check_Button_Edit_v2($caption, $allowed, $edit_url){
		if ($allowed == 1){
		echo ' <a href="'.$edit_url.'" class="btn btn-mini"><i class="icon-pencil"></i>'.$caption.'</a> ';
		}
	}

	function Check_Button_Delete($caption, $allowed, $edit_id){
		if ($allowed == 1){
		// Sorry but id value should only have single instance in an html page. I need to add class.
		echo ' <button id="delete-button" class="btn btn-mini btn-danger delete-button" type="button" value="'.$edit_id.'"><i class="icon-remove icon-white"></i>'.$caption.'</button> ';
		}
	}

	function Check_Button_Manage($caption, $allowed, $edit_id){
		if ($allowed == 1){
		echo ' <a href="manage/'.$edit_id.'" class="btn btn-mini"><i class="icon-list-alt"></i>'.$caption.'</a> ';
		}
	}
	
}

/* End of file Someclass.php */