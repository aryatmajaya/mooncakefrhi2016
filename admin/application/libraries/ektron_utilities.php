<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Ektron_Utilities {

	

	function insert_image_create_fields($mode, $proppho_value = NULL){
	
		echo "<input type=\"text\" placeholder=\"Photo Name\" maxlength=\"255\" class=\"span4\" name=\"field_name\" value=\"$proppho_value\" />\n";
		echo "<input type=\"hidden\" name=\"entry_type\" value=\"text_box\" />";
		echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	}

	function content_box_create_fields($mode, $empattrib_value = NULL, $empattrib_entrytype_value = NULL){
	
	echo "<input type=\"text\" placeholder=\"Question / Attribute Name\" maxlength=\"255\" class=\"span5\" name=\"field_name\" value=\"$empattrib_value\" />\n";
	echo "<select name=\"entry_type\">";
	echo "<option value=\"text_box\"";
	if ($empattrib_entrytype_value == "text_box")
		echo " selected";
	echo ">Text Box</option>";
	echo "<option value=\"yes_no\"";
	if ($empattrib_entrytype_value == "yes_no")
		echo " selected";
	echo ">Yes and No</option>";
	
	echo "</select> <button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	}
	
	function drop_list_create_fields($mode, $de_text = NULL, $de_text2 = NULL, $de_list = NULL){
	
		//span5 lra-left-top-input
		echo '<div>';
		echo '<div style="margin-bottom:10px;">';
		echo "<input type=\"text\" class=\"span9\" name=\"field_name\" value=\"$de_text\" />";
		echo '</div>';
		echo '<div style="float:left; padding-right: 50px;">';
		echo "<input type=\"text\" class=\"\" name=\"answer_full_text\" value=\"$de_text2\" />";
		echo '</div>';
		echo "<div style=\"float:left; padding-right: 10px;\"><textarea name=\"dropdown_list\" rows=\"5\" placeholder=\"Enter List here\">$de_list</textarea><br />";
		?>
		<a class="bs-tooltip lra-sample-format-link" href="#" data-toggle="tooltip" title="" data-original-title="Frustrated<br />Neglected<br />Hurried<br />Indifferent<br />Comfortable">Sample Format</a>
		
		<?
		echo "</div>";
		echo '<div style="float: left;">';
		echo "<input type=\"hidden\" name=\"entry_type\" value=\"text_box\" />";
		echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
		echo "</div>";
		echo "<div class\"lra-clear\">&nbsp;</div>";
		echo "</div>";
		echo "<div class=\"lra-clear\"></div>\n";
	}

	function gen_desc_create_fields($mode, $en_value = NULL, $en_entrytype_value = NULL){
		echo "<input type=\"text\" class=\"span7\" placeholder=\"\" name=\"field_name\" value=\"$en_value\" />\n";
		echo "<input type=\"hidden\" name=\"entry_type\" value=\"full_text\" />";
		echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	}

	function calendar_create_fields($mode, $io_value = NULL, $io_entrytype_value = NULL){
		echo "<input type=\"text\" placeholder=\"Field Name\" name=\"field_name\" value=\"$io_value\" />\n";
		echo "<select name=\"entry_type\">";
		echo "<option value=\"text_box\"";
		if ($io_entrytype_value == "text_box")
			echo " selected";
		echo ">Text Box</option>";
		echo "<option value=\"datepicker\"";
		if ($io_entrytype_value == "datepicker")
			echo " selected";
		echo ">Date Picker</option>";
		echo "</select> <button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	}

	function title_tags_create_fields($mode, $enhancer_value = NULL, $e_entrytype_value = NULL){
		echo "<input type=\"text\" placeholder=\"Enhancer Name\" name=\"field_name\" value=\"$enhancer_value\" />\n";
		echo "<select name=\"entry_type\">";
		echo "<option value=\"text_box\"";
		if ($e_entrytype_value == "text_box")
			echo " selected";
		echo ">Text Box</option>";
		echo "<option value=\"check_box\"";
		if ($e_entrytype_value == "check_box")
			echo " selected";
		echo ">Check Box</option>";
		echo "</select> <button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	}

	function yn_question_create_fields($mode, $guper_value = NULL, $guper_entrytype_value = NULL, $guper_list_value = NULL){
		echo '<div>';
		echo '<div style="float:left; padding-right: 10px;">';

		echo "<input type=\"text\" placeholder=\"Question\" maxlength=\"255\" style=\"width:350px;\" name=\"field_name\" value=\"$guper_value\" />\n";
		echo "<input type=\"hidden\" name=\"entry_type\" value=\"yes_no\" />\n";	
		echo '</div>';
		
		echo '<div style="float: left;">';
		echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
		echo "</div>";
		//echo "<div class=\"lra-clear\">&nbsp;</div>";
		echo "</div>";
	}

	function content_query_fields($mode, $employcon_value = NULL, $employcon_entrytype_value = NULL, $em_ctr = NULL){
		echo '<div>';
		if ($mode == 'add')
			echo '<div class="entry_type_0" style="float:left; padding-right: 10px;">';
		else
			echo '<div class="entry_type_'.$em_ctr.'" style="float:left; padding-right: 10px;">';
		
		
		if ($mode == 'add')
			echo "<input type=\"text\" placeholder=\"Caption\" maxlength=\"255\" style=\"width:300px;\" name=\"field_name\" value=\"$employcon_value\" />\n";
		else {
			if ($employcon_entrytype_value == "yes_no")
				echo "<input type=\"text\" placeholder=\"Caption\" maxlength=\"255\" style=\"width:300px;\" name=\"field_name\" value=\"$employcon_value\" />\n";
			else
				echo "<textarea name=\"dropdown_list\" style=\"width:300px;\" rows=\"4\">$employcon_value</textarea>";
	
		}
		
		echo '</div>';
		echo "<div style=\"float:left; padding-right: 10px;\">";
		//echo "<input type=\"hidden\" name=\"entry_type\" value=\"yes_no\" />";
		if ($mode == 'add')
			echo "<select name=\"entry_type\" class=\"entry_type\" id=\"entry_type_0\">";	
		else
			echo "<select name=\"entry_type\" class=\"entry_type\" id=\"entry_type_$em_ctr\">";	
		
		
		echo "<option value=\"yes_no\"";
		if ($employcon_entrytype_value == "yes_no")
			echo " selected";
		echo ">Yes/No Entry</option>";
		//header text
		echo "<option value=\"header_txt\"";
		
		if ($employcon_entrytype_value == "header_txt")
			echo " selected";
		echo ">Header Caption</option>";

		//footer text
		echo "<option value=\"footer_txt\"";
		if ($employcon_entrytype_value == "footer_txt")
			echo " selected";
		echo ">Footer Text</option>";
		echo "</select> ";
		echo "</div>";
		echo '<div>';
		echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
		echo "</div>";
	
		echo "</div>";
		//echo "<div class\"lra-clear\">&nbsp;</div>";
	}
	
	
}

/* End of file ektron_utilities.php */