<?php

require_once("html2pdf.class.php");

class Pdf extends HTML2PDF{
	function __construct($data=array()){
		extract($data);
		
		if(!isset($orientation)) $orientation = 'P';
		if(!isset($format)) $format = 'A4';
		if(!isset($langue)) $langue = 'fr';
		if(!isset($unicode)) $unicode = true;
		if(!isset($encoding)) $encoding = 'UTF-8';
		if(!isset($marges)) $marges = array(5, 5, 5, 8);
		
		parent::__construct($orientation,$format,$langue,$unicode,$encoding,$marges);
	}
}