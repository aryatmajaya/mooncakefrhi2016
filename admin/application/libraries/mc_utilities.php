<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Mc_utilities {
    
    function Discounted_Price($total_amount,$discount){
        return $total_amount - ($total_amount * ($discount/100));
    }

	
    public function order_status($array_id = NULL){
        $order_status = array(
            //1 => "open",
            2 => "collected",
            3 => "delivered",
            4 => "cancelled",
            //5 => "credit-card-problem",
            6 => "incomplete",
            7 => "error",
            8 => "in-process", //used to be paid-open
            9 => "completed", //used to be process
            10 => "offline-incomplete"
            );
        if ($array_id != "")
            return $order_status[$array_id];
        else
            return $order_status;
		
    }
    
    function order_status_id($the_string = NULL){
            $order_status = array(
                //1 => "open",
                2 => "collected",
                3 => "delivered",
                4 => "cancelled",
                //5 => "credit-card-problem",
                6 => "incomplete",
                7 => "error",
                8 => "in-process",
                9 => "completed",
                10 => "offline-incomplete"
                );            
            return array_keys($order_status,$the_string);
    }

    function Credit_Card_Type($the_code){
        $card_types = array(                
            2 => "Master Credit Card",
            3 => "VISA Credit Card",
            5 => "Amex Credit Card",
            22 => "Diners Credit Card",
            23 => "JCB Credit Card",
            25 => "China UnionPay Card",
            41 => "ENets",
            );
        if ($the_code != 0)    
            return $card_types[$the_code];
        else
            return false;
            
    }
	
    /*function Calculate_Delivery_Charges($delivery_items, $total_qty){
        $delivery_charge = 50;
        if ($delivery_items){
            $dctr = 1;
            $total_delivery_charge = 0;
            foreach($delivery_items as $delivery_charges){
                if ($total_qty < 50){
                    $total_delivery_charge = $total_delivery_charge + $delivery_charge;
                }
                elseif ($total_qty >= 50  && $dctr == 2){
                    //echo 'test';
                    $total_delivery_charge = $delivery_charge;
                }
                $dctr++;
            }
            return $total_delivery_charge;
        }
    }*/
	
	function Calculate_Delivery_Charges($cart_total, $service_type) {
        $site_config = get_instance()->config->item('site');
        $charges = 0;

        if($service_type=="delivery"){
			if ($cart_total < $site_config['minimum_delivery_quantity']) {
				$charges = $site_config['delivery_charge_lower'];
			} else {
				$charges = $site_config['delivery_charge_upper'];
			}
			return $charges;
		}else{
			return 0;	
		}
    }
    
    function PayUsing($the_code) {
        if ($the_code == "uob")
            return 'UOB Visa & Master Credit Cards Only';
        elseif ($the_code == "nondbs")
            return 'All Other Credit Cards';
    }

    function get_order_status_class($status_id){
        /**
         * //1 => "open",
         *   2 => "collected",
         *   3 => "delivered",
         *   4 => "cancelled",
         *   //5 => "credit-card-problem",
         *   6 => "incomplete",
         *   7 => "error",
         **   8 => "paid-open",
         *   9 => "process",
         *   10 => "offline-incomplete"
         */
        $class='';
        switch($status_id){
            
            case 2 : 
            case 3 : 
            case 8 : $class="success"; break;
            case 4 : 
            case 5 :
            case 7 : $class="important"; break;
            case 6 : $class="warning"; break;
            case 9 : $class="info"; break;
            case 10 : $class="inverse"; break;
            default: $class = "";
        }

        return $class;
    }
    
    
    function Send_Confirmation($order_ref, $priority_email = NULL, $method = 'email'){
        /*
         * Note: If $priority_email is not null the the confirmation will be sent
         * to the $priority_email and not to the customer. (This is usually use by CMS admin)
         * 
         * This is parallel version to the fron-end's Utilities::Send_Confirmation.
         * This is to avoid auto logout, that's why where replicating it.
         * So if there's any modifications on the front-end then this should be edited also and it's dependencies.
         */
        
        //GETTING ORDER DATA
        $this->load->model('front_orders_model');
            
        $data['order_ref'] = $order_ref;
        $data['order'] = front_orders_model::order_summary($order_ref);
        
        
        if ($data['order']){
            $data['items'] = front_orders_model::get_ordered_items($data['order']->id);
            
            $this->load->model('front_delivery_model');
            $data['delivery_charges'] = front_delivery_model::get_total_delivery_qty($order_ref);
            $data['fo'] = front_delivery_model::get_delivery_option3($order_ref);

            //NOW SENDING EMAIL
            

            $the_message = $this->load->view('common/confirmation_email', $data, true);
            //var_dump($the_message); die();
            
            if (ENVIRONMENT != 'development'){
                $subject = 'Order Confirmation #' . $order_ref . ' - Mooncakes from Fairmont Singapore & Swissotel The Stamford';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                // Additional headers
                $headers .= 'From: Premium Mooncake Selection <'.ADMIN_EMAIL.'>' . "\r\n";
                $headers .= 'Reply-To: ' . ADMIN_EMAIL . "\r\n";
                $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
                $headers .= "Bcc: james@cprvision.com, fairmontsingapore.mooncakes@fairmont.com" . "\r\n";
            
                if (!empty($priority_email)) {
                    mail($priority_email, $subject, $the_message, $headers);
                } else {
                    
                    if ($method == 'email') {
                    
                        mail($data['order']->email, $subject, $the_message, $headers);                    
                        //LOGGING THE MAIL NOTIFICATION
                        common::mail_notification_log($order_ref, $data['order']->email);

                        return TRUE;
                    } else {
                        $ndata['email'] = $data['order']->email;
                        $ndata['header'] = $headers;
                        $ndata['subject'] = $subject;
                        $ndata['message'] = $the_message;
                        return $ndata;
                    }
                }
            }else{ // Production
                //mail($data['order']->email, $subject, $the_message, $headers);
            }
        }
    }
	

	
	
	
}

/* End of file mc_utilities.php */