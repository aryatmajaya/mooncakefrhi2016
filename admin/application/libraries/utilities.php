<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Utilities {

	function BreadCrumbs($the_links){
		echo "<ul class=\"breadcrumb\">\n";
		  foreach ($the_links as $bc){
		    $bc_title = explode("|",$bc);
		    echo "<li class=\"active\">";
		    if ($bc_title[1] != "")
		    	echo "<a href=\"$bc_title[1]\">";
		    echo $bc_title[0];
		    if ($bc_title[1] != "")
		    	echo "</a>";
		    echo "<span class='divider'> /</span></li>\n";
		    
		  }
		echo "</ul>\n";	
		
	}

	function proppho_create_fields($mode, $proppho_value = NULL){
	
		echo "<input type=\"text\" placeholder=\"Photo Name\" maxlength=\"255\" class=\"span4\" name=\"field_name\" value=\"$proppho_value\" />\n";
		echo "<input type=\"hidden\" name=\"entry_type\" value=\"text_box\" />";
		echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	}
	
	
	function Calculate_Delivery_Charges($cart_total, $service_type) {
        $site_config = get_instance()->config->item('site');
        $charges = 0;
        
        if($service_type=="delivery"){
			if ($cart_total < $site_config['minimum_delivery_quantity']) {
				$charges = $site_config['delivery_charge_lower'];
			} else {
				$charges = $site_config['delivery_charge_upper'];
			}
			return $charges;
		}else{
			return 0;	
		}
    }
	
	
}

/* End of file Utilities.php */