<?php
class Audits_model extends CI_Model {
	
	function get_all_templates(){
		$sql = 	"SELECT * from audit_templates".
				" WHERE deleted = 0".
				" ORDER BY date_created ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
	
	function get_applications_list(){
		$sql = 	"SELECT applications.*, application_groups.group_name from applications".
				" LEFT JOIN application_groups ON applications.app_group = application_groups.id".
				" WHERE applications.status = 1".
				" ORDER BY applications.app_group,applications.position ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_app_group_permissions($app_id,$group_id){
		//This is to get the app permission of a specific app_id and group_id
		$sql = 	"SELECT applications.id, applications.app_name, user_group_app_permissions.id, user_group_app_permissions.app_id, user_group_app_permissions.group_id, user_group_app_permissions.add_role, user_group_app_permissions.view_role, user_group_app_permissions.edit_role, user_group_app_permissions.delete_role".
				" FROM applications".
				" LEFT JOIN user_group_app_permissions ON applications.id = user_group_app_permissions.app_id".
				" WHERE applications.status = 1".
				" AND user_group_app_permissions.app_id = ?".
				" AND user_group_app_permissions.group_id = ?".
				" ORDER BY applications.app_name ASC";
		$query = $this->db->query($sql, array($app_id,$group_id));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
	}

	function get_template_by_id($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT * FROM audit_templates".
					" WHERE id=?";
			$query = $this->db->query($sql, array($the_id));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}

	function Get_All_Templates_for_Auditors(){
		/* 
		currently the templates are viewalbe to all auditors
		so this has to change, the templates should appear according to assigned auditor in the basis
		of property permission
		*/

		$sql = 	"SELECT * from audit_templates".
				" WHERE deleted = 0".
				" ORDER BY date_created ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	// FOR APPLICATION GROUPS
	function get_appgroup_by_id($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT * FROM application_groups".
					" WHERE id=?";
			$query = $this->db->query($sql, array($the_id));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}
	function get_all_application_groups(){
		$sql = 	"SELECT * from application_groups".
				" WHERE deleted = 0".
				" ORDER BY position ASC";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}


	function get_application_groups_list(){
		$sql = 	"SELECT id as list_id, group_name as list_name from application_groups".
				" WHERE status = 1".
				" AND deleted = 0".
				" ORDER BY position ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function Get_All_Touchpoints_of_a_Template($template_id){
		$sql = 	"SELECT audit_template_touchpoints.id,audit_template_touchpoints.template_id,audit_template_touchpoints.touch_point_id,audit_template_touchpoints.touch_point_name,touch_points.touch_point as tp_name".
				" from audit_template_touchpoints".
				" LEFT JOIN touch_points ON audit_template_touchpoints.touch_point_id = touch_points.id".
				" WHERE template_id = ?".
				" AND audit_template_touchpoints.status = 1".
				" AND audit_template_touchpoints.deleted = 0".
				" ORDER BY audit_template_touchpoints.date_created ASC";
		$query = $this->db->query($sql, array($template_id));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function Get_All_Touchpoints_of_a_Template_in_manage_view($template_id){
		$sql = 	"SELECT audit_template_touchpoints.id,audit_template_touchpoints.template_id,audit_template_touchpoints.touch_point_id,audit_template_touchpoints.touch_point_name as tp_name,audit_touchpoint_elements.caption, count(audit_touchpoint_elements.id) as total_elements".
				" from audit_template_touchpoints".
				" LEFT JOIN audit_touchpoint_elements ON audit_template_touchpoints.id = audit_touchpoint_elements.touchpoint_id".
				" WHERE audit_template_touchpoints.template_id = ?".
				" AND audit_template_touchpoints.status = 1".
				" AND audit_template_touchpoints.deleted = 0".
				//" GROUP BY audit_template_touchpoints.touch_point_id".
				" GROUP BY audit_template_touchpoints.id".
				" ORDER BY audit_template_touchpoints.date_created ASC";
		$query = $this->db->query($sql, array($template_id));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function Get_Touchpoint_of_a_Template_by_id($touchpoint_id){
		$sql = 	"SELECT audit_template_touchpoints.id,audit_template_touchpoints.template_id,audit_template_touchpoints.touch_point_id,audit_template_touchpoints.touch_point_name, audit_templates.template_name".
				" from audit_template_touchpoints".
				" LEFT JOIN audit_templates ON audit_template_touchpoints.template_id = audit_templates.id".
				" WHERE audit_template_touchpoints.id = ?".
				" AND audit_template_touchpoints.status = 1".
				" AND audit_template_touchpoints.deleted = 0";
				
		$query = $this->db->query($sql, array($touchpoint_id));

		if($query->num_rows() > 0){
		   return $query->row(); 
		}

	}

	function Get_All_Elements_from_a_Touchpoint($template_id, $touchpoint_id){
		$sql = 	"SELECT audit_touchpoint_elements.*".
				" from audit_touchpoint_elements".
				" WHERE audit_touchpoint_elements.template_id = ?".
				" AND audit_touchpoint_elements.touchpoint_id = ?".
				" AND audit_touchpoint_elements.status = 1".
				" AND audit_touchpoint_elements.deleted = 0".
				" ORDER BY position, id ASC";
				
				
		$query = $this->db->query($sql, array($template_id, $touchpoint_id));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}

	}

	function Get_Entries_of_an_Element($element_id){
		$sql = 	"SELECT *".
				" from audit_touchpoint_entries".
				" WHERE element_id = ?".
				" AND deleted = 0".
				" ORDER BY position, id ASC";
				
		$query = $this->db->query($sql, array($element_id));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}

	}

	function Check_If_Element_Is_Added($touchpoint_id, $element_type){
		/* 
		this is to check if the an element is already added in a specific touchpoint
		*/
		$sql = 	"SELECT id, touchpoint_id, element_type".
				" from audit_touchpoint_elements".
				" WHERE touchpoint_id = ?".
				" AND element_type = ?".
				" AND deleted = 0";
				
		$query = $this->db->query($sql, array($touchpoint_id, $element_type));

		if($query->num_rows() > 0){
		   return TRUE;
		}
		else
			return FALSE;



	}


		
}
?>