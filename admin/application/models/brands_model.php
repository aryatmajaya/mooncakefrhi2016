<?php
class Brands_model extends CI_Model {
	
	
	
	

	function get_all_brands()
	{
		$sql = 	"SELECT * from brands".
				
				" WHERE deleted = 0".
				" ORDER BY brand_name ASC";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_record($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT brand_id, brand_name, status FROM brands".
					" WHERE brand_id=?";
			$query = $this->db->query($sql, array($the_id));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}

	function get_brands_list()
	{
		$sql = 	"SELECT brand_id, brand_name from brands".
				" WHERE brands.status = 1".
				" AND brands.deleted = 0".
				" ORDER BY brand_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
		
}
?>