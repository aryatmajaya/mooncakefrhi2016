<?php
class Categories_model extends CI_Model {
	
	
	
	

	function get_all_categories()
	{
		$sql = 	"SELECT * from categories".
				" WHERE deleted = 0".
				" ORDER BY category_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_categories_list()
	{
		$sql = 	"SELECT id as list_id, category_name as list_name from categories".
				" WHERE status = 1".
				" AND deleted = 0".
				" ORDER BY category_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
/*
	function get_record($the_id){
		$sql = 	"SELECT region_id, region, status FROM regions".
				" WHERE region_id=$the_id";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->row(); 
		}
	}*/
		
}
?>