<?php
class Corporate_orders_model extends CI_Model {
    
    function Get_Customers($type='offline') {
        $this->db->select('c.*')
                ->from('mc_customers as c')
                ->where('password', '*'.$type.'-order-only*')
                ->order_by('last_name, company_name', 'asc');
        $query = $this->db->get();
        
        if($query->num_rows() > 0)
            return $query->result(); 
        else
            return false;
    }
    
    function Order_Details($transaction_id) {
        $this->db->select('o.id as order_id, o.customer_id, o.order_ref, 
                            o.corporate_discount, o.discounted_amount, o.total_amount,
                            oc.notes1, oc.notes2, c.first_name, oc.sales_manager, oc.order_section, oc.hot_stamping, oc.logo_id,
                            oc.customization, oc.payment_mode,
                            c.last_name, c.company_name, c.address, c.mobile')
                ->from('mc_orders as o')
                ->join('mc_customers as c', 'c.id = o.customer_id', 'left')
                ->join('mc_orders_corporate as oc', 'oc.order_id = o.id', 'left')
                ->where('o.id', $transaction_id);
        $query = $this->db->get();
        
        if($query->num_rows() > 0)
            return $query->row(); 
        else
            return false;
    }
    
    function LoadOrderItem($order_id, $product_id) {
        $this->db->select('item.*')
                ->from('mc_orders_items as item')
                ->where('item.order_id', $order_id)
                ->where('item.product_id', $product_id);
        $query = $this->db->get();
        
        if($query->num_rows() > 0)
            return $query->row(); 
        else
            return false;
    }
    
    function Get_OrderDetails_by_TransactionID($transaction_id) {
        $this->db->select('o.order_ref, 
                            o.corporate_discount, 
                            o.card_discount, 
                            o.promo_code_discount, 
                            o.date_ordered, 
                            o.approval_code, 
                            o.ordering_method,
                            o.delivery_charge,
							oc.special_requirements,
                            c.first_name, 
                            c.last_name, 
                            c.email, 
                            c.mobile, 
                            c.alt_contact,
                            c.company_name,
                            c.address,
                            c.postal_code');
        $this->db->from('mc_orders o');
		$this->db->join('mc_orders_corporate oc','oc.order_id = o.id', 'inner');
        $this->db->join('mc_delivery d','d.order_ref = o.order_ref', 'left');
        $this->db->join('mc_customers c','c.id = o.customer_id', 'left');
        
        $this->db->where('o.id', $transaction_id);

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->row(); 
        } else
            return FALSE;
    }
    
    function Get_OrderItems_by_OrderID($order_id) {
        $this->db->select('oi.product_id, oi.product_name, oi.qty, oi.price, p.pc_per_box');
        $this->db->from('mc_orders_items as oi');
		$this->db->join('mc_products p', 'oi.product_id = p.id', 'inner');
        $this->db->where('oi.order_id', $order_id);

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->result(); 
        } else
            return FALSE;
        
    }
    
    
    
    function Check_Delivery_Charge($order_id, $order_ref, $service_type, $custom_charge) {
        if ($service_type == "self_collection"):
            $delivery_charge = 0;            
        elseif ($service_type == "delivery"):
            
            if ($custom_charge > 0):
                $delivery_charge = $custom_charge;
            else:
                $total_qty = Get_Total_Qty($order_id);
                if ($total_qty < 50)
                    $delivery_charge = 50;
                else
                    $delivery_charge = 0;
            endif;
        endif;
        
        return $delivery_charge;
        
    }
    
    function Get_Total_Qty($order_id) {
        $this->db->select('SUM(qty) as total_qty');
        $this->db->from('mc_orders_items');
        $this->db->where('mc_orders_items.order_id', $order_id);

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->row()->total_qty;
        } else
            return 0;
    }
    
    function Get_SalesManagers()
    {
        $this->db->select('sales_manager')
                ->from('mc_orders_corporate')
                ->order_by('sales_manager', 'ASC')
                ->group_by('sales_manager');
        
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        } else
            return FALSE;
    }
    
}
?>