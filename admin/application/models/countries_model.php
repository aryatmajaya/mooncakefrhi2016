<?php
class Countries_model extends CI_Model {
	
	
	
	

	function get_all_countries()
	{
		$sql = 	"SELECT countries.country_id, countries.country, countries.region_id, countries.status, regions.region from countries".
				" LEFT JOIN regions ON countries.region_id = regions.region_id".
				" WHERE countries.deleted = 0".
				" ORDER BY regions.region, countries.country ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_record($the_id){
		if (is_numeric($the_id)){
			$sql = 	"SELECT country_id, region_id, country, status FROM countries".
					" WHERE country_id=$the_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}

	function get_countries_list()
	{
		$sql = 	"SELECT country_id, country from countries".
				" LEFT JOIN regions ON countries.region_id = regions.region_id".
				" WHERE regions.status = 1".
				" AND countries.status = 1".
				" AND countries.deleted = 0".
				" ORDER BY country ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_countries_list_by_region_id($the_region_id){
		$sql = 	"SELECT country_id, country from countries".				
				" WHERE region_id = $the_region_id".				
				" AND status = 1".
				" AND deleted = 0".
				" ORDER BY country ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
		
}
?>