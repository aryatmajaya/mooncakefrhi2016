<?php
class Discount_module_model extends CI_Model {

	function get_all(){
		$this->db->order_by("id","DESC");
		$query = $this->db->get("discount_module");
		return $query->result();
	}


	function get($id){
		$query = $this->db->get_where("discount_module", array('id'=>$id));
		return $query->row();
	}
	
	function setAllDefaultZero(){
		$this->db->update("discount_module",array("default"=>0));	
	}
	
	function update($id){
		$data = array(
		   'mid' => $this->input->post("mid"),
		   'discount' => $this->input->post("discount"),
		   'sort_order' => $this->input->post("sort_order"),
		   'status' => $this->input->post("status"),
		);
		
		if($this->input->post("default")){
			$this->setAllDefaultZero();
			$data['default']=$this->input->post("default");
		}
		
		$this->db->where('id', $id);
		$this->db->update('discount_module', $data); 	
	}
}
