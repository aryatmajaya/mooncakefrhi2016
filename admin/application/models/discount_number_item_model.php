<?php
class Discount_number_item_model extends CI_Model {

	function get_all(){
		$this->db->order_by("discount_id","DESC");
		$this->db->where("status",1);
		$query = $this->db->get("discount_number_item");
		
		return $query->result();
	}


	function get($id){
		$query = $this->db->get_where("discount_number_item", array('discount_id'=>$id));
		return $query->row();
	}
	
	function insert(){
		$data = array(
		   'start_date' => date('Y-m-d',strtotime($this->input->post("start_date"))),
		   'end_date' => date('Y-m-d',strtotime($this->input->post("end_date"))),
		   'total_item' => $this->input->post("total_item"),
		   'discount' => $this->input->post("discount"),
		   'status' => 1,
		   'created_by' => $this->session->userdata('user_id')
		);
		
		$this->db->insert('discount_number_item', $data);
		$last_id = $this->db->insert_id();
	}
	
	function update($id){
		$data = array(
		   'start_date' => date('Y-m-d',strtotime($this->input->post("start_date"))),
		   'end_date' => date('Y-m-d',strtotime($this->input->post("end_date"))),
		   'total_item' => $this->input->post("total_item"),
		   'discount' => $this->input->post("discount"),
		   'status' => 1
		);
		
		$this->db->where('discount_id', $id);
		$this->db->update('discount_number_item', $data); 	
	}
}
