<?php
class Discount_number_item_products_model extends CI_Model {
	
	function update(){
		$this->db->empty_table('discount_number_item_products');
		
		if($this->input->post("products")){
			foreach($this->input->post("products") as $product){
				$this->db->insert("discount_number_item_products",array("product_id"=>$product));	
			}
		}
	}
	
	function getProductsChecked(){
		$query = $this->db->get("discount_number_item_products");
		
		$datas=$query->result();
		$returns=array();
		foreach($datas as $data){
			$returns[] = $data->product_id;	
		}
		
		return $returns;
	}
	
}
