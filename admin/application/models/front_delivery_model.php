<?php
class Front_delivery_model extends CI_Model {
	
    function add_delivery($insert_data){
        
        $this->db->set($insert_data);
        $this->db->insert('mc_delivery');
    }

    
    
    function add_delivery_item($insert_item_data){
        $this->db->set($insert_item_data);
        $this->db->insert('mc_delivery_items');
    }
    
    function get_total_delivered_product_qty($order_ref, $product_id){
        /* 
         * This will get the total of product qty from the
         * delivery. Once we have the total, it will be use
         * to get to compute (remaining qty = current qty - total delivered qty)
         */
        
    	$sql = 	"SELECT mc_delivery.id,SUM(mc_delivery_items.qty) as totalqty ".
                        " FROM mc_delivery".
                        " LEFT JOIN mc_delivery_items ON mc_delivery.id = mc_delivery_items.delivery_id".
    		" WHERE mc_delivery.order_ref = ? AND mc_delivery_items.product_id = ?";
                        " GROUP BY mc_delivery_items.product_id";
    		
    	$query = $this->db->query($sql, array($order_ref, $product_id));

    	if($query->num_rows() > 0){
    	   return $query->row(); 
    	}
	}
    
    function get_delivery_option($order_ref){
        
        $this->db->select('*');
        $this->db->from('mc_delivery');            
        $this->db->where('order_ref',$order_ref);
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
	}
        
    function get_delivery_option2($order_ref){
        
        $this->db->select('*');
        $this->db->from('mc_delivery');            
        $this->db->where('order_ref',$order_ref);
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
    
    function get_delivery_option3($order_ref)
    {
        /*This version is to avoid duplicate displays of fulfillment options.
         * 
         */
        
        $this->db->select('*');
        $this->db->from('mc_delivery');            
        $this->db->where('order_ref',$order_ref);
        $this->db->order_by("id", "asc");
        $this->db->group_by("order_ref");
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
        
    function get_delivery_option_items($delivery_id){            
        $this->db->select('*');
        $this->db->from('mc_delivery_items');            
        $this->db->where('delivery_id', $delivery_id);
        $this->db->order_by("product_id", "asc");
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
    
    function get_delivery_option_items2($order_ref){            
        $this->db->select('*');
        $this->db->from('mc_delivery_items');            
        $this->db->where('order_ref', $order_ref);
        $this->db->order_by("product_id", "asc");
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
        
    function get_total_self_colletion($order_ref){
        $this->db->select('service_type');
        $this->db->from('mc_delivery');            
        $this->db->where('order_ref',$order_ref);
        $this->db->where('service_type','self collection');
        return $this->db->count_all_results();
	}
        
    function get_total_delivery($order_ref){            
        $this->db->select('service_type');
        $this->db->from('mc_delivery');            
        $this->db->where('order_ref',$order_ref);
        $this->db->where('service_type','delivery');
        return $this->db->count_all_results();            
	}
        
    function delete_delivery($delivery_id, $order_ref){
        if (isset($delivery_id) && isset($order_ref)){
            
            $this->db->where('id', $delivery_id);
            $this->db->where('order_ref', $order_ref);
            $this->db->delete('mc_delivery');
            
            $this->db->where('delivery_id', $delivery_id);
            $this->db->where('order_ref', $order_ref);
            $this->db->delete('mc_delivery_items');  
        }
    }
        
    function cleanUp_delivery($order_ref){
        /* Note: this is used to delete or remove previous delivery and 
         * delivery items when User tries to go back to cart and do checkout
         * again.
         */
        if (isset($order_ref)){                
            
            $this->db->where('order_ref', $order_ref);
            $this->db->delete('mc_delivery');                
            
            $this->db->where('order_ref', $order_ref);
            $this->db->delete('mc_delivery_items');                
        }
        
    }
        
    function get_total_delivery_qty_old($order_ref){
        /*
         * This is to tell the if the total qty is already exceeded
         * to 50 or more.
         */
        $this->db->select('mc_delivery.service_type,mc_delivery.service_date, SUM(mc_delivery_items.qty) AS dctotal_qty');
        $this->db->from('mc_delivery');                        
        $this->db->join('mc_delivery_items', 'mc_delivery_items.delivery_id = mc_delivery.id', 'left');
        $this->db->where('mc_delivery.order_ref', $order_ref);
        $this->db->where('mc_delivery.service_type', 'delivery');
        $this->db->group_by('mc_delivery_items.delivery_id');
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }

    function Get_Total_Delivery_Qty($order_ref){
        /*
         * This is to tell the if the total qty is already exceeded
         * to 50 or more.
         */
        $this->db->select('mc_delivery.service_type,mc_delivery.service_date, SUM(mc_delivery_items.qty) AS dctotal_qty');
        $this->db->from('mc_delivery');                        
        $this->db->join('mc_delivery_items', 'mc_delivery_items.delivery_id = mc_delivery.id', 'left');
        $this->db->where('mc_delivery.order_ref', $order_ref);
        $this->db->where('mc_delivery.service_type', 'delivery');
        $this->db->group_by('mc_delivery.order_ref');
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
        
    function get_all_block_dates($block_type)
	{
		$sql = 	"SELECT block_date from mc_block_dates".
                        " WHERE block_type=?".
			" ORDER BY block_date ASC";

		$query = $this->db->query($sql, array($block_type));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
	
    function get_total_delivery_charge($order_ref){
        $sql =  "SELECT a.*, b.qty FROM mc_delivery a 
                LEFT JOIN mc_delivery_items b ON b.delivery_id = a.id
                WHERE a.service_type = 'delivery' AND a.order_ref=? group by a.order_ref";

        $query = $this->db->query($sql, array($order_ref));
        $delivery_charge = 0;
        if($query->num_rows() > 0){
            $result = $query->result();
            
           
            $complimentary_location = 1;
            foreach ($result as $key => $value) {
                $delivery_charge += DELIVERY_CHARGE;
                if($complimentary_location == 1 AND $value->qty >= DELIVERY_COMPLIMENTARY_QTY){
                    $delivery_charge -= DELIVERY_CHARGE;
                }
                  
                 
            }
        }

        return $delivery_charge;

    }
		
    function get_order_items($order_id)
    {
        $sql =  "SELECT *".
                        " FROM mc_orders_items".
                        " WHERE order_id=$order_id";
                
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
		
}
?>