<?php
class Mc_ccard_promo_model extends CI_Model {
	private $table = 'mc_ccard_promo';

	function get_all(){
		$sql = "SELECT a.*, b.name card_name, b.id card_id FROM " . $this->table . " a 
				LEFT JOIN mc_credit_cards b ON b.id = a.credit_card_id
				WHERE a.status=1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0){
			return $query->result();
		}

		return false;
	}


	function get($id){
		$query = $this->db->get_where($this->table, array('status'=>1, 'id'=>$id));
		
		if($query->num_rows() > 0){
			return $query->row(0);
		}

		return false;
	}
}
