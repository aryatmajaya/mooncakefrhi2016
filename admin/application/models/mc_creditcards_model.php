<?php
class Mc_creditcards_model extends CI_Model {
	private $table = 'mc_credit_cards';

	function get_all(){
		$query = $this->db->get_where($this->table);
		
		if($query->num_rows() > 0){
			return $query->result();
		}

		return false;
	}


	function get($id){
		$query = $this->db->get_where($this->table, array('active'=>1, 'id'=>$id));
		
		if($query->num_rows() > 0){
			return $query->row(0);
		}

		return false;
	}
}
