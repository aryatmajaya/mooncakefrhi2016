<?php
class Mc_customers_model extends CI_Model {
	
	

    function Get_All_Customers($search_field = NULL, $search = NULL)
    {
            $sql = 	"SELECT *".
                    " FROM mc_customers".
                    " WHERE deleted = 0 and status=1";

            if($search_field)
                $sql .= " AND $search_field LIKE '%$search%'";                    

            $sql .=  " ORDER BY last_name ASC";
            
            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
               return $query->result(); 
            }
    }

    function Get_Customer_Details($the_id){
            if (is_numeric($the_id)){
                $sql = 	"SELECT *".
                        " FROM mc_customers".
                        " WHERE mc_customers.id=$the_id";
                $query = $this->db->query($sql);

                if($query->num_rows() > 0){
                   return $query->row(); 
                }
                else
                    return false;
            }
            else
                return false;
    }





    function Update_Customer($field_id, $customer_data)
    {
        $this->db->where('id', $field_id);
        $this->db->update('mc_customers', $customer_data);
    }
		
}
?>