<?php
class Mc_paymentgateways_model extends CI_Model {
	private $table = 'mc_payment_gateways';

	function get_all(){
		$this->db->select('id');
		$this->db->select('name');
		$query = $this->db->get_where($this->table, array('status'=>1));
		
		if($query->num_rows() > 0){
			return $query->result();
		}

		return false;
	}


	function get($id){
		$query = $this->db->get_where($this->table, array('status'=>1, 'id'=>$id));
		
		if($query->num_rows() > 0){
			return $query->row(0);
		}

		return false;
	}
}
