<?php
class Mc_product_categories_model extends CI_Model {

	function get_all(){
		$query = $this->db->query("SELECT * FROM mc_product_categories WHERE deleted = 0");

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_parent_categories() {
		$query = $this->db->query("SELECT * FROM mc_product_categories WHERE status = 1 and parent_category = 0");

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
}