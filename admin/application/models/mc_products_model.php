<?php
class Mc_products_model extends CI_Model {
	
	function getProductsOrder(){
		$this->db->select("p.id as product_id, p.product_name");
		$this->db->where("p.id in (select oi.product_id from mc_orders_items as oi group by oi.product_id)");
		$this->db->from("mc_products as p");
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_all_products()
	{
		$sql = 	"SELECT a.*, b.name category_name from mc_products a
			LEFT JOIN mc_product_categories b ON a.category_id = b.id
		 ORDER BY sort_order ASC";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_record($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT brand_id, brand_name, status FROM brands".
					" WHERE brand_id=?";
			$query = $this->db->query($sql, array($the_id));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}

	function get_product ($the_id) {
		if (is_numeric($the_id)){

			$query = "SELECT a.* FROM mc_products a WHERE id=?";
			$result = $this->db->query($query, array($the_id));

			if($result->num_rows() > 0){
			   return $result->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}
		
}
?>