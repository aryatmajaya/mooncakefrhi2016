<?php
class Mc_promocode_model extends CI_Model {
	private static $table = 'mc_far_promocodes';

	function get_all(){
		$sql = "SELECT a.*, b.name as card_name, b.id as card_id
				FROM " . self::$table . " a
				LEFT JOIN mc_credit_cards b ON a.credit_card_id = b.id
				WHERE a.status=1";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			return $query->result();
		}

		return false;
	}

	function get($id){
		$query = $this->db->get_where(self::$table, array('status'=>1, 'id'=>$id));
		if($query->num_rows() > 0 ){
			return $query->row(0);
		}

		return false;
	}
}