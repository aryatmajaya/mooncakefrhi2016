<?php
class Mc_sales_report_model extends CI_Model {
    
    function GetProducts()
    {
        $this->db
                ->select('prod.id as prod_id, prod.product_name')
                ->from('mc_products as prod')
                ->where('status', 1)
                ;
        
        $query = $this->db->get();

        if($query->num_rows() > 0):
           return $query->result(); 
        else:
            return FALSE;
        endif;
    }
    
	
	function getDeliveries($start_date, $end_date, $status=false, $order_type=false, $service_type="self"){
		$this->db->select("
			d.id,
			d.service_date,
			oi.product_id,
			sum(oi.qty) as total
		");
		
		$this->db->from("mc_delivery as d");
		$this->db->join("mc_orders as o","d.order_ref = o.order_ref","left");
		$this->db->join("mc_orders_items as oi","o.id = oi.order_id","left");
		if($service_type=="self"){
			$this->db->where("(d.service_type = 'self_collection' or d.service_type = 'self collection')");
		}elseif($service_type=="delivery"){
			$this->db->where("(d.service_type = 'delivery')");
		}
		$this->db->where("d.service_date >=", $start_date);
		$this->db->where("d.service_date <=", $end_date);
		if($status){
			$this->db->where_in("o.status",$status);
		}
		if($order_type){
			$this->db->where_in("o.ordering_method", $order_type);
		}
		$this->db->group_by("oi.product_id, d.service_type");
		$this->db->order_by("d.service_date","desc");
		
		$query = $this->db->get();
	
		//echo $this->db->last_query();
		
		return $query->result();
	}
	
    function GetSalesReportPerDate($product_id, $what_date, $order_status, $order_type)
    {
        $this->db->select('SUM(items.qty) as total_qty, SUBSTRING(orders.date_ordered,12,3) as the_date,SUBSTRING(orders.date_ordered,1,13) as the_group', FALSE)
                ->from('mc_orders_items as items')
                ->join('mc_orders as orders', 'orders.id = items.order_id', 'left')
                ->where('items.product_id', $product_id)
                ->where('DATE(orders.date_ordered)', $what_date)
                ->where_in('orders.status', $order_status)
                ->where_in('orders.ordering_method', $order_type);
        //$date_ordered = substr(orders.date_ordered,0,13);
        $this->db->group_by('the_group');
        
        $query = $this->db->get();
        //echo $this->db->last_query();
        if($query->num_rows() > 0):
           return $query->result(); 
        else:
            return FALSE;
        endif;
    }
    
    function By_Product()
    {
        $this->db
                ->select('prod.product_name')
                ->from('mc_products as prod')
                ->join('mc_orders_items as item', 'item.product_id = prod.id', 'left')
                ->join('mc_orders as order','order.order_id = item.order_id', 'left')
                ->where('DATE(mc_orders.date_ordered) >=', $from_date)
                //->limit(20)
                ;
        
        
        //$from_date = date("Y-m-d",strtotime($from_date));
          //      $to_date = date("Y-m-d",strtotime($to_date));
                
            //$this->db->where('DATE(mc_orders.date_ordered) >=', $from_date);
            //$this->db->where('DATE(mc_orders.date_ordered) <=', $to_date);
        
        $query = $this->db->get();

        if($query->num_rows() > 0):
           return $query->result(); 
        else:
            return FALSE;
        endif;
    }
	
	/*function By_SalesManager($param = NULL)
    {
        $from_date = date("Y-m-d",strtotime($param['from_date']));
        $to_date = date("Y-m-d",strtotime($param['to_date']));
        
        $this->db->select('
				o.order_ref,
				o.corporate_discount,
				o.delivery_charge, o.status,
                customer.company_name, 
                delivery.service_date,
                corporate.sales_manager,
                (select sum(oi.qty) from mc_orders_items oi where oi.order_id=o.id) as numitems,
                (select sum(oi2.price) from mc_orders_items oi2 where oi2.order_id=o.id) as gross');
		
		$this->db->from('mc_orders o');
		$this->db->join('mc_orders_corporate corporate','corporate.order_id = o.id', 'left');
		$this->db->join('mc_customers customer','customer.id = o.customer_id', 'left');
		$this->db->join('mc_delivery delivery','delivery.order_ref = o.order_ref', 'left');
        $this->db->where('o.ordering_method', 'corporate');
        $this->db->where_in('o.status', $param['order_status']);
        $this->db->where('DATE(delivery.service_date) >=', $from_date);
        $this->db->where('DATE(delivery.service_date) <=', $to_date);
       
        if ($param['sales_manager'] != 'View All'){
            $this->db->where('corporate.sales_manager', $param['sales_manager']);
		}
            
        $this->db->order_by('corporate.sales_manager', 'asc');
        $this->db->order_by('delivery.service_date', 'asc');
        //$this->db->group_by('oitem.order_id');
       	
        $query = $this->db->get();
		
		echo $this->db->last_query();

        if($query->num_rows() > 0):
           return $query->result(); 
        else:
            return FALSE;
        endif;
    }*/
    
    function By_SalesManager($param = NULL)
    {
        $from_date = date("Y-m-d",strtotime($param['from_date']));
        $to_date = date("Y-m-d",strtotime($param['to_date']));
        
        $this->db
                ->select('o.order_ref, o.corporate_discount, o.delivery_charge, o.status,
                    customer.company_name, 
                    delivery.service_date,
                    corporate.sales_manager,
                    SUM(oitem.qty) as numitems,
                    SUM(oitem.qty * oitem.price) as gross
                    
                    ', FALSE)
                ->from('mc_orders as o')
                ->join('mc_orders_items as oitem', 'oitem.order_id = o.id', 'left')
                ->join('mc_customers as customer','customer.id = o.customer_id', 'left')
                ->join('mc_delivery as delivery','delivery.order_ref = o.order_ref', 'left')
                ->join('mc_orders_corporate as corporate','corporate.order_id = o.id', 'left')
                //->join('mc_delivery_items as items','items.order_ref = o.order_ref', 'left')
                ->where('o.ordering_method', 'corporate')
                ->where_in('o.status', $param['order_status'])
                
                ->where('DATE(delivery.service_date) >=', $from_date)
                ->where('DATE(delivery.service_date) <=', $to_date);
                //->limit(20)
        if ($param['sales_manager'] != 'View All')
            $this->db->where('corporate.sales_manager', $param['sales_manager']);
            
        $this->db->order_by('corporate.sales_manager', 'asc')
                ->order_by('delivery.service_date', 'asc')
                //->group_by('oitem.order_id')
                ->group_by('oitem.order_id')
                //->limit(100)
                ;
        
        
        //$from_date = date("Y-m-d",strtotime($from_date));
          //      $to_date = date("Y-m-d",strtotime($to_date));
                
            //$this->db->where('DATE(mc_orders.date_ordered) >=', $from_date);
            //$this->db->where('DATE(mc_orders.date_ordered) <=', $to_date);
        
        $query = $this->db->get();
		
		//echo $this->db->last_query();
		

        if($query->num_rows() > 0):
           return $query->result(); 
        else:
            return FALSE;
        endif;
    }
    
    function Get_SalesManagers()
    {
        $this->db->select('sales_manager')
                ->from('mc_orders_corporate')
                ->order_by('sales_manager', 'ASC')
                ->group_by('sales_manager');
        
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
           return $query->result(); 
        } else
            return FALSE;
    }
    

    function get_all_deliveries2($status, $from_date = NULL, $to_date = NULL, $service_type, $param = NULL) {
        /*
         * Created By James Castaneros - 22 July 2014
         * This version is designed for Celebration's requirement (single fullfilment option only)
         * so we need to group or just display a single info for collection/delivery and total amount is exactly the same with
         * transaction module (total, discount, delivery charge, gst)
         */
            if (!empty($param)) {
                /* Param Note:
                 * 0 - tool_type (dafault tool, sort order date)
                 * 1 - ordering method (online, corporate)
                 */
                $params = explode('_', $param);
                //echo $params[0]; die();
            }
            
            $sql =  "SELECT mc_delivery.id as delivery_id, mc_delivery.service_date, mc_delivery.address, 
                        mc_delivery.postal_code,mc_delivery.delivered_to, mc_delivery.delivered_contact, 
                        mc_orders.id,mc_orders.order_ref,mc_orders.status,mc_orders.date_ordered, mc_orders.total_amount, 
                        mc_orders.discounted_amount, mc_customers.email, mc_customers.first_name, mc_customers.last_name".
                        " FROM mc_delivery".
                        " LEFT JOIN mc_orders ON mc_orders.order_ref = mc_delivery.order_ref".
                        " LEFT JOIN mc_customers ON mc_customers.id = mc_orders.customer_id".
                        " WHERE mc_orders.status = $status"; //paid-open and offline incomplete
            //die($sql);  
            if (!empty($params) && $params[0] == "default-tool") {
                if (!empty($from_date))
                    $sql .= " AND (date(mc_delivery.service_date) >='".date("Y-m-d", strtotime($from_date))."' AND date(mc_delivery.service_date) <='".date("Y-m-d", strtotime($to_date))."' )";
            } else if (!empty($params) && $params[0] == "sort-date-ordered") {
                if (!empty($from_date))
                    $sql .= " AND (date(mc_orders.date_ordered) >='".date("Y-m-d", strtotime($from_date))."' AND date(mc_orders.date_ordered) <='".date("Y-m-d", strtotime($to_date))."' )";
            }
            
            if (!empty($params[1]) && $params[1] == "corporate") {
                $sql .= " AND mc_orders.ordering_method = 'corporate'";
            
            }
                    
            if ($service_type == "self-collection")    
                $sql .= " AND mc_delivery.service_type = 'self_collection'";
            elseif ($service_type == "delivery")    
                $sql .= " AND mc_delivery.service_type = '$service_type'";
            
            //$sql .= " AND mc_delivery_items.qty > 0";
                //
            //$sql .=  " GROUP BY mc_orders_items.order_id DESC";
            $sql .=  " GROUP BY mc_delivery.order_ref ORDER BY mc_delivery.service_date DESC";
            
            $query = $this->db->query($sql);
            
            //echo $this->db->last_query();

            if($query->num_rows() > 0){
               return $query->result(); 
            }
    }

    function get_all_deliveries3($from_date = NULL, $to_date = NULL, $service_type, $param = NULL) {
       
            if (!empty($param)) {
                /* Param Note:
                 * 0 - tool_type (dafault tool, sort order date)
                 */
                $params = explode('_', $param);
                //echo $params[0]; die();
            }
            
            $sql =  "SELECT mc_delivery.id as delivery_id, mc_delivery.service_date, mc_delivery.address, 
                        mc_delivery.postal_code,mc_delivery.delivered_to, mc_delivery.delivered_contact, 
                        mc_orders.id,mc_orders.order_ref,mc_orders.status,mc_orders.date_ordered, mc_orders.total_amount, 
                        mc_orders.discounted_amount, mc_customers.email, mc_customers.first_name, mc_customers.last_name".
                        " FROM mc_delivery".
                        " LEFT JOIN mc_orders ON mc_orders.order_ref = mc_delivery.order_ref".
                        " LEFT JOIN mc_customers ON mc_customers.id = mc_orders.customer_id".
                        " WHERE mc_orders.status = ". ORDER_PROCESS ." OR mc_orders.status = " . ORDER_OFFLINE_INCOMPLETE; //paid-open and offline incomplete
             
            if (!empty($params) && $params[0] == "default-tool") {
                if (!empty($from_date))
                    $sql .= " AND (date(mc_delivery.service_date) >='".date("Y-m-d", strtotime($from_date))."' AND date(mc_delivery.service_date) <='".date("Y-m-d", strtotime($to_date))."' )";
            } else if (!empty($params) && $params[0] == "sort-date-ordered") {
                if (!empty($from_date))
                    $sql .= " AND (date(mc_orders.date_ordered) >='".date("Y-m-d", strtotime($from_date))."' AND date(mc_orders.date_ordered) <='".date("Y-m-d", strtotime($to_date))."' )";
            }
                    
            if ($service_type == "self-collection")    
                $sql .= " AND mc_delivery.service_type = 'self_collection'";
            elseif ($service_type == "delivery")    
                $sql .= " AND mc_delivery.service_type = 'delivery'";
            
            //$sql .= " AND mc_delivery_items.qty > 0";
                //
            //$sql .=  " GROUP BY mc_orders_items.order_id DESC";
            $sql .=  " GROUP BY mc_delivery.order_ref ORDER BY mc_delivery.service_date DESC";
            
            $query = $this->db->query($sql);
            
            //echo $this->db->last_query();

            if($query->num_rows() > 0){
               return $query->result(); 
            }
    }
        
        
        function Get_Total_Delivery_Amount($delivery_id) {
            //$sql .= "SELECT mc_delivery.id as delivery_id, mc_delivery.order_ref, mc_delivery.service_type,mc_delivery_items.product_id,mc_delivery_items.product_name, mc_delivery_items.qty, mc_orders_items.price, (mc_delivery_items.qty * mc_orders_items.price) as delivery_price".
            $sql = "SELECT SUM(mc_delivery_items.qty * mc_orders_items.price) as delivery_amount".
                    " FROM mc_delivery".
                    " LEFT JOIN mc_delivery_items ON mc_delivery_items.delivery_id = mc_delivery.id".
                    " LEFT JOIN mc_orders ON mc_orders.order_ref = mc_delivery_items.order_ref".
                    " LEFT JOIN mc_orders_items ON mc_orders_items.order_id = mc_orders.id AND mc_orders_items.product_id = mc_delivery_items.product_id".
                    " WHERE mc_delivery.id = $delivery_id".
                    " AND mc_delivery_items.qty > 0";
            
            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
               return $query->row(); 
            }
            else
                return false;
        }

	function Get_Order_Details($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT mc_orders.*, mc_customers.id as customer_id, mc_customers.email, mc_customers.first_name, mc_customers.last_name, mc_customers.mobile, mc_customers.send_sms,mc_customers.alt_contact FROM mc_orders".
                                " LEFT JOIN mc_customers ON mc_orders.customer_id=mc_customers.id".
					" WHERE mc_orders.id=$the_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
                            return false;
		}
		else
                    return false;
	}
        
        function Get_Total_Amount($the_id){
            $sql = 	"SELECT SUM(qty*price) as total FROM mc_orders_items".
                                
			" WHERE order_id=$the_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
                            return false;
        }
        
        function Get_Ordered_Items_R1($order_id)
	{
		$sql = 	"SELECT *".
                        " FROM mc_orders_items".
                        " WHERE order_id=$order_id";
                
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
        /*
        function Get_Ordered_Items($order_id) {
            $this->db->select("*")
                    ->from("mc_orders_items")
                    ->where('order_id', $order_id)
                    ->group_by('product_id');
                
            $query = $this->db->get();

            if($query->num_rows() > 0){
                return $query->result(); 
            } else {
                return FALSE;
            }
            
	}
         * 
         */
        
        function Get_All_Delivery_Options($delivery_id){
            /*This is to display all entries from mc_delivery
             * where order ref = $order_ref
             */
            $this->db->select('*');
            $this->db->from('mc_delivery');
            $this->db->where('id', $delivery_id);
            //$this->db->where('service_type', 'delivery');
            $this->db->order_by("id", "asc");
            $this->db->group_by('order_ref');
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
            
        }
        
        function Get_All_Delivery_Options_By_OrderRef($order_ref){
            /*This is to display all entries from mc_delivery
             * where order ref = $order_ref
             */
            $this->db->select('*');
            $this->db->from('mc_delivery');
            $this->db->where('order_ref', $order_ref);
            //$this->db->where('service_type', 'delivery');
            $this->db->order_by("id", "asc");
            $this->db->group_by('order_ref');
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
            
        }
        
        function Get_All_Fulfillment_Option_Items($delivery_id){            
            $this->db->select('*');
            $this->db->from('mc_delivery_items');            
            $this->db->where('delivery_id', $delivery_id);
            $this->db->order_by("product_id", "asc");
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
	}
        
        function Update_Customer($field_id, $customer_data)
	{
            $this->db->where('id', $field_id);
            $this->db->update('mc_customers', $customer_data);
	}
        
        function Update_Order($field_id, $order_data)
	{
            $this->db->where('id', $field_id);
            $this->db->update('mc_orders', $order_data);
	}

	
        function Get_Total_Delivery_Qty($order_ref){
            /*
             * This is to tell the if the total qty is already exceeded
             * to 50 or more.
             */
            $this->db->select('mc_delivery.service_type,mc_delivery.service_date, SUM(mc_delivery_items.qty) AS dctotal_qty');
            $this->db->from('mc_delivery');                        
            $this->db->join('mc_delivery_items', 'mc_delivery_items.delivery_id = mc_delivery.id', 'left');
            $this->db->where('mc_delivery.order_ref', $order_ref);
            $this->db->where('mc_delivery.service_type', 'delivery');
            $this->db->group_by('mc_delivery_items.delivery_id');
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
        }
        
        function Get_Total_Qty($order_id){
            $this->db->select('qty');
            
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            $this->db->group_by('product_id');
            
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
                $total_qty = array();
                foreach ($query->result() as $itemqty) {
                    //echo $itemqty->qty;
                    $total_qty[] = $itemqty->qty;
                }
                return $total_qty;
                //return $query->result_array();
                //var_dump($total_qty);
                
                //echo array_sum($total_qty);
                //return $total_qty;
               //return $query->row(); 
            }
        }
        
        function Get_Raw_Total_Qty($order_id){
            /*
             * Note: This function is used for debugging purpose.
             * This is to fetch the total qty even the duplicate producs
             */
            $this->db->select('sum(qty) as total_qty');
            
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            
            $query = $this->db->get();
            if($query->num_rows() > 0){
                return $query->row(); 
            }
        }
        
        function Get_Order_Total_Amount($order_id){
            $this->db->select('qty,price');
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            $this->db->group_by('product_id');
            
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
                $computed_total = 0;
                foreach ($query->result() as $item) {
                    //echo $itemqty->qty;
                    $computed_total += $item->qty*$item->price;
                }
                return $computed_total;
                
               //return $query->row()->total; 
            } else
                return FALSE;
        }
        function Get_Order_Total_Amount_R1($order_id){
            $this->db->select('SUM(qty*price) as total');
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            $this->db->group_by('product_id');
            
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->row()->total; 
            } else
                return FALSE;
        }
    
    function transaction_count($status) {
        
        if ($status < 99) {
            if ($status == 2)
                $this->db->where_in('status',('2,3'));
            else                            
                $this->db->where('status',$status);
        }
        $this->db->from('mc_orders');
        $count = $this->db->count_all_results();
        return $count;
    }
    
    
    
    function Get_OrderDetails_by_deliveryID($delivery_id) {
        $this->db->select('mc_orders.order_ref, 
                            mc_orders.corporate_discount, 
                            mc_orders.card_discount, 
                            mc_orders.promo_code_discount, 
                            mc_orders.date_ordered, 
                            mc_orders.approval_code, 
                            mc_customers.first_name, 
                            mc_customers.last_name, 
                            mc_customers.email, 
                            mc_customers.mobile, 
                            mc_customers.alt_contact');
        $this->db->from('mc_delivery');
        $this->db->join('mc_orders','mc_orders.order_ref = mc_delivery.order_ref', 'left');
        $this->db->join('mc_customers','mc_customers.id = mc_orders.customer_id', 'left');
        
        $this->db->where('mc_delivery.id', $delivery_id);

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->row(); 
        } else
            return FALSE;
    }
    
    function Get_OrderItems_by_deliveryID($delivery_id) {
        $this->db->select('mc_orders_items.product_id, mc_orders_items.product_name, mc_orders_items.qty, mc_orders_items.price');
        $this->db->from('mc_delivery');
        $this->db->join('mc_orders','mc_orders.order_ref = mc_delivery.order_ref', 'left');
        $this->db->join('mc_orders_items','mc_orders_items.order_id = mc_orders.id', 'left');
        
        $this->db->where('mc_delivery.id', $delivery_id);

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->result(); 
        } else
            return FALSE;
        
    }
    
    function Get_Specific_Delivery_Item($delivery_id, $product_id) {
        $this->db->select('di.product_id, di.product_name, di.qty, oi.price as oi_price');
        $this->db->from('mc_delivery_items as di');
        $this->db->join('mc_orders as orders', 'orders.order_ref = di.order_ref', 'left');
        $this->db->join('mc_orders_items as oi', 'oi.order_id = orders.id AND oi.product_id = di.product_id', 'left');
        
        $this->db->where('di.delivery_id', $delivery_id);
        $this->db->where('di.product_id', $product_id);
        $this->db->where('di.qty > 0');
        

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->row(); 
        } else
            return FALSE;
    }
    
    function Get_Specific_Delivery_Item_ByOrderRef($order_ref, $product_id) {
        $this->db->select('di.product_id, di.product_name, di.qty, oi.price as oi_price');
        $this->db->from('mc_delivery_items as di');
        $this->db->join('mc_orders as orders', 'orders.order_ref = di.order_ref', 'left');
        $this->db->join('mc_orders_items as oi', 'oi.order_id = orders.id AND oi.product_id = di.product_id', 'left');
        
        $this->db->where('di.order_ref', $order_ref);
        $this->db->where('di.product_id', $product_id);
        $this->db->where('di.qty > 0');
        

        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->row(); 
        } else
            return FALSE;
    }
    
    
}
?>