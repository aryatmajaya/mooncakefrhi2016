<?php
class Mc_settings_model extends CI_Model {
	
	
	
	

	function get_all_block_dates($block_type)
	{
		$sql = 	"SELECT * from mc_block_dates".
                        " WHERE block_type=?".
			" ORDER BY block_date ASC";

		$query = $this->db->query($sql, array($block_type));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
        
    function get_all_promotion_rules()
    {
        $sql = 	"SELECT * from mc_promotion_rules".
                " ORDER BY id ASC";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
		
}
?>