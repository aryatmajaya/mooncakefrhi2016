<?php
class Mc_transactions_model extends CI_Model {
	
	

	function get_all_transactions_R1($status, $search_field = NULL, $search = NULL)
	{
		$sql = 	"SELECT mc_orders.id,mc_orders.order_ref,mc_orders.status,mc_orders.date_ordered, mc_orders.far_card, mc_orders.discount, mc_customers.email, mc_customers.first_name, mc_customers.last_name, SUM(mc_orders_items.qty*mc_orders_items.price) as total".
                        " FROM mc_orders".
                        " LEFT JOIN mc_customers ON mc_orders.customer_id = mc_customers.id".
                        " LEFT JOIN mc_orders_items ON mc_orders.id = mc_orders_items.order_id";
                
                
                if ($status < 99){
                    if ($status == 2)
                        $sql .= " WHERE mc_orders.status IN (2,3)";
                    else                            
                        $sql .= " WHERE mc_orders.status=$status";
                }
                
                if($search_field) {
                    if ($search_field == "customer_id")
                        $sql .= " WHERE customer_id = $search";
                    else
                        $sql .= " WHERE ".$search_field." LIKE '%".$search."%'";
                }
                    
                    
                //$sql .=  " GROUP BY mc_orders_items.order_id DESC";
                $sql .=  " GROUP BY mc_orders.id, mc_orders_items.product_id ORDER BY mc_orders.id DESC";
                
                //var_dump($sql);die();
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
		

	}
  
  function get_order_history($status, $search_field = NULL, $search = NULL)
	{
		$sql = 	"SELECT mc_orders.id,mc_orders.order_ref,mc_orders.status,mc_orders.date_ordered, mc_orders.discounted_amount".
            ", mc_orders.card_discount, mc_customers.email, mc_customers.first_name, mc_customers.last_name".
            ", mc_orders.total_amount".
                        " FROM mc_orders".
                        " LEFT JOIN mc_customers ON mc_orders.customer_id = mc_customers.id";
                
                
                if ($status < 99){
                    if ($status == 2)
                        $sql .= " WHERE mc_orders.status IN (2,3)";
                    else                            
                        $sql .= " WHERE mc_orders.status=$status";
                }
                
                if($search_field) {
                    if ($search_field == "customer_id")
                        $sql .= " WHERE customer_id = $search";
                    else
                        $sql .= " WHERE ".$search_field." LIKE '%".$search."%'";
                }
                    
                    
                //$sql .=  " GROUP BY mc_orders_items.order_id DESC";
                $sql .=  " GROUP BY mc_orders.id ORDER BY mc_orders.id DESC";
                
                //var_dump($sql);die();
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
		

	}
        
    function get_all_transactions($status, $search_field = NULL, $search = NULL, $limit = NULL, $start_page = NULL, $from_date = NULL, $to_date = NULL, $sort="id", $order="desc"){
		
		$this->db->select("
			o.id,
			o.order_ref,
			o.status, 
			o.date_ordered,
			c.email, 
			c.first_name, 
			c.last_name,
			o.ordering_method,
			o.promo_code_discount, 
			o.card_discount,
			o.corporate_discount,
			o.discounted_amount,
			o.total_amount,
			d.service_date"
		);
		
		$this->db->from("mc_orders o");
		$this->db->join("mc_customers c","o.customer_id = c.id","left");
		$this->db->join("mc_delivery d","d.order_ref = o.order_ref","left");
		
		$this->db->where("o.ordering_method !=","corporate");

		if ($status < 99){
			if ($status == 2){
				$this->db->where_in("o.status",array(2,3));
			}else{
				$this->db->where("o.status",$status);
			}
		}
            
		if($search_field) {
			if ($search_field == "customer_id"){
				$this->db->where("o.customer_id", $search);
			}else{
				$this->db->like($search_field, $search); 
			}
		}
		
		if (!empty($from_date)){
			$this->db->where("( date(o.date_ordered) >='".date("Y-m-d", strtotime($from_date))."' AND date(o.date_ordered) <='".date("Y-m-d", strtotime($to_date))."' )");		
		}
		
		$this->db->group_by("o.id"); 
        
		switch($sort){
			case 'status':
				$string_sort = "o.status";
			break;
			default:
				$string_sort = "o.id";
			break;
		}

		$this->db->order_by($string_sort, $order);
		
		if (!empty($limit)){
			$this->db->limit($limit, $start_page);
		}
		
		$query = $this->db->get();

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
		
	}

	function Get_Order_Details($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT mc_orders.*, mc_customers.id as customer_id, mc_customers.email, mc_customers.first_name, mc_customers.last_name, mc_customers.mobile, mc_customers.send_sms,mc_customers.alt_contact FROM mc_orders".
                                " LEFT JOIN mc_customers ON mc_orders.customer_id=mc_customers.id".
					" WHERE mc_orders.id=$the_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
                            return false;
		}
		else
                    return false;
	}
	
	function get_order_corporate($order_id){
		$order_corporate = $this->db->get_where("mc_orders_corporate",array("order_id"=>$order_id));
		return $order_corporate->row();
	}
        
        function Get_Total_Amount($the_id){

            $sql = 	"SELECT SUM(qty*price) as total 
                        FROM mc_orders_items WHERE order_id=$the_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(0)->total; 
			}
			else
            return false;
        }
        
        function Get_Ordered_Items_R1($order_id)
	{
		$sql = 	"SELECT *".
                        " FROM mc_orders_items".
                        " WHERE order_id=$order_id";
                
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
        
        function Get_Ordered_Items($order_id) {
            $this->db->select("*")
                    ->from("mc_orders_items")
                    ->where('order_id', $order_id)
                    ->group_by('product_id');
                
            $query = $this->db->get();

            if($query->num_rows() > 0){
                return $query->result(); 
            } else {
                return FALSE;
            }
            
	}
        
        function Get_All_Fulfillment_Options($order_ref){
            /*This is to display all entries from mc_delivery
             * where order ref = $order_ref
             */
            $this->db->select('*');
            $this->db->from('mc_delivery');
            $this->db->where('order_ref', $order_ref);
            $this->db->order_by("id", "asc");
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
            
        }
        function Get_All_Fulfillment_Options2($order_ref){
            /*
             * Created by James Castaneros - 23 July 2014
             * Update model and added a group by
             */
            $this->db->select('*');
            $this->db->from('mc_delivery');
            $this->db->where('order_ref', $order_ref);
            $this->db->order_by("id", "asc");
            $this->db->group_by('order_ref');
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
            
        }
        
        function Get_All_Fulfillment_Option_Items($delivery_id){            
            $this->db->select('*');
            $this->db->from('mc_delivery_items');            
            $this->db->where('delivery_id', $delivery_id);
            $this->db->order_by("product_id", "asc");
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
	}
        
        function Get_All_Fulfillment_Option_Items_by_OrderRef($order_ref){            
            $this->db->select('*');
            $this->db->from('mc_delivery_items');            
            $this->db->where('order_ref', $order_ref);
            $this->db->order_by("product_id", "asc");
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
	}
        
        function Update_Customer($field_id, $customer_data)
	{
            $this->db->where('id', $field_id);
            $this->db->update('mc_customers', $customer_data);
	}
        
        function Update_Order($field_id, $order_data)
	{
            $this->db->where('id', $field_id);
            $this->db->update('mc_orders', $order_data);
	}

	
        function Get_Total_Delivery_Qty($order_ref){
            /*
             * This is to tell the if the total qty is already exceeded
             * to 50 or more.
             */
            $this->db->select('mc_delivery.service_type,mc_delivery.service_date, SUM(mc_delivery_items.qty) AS dctotal_qty');
            $this->db->from('mc_delivery');                        
            $this->db->join('mc_delivery_items', 'mc_delivery_items.delivery_id = mc_delivery.id', 'left');
            $this->db->where('mc_delivery.order_ref', $order_ref);
            $this->db->where('mc_delivery.service_type', 'delivery');
            $this->db->group_by('mc_delivery_items.delivery_id');
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            }
        }
        
        function Get_Total_Qty($order_id){
            $this->db->select('qty');
            
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            $this->db->group_by('product_id');
            
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
                $total_qty = array();
                foreach ($query->result() as $itemqty) {
                    //echo $itemqty->qty;
                    $total_qty[] = $itemqty->qty;
                }
                return $total_qty;
                //return $query->result_array();
                //var_dump($total_qty);
                
                //echo array_sum($total_qty);
                //return $total_qty;
               //return $query->row(); 
            }
        }
        
        function Get_Raw_Total_Qty($order_id){
            /*
             * Note: This function is used for debugging purpose.
             * This is to fetch the total qty even the duplicate producs
             */
            $this->db->select('sum(qty) as total_qty');
            
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            
            $query = $this->db->get();
            if($query->num_rows() > 0){
                return $query->row(); 
            }
        }
        
        function Get_Order_Total_Amount($order_id){
            $this->db->select('qty,price');
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            $this->db->group_by('product_id');
            
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
                $computed_total = 0;
                foreach ($query->result() as $item) {
                    //echo $itemqty->qty;
                    $computed_total += $item->qty*$item->price;
                }
                return $computed_total;
                
               //return $query->row()->total; 
            } else
                return FALSE;
        }
        function Get_Order_Total_Amount_R1($order_id){
            $this->db->select('SUM(qty*price) as total');
            $this->db->from('mc_orders_items');
            $this->db->where('order_id', $order_id);
            $this->db->group_by('product_id');
            
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->row()->total; 
            } else
                return FALSE;
        }
    
    function transaction_count($status) {
        
        if ($status < 99) {
            if ($status == 2)
                $this->db->where_in('status',('2,3'));
            else                            
                $this->db->where('status',$status);
        }
        $this->db->from('mc_orders');
        $count = $this->db->count_all_results();
        return $count;
    }
}
?>