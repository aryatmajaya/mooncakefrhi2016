<?php
class Mooncakes_model extends CI_Model {
    
    /* FOR ORDER QUERIES */
    function Get_Order_Details_by_RefID($order_ref){
        $this->db->select('mc_orders.*, mc_customers.id as customer_id, mc_customers.email, mc_customers.first_name, mc_customers.last_name, mc_customers.mobile, mc_customers.send_sms,mc_customers.alt_contact')
                ->from('mc_orders')
                ->join('mc_customers', 'mc_customers.id=mc_orders.customer_id', 'left')
                ->where('mc_orders.order_ref', $order_ref);
        
        $query = $this->db->get();

        if($query->num_rows() > 0){
           return $query->row(); 
        }
        else
            return FALSE;
    }
    
    /* FOR DELIVERY/COLLECTION QUERIES */
        
        function get_all_collectiondelivery() {
            $this->db->select('d.id as did, d.order_ref, d.customer_id,d.service_type, d.service_date, d.time_slot, d.address, d.postal_code, d.delivered_to, d.delivered_contact,
                c.first_name, c.last_name')
                    ->from('mc_delivery as d')
                    ->join('mc_customers as c', 'c.id = d.customer_id', 'left')
                    ->order_by('d.service_date', 'desc');
		/*$sql = 	"SELECT mc_orders.id,mc_orders.order_ref,mc_orders.status,mc_orders.date_ordered, mc_orders.far_card, mc_orders.discount, mc_customers.email, mc_customers.first_name, mc_customers.last_name".
                        " FROM mc_orders".
                        " LEFT JOIN mc_customers ON mc_orders.customer_id = mc_customers.id";
               */ 
                
                /*if ($status < 99){
                    if ($status == 2)
                        $sql .= " WHERE mc_orders.status IN (2,3)";
                    else                            
                        $sql .= " WHERE mc_orders.status=$status";
                }
                
                if($search_field) {
                    if ($search_field == "customer_id")
                        $sql .= " WHERE customer_id = $search";
                    else
                        $sql .= " WHERE ".$search_field." LIKE '%".$search."%'";
                }
                
                if (!empty($from_date))
                    $sql .= " AND (date(mc_orders.date_ordered) >='".date("Y-m-d", strtotime($from_date))."' AND date(mc_orders.date_ordered) <='".date("Y-m-d", strtotime($to_date))."' )";
                    
            //$sql .=  " GROUP BY mc_orders_items.order_id DESC";
            $sql .=  " GROUP BY mc_orders.id ORDER BY mc_orders.id DESC";
            
            
            
            if (!empty($limit))
                $sql .=  " LIMIT $start_page, $limit";
                 * 
                 */
            //var_dump($sql);die();
            $query = $this->db->get();
            
            //echo $this->db->last_query();

            if($query->num_rows() > 0){
               return $query->result(); 
            } else {
                return FALSE;
            }
            
	}
        
        function DeliveryCollectionDetails($delivery_id) {
            $this->db->select('*');
            $this->db->from('mc_delivery as d');
            $this->db->where('d.id', $delivery_id);
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->row(); 
            } else {
                FALSE;
            }
        }
        
        function DeliveryCollectionItems($delivery_id) {
            $this->db->select('di.*');
            $this->db->from('mc_delivery_items as di');
            $this->db->where('di.delivery_id', $delivery_id);
            $this->db->order_by("di.id", "asc");
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            } else {
                FALSE;
            }
        }

	
        
        
        

	
       
        
        
       
}
?>