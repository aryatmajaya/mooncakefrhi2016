<?php
class Orders_model extends CI_Model {
	
		function add_order($insert_data){
			$this->db->set('date_ordered', 'NOW()', FALSE);            
			$this->db->set($insert_data);
			$this->db->insert('mc_orders');
		}
		
		function add_order_item($insert_item_data){
			//$this->db->set('date_created', 'NOW()');            
			$this->db->set($insert_item_data);
			$this->db->insert('mc_orders_items');
		}
		
		function update_order($field_name, $field_id, $update_data){
			$this->db->where($field_name, $field_id);
			$this->db->update('mc_orders', $update_data);
		}
		
		function Get_Discount($discount_type){
			//if (!is_null($discount_type)){
				$date_today = date("Y-m-d");
				$sql =  "SELECT * FROM mc_promotion_rules".
						" WHERE promo_name='$discount_type'".
						" AND '$date_today' BETWEEN date_start AND date_end";
				$query = $this->db->query($sql);

				if($query->num_rows() > 0){
				   return $query->row();               
				}
				else
					return false;
			//}
			//else
			//    return FALSE;
			
		}
		
		function Get_Order_Id($order_ref, $customer_id){            
				$date_today = date("Y-m-d");
				$sql =  "SELECT id FROM mc_orders".
						" WHERE order_ref='$order_ref'".
						" AND customer_id=$customer_id";
				$query = $this->db->query($sql);

				if($query->num_rows() > 0){
				   return $query->row();               
				}
				else
					return false;
		}
		
		function Delete_Ordered_Items($order_id){
			$this->db->where('order_id', $order_id);
			$this->db->delete('mc_orders_items');
		}
		
		function Get_Order_Details($order_ref){
			$this->db->select('*');
			$this->db->from('mc_orders');
			$this->db->where('order_ref', $order_ref);
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				return $query->row();               
			}
			else
				return false;  
			 
		}
		
		function Order_Summary_byID($order_id){
			$this->db->select('mc_orders.id as order_id, mc_orders.date_ordered, 
								mc_orders.order_ref, mc_orders.approval_code, mc_orders.corporate_discount, 
								mc_customers.id as customer_id, mc_customers.first_name, mc_customers.last_name, 
								mc_orders.total_amount, mc_orders.discounted_amount, mc_orders.card_discount,
								mc_orders.promo_code_discount,
								mc_customers.mobile, mc_customers.alt_contact, mc_customers.email');
			$this->db->from('mc_orders');
			$this->db->join('mc_customers','mc_customers.id = mc_orders.customer_id', 'left');
			$this->db->where('mc_orders.id', $order_id);
			
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				return $query->row();               
			 }
			 else
				 return false;            
		}
		
		function Order_Summary($order_ref){
			$this->db->select('mc_orders.id, mc_orders.date_ordered, mc_orders.order_ref, 
								mc_orders.approval_code, mc_orders.corporate_discount, 
								mc_customers.first_name, mc_customers.last_name, 
								mc_customers.mobile, mc_customers.alt_contact, mc_customers.email');
			$this->db->from('mc_orders');
			$this->db->join('mc_customers','mc_customers.id = mc_orders.customer_id', 'left');
			$this->db->where('mc_orders.order_ref', $order_ref);
			
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				return $query->row();               
			 }
			 else
				 return false;            
		}
		
	function Get_Ordered_Items($order_id){
		$this->db->select('*');
		$this->db->from('mc_orders_items');
		$this->db->where('order_id', $order_id);

		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();               
		 }
		 else
			 return false;            
	}
	
	function GetMaxID(){
		$this->db->select_max('id');
		$query = $this->db->get('mc_orders');
		if($query->num_rows() > 0){
			return $query->row()->id;               
		 }
		 else
			 return false;
	}

	function Get_Total_Delivery($order_ref){            
		$this->db->select('service_type');
		$this->db->from('mc_delivery');            
		$this->db->where('order_ref',$order_ref);
		$this->db->where('service_type','delivery');
		return $this->db->count_all_results();            
	}
	
	function get_all_block_dates($block_type)
	{
		$sql = 	"SELECT block_date from mc_block_dates".
						" WHERE block_type=?".
			" ORDER BY block_date ASC";

		$query = $this->db->query($sql, array($block_type));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
		
	function add_delivery($insert_data)
	{
			$this->db->set($insert_data);
			$this->db->insert('mc_delivery');
	}
		
	function add_delivery_item($insert_data)
	{
			$this->db->set($insert_data);
			$this->db->insert('mc_delivery_items');
	}

	function Get_Delivery_Option($order_ref){
			$this->db->select('*');
			$this->db->from('mc_delivery');            
			$this->db->where('order_ref',$order_ref);
			$this->db->order_by("id", "asc");
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
			   return $query->result(); 
			}
	}	
		
	function Get_Delivery_Option_Items($delivery_id){            
			$this->db->select('*');
			$this->db->from('mc_delivery_items');            
			$this->db->where('delivery_id', $delivery_id);
			$this->db->order_by("product_id", "asc");
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
			   return $query->result(); 
			}
	}
		
	function Get_Total_Delivered_Product_Qty($order_ref, $product_id){
			/* 
			 * This will get the total of product qty from the
			 * delivery. Once we have the total, it will be use
			 * to get to compute (remaining qty = current qty - total delivered qty)
			 */
			
		$sql = 	"SELECT mc_delivery.id,SUM(mc_delivery_items.qty) as totalqty ".
						" FROM mc_delivery".
						" LEFT JOIN mc_delivery_items ON mc_delivery.id = mc_delivery_items.delivery_id".
			" WHERE mc_delivery.order_ref = ? AND mc_delivery_items.product_id = ?";
						" GROUP BY mc_delivery_items.product_id";
			
		$query = $this->db->query($sql, array($order_ref, $product_id));

		if($query->num_rows() > 0){
		   return $query->row(); 
		}
	}
		
	function Delete_Delivery($delivery_id, $order_ref){
			if (isset($delivery_id) && isset($order_ref)){
				
				$this->db->where('id', $delivery_id);
				$this->db->where('order_ref', $order_ref);
				$this->db->delete('mc_delivery');
				
				$this->db->where('delivery_id', $delivery_id);
				$this->db->where('order_ref', $order_ref);
				$this->db->delete('mc_delivery_items');
				
			}
			
		}
		
	function get_all_offline_transactions() {
		$sql = 	"SELECT mc_orders.id, mc_orders.order_ref, mc_orders.status, 
						mc_orders.date_ordered, mc_orders.total_amount,
						mc_orders.card_discount, mc_customers.email, mc_customers.first_name, 
						mc_customers.last_name".
						" FROM mc_orders".
						" LEFT JOIN mc_customers ON mc_orders.customer_id = mc_customers.id";
				$sql .= " WHERE mc_orders.ordering_method = 'offline'";
				
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
			   return $query->result(); 
			}
	}

	function get_all_corporate_transactions() {
            $this->db->select('mc_orders.id, mc_orders.order_ref, mc_orders.status, mc_orders.date_ordered, mc_orders.total_amount, mc_orders.card_discount, mc_customers.email, mc_customers.first_name, mc_customers.last_name')
                    ->from('mc_orders')
                    ->join('mc_customers', 'mc_customers.id = mc_orders.customer_id', 'LEFT')
                    ->where('mc_orders.ordering_method', 'corporate')
                    ->order_by('date_ordered', 'DESC');
            				
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            } else
                return FALSE;
	}
        
	function get_all_corporate_transactions2($status, $search_field = NULL, $search = NULL, $per_page = NULL, $page = NULL, $from_date = NULL, $to_date = NULL) {
            /*
             * Added By James Castaneros - 6 August 2014
             */
            //var_dump($status);
            $this->db->select('mc_orders.id, mc_orders.notification_sent, mc_orders.order_ref, mc_orders.status, mc_orders.date_ordered, mc_orders.total_amount, mc_orders.card_discount, mc_orders.delivery_charge, mc_orders.ordering_method, mc_customers.email, mc_customers.first_name, mc_customers.last_name, mc_delivery.service_type, mc_delivery.service_date')
                    ->from('mc_orders')
                    ->join('mc_customers', 'mc_customers.id = mc_orders.customer_id', 'LEFT')
                    ->join('mc_delivery', 'mc_delivery.order_ref = mc_orders.order_ref', 'LEFT')
                    ->where('mc_orders.ordering_method', 'corporate');
            
            if ($status < 99){
                if ($status == 2)
                    $this->db->where_in('mc_orders.status', array(2,3));
                else                            
                    $this->db->where('mc_orders.status', $status);
            }
            
            if (!empty($from_date)):
                $from_date = date("Y-m-d",strtotime($from_date));
                $to_date = date("Y-m-d",strtotime($to_date));
                
                $this->db->where('DATE(mc_orders.date_ordered) >=', $from_date);
                $this->db->where('DATE(mc_orders.date_ordered) <=', $to_date);
            endif;
                //$sql .= " AND (."' AND date(mc_orders.date_ordered) <='".date("Y-m-d", strtotime($to_date))."' )";
            
                    
            $this->db->order_by('date_ordered', 'DESC');
            				
            $query = $this->db->get();
            //echo $this->db->last_query();die();
            
            if($query->num_rows() > 0){
               return $query->result(); 
            } else
                return FALSE;
	}
		
}
?>