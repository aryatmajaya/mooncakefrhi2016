<?php
class Properties_model extends CI_Model {
	
	

	function get_all_properties()
	{
		$sql = 	"SELECT properties.*, regions.region as the_region_name, countries.country as the_country_name from properties".
				" LEFT JOIN brands ON properties.brand_id = brands.brand_id".
				" LEFT JOIN countries ON properties.country_id = countries.country_id".
				" LEFT JOIN regions ON countries.region_id = regions.region_id".
				" WHERE properties.deleted = 0".
				" ORDER BY regions.region, countries.country, properties.hotel_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
		

	}

	function get_record($the_id){
		if (is_numeric($the_id)){

			$sql = 	"SELECT * FROM properties".
					" WHERE property_id=$the_id";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		}
		else
			return false;
	}

	/*
	function get_properties_list()
	{
		// This is used for any drop-down list reference
		
		$sql = 	"SELECT brand_id, brand_name from brands".
				" WHERE status = 1".
				" ORDER BY brand_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
	*/

	function get_properties_bybrand_list()
	{
		// This is used for any drop-down list reference
		
		$sql = 	"SELECT properties.property_id, properties.hotel_name, brands.brand_name".
				" FROM properties".
				" LEFT JOIN brands ON properties.brand_id = brands.brand_id".

				" WHERE properties.status = 1".
				" ORDER BY properties.hotel_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function properties_with_brand_list()
	{
		// This is used for any drop-down list reference in Audit Template Creator, where it shows the Brand as the Top Category
		// Each property displays the country as well.
		
		$sql = 	"SELECT properties.property_id, properties.hotel_name, properties.brand_id, brands.brand_name".
				" FROM properties".
				" LEFT JOIN brands ON properties.brand_id = brands.brand_id".

				" WHERE properties.status = 1 AND properties.deleted = 0".
				" ORDER BY properties.brand_id, region_id, hotel_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

		
		
}
?>