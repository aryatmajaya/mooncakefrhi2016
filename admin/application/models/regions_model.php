<?php
class Regions_model extends CI_Model {
	
	
	
	

	function get_all_regions()
	{
		$sql = 	"SELECT * from regions".
				" WHERE deleted = 0".
				" ORDER BY region ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_regions_list()
	{
		$sql = 	"SELECT region_id, region from regions".
				" WHERE status = 1".
				" AND deleted = 0".
				" ORDER BY region ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_record($the_id){
		$sql = 	"SELECT region_id, region, status FROM regions".
				" WHERE region_id=$the_id";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->row(); 
		}
	}
		
}
?>