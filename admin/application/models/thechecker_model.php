<?php
class Thechecker_model extends CI_Model {
	
	
	function Get_App_ID($the_app_url){
		$sql = 	"SELECT id, app_name".
				" FROM applications".
				" WHERE app_url = '".$the_app_url."'";
		$query = $this->db->query($sql, array($the_app_url));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
	}
	
	function Check_App_Permission($the_app_id, $user_group_id){
		$sql = 	"SELECT id, add_role, edit_role, delete_role, group_id".
				" FROM user_group_app_permissions".
				" WHERE user_group_app_permissions.app_id = ?".
				" AND user_group_app_permissions.group_id = ?";

		$query = $this->db->query($sql, array($the_app_id, $user_group_id));

		if($query->num_rows() > 0){
		   return $query->row(); 
		}
		else
			return false;
	}
		
}
?>