<?php
class Touchpoints_model extends CI_Model {
	
	
	
	

	function get_all_touchpoints()
	{
		$sql = 	"SELECT touch_points.*, categories.category_name from touch_points".
				" LEFT JOIN categories ON touch_points.category_id = categories.id".
				" WHERE touch_points.deleted = 0".
				" ORDER BY touch_points.touch_point ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_touchpoints_by_category($the_category)
	{
		$sql = 	"SELECT * from touch_points".
				" WHERE category_id = ?".
				" AND status = 1 AND deleted = 0".
				" ORDER BY touch_point ASC";
		$query = $this->db->query($sql, array($the_category));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_categories_list()
	{
		$sql = 	"SELECT * from categories".
				" WHERE status = 1 AND deleted = 0".
				" ORDER BY category_name ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function Check_Touchpoint_Exists($category_id, $touch_point){
		if ($category_id != ""){
			$sql = 	"SELECT id,touch_point, category_id from touch_points".
				" WHERE category_id = $category_id AND touch_point = '$touch_point'";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0)
			   return TRUE;
			else
				return FALSE;	
		}
		else
			return FALSE;
		
	}

	/*function get_regions_list()
	{
		$sql = 	"SELECT region_id, region from regions".
				" WHERE status = 1".
				" AND deleted = 0".
				" ORDER BY region ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_record($the_id){
		$sql = 	"SELECT region_id, region, status FROM regions".
				" WHERE region_id=$the_id";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->row(); 
		}
	}*/
		
}
?>