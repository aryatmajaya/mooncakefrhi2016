<?php
class Users_model extends CI_Model {
	
	
	function check_login($username, $password)
	{
            $this->load->library('encrypt');
            //$secured_password = sha1($password);
            $secured_password = $this->encrypt->encode($password);
            
            
		$sql = 	"SELECT users.id,users.username, 
						users.password ,users.first_name,users.last_name,
						users.account_type,users.status,users.last_login,
						user_groups.group_name".
				" FROM users".
				" LEFT JOIN user_groups ON users.account_type = user_groups.group_id".
				" WHERE username = ?".
				" AND users.deleted = 0";
		$result = $this->db->query($sql, array($username));
		
		if ($result->num_rows() == 1)
		{
                    //echo $this->encrypt->decode($result->row(0)->password);
            if ($this->encrypt->decode($secured_password) == $this->encrypt->decode($result->row(0)->password)) {
                        
				if ($result->row(0)->status == 1){

					$newdata = array(
	                                    'user_id' => $result->row(0)->id,
	                                    'suser_username' => $result->row(0)->username,
	                                    'user_fullname' => $result->row(0)->first_name.' '.$result->row(0)->last_name,
	                                    'user_firstname' => $result->row(0)->first_name,
	                                    'user_lastname' => $result->row(0)->last_name,
	                                    'sess_user_account_type' => $result->row(0)->account_type,
	                                    'sess_user_account_type_name' => $result->row(0)->group_name,
	                                    'sess_log_date' => $result->row(0)->last_login,
	                                    'session_login' => 1
	                                );
						
					$this->session->set_userdata($newdata);				
					
					$this->db->set('last_login', 'NOW()', FALSE);				
					$this->db->where('id',  $result->row(0)->id);
					$this->db->update('users');

					return $result->row(0)->id;
					
				}
			else{
				return 'inactive';
			}
                    } else {
                        return false;
                    }
				
		}
		else
		{
			return false;
		}
	}
	
	function get_all_users()
	{
            
            $this->db->select('users.id, users.username, users.first_name, users.last_name, users.account_type, users.status, user_groups.group_id, user_groups.group_name')
                    ->from('users')
                    ->join('user_groups', 'users.account_type = user_groups.group_id', 'LEFT')
                    ->where('users.deleted',0);
                    if ($this->session->userdata('sess_user_account_type') >= 2)
                        $this->db->where('users.account_type >= 2');
            $this->db->order_by('users.last_name, users.first_name', 'ASC');
            

            $query = $this->db->get();

			if($query->num_rows() > 0){
			   return $query->result(); 
			}
	}
	
	function get_user_data($user_id)
		{
			if (is_numeric($user_id)){

				$sql =	"select * from users where id=?".
						" AND users.deleted = 0";
				$query = $this->db->query($sql,array($user_id));

				if($query->num_rows() > 0){
				   return $query->row(); 
				}
				else
					false;
			} else if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
   				die("Email");
			}
			else
				false;
		}
	
	function insert($insert_data)
	{
		$this->db->set('date_created', 'NOW()',false);
		$this->db->set($insert_data);
 
		$this->db->insert('users');
	}
	
	function update($user_id, $update_data)
	{
		$this->db->where('id', $user_id);
		$this->db->update('users', $update_data);;
	}

	/* USER GROUPS */
	function get_all_user_groups()
	{
		$sql = 	"SELECT * FROM user_groups".
				" WHERE user_groups.deleted = 0";
		$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->result(); 
			}
	}

	function get_user_group_record($the_id)
	{
		if (is_numeric($the_id)){
			$sql = 	"select * from user_groups".
					" WHERE group_id=$the_id".
					" AND user_groups.deleted = 0";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				false;
		}
		else
			false;
			
	}

	function get_user_data_by_email($email){

	}

	
	
		
}
?>