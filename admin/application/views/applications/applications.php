<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }

$url = site_url('applications');
?>
<? $this->check_permission->Check_Button_Add(' Add Application',$current_app_info->add_role, $url); ?>
<a href="<?php echo site_url("applicationgroups");?>" class="btn btn-info pull-right">Application Groups</a>
	

<table id="sort_table" class="table table-striped table-condensed table-hover">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>Application</th>
			
			<th>Status</th>
			<th>&nbsp;</th>
			<th>Sort</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		
		$current_app_group = '';
		foreach ($entries as $entries){
			

			if ($current_app_group != $entries->group_name){
				$current_app_group = $entries->group_name;
				echo "<tr class='not_sortable'><td colspan='5'><strong>$entries->group_name</strong></td></tr>\n";
				$ctr = 1;
			}

			echo "<tr id='tr_".$entries->id."'>\n";
			echo "<td>&nbsp;</td>\n";
			
			echo "<td>".$entries->app_name."</td>\n";
			
			echo "<td>";
			if ($entries->status == 1){
				echo '<span class="label label-success">Active</span>';
			} else {
				echo '<span class="label">Inactive</span>';
			}
			echo "</td>\n";

			echo "<td>";
			$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id, $url);

			$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);
				
			echo "</td>\n";
			echo "<td id='move_td' style='background-color: #CCC; width: 30px; text-align: center;'><img src='".base_url()."assets/images/move_icon.png' id='move_icon'></td>\n";
			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>
<!-- FOR JQUERY SORTABLE -->
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	
			$.post("<?php echo $url;?>/ajax_delete",{the_id:the_id},function(result){
				
				window.location.reload();
				
			});
		  }
		

    });

    $(document).ready(function() {
		$('#move_td').css('cursor','pointer');
	
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
		
		$("#sort_table tbody").sortable({ 
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: 'tr:not(.tableHeader,.not_sortable)',
			handle: '#move_td',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				$.post("<?php echo $url;?>/update_app_sortorder", order, function(theResponse){
					//$("#contentRight").html(theResponse);
				}); 															 
			}								  
		});
		
		
		
	});
</script>