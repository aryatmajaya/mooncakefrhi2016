

<form class="form-horizontal" method="POST" action="<?=($action == "add")?site_url($this->router->fetch_class().'/add/'):site_url($this->router->fetch_class().'/update/')?>" enctype="multipart/form-data" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  if ($action == "edit"){
    echo '<input type="hidden" name="id" value="'.$entries->id.'" />';
    
  }
  ?>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Property</label>
    <div class="controls">
      <select name="property_id">
        <option></option>
        <?php
        $current_brand_id = 'none';
        if ($properties_list){
          foreach ($properties_list as $pl){
            // For Optgroup
            if ($current_brand_id == 'none'){
              echo "<optgroup label=\"$pl->brand_name\">\n";
              $current_brand_id = $pl->brand_id;
            }
            else{
              if ($current_brand_id != $pl->brand_id){
                echo "</optgroup>\n";
                echo "<optgroup label=\"$pl->brand_name\">\n";
                $current_brand_id = $pl->brand_id;
              }                
            }
            // End Optgroup

            // display the hotels
            echo "<option value='$pl->property_id'";
            if ($action == "add"){              
              echo set_select('property_id',$pl->property_id);
            } elseif ($action == "edit"){
              if ($pl->property_id == $entries->property_id)
                echo " selected";
            }
            echo ">$pl->hotel_name</option>\n";
            // End display the hotels
          }
          echo "</optgroup>\n"; //closing the tag
        }
        ?>
      </select>
    </div>
  </div>

  

  <div class="control-group">
    <label class="control-label">Template Name</label>
    <div class="controls">
      <input type="text" maxlength="255" class="span6" name="template_name" value="<?=($action == "add")?set_value('template_name'):$entries->template_name?>">
    </div>
  </div>

  

  

  

  
  
  <div class="control-group">
      <label class="control-label">Published</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="published" value="1"
          <?
          echo set_radio('published');
          if ($action == "add"){

            echo set_radio('published', '1', TRUE);
              
          }
          else{
            if ($entries->published == 1)
              echo ' checked="checked"';           
          }          
          ?>
           />Yes
          
        </label>
        <label class="radio">
          
          <input type="radio" name="published" value="0"
          <?
          if ($action == "add"){
            echo set_radio('published', '0', TRUE);
          }
          elseif ($action == "edit"){
            
            if ($entries->published == 0)
              echo ' checked="checked"';           
          }          
          ?>
           />No
        </label>
      </div>
    </div>
  

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="<?php echo site_url($this->router->fetch_class()); ?>" class="btn">Cancel</a>
  </div>

</form>
