<?
$this->utilities->BreadCrumbs($bread_crumbs);
?>

<form id="touch_point_form" class="form-inline" method="POST" action="<?=base_url().$this->router->fetch_class().'/add_touchPoint_in_template/'?>" enctype="multipart/form-data" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  ?>
  <input type="hidden" name="template_id" value="<?php echo $template_details->id; ?>" />
  <div style="float:left;">Add Touch Point:
  <select id="category_droplist" name="touchpoint_id">
        <option value="">Select a Category</option>
        <?php
        if ($cat_list){
          foreach ($cat_list as $cat_list){
            echo "<option value='$cat_list->id'";
            
            echo ">$cat_list->category_name</option>\n";
          }
        }
        ?>
      </select>
</div>
<div id="touch_point_container" style="float:left;">
</div>
<input type="hidden" id="touch_point_name" name="touch_point_name" />
<button id="add_touch_point" type="submit" class="btn btn-primary" style="display: none;">Save</button>

  
</form>

<hr>

<table class="table table-striped table-condensed">
  <thead>
    <tr>
      <th>#</th>
      <th>Touch Points</th>
      <th class="align-center"># of Elements</th>      
      <th>&nbsp;</th>      
    </tr>
  </thead>
  <?php
  if ($entries) {
    echo "<tbody>\n";
    
    $ctr = 1;
    foreach ($entries as $entries){
      echo "<tr id='tr_".$entries->id."'>\n";
      echo "<td>".$ctr."</td>\n";
      
      echo "<td>".$entries->tp_name."</td>\n";
      echo "<td class='align-center'>$entries->total_elements</td>\n";
      echo "<td>&nbsp;</td>\n";
      echo "<td>";
        echo "<a href='".base_url().$this->router->fetch_class()."/manage_tp/$entries->id' class='btn btn-mini'><i class='icon-list-alt'></i> Manage</a>";
        echo " <button id=\"tp-$entries->id-$entries->tp_name\" class='btn btn-mini duplicate-touchpoint-button'><i class='icon-th-large'></i> Duplicate</button>";
        //$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id);
        echo " <a href='".base_url().$this->router->fetch_class()."/edit_tp/$entries->id' class='btn btn-mini'><i class='icon-pencil'></i> Edit</a>";

        $this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);
        
      echo "</td>\n";

      echo "</tr>\n";
      $ctr++;
    }

    echo "</tbody>\n";
  } ?>
</table>
<!-- FOR JQUERY DIALOG -->
<link href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui-bootstrap-1.10.0.custom.css" rel="stylesheet" media="screen">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui-bootstrap-1.10.0.custom.min.js" type="text/javascript"></script>

<!--<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
 END jQuery Sortable -->
<script type="text/javascript">
  $(document).ready(function() {
    $("#category_droplist").on("change",function(){
      var selected_category = $(this).val();
        
      $.post("../../load_touchpoint_selection",{the_category:selected_category},function(result){
        //$("#touch_point_id").remove();
        //$("#add_touch_point").remove(); // removing the add touch point button
        $("#touch_point_container").html(result);
        if (result)
          $('#add_touch_point').show();
        else
          $('#add_touch_point').hide();
      });

    });

    $("#touch_point_id").on("change",function(){
      //temp_touchpoint_id
      alert('test');
      //alert($('#touch_point_id option:selected').val());
    });       
     
    
    $('form#touch_point_form #add_touch_point').on("click",function(e){
    e.preventDefault();
      var test = $('select option:selected').val();
      //alert('test');
      $('#the_notifications').html('');
      var error_text = '';
      var hasError = false;      
      
      var category_droplist = $('#category_droplist option:selected').val();
      if(category_droplist == '') {      
        error_text += 'Category field is required.';
        hasError = true;
      }
      
      var touch_point_id = $('#touch_point_id option:selected').val();
      if(touch_point_id == '') {      
        error_text += 'Touch Point field is required.';
        hasError = true;
      }    
    
      if(hasError == false) {
        //console.log('no errors');
        $('#touch_point_name').val($('#touch_point_id option:selected').text());
        
        $('#touch_point_form').submit();  
        return true;
      }
      else{
        //console.log('has errors');
        //$('#the_notifications').html('<div class="alert alert-error">'+error_text+'</div>');
        //$('html').animate({scrollTop:0}, 'slow'); //IE, FF
          //$('body').animate({scrollTop:0}, 'slow'); //chrome, don't know if safary works
        alert(error_text);
        return false;        
      }
      //return false;    
    });

    // DELETE A TOUCHPOINT
    $("button#delete-button").on("click",function(){
    //    alert('test');
        var confirm_delete = confirm("Are you sure you want to delete?");
        if (confirm_delete == true){
            var the_id = $(this).val();
            $.post("<?php echo AJAX_URL; ?>audit_templates/ajax_delete_tp",{the_id:the_id},function(result){
              window.location.reload();
              /*$("#the_caption").val($('#element_type option:selected').text());
              $("#the_element_type").val(selected_element);

              $("#add_element").show();*/
            });
        }
       
         
    });

    $(".duplicate-touchpoint-button").click(function(){
      var tp_info = $(this).attr('id').split("-");
      var new_tp_name = prompt("Please enter your desired touch point name?",tp_info[2]+" - Copy");
        if (new_tp_name != null){
            
            var tp_id = tp_info[1];
            $.post("<?php echo AJAX_URL; ?>audit_templates/ajax_duplicate_tp",{tp_id:tp_id, new_tp_name:new_tp_name},function(result){
              window.location.reload();              
            });
        }

    });
    
  });
</script>
     

