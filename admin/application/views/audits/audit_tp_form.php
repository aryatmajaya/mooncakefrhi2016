<?php
/*
This form is for Auditors to fill-in generated Audit Form
*/

$this->utilities->BreadCrumbs($bread_crumbs);

if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>

<form id='touch_point_form' method='POST' action='<?php echo base_url().$this->router->fetch_class()."/save_audit_answers/";?>' enctype="multipart/form-data" />
  <input type="hidden" name="touchpoint_id" value="<?php echo $tp_details->id; ?>" />
  <input type="hidden" name="template_id" value="<?php echo $tp_details->template_id; ?>" />
  <?php
  if ($elements > 0){
  foreach ($elements as $elements){
    
    echo "<div class=''>\n";
    echo "<div class='auditors_element_caption'>\n";
    
    echo "<div class='pull-left'><input type='hidden' name='element_id' value='$elements->id' />$elements->caption</div>\n";
    echo "<div class='pull-right'>\n";
    
    echo "</div>\n";
    
    echo "</div>\n";
    echo "<div class='element_fields'>\n";
    $content['touchpoint_id'] = $tp_details->id;
    $content['element_id'] = $elements->id;
    $content['entries'] = $this->audits_model->Get_Entries_of_an_Element($elements->id);
    $content['action'] = "answer";
    //var_dump($content['entries']);
    $this->load->view('audits/templates/'.$elements->element_type,$content);
    echo "</div>\n";
    echo "</div>\n";
    
  }
}
  ?>
  <div class="form-actions align-center">
  <button type="submit" class="btn btn-primary">Save changes</button>
  <a href="<?php echo base_url().'audits/audit_tp/'.$tp_details->template_id; ?>" class="btn">Cancel</a>
  </div>
</form>



<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui-bootstrap-1.10.0.custom.min.js"></script>
<!-- start light box -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.lightbox-0.4.css" media="screen" />
<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.lightbox-0.4.js"></script>
<!-- end light box -->

<script type="text/javascript">
  $(document).ready(function() {
    $('a.lightbox').lightBox();

    $("#element_type").on("change",function(){
      var selected_element = $(this).val();
        
      $.post("../../Show_Elements",{element_type:selected_element},function(result){
        //$("#touch_point_id").remove();
        //$("#add_touch_point").remove(); // removing the add touch point button
        //$("#element_container").html(result);
        
        $("#the_caption").val($('#element_type option:selected').text());
        $("#the_element_type").val(selected_element);

        
      });

    });

    $("#touch_point_id").on("change",function(){
      //temp_touchpoint_id
      alert($('#touch_point_id option:selected').val());
    });
    
    /*$("#add-touch-point").click(function(){
        
        $( "#dialog" ).dialog({
      resizable: false,
      height:140,
      modal: true,
      buttons: {
        "Delete all items": function() {
          $( this ).dialog( "close" );
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });*/
      /*
      $.post("../../editor_loader",{editor:'touch-point'},function(result){
        $("#lra-audit-content").append(result);
      });
  
    });*/
    
     $("#cancel-touch-point").on("click", function(event){
      alert('cancel');
      $("#category-container").remove();
    });
    
    $('form#touch_point_form123 #add_touch_point123').on("click",function(e){
    e.preventDefault();
      var test = $('select option:selected').val();
      //alert('test');
      $('#the_notifications').html('');
      var error_text = '';
      var hasError = false;
      
      
      var category_droplist = $('#category_droplist option:selected').val();
      if(category_droplist == '') {      
        error_text += 'Category field is required.';
        hasError = true;
      }
      
      var touch_point_id = $('#touch_point_id option:selected').val();
      if(touch_point_id == '') {      
        error_text += 'Touch Point field is required.';
        hasError = true;
      }
    
    
      if(hasError == false) {
        console.log('no errors');
        
        $('#touch_point_form').submit();  
        return true;
      }
      else{
        console.log('has errors');
        //$('#the_notifications').html('<div class="alert alert-error">'+error_text+'</div>');
        //$('html').animate({scrollTop:0}, 'slow'); //IE, FF
          //$('body').animate({scrollTop:0}, 'slow'); //chrome, don't know if safary works
        alert(error_text);
        return false;

        
      }
      //return false;
    
    });   
    
  });

</script>
     

