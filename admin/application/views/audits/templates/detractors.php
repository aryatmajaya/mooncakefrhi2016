<?php

function detractor_create_fields($mode, $detractor_value = NULL, $detractor_entrytype_value = NULL){
	echo "<input type=\"text\" placeholder=\"Detractor Name\" name=\"field_name\" value=\"$detractor_value\" />\n";
	echo "<select name=\"entry_type\">";
	echo "<option value=\"text_box\"";
	if ($detractor_entrytype_value == "text_box")
		echo " selected";
	echo ">Text Box</option>";
	echo "<option value=\"check_box\"";
	if ($detractor_entrytype_value == "check_box")
		echo " selected";
	echo ">Check Box</option>";
	echo "</select> <button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
}

if ($action != "answer") {
	?>
<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />

	<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
	<input type='hidden' name='element_id' value='<?=$element_id?>' />
	<input type="hidden" name="element_type" value="detractors" />
	<? detractor_create_fields('add'); ?>
	<hr>
	</form>
	<div id="detractors_elements">

	<?php
	if ($entries > 0){
		foreach ($entries as $entry) {
			//echo "<div id='entry_$entry->id' class='element_entry'>$entry->field_name</div>";
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entry->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entry->id."\" />\n";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			detractor_create_fields('update',$entry->field_name, $entry->entry_type);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";
		}
		
	}
	?>
	</div>

<? } else {
	if ($entries > 0){
		echo "<table>\n";
		foreach ($entries as $entry) {
			if ($entry->entry_type == "check_box"){
				echo "<tr><td colspan='2'><label class='checkbox'><input id='ec-$entry->id' class='detractor_checkbox' value='5' type='checkbox'";
				if ($entry->answer_integer == 5)
					echo ' checked="checked"';
				echo " /> $entry->field_name</label>";
				echo "<input type='hidden' name='entry-$entry->id-answer_integer' class='hec-$entry->id' value='".(($entry->answer_integer == 0)?1:$entry->answer_integer)."' />\n";
				echo "</td></tr>\n";
			}
			else {
				echo "<tr><td>$entry->field_name</td><td><textarea name='entry-$entry->id-answer_full_text' class='span6'>$entry->answer_full_text</textarea></td></tr>\n";
			}
			
		}
		echo "</table>";
		
	}
}
?>


<script type="text/javascript">
	


    $(document).ready(function() {

		
		
		$("#detractors_elements").sortable({ 
			connectWith: '#detractors_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("../../update_sortorder_entries", order, function(theResponse){
					
					
				});															 
			}								  
		});
		
		//AUDITOR'S FORM
		$('.detractor_checkbox').on('change', function(){    
          var the_obj = $(this).attr('id');
          //alert(the_obj);
          var check_status = this.checked ? true : false;
          if (check_status == true)
          	$('.h'+the_obj).val('5'); //check
          else
          	$('.h'+the_obj).val('1'); //uncheck
        });
		
	});
</script>