<?php
function empattrib_create_fields($mode, $empattrib_value = NULL, $empattrib_entrytype_value = NULL){
	
	echo "<input type=\"text\" placeholder=\"Question / Attribute Name\" maxlength=\"255\" class=\"span5\" name=\"field_name\" value=\"$empattrib_value\" />\n";
	echo "<select name=\"entry_type\">";
	echo "<option value=\"text_box\"";
	if ($empattrib_entrytype_value == "text_box")
		echo " selected";
	echo ">Text Box</option>";
	echo "<option value=\"yes_no\"";
	if ($empattrib_entrytype_value == "yes_no")
		echo " selected";
	echo ">Yes and No</option>";
	echo "<option value=\"pointscale\"";
	if ($empattrib_entrytype_value == "pointscale")
		echo " selected";
	echo ">5 Point Scale</option>";
	echo "</select> <button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
}

if ($action != "answer") {
	?>
<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
	
	<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
	<input type='hidden' name='element_id' value='<?=$element_id?>' />
	<input type="hidden" name="element_type" value="employee_attributes" />
	
	<? empattrib_create_fields('add'); ?>

</form>	
	<hr>
	<div id="employee_attributes_elements">

	<?php
	if ($entries > 0){
		foreach ($entries as $entry) {
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entry->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entry->id."\" />\n";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			empattrib_create_fields('update',$entry->field_name,$entry->entry_type);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";

			//echo "<div id='entry_$entry->id' class='element_entry'>$entry->field_name</div>";

		}
		
	}
	?>
	</div>

<? } else { // FOR AUDITOR'S FORM
	if ($entries > 0){
		echo "<table class='table table-condensed table-hover table-no-border'>\n";
		foreach ($entries as $entry) {
			if ($entry->entry_type == "text_box"){
				echo "<tr>\n";
				echo "<td colspan='3'>$entry->field_name</td>\n";
				echo "</tr>\n";
			}
			else {
				echo "<tr>\n";
				echo "<td>$entry->field_name</td>\n";
				echo "<td>";
				if ($entry->entry_type == "yes_no"){ // FOR YES AND NO OPTIONS
					echo "<select name='entry-$entry->id-answer_integer' class='droplist'>\n";
					//FOR YES
					echo "<option value='1'";
					if ($entry->answer_integer == 1)
						echo ' selected';
					echo ">Yes</option>\n";
					//FOR NO
					echo "<option value='0'";
					if ($entry->answer_integer == 0)
						echo ' selected';
					echo ">No</option>\n";
					echo "</select>\n";
				}
				elseif ($entry->entry_type == "pointscale"){ // FOR SCALING OPTIONS
					echo "<select name='entry-$entry->id-answer_integer' class='droplist'>\n";
					echo "<option value=\"5\"".(($entry->answer_integer == 5)?' selected':'').">5</option>\n";
					echo "<option value=\"4\"".(($entry->answer_integer == 4)?' selected':'').">4</option>\n";
					echo "<option value=\"3\"".(($entry->answer_integer == 3)?' selected':'').">3</option>\n";
					echo "<option value=\"2\"".(($entry->answer_integer == 2)?' selected':'').">2</option>\n";
					echo "<option value=\"1\"".(($entry->answer_integer == 1)?' selected':'').">1</option>\n";
					echo "</select>\n";
				}
				echo "</td>\n";
				echo "<td><textarea name='entry-$entry->id-answer_full_text' class='span5'>$entry->answer_full_text</textarea></td>\n";
				echo "</tr>\n";
			}
			
		}
		echo "</table>\n";
		
	}
}
?>


<script type="text/javascript">
	
    $(document).ready(function() {

		$("#employee_attributes_elements").sortable({ 
			connectWith: '#employee_attributes_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("../../update_sortorder_entries", order, function(theResponse){
					
					
				});															 
			}								  
		});
		
		//alert($('#sort_table >tbody >tr').length);	
		
	});
</script>