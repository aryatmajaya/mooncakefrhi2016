<?php
function guper_create_fields($mode, $guper_value = NULL, $guper_entrytype_value = NULL, $guper_list_value = NULL){
	echo '<div>';
	echo '<div style="float:left; padding-right: 10px;">';

	echo "<input type=\"text\" placeholder=\"Question\" maxlength=\"255\" class=\"\" name=\"field_name\" value=\"$guper_value\" />\n";
	echo '</div>';
	echo "<div style=\"float:left; padding-right: 10px;\">";
	
	if ($mode == "add")
		echo "<select name=\"entry_type\" class=\"droplist150 scoring_method\">";
	else
		echo "<select name=\"entry_type\" class=\"droplist150\">";
	
	echo "<option value=\"yes_no\"";
	if ($guper_entrytype_value == "yes_no")
		echo " selected";
	echo ">Yes and No</option>";
	echo "<option value=\"pointscale\"";
	if ($guper_entrytype_value == "pointscale")
		echo " selected";
	echo ">5 Point Scale</option>";
	echo "</select>\n";
	echo "</div>";
	
	echo '<div style="float: left;" id="gp_pointscale_description">';

	echo "<textarea name=\"dropdown_list\" class=\"textarea150\" rows=\"4\">$guper_list_value</textarea>\n";
	//if ($mode == "update"){
		echo '<br /><a class="bs-tooltip lra-sample-format-link"  href="#" data-toggle="tooltip" title="" data-original-title="1=Description 1<br />2=Description 2<br />3=Description 3<br />4=Description 4<br />5=Description 5">Sample Format</a>';	
	//}
	
	echo "</div>";
	echo '<div style="float: left;">';
	echo "<button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
	echo "</div>";
	//echo "<div class=\"lra-clear\">&nbsp;</div>";
	echo "</div>";
}	

if ($action != "answer") { //TEMPLATE ADMIN MODE
	?>
<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
	
	<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
	<input type='hidden' name='element_id' value='<?=$element_id?>' />
	<input type="hidden" name="element_type" value="guest_perception" />
	<div class=\"span11\">
	<? guper_create_fields('add'); ?>
	</div>
	<div class="lra-clear">&nbsp;</div>
</form>
	<hr>
	<div id="gp_elements">

	<?php
	if ($entries > 0){
		foreach ($entries as $entry) {
			//echo "<div id='entry_$entry->id' class='element_entry'>$entry->field_name</div>";
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entry->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entry->id."\" />\n";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			
			guper_create_fields('update', $entry->field_name, $entry->entry_type, $entry->dropdown_list);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";
		}
		
	}
	?>
	</div>

<? } else { // AUDITOR FORM MODE
	if ($entries > 0){
		echo "<table class='table table-condensed table-hover table-no-border'>\n";
		foreach ($entries as $entry) {
			
				
				echo "<tr>\n";
				echo "<td>$entry->field_name</td>\n";
				echo "<td>";
				if ($entry->entry_type == "yes_no"){
					echo "<select name='entry-$entry->id-answer_integer' class='droplist'>\n";
					//FOR YES
					echo "<option value='5'";
					if ($entry->answer_integer == 5)
						echo ' selected';
					echo ">Yes</option>\n";
					//FOR NO
					echo "<option value='1'";
					if ($entry->answer_integer == 1)
						echo ' selected';
					echo ">No</option>\n";

					echo "</select>\n";
				}
				elseif ($entry->entry_type == "pointscale"){
					$point_selection = explode("\n",$entry->dropdown_list);
					echo "<select name='entry-$entry->id-answer_integer'>\n";
					if ($point_selection){
						foreach($point_selection as $ps){
							$selection_values = explode("=",$ps);
							echo "<option value='$selection_values[0]'";
							if (trim($selection_values[0]) == $entry->answer_integer)
								echo ' selected';
							echo ">";
							if (isset($selection_values[0]) && isset($selection_values[1]))
							echo $selection_values[0]." - ".$selection_values[1];
							echo "</option>\n";
						}	
					}
					
					echo "<option value='99'";
					if ($entry->answer_integer == 0 || $entry->answer_integer == 99)
							echo ' selected';
					echo ">N/A</option>\n";
					
					echo "</select>\n";
				}
				echo "</td>\n";
				//echo "<td><textarea class='span5'></textarea></td>\n";
				echo "</tr>\n";
			
			
		}
		echo "</table>\n";
		
	}
}
?>

<script type="text/javascript">
	
	

    $(document).ready(function() {
    	$('#gp_pointscale_description').hide();

    	<?php //if ($mode == "update") { ?>

    	$('.scoring_method').on('change', function(){
    		if ($(this).val() != "yes_no"){
    			$('#gp_pointscale_description').show();
    		}
    		else
    			$('#gp_pointscale_description').hide();
    	});

    	<? //} ?>

		//$('.element_entry').css('cursor','pointer');
	/*
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};*/
		
		$("#gp_elements").sortable({ 
			connectWith: '#gp_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("../../update_sortorder_entries", order, function(theResponse){
					
					
				});															 
			}								  
		});
		
		//alert($('#sort_table >tbody >tr').length);	
		
	});
</script>