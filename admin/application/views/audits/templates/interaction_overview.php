<?php

function io_create_fields($mode, $io_value = NULL, $io_entrytype_value = NULL){
	echo "<input type=\"text\" placeholder=\"Field Name\" name=\"field_name\" value=\"$io_value\" />\n";
	echo "<select name=\"entry_type\">";
	echo "<option value=\"text_box\"";
	if ($io_entrytype_value == "text_box")
		echo " selected";
	echo ">Text Box</option>";
	echo "<option value=\"datepicker\"";
	if ($io_entrytype_value == "datepicker")
		echo " selected";
	echo ">Date Picker</option>";
	echo "</select> <button type=\"submit\" class=\"btn btn-mini\">".ucfirst($mode)."</button>";
}

if ($action != "answer") { //FOR TEMPLATE ADMINISTRATOR
	?>
	<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
	
		<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
		<input type='hidden' name='element_id' value='<?=$element_id?>' />
		<input type="hidden" name="element_type" value="interaction_overview" />
		<? io_create_fields('add'); ?>	
		<hr>
	</form>
	<div id="io_elements">
	<?php
	if ($entries > 0){
		foreach ($entries as $entry) {
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value='$entry->element_id' />";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />";
			echo "<input type='hidden' name='entry_id' value='$entry->id' />";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			io_create_fields('update',$entry->field_name, $entry->entry_type);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";

			
		}
		
	}
	?>
	</div>

<? } else { // FOR AUDITOR'S FORM
	if ($entries > 0){
		foreach ($entries as $entry) {
			echo "<div class=\"span4\"><label>$entry->field_name</label>";
			/* NOTE:
			format: entry-{entry->id}-answer_type} (e.g. entry-123-answer_date)
			- this means that the answer will be saved to the field "answer_date"
			*/
			if ($entry->entry_type == "datepicker")
				echo "<input type='text' name='entry-$entry->id-answer_date' class='$entry->entry_type' value=\"$entry->answer_date\" />\n";
			else
				echo "<input type='text' name='entry-$entry->id-answer_text' class='$entry->entry_type' value=\"$entry->answer_text\" />\n";

			echo '</div>';
		}
		echo "<div class='clearfix'></div>";
	}
}
?>

<!-- FOR JQUERY SORTABLE -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<!-- END jQuery Sortable -->

<script type="text/javascript">
	


    $(document).ready(function() {
    	$('.datepicker').datepicker({
    		showOn: "focus",
    		dateFormat: "yy-mm-dd"
			//buttonImage: "<?php echo base_url(); ?>assets/images/calendar.gif",
			//buttonImageOnly: true
    	});

		//$('.element_entry').css('cursor','pointer');
	/*
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};*/
		
		$("#io_elements").sortable({ 
			connectWith: '#io_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("<?php echo AJAX_URL; ?>audit_templates/update_sortorder_entries", order, function(theResponse){
					
					
				});															 
			}								  
		});
		
		//alert($('#sort_table >tbody >tr').length);	
		
	});
</script>