<?php



if ($action != "answer") {
	?>
	<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
	
	<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
	<input type='hidden' name='element_id' value='<?=$element_id?>' />
	<input type="hidden" name="element_type" value="property_photos" />
	<? $this->utilities->proppho_create_fields('add'); ?>
	</form>
	
	<hr>
	<div id="property_photos_elements">

	<?php
	if ($entries > 0){
		foreach ($entries as $entry) {
			//echo "<div id='entry_$entry->id' class='element_entry'>$entry->field_name</div>";
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entry->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entry->id."\" />\n";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";			
			$this->utilities->proppho_create_fields('update',$entry->field_name);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";
		}
		
	}
	?>
	</div>

<? } else { // FOR AUDITOR'S FORM
	if ($entries > 0){
			$path = "uploads/audits/$tp_details->template_id/";

		echo "<table class='table table-condensed table-hover table-no-border'>\n";
		foreach ($entries as $entry) {
			echo "<tr>\n";
				echo "<td>$entry->field_name</td>\n";
				echo "<td>";
				echo "<input name='image-$entry->id' type='file' />\n";
				//echo "<input name='entry-$entry->id-image' type='text' />";
				
				echo "<br /><textarea name='entry-$entry->id-answer_full_text' class='span5' placeholder='Enter Description here'>$entry->answer_full_text</textarea></td>\n";
				
				
				echo "<td>";
				//echo "<img src=\"$path$entry->answer_text\" />";
				if(file_exists($path.$entry->answer_text) && $entry->answer_text != "") {
					echo '<a href="../../../'.$path.$entry->answer_text.'" class="lightbox"><img src="../../../assets/phpthumb/phpThumb.php?src=../../'.$path.$entry->answer_text.'&amp;w=100&amp;h=100&amp;f=png&amp;zc=1&amp;q=99" /></a>';
				}
				echo "</td>";
				echo "</tr>\n";
		}
		echo "</table>\n";
		
	}
}
?>


<script type="text/javascript">
	


    $(document).ready(function() {

		//$('.element_entry').css('cursor','pointer');
	/*
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};*/
		
		$("#property_photos_elements").sortable({ 
			connectWith: '#property_photos_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("../../update_sortorder_entries", order, function(theResponse){
					
					
				});															 
			}								  
		});
		
		//alert($('#sort_table >tbody >tr').length);	
		
	});
</script>