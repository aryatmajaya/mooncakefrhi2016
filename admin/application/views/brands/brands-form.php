

<form class="form-horizontal" method="POST" action="<?=($action == "add")?base_url().$this->router->fetch_class().'/add/':base_url().$this->router->fetch_class().'/update/'?>" enctype="multipart/form-data" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }
  
  if ($action == "edit"){
    echo '<input type="hidden" name="id" value="'.$entries->brand_id.'" />';
    echo '<input type="hidden" name="old_logo_file" value="'.$entries->logo_filename.'" />';
  }
  ?>
  <div class="control-group">
    <label class="control-label">Client</label>
    <div class="controls">
      <select name="client_id">
        <option></option>
        <?php
        if ($clients_list){
          foreach ($clients_list as $clients_list){
            echo "<option value='$clients_list->id'";
            if ($action == "add"){              
              echo set_select('client_id',$clients_list->id);
            } elseif ($action == "edit"){
              if ($clients_list->id == $entries->client_id)
                echo " selected";
            }
            echo ">$clients_list->client_name</option>\n";
          }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Brand Name</label>
    <div class="controls">
      <input type="text" name="brand_name" maxlength="200" value="<?=($action == "add")?set_value('brand_name'):$entries->brand_name?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="inputEmail">Logo Image</label>
    <div class="controls">
      <input type="file" id="logo_image" name="logo_image" />
    </div>
  </div>
  
  <div class="control-group">
      <label class="control-label" for="inputEmail">Status</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="status" value="1"
          <?
          if ($action == "add"){
            echo ' checked="checked"';
          }
          else{
            if ($entries->status == 1)
              echo ' checked="checked"';           
          }          
          ?>
           />Active
          
        </label>
        <label class="radio">
          
          <input type="radio" name="status" value="0"
          <?
          if ($action == "edit"){
            
            if ($entries->status == 0)
              echo ' checked="checked"';           
          }          
          ?>
           />Inactive
        </label>
      </div>
    </div>
    

    

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Save</button>
      <a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn">Cancel</a>
    </div>
</form>
