<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }

$this->check_permission->Check_Button_Add(' Add Brand',$current_app_info->add_role);
?>


<table class="table table-striped table-condensed table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Brand Name</th>
			
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->brand_id."'>\n";
			echo "<td>".$ctr."</td>\n";
			
			echo "<td>".$entries->brand_name."</td>\n";
			
			echo "<td>";
			if ($entries->status == 1){
				echo '<span class="label label-success">Active</span>';
			} else {
				echo '<span class="label">Inactive</span>';
			}
			echo "</td>\n";

			echo "<td>";
			$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->brand_id);

			$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->brand_id);
				
			echo "</td>\n";

			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("ajax_delete",{brand_id:the_id},function(result){
				//$('#tr_' + the_id).remove();
				window.location.reload();
				//$("#dialog").dialog("close");
			});
		  }
		

    });
</script>