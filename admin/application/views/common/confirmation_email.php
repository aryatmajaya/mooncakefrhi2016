<?
$subtotal = 0;
 
$grandtotal = 0;
foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal = $subtotal + $price_total;
}

//$grandtotal = $subtotal - $order->discounted_amount;



$total_delivery_charge = 0;
//if ($review_page == "on"){
$total_delivery_charge = front_delivery_model::get_total_delivery_charge($order_ref);
$grandtotal = $subtotal - $order->discounted_amount;
$gst_charge = ($subtotal - $order->discounted_amount + $total_delivery_charge) * (GST_PERCENT /100);
//hack: remove gst charge for frhi
$gst_charge = 0;
$grandtotal = $grandtotal + $total_delivery_charge + $gst_charge;

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Your Order Confirmation at Premium Mooncake Selection</title>

    </head>
    
    <body>
    <table width="100%">
        <tr>
            <td><img src="http://www.celebrationscentral.com.sg/img/mooncakes-receipt-logo.png" border="0"></td>
            <td align="right">Premium Mooncake Selection</td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>Dear <?=$order->first_name.' '.$order->last_name?></strong><br />
                Thank you for ordering your mooncakes.<br />
                Your order confirmation details are as follows:
            </td>   
        </tr>
    </table>
    <br /><br />

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
              <td width="20%" align="left" valign="top"><strong>Order Date: </strong></td>
              <td width="30%" align="left" valign="top"><?=date("d.m.y", strtotime($order->date_ordered))?></td>
              <td width="20%" align="left" valign="top"><strong>Customer: </strong></td>
              <td width="30%" align="left" valign="top"><?=$order->first_name.' '.$order->last_name?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Order Ref: </strong></td>
            <td align="left" valign="top"><?=$order->order_ref;?></td>
            <td align="left" valign="top"><strong>Mobile: </strong></td>
            <td align="left" valign="top"><?=$order->mobile;?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Approval Code: </strong></td>
            <td align="left" valign="top"><?=$order->approval_code?></td>
            <td align="left" valign="top"><strong>Alt Contact #:</strong></td>
            <td align="left" valign="top"><?=(($order->alt_contact)?$order->alt_contact:'&nbsp;');?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Discount: </strong></td>
            <td align="left" valign="top"><?=($order->promo_code_discount + $order->card_discount + $order->corporate_discount);?>%</td>
            <td align="left" valign="top"><strong>Email: </strong></td>
            <td align="left" valign="top"><?=$order->email?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Amount: </strong></td>
            <td align="left" valign="top">SGD <?=number_format($grandtotal,2);?></td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    
        
        
    
        <br />
        <table width="100%" border="1" cellspacing="0" cellpadding="5" class="text_12px">
            <tr>

                <td width="50%" align="left" valign="top"><strong> Product Name </strong></td>
                <td width="20%" align="center" valign="top"><strong> Unit Price </strong></td>
                <td width="10%" align="center" valign="top"><strong> Qty </strong></td>
                <td width="20%" align="center" valign="top"><strong> Total </strong></td>
                
            </tr>
            <?
            $total_qty = 0;
            $subtotal = 0;

            foreach ($items as $item) {
                $price_total = $item->qty * $item->price;
                ?>        
                <tr>
                    <td><? echo html_entity_decode($item->product_name); ?></td>
                    <td align="center">SGD <? echo number_format($item->price, 2); ?></td>
                    <td align="center"><? echo $item->qty; ?></td>
                    <td align="right">SGD <? echo number_format($price_total, 2); ?></td>
                </tr>
                <?
                $total_qty = $total_qty + $item->qty;
                $subtotal = $subtotal + $price_total;
                $discounted_amount = $subtotal * (($order->promo_code_discount + $order->card_discount + $order->corporate_discount)/100);
            }
            ?>
                <tr>
                    <td colspan="3" align="right"><strong>Sub-total:</strong></td>
                    <td align="right">SGD <? echo number_format($subtotal,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>Discount: <?=($order->promo_code_discount + $order->card_discount + $order->corporate_discount );?>%</strong></td>
                    <td align="right">SGD -<? echo number_format($discounted_amount,2); ?></td>
                </tr>
                <?
                
                ?>
                <tr>
                    <td colspan="3" align="right"><strong>Delivery Charge(s): </strong></td>
                    <td align="right">SGD <?=number_format($total_delivery_charge,2); ?></td>
                 </tr>
                 <!-- <tr>
                    <td colspan="3" align="right"><strong>GST Charge: </strong></td>
                    <td align="right">SGD <?=number_format($gst_charge,2); ?></td>
                 </tr>   -->
                 <? 
                    //}
                    ?>
                <tr>
                    <td colspan="3" align="right"><strong>Total Amount:</strong></td>
                    <td align="right">SGD <?=number_format($grandtotal,2);?></td>
                </tr>
        </table>
        <!-- END items table -->
        
    <!-- START FULFILLMENT OPTIONS -->
    <? if ($fo){ ?>
    <h2>FULFILLMENT OPTIONS</h2>
    <?
    foreach ($fo as $fo){
    ?>
    <table width="100%" border="1" cellpadding="5" cellspacing="0">
        <tr>
            <td>
                <?
                echo '<strong>'.ucwords($fo->service_type).': </strong>'.date("d F Y", strtotime($fo->service_date)).(($fo->time_slot)?', '.$fo->time_slot:'');
                if ($fo->service_type == "delivery"){
                    echo '<br /><strong>Address:</strong> '.$fo->address.', '.$fo->postal_code;
                    echo '<br /><strong>Delivered To:</strong> '.$fo->delivered_to;
                    echo '<br /><strong>Contact No.:</strong> '.$fo->delivered_contact;
                }
                ?>
                <br /><br /><strong>Collection:</strong> Szechuan Court Mooncake Booth, Level 2, Fairmont Singapore, 80 Bras Basah Road Singapore 189560.
            </td>
        </tr>
    </table>
    <table width="100%" border="1" cellpadding="5" cellspacing="0">
        
            <tr>
                <td><strong>Items</strong></td>
                <td align="center"><strong>Qty</strong></td>
            </tr>
            
       
            <? 
            $delivery_items = front_delivery_model::get_delivery_option_items2($fo->order_ref);
                   
            foreach ($delivery_items as $di) {
            ?>
            <tr>
                <td><?=$di->product_name?></td>
                <td align="center"><? echo $di->qty;?></td>
            </tr>
            <? } ?>
        
    </table>
    <br />
    <? } ?>
    <!-- END FULFILLMENT OPTIONS -->
    <? } //End if ($fo)?>

<table width="100%" border="0" cellpadding="20" cellspacing="0">
    <tr>
        <td align="center">
            <p>For enquiries, please call <strong>+65 6338 8785 </strong>.</p>
            <p>This is system generated, please do not reply to this email.</p>
        </td>
    </tr>
</table>
        
    </body>
</html>
    
