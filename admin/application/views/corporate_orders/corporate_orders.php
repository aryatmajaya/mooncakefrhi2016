<?php if ($this->session->flashdata('success_notification')): ?>
    <div class="alert alert-success">    
    <?= $this->session->flashdata('success_notification'); ?>
    </div>
    <br />
    <br />
<?php endif ?>



<div class="pull-left">
	<a class="btn btn-success" href="http://celebrationscentral.com.sg/staff_form" target="_blank"><i class="icon-plus icon-white"></i> New Order</a>
    <?php /*$this->check_permission->Check_Button_Add_v2(' New Order', $current_app_info->add_role, site_url('corporate_orders/add'));*/ ?>
</div>
<div class="pull-right">
    <button type="button" id="print2-button" class="btn"><i class="icon-print"></i> Print Slips</button>
</div>

<div class="clearfix"></div>
<p>&nbsp;</p>
<?php

if (in_array($view_type, array('in-process','completed','collected','offline-incomplete'))) {
?>

<div class="well well-small" id="total-sales-tool">
    <form class="form-inline" method="POST" action="<?= site_url("corporate_orders/total_sales");?>">
        <strong>Select Date:</strong>
        <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=(empty($from_date))?'01-Jun-2014':$from_date?>" />
        <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=(empty($to_date))?date('d-M-Y'):$to_date?>" />
        <input type="hidden" name="view_type" value="<?=$view_type?>" />
        <input type="hidden" id="status_type" name="status_type" value="<?=$view_type?>" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
    <div>
        <span><strong>Total Sales: </strong></span>
        <span id="grand_total_shortcut"></span>
    </div>
</div>
<?php } ?>




<ul class="nav nav-tabs">
  <li<?=($view_type=="" || $view_type=="all")?' class="active"':''?>>
    <a href="<?=site_url('corporate_orders/index/all');?>">View All</a>
  </li>
  <li<?=($view_type=="in-process")?' class="active"':''?>><a href="<?=site_url('corporate_orders/index/in-process');?>">In-Process</a></li>
  <!--<li<?=($view_type=="completed")?' class="active"':''?>><a href="<?=site_url('corporate_orders/index/completed');?>">Completed</a></li>-->
  
  <li<?=($view_type=="collected")?' class="active"':''?>><a href="<?=site_url('corporate_orders/index/collected');?>">Collected/Delivered</a></li>
  
  <!--<li<?=($view_type=="cancelled")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/cancelled/'?>">Cancelled</a></li>-->
  <!--<li<?=($view_type=="error")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/error'?>">Problem CC</a></li>-->
  <li<?=($view_type=="offline-incomplete")?' class="active"':''?>><a href="<?=site_url('corporate_orders/index/offline-incomplete');?>">Offline-Incomplete</a></li>
</ul>
<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Order Date</th>
            <th>Delivery Date</th>
            <th>Collection Date</th>
            <th>Order Ref</th>
            <th>Customer</th>
            <th class="span1"><div class="text-right">Amount</div></th>
            <th><div class="text-center">Status</div></th>
            <th></th>
            <th></th>
            <th><input type="checkbox" id="toggle_check" name="toggle_check" checked /></th>
        </tr>
    </thead>
	<?php if ($transactions): ?>
        <tbody>
            <?php 
            $total_amount = 0;
            $grand_total = 0;
            foreach ($transactions as $key=>$transaction): ?>
                <tr>
                    <td><?= date('d.m.y',strtotime($transaction->date_ordered)) ?></td>
                    <td>
                        <!-- <?=$transaction->service_type;?> -->
                    <?=($transaction->service_type == "delivery")?date('d.m.y',strtotime($transaction->service_date)):''; ?></td>
                    <td><?=($transaction->service_type == "self_collection" || $transaction->service_type == "self collection")?date('d.m.y',strtotime($transaction->service_date)):''; ?></td>
                    <td><a href="<?= site_url('mc_transactions/view/'.$transaction->id)?>"><?= $transaction->order_ref ?></a></td>
                    <td><?= $transaction->first_name .' ' . $transaction->last_name?></td>
                    <td nowrap>
                        <div class="text-right">
                            <?php
                            $total_amount = common::Compute_Total_Amount($transaction->order_ref, $transaction->service_type, $transaction->delivery_charge, $transaction->ordering_method);
                            
                            
                            $grand_total += number_format($total_amount,2,".","");
                            echo number_format($total_amount,2); 
                            
                            /*if ($this->session->userdata('sess_user_account_type') == 1)
                                echo ' - '.$grand_total;
                             * 
                             */
                            ?>
                        </div>
                    </td>
                    <td><div class="text-center"><span class="label label-<?= mc_utilities::get_order_status_class($transaction->status)?>"><?= strtoupper(mc_utilities::order_status($transaction->status)) ?></span></div></td>
                    <td>
                        <a href="javascript:;" class="send-notification btn btn-mini" rel="<?php echo $transaction->order_ref; ?>"><i class="icon-envelope"></i>
                        	<?php if($transaction->notification_sent){ ?>
                            	Resend
                            <?php }else{ ?>
                        		Send Notification
                        	<?php } ?>
                        
                        </a>
                        <!--<a href="/admin/coporate_orders/view_notification/<?php echo $transaction->order_ref;?>" target="_blank">.</a>-->
                        <div class="last_send"></div>
                    </td>
                    <td>
                    <?= $this->check_permission->Check_Button_Edit_v2(' Edit',$current_app_info->edit_role,site_url('corporate_orders/edit/'.$transaction->id)); ?>
                    
                    </td>
                    <td><input type="checkbox" class="transaction_checkbox" name="transaction_id[]" value="<?php echo $transaction->id; ?>" checked /></td>
                </tr>
            <?php 
            endforeach; 
            
            if (isset($total_sales)) {
                echo '<tr class="info">';
                //if (in_array($view_type, $allowed_collection_date))
                    //echo "<td>&nbsp;</td>\n";
                echo '<td colspan="3"><span class="pull-right"><strong>TOTAL:</strong></span></td>';
                echo '<td nowrap><span id="grand-total" class="pull-right">$ '.number_format($grand_total,2).'</span></td>';
                echo '<td colspan="4">&nbsp;</td>';

                echo '</tr>';
            }
            ?>
            
        </tbody>
    <?php endif; ?>
   
</table>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#toggle_check').click(function() {
        
        if (this.checked) {
            $('input[type="checkbox"].transaction_checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $('input[type="checkbox"].transaction_checkbox').each(function() {
                this.checked = false;
            });
        }
            
        
    });
    
    $('#print2-button').click(function(){
        var transaction_ids = [];
        $('input[type="checkbox"].transaction_checkbox').each(function() {
            if (this.checked)
                transaction_ids.push($(this).val());
        });
        transaction_ids = transaction_ids.join("-");
        
        var print_url = '<?=site_url('corporate_orders/print_slips_PDF');?>/'+transaction_ids;
        window.open(print_url);
    });
        
        
    $('.send-notification').click(function() {
        var order_ref = $(this).attr('rel');
        $.post("<?php echo FRONT_URL."order/send_notification";?>/" + order_ref,{the_id:'none'},function(result){
            alert(result);
			window.location.href="<?php echo site_url("corporate_orders");?>";
        });
    });
    
    /*$('.send-notification').hover(function() {
        var order_ref = $(this).attr('rel');
        $.post("/admin/ajax_query/get_last_notification/"+order_ref,{order_ref:order_ref},function(result){
            $(this).closest('.last_send').html(result);
        });
    });
    */

    function Popup(data) 
    {
        
    
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Delivery Report</title>');
        
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
        
    }

        
        <? if (isset($total_sales)) { ?>
                $('#total-sales-tool').show();
        <? } ?>
        
        $('#grand_total_shortcut').text($('#grand-total').text());
        $('.from_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        
        $('#open-search-tool').click(function() {
           $('#search-tool').show();
           $('#total-sales-tool').hide();
        });
        
        $('#open-total-sales-tool').click(function() {
            $('#search-tool').hide();
            $('#total-sales-tool').show();
           
        });
        
        
        
    });
    
	
</script>