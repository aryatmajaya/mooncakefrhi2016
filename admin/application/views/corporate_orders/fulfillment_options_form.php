<form class="form-horizontal" method="POST" action="<?=BASE_URL?>corporate_orders/add_fulfillment/" enctype="multipart/form-data" />
  <?php
      if ($this->session->flashdata('required_error')){
        echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
      }
      if ($this->session->flashdata('fulfillment_required_error')){
        echo '<div class="alert alert-error">'.$this->session->flashdata('fulfillment_required_erro').'</div>';
      }
      if ($action == "edit"){
        echo '<input type="hidden" name="id" value="'.$entries->brand_id.'" />';
        echo '<input type="hidden" name="old_logo_file" value="'.$entries->logo_filename.'" />';
      }
  ?>
    <input type="hidden" name="order_id" value="<?//=$order_details->order_id?>" />
    <input type="hidden" name="order_ref" value="<?//=$order_details->order_ref?>" />
    <input type="hidden" name="customer_id" value="<?//=$order_details->customer_id?>" />
    <legend>Order Details</legend>
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Product Name</th>
                <th>Unit Price</th>
                <th class="text-center">Qty</th>
                <th>Total Price</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total_price = 0;
            $grand_total = 0;
            if ($action == "add"):
                $ctr = 1;
                $subtotal_amount = 0;
                if($items){
                    foreach($items as $key=>$item) {
                        echo '<tr>';
                        echo '<td>'.($ctr++).'</td>';
                        echo '<td>'.$item['name'].'</td>';
                        echo '<td>$'.$item['price'].'</td>';
                        echo '<td><div class="text-center">'.$item['qty'].'</div></td>';
                        $total_price = $item['price'] * $item['qty'];
                        echo '<td><div class="text-right">$'.number_format($total_price, 2).'</div></td>';
                        echo '</tr>';
                        $subtotal_amount = $subtotal_amount + $total_price;
                    }    
                }
                
                $discount_amount = $subtotal_amount * ($corporate_discount/100);
                
                $gst_amount = (($subtotal_amount - $discount_amount) + $delivery_charge) * (GST_PERCENT/100);
                //hack: remove gst charge for frhi
                $gst_amount = 0;

                $total_amount = common::Instant_Compute_Total($subtotal_amount, $discount_amount, $delivery_charge);
                //$grandtotal_amount = $total_amount + $delivery_charge;
            endif;
            ?>
            <tr>
                <td colspan="4"><div class="text-right"><strong>Sub total:</strong></div></td>
                <td>
                    <div class="text-right">$ <?=number_format($subtotal_amount,2);?></div>
                    
                </td>
            </tr>
                <tr>
                    <td colspan="4"><div class="text-right"><strong>Discount: (<?=$corporate_discount;?>%)</strong></div></td>
                    <td>
                        <div class="text-right">$ <?=number_format($discount_amount, 2)?> </div>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><div class="text-right"><strong>Delivery Charge:</strong></div></td>
                    <td>
                        <div class="text-right">$ <?=number_format($delivery_charge, 2)?> </div>
                        
                    </td>
                </tr>
                <!-- <tr>
                    <td colspan="4"><div class="text-right"><strong>GST:</strong></div></td>
                    <td><div class="text-right">$ <?=number_format($gst_amount, 2)?> </div>
                        
                    </td>
                </tr> -->
                <tr>
                    <td colspan="4"><div class="text-right"><strong>Total Amount:</strong></div></td>
                    <td><div class="text-right">$ <?= number_format($total_amount, 2) ?></div>
                        
                    </td>
                </tr>
        </tbody>
    </table>
    testtest 2
    <legend>Fulfillment Options</legend>
        <?php 
        if (empty($fulfillment_options)):
        ?>
        <div id="fulfillment_options">
            <div style="padding: 10px; border: 1px solid #CCC;">
                <p style="padding:10px; background-color: #E0E0E0;">
                    Select Self Collection or Delivery and choose a date.
                </p>
                <div style="padding: 0px 5px 5px 5px;">
                    <table>
                        <tr>
                            <td>Select Type:</td>
                            <td>
                                <select name="service_type" style="margin-right: 20px; width:150px;" class="delivery_select">
                                    <option value="self collection">Self Collection</option>
                                    <option value="delivery">Delivery</option>
                                </select>
                            </td>
                            <td>Date: </td>
                            <td>
                                <input type="text" id="selfcollect_date" name="delivery_date" class="pull-left delivery_date" readonly="readonly" />
                                <input type="text" id="delivery_date" name="delivery_date" class="pull-left delivery_date" readonly="readonly" />
                                <input name="delivery_date_field" id="delivery_date_field" type="hidden" />
                            </td>
                        </tr>
                    </table>
                    <div id="other_fields" style="padding: 5px 0 5px 0;">
                        <table>
                            <tr>
                                <td>Time Slot: </td>
                                <td>
                                    <select name="time_slot" style="width: 180px;" class="delivery_select">
                                        <option>Morning (9am-12pm)</option>
                                        <option>Afternoon (2pm-6pm)</option>
                                    </select>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td><input type="text" name="address" id="address" type="textbox" class="delivery_address" /></td>
                                <td>&nbsp;&nbsp;&nbsp;Postal Code:</td>
                                <td><input type="text" name="postal_code" id="postal_code" type="textbox" class="delivery_date" /></td>
                            </tr>
                            <tr>
                                <td>Recipient:</td>
                                <td><input type="text" name="delivered_to" id="delivered_to" type="textbox" class="delivery_address" /></td>
                                <td>&nbsp;&nbsp;&nbsp;Contact No.:</td>
                                <td><input type="text" name="delivered_contact" id="delivered_contact" type="textbox" class="delivery_date" /></td>
                            </tr>
                            <tr>
                                <td>Delivery Charge:</td>
                                <td><input type="number" name="delivery_charge" id="delivery_charge" type="textbox" class="span2" /></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <label class="pull-left"></label>
                        <br />
                        <label> </label>
                        <label class="label3"> </label>
                        <br />
                        <label> </label>
                        <label class="label3"> </label>
                    </div>
                </div>
                <div style="padding: 10px; background-color: #EEE;">
                    <!--start FULFILLMENT OPTIONS main table -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="3" class="text_12px">
                        <tr>
                            <td width="350" align="left" valign="top" class="tbl_title">Items</td>
                            <td width="100" align="center" valign="top" class="tbl_title">Quantity</td>                        
                            <td width="50" align="center" valign="top" class="tbl_title">&nbsp;</td>
                        </tr>
                        <?php
                        $ctr = 1;
                        foreach ($items as $item) {                        
                            
                                ?>        
                                <tr>
                                    <td align="left" valign=""><?//$item['id'];?><?=$item['name'] ?>
                                        <input type="hidden" name="prod_name[<?=$item['id']?>]" value="<?=$item['name'] ?>" />
                                    
                                    </td>                                
                                    <td align="center">
                                        <?
                                        //$total_qty = orders_model::Get_Total_Delivered_Product_Qty($order_details->order_ref, $item->product_id);
                                        
                                        ?>
                                        <input type="hidden" class="prod_id" name="product_id[]" value="<?=$item['id']?>" />
                                        <input type="hidden" class="prod_name" name="product_name[]" value="<?=$item['name']?>" />
                                        <input type="hidden" name="product_qty[]" value="<?php echo $item['qty']; ?>" />
                                        
                                        <?php
                                        echo $item['qty'];
                                        ?>
                                        
                                        
                                    </td>
                                    <td>&nbsp;</td>                                
                                </tr>
                                
                                <?                            
                            
                        }                    
                        ?>
                        <tr>
                            <td colspan="3" align="center">
                                <div id="create_fulfillment_button">
                                    <div id="saving_fulfillment" class="button2" style="display:none;">Saving Fulfillment Option</div>
                                    <input type="submit" id="add_delivery" class="btn" value="Create Fulfillment" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!--end FULFILLMENT OPTIONS main table -->
                </div>
            </div>
        </div>
        <? endif; ?>
        <!-- added fulfillment options -->
        <div style="padding: 10px 0 0 0;">
            <!-- start the added options -->
            <?php
            if (!empty($fulfillment_options)){
            $opt_ctr = 1;
            
            $dopt = $fulfillment_options;
            
            ?>
            <div style="padding: 10px; margin: 0 0 10px 0; border: 1px #CCC dotted;">
                <div>
                    <div class="left"><strong>You have selected: <?=ucwords($dopt['service_type']).' ('.date("d F Y", strtotime($dopt['service_date'])).((!empty($dopt['time_slot']))?', '.$dopt['time_slot']:'').')'?></strong>
                    <?
                    if ($dopt['service_type'] == "delivery"){
                        echo '<br /><strong>Address:</strong> '.$dopt['address'].', '.$dopt['postal_code'];
                        echo '<br /><strong>Delivered To:</strong> '.$dopt['delivered_to'];
                        echo '<br /><strong>Contact No.:</strong> '.$dopt['delivered_contact'];
                    }
                    ?>
                    </div>
                    <? if ($action == "add"){ ?>
                    <div class="pull-right delete_option"><a href="<?=BASE_URL.'corporate_orders/delete_option/';?>" class="btn btn-mini btn-danger">Delete Option</a></div>
                    <? } ?>
                </div>
                <div class="clear"></div>
            <div>
                <!-- start option table -->
                <table width="100%" border="0" cellspacing="0" cellpadding="3" class="text_12px">
                    <tr>
                        <td width="350" align="left" valign="top" class="tbl_title">Items</td>
                        <td width="100" align="center" valign="top" class="tbl_title">Quantity</td>                        
                        <td width="50" align="center" valign="top" class="tbl_title">&nbsp;</td>
                    </tr>
                    <?
                    //$delivery_items = orders_model::Get_Delivery_Option_Items($dopt->id);
                    if($items){
                        foreach ($items as $di) {
                            ?>        
                            <tr>
                                <td align="left" valign=""><?=$di['name'] ?></td>                                
                                <td align="center" valign=""><?=$di['qty']?></td>
                                <td>&nbsp;</td>                                
                            </tr>

                            <?                        
                        }
                    }
                    ?>
                </table>
                <!-- end option table -->
            </div>
            <!-- end the added options -->
            </div>

            <? 
            }
            ?>
        </div>
        <!-- end added fulfillment optins -->
    
</form>
<?php if (!empty($fulfillment_options)): ?>
<form class="form-horizontal" method="POST" action="<?=BASE_URL?>corporate_orders/save_order/" enctype="multipart/form-data">
    <input type="hidden" name="subtotal_amount" value="<?=number_format($subtotal_amount,2,'.','');?>" />
    <input type="hidden" name="discount_amount" value="<?=number_format($discount_amount,2,'.','');?>" />
    <input type="hidden" name="gst_amount" value="<?=number_format($gst_amount,2,'.','');?>" />
    <input type="hidden" name="total_amount" value="<?=number_format($total_amount,2,'.','');?>" />
    <div class="align-center">
      <a href="<?php echo base_url().$this->router->fetch_class().'/add'; ?>" class="btn">Back</a>
      
      <input type="submit" id="save_order" class="btn btn-primary" value="Save Order" />
      <!--<a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn">Cancel</a>-->
    </div>
</form>
<? else: ?>
    <div class="align-center">
      <a href="<?php echo base_url().$this->router->fetch_class().'/add'; ?>" class="btn">Back</a>
      
    </div>
<? endif; ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#other_fields').hide();
    
    if ($('select.delivery_select').val() == "delivery"){
        $('#other_fields').show();
        $('#selfcollect_date').hide();
        $('#delivery_date').show();
    }    
    else{
        $('#selfcollect_date').show();
        $('#delivery_date').hide();    
        $('#other_fields').hide();
    }
    /*
     * This is to check if all product quantity are zero
     * If all are zeros, the it will hide the Add Fulfillment Option Tool
     */
        /*var totalqty = 0;        
        $('select.qtybox').each(function(){
            var thevalue = $(this).val();
            totalqty = totalqty + thevalue;
        });
        
        if (totalqty == 0){                
            $('#fulfillment_options').hide();
        }
        */
    //End
    
    //var dateToday = new Date();
    // START Generic dateblocking functions
    /* utility functions */
    function BlockCollectionDays(date) {
        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
        for (i = 0; i < disabledCollectionDays.length; i++) {
            if($.inArray((m+1) + '-' + d + '-' + y,disabledCollectionDays) != -1 || new Date() > date) {
                return [false];
            }
        }
        return [true];
    }
    function BlockCollectionDaysConfig(date) {            
        return BlockCollectionDays(date);
    }
    //End Generic dateblocking functions
    
    // START collection settings
    /* create an array of days which need to be disabled */
    <?
    $the_bc = "'".date("n-j-Y")."'";
    if ($bc){
        foreach($bc as $bc){        
            $value[] = "'".date("n-j-Y",strtotime($bc->block_date))."'";
         }
         $the_bc = implode(",", $value);
    }
    ?>
    var disabledCollectionDays = [<?=$the_bc?>]; 
    
    $('#selfcollect_date').datepicker({
        showOn: "focus",
        dateFormat: "dd MM yy",
        altField: "#delivery_date_field",
        altFormat: "yy-mm-dd",          
    
        buttonImageOnly: true                    
        //beforeShowDay: BlockCollectionDaysConfig
    });
    // END collection Settings
    
    // FOR DELIVERY SETTINGS
    /* utility functions */
    function BlockDeliveryDays(date) {
        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
        for (i = 0; i < disabledDeliveryDays.length; i++) {
            if($.inArray((m+1) + '-' + d + '-' + y,disabledDeliveryDays) != -1 || new Date() > date) {
                return [false];
            }
        }
        return [true];
    }
    function BlockDeliveryDaysConfig(date) {            
        return BlockDeliveryDays(date);
    }
    //End Generic dateblocking functions
    
    // START collection settings
    /* create an array of days which need to be disabled */
    <?
    $the_bd = "'".date("n-j-Y")."'";
    if ($bd){
        
        foreach($bd as $bd){        
            $bdvalue[] = "'".date("n-j-Y",strtotime($bd->block_date))."'";
         }
         $the_bd = implode(",", $bdvalue);
    }
    ?>
    var disabledDeliveryDays = [<?=$the_bd?>]; 
    
    $('#delivery_date').datepicker({
                    showOn: "focus",
                    dateFormat: "dd MM yy",
                    altField: "#delivery_date_field",
                    altFormat: "yy-mm-dd",          
                    
                    buttonImageOnly: true
                    //beforeShowDay: BlockDeliveryDaysConfig
            });
    
    $('select.delivery_select').change(function(){
        if ($(this).val() == "self collection"){
            $('#other_fields').hide();
            $('#selfcollect_date').show();
            $('#delivery_date').hide(); 
            
        }else{
            $('#other_fields').show();
            $('#selfcollect_date').hide();
            $('#delivery_date').show(); 
            
        }
    });
    /*
    $('form#options_form #add_delivery').click(function(e){        
        //e.preventDefault();
        
        var error_text = '';
        var hasError = false;
        var totalqty = 0;
        
        $('select.qtybox').each(function(){
            var thevalue = $(this).val();
            totalqty = totalqty + thevalue;
        });
        
        if (totalqty == 0){
                hasError = true;
                error_text += "You have already completed all self-collection or delivery options for your current order.\n";
        }
        //alert($("select.delivery_select option:selected").val());
        
        if ($("select.delivery_select option:selected").val() == "self collection"){
            var selfcollection_limit = $("#selfcollection_limit").val();
            if(selfcollection_limit == 3) {			
                error_text += "You can only add 3 Self Collections.\n";
                hasError = true;
            }
            
            var selfcollect_date = $("#selfcollect_date").val();        
            if(selfcollect_date == "") {			
                error_text += "Date is required.\n";
                hasError = true;
            }
        }
        
        if ($("select.delivery_select option:selected").val() == "delivery"){
            var delivery_limit = $("#delivery_limit").val();
            if(delivery_limit == 2) {			
                error_text += "You can only add 2 Deliveries.\n";
                hasError = true;
            }
            
            var delivery_date = $("#delivery_date").val();        
            if(delivery_date == "") {			
                error_text += "Date is required.\n";
                hasError = true;
            }
        }
        
        
        
        //alert($('select.delivery_select').val());
        if ($('select.delivery_select').val() == "delivery"){
            var address = $("#address").val();        
            if(address == "") {			
                error_text += "Address is required.\n";
                hasError = true;
            }
            
            var postal_code = $("#postal_code").val();        
            if(postal_code == "") {			
                error_text += "Postal code is required.\n";
                hasError = true;
            }
        }
        
        
				
		
        if(hasError == false) {
                $('#add_delivery').hide();
                $('#saving_fulfillment').show();
                //$("#add_delivery").attr("disabled", "disabled");
                
                return true;
                //$('#jobpost_form').submit();	

        }
        else{
            alert(error_text);
            return false;

        }
		//return false;
		
    });		
    */
   /*
   $('#add_delivery').click(function() {
        var Aqty = [];
        $('.prod_id').each(function() {
            var prod_id = $(this).val();
            var qty = $('#qty_'+prod_id).val();
            console.log(prod_id+' '+qty);
            Aqty[prod_id] = qty;
            
            
        });
        
        //var total_qty = total_qty + qty
        console.log(Aqty);
       
   });
   */

    $('form#delivery_form #review_button').click(function(e){        
        //e.preventDefault();
        
        var error_text = '';
        var hasError = false;
        var totalqty = 0;
        
        $('select.qtybox').each(function(){
            var thevalue = $(this).val();
            totalqty = totalqty + thevalue;
        });
        
        if (totalqty > 0){
                hasError = true;
                error_text += "You have not yet decided on a self-collection or delivery option for the remaining items.\n";
        }
        
        if(hasError == false) {
                return true;
                //$('#jobpost_form').submit();	
        }
        else{
            alert(error_text);

            return false;
        }
		//return false;
		
    });
		
});
</script>