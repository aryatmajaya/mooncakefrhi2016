
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Celebrations Central</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	   
     <!-- jQuery -->
    <script src="<?=BASE_URL?>assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script> 
    <!-- end jQuery -->
    <!-- Bootstrap -->
    <link href="<?=BASE_URL?>assets/bootstrap/css/bootstrap-1024.css" rel="stylesheet" media="screen"> 
    <link href="<?=BASE_URL?>assets/css/styles.css" rel="stylesheet" media="screen">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?=BASE_URL?>assets/bootstrap/js/html5shiv.js"></script>
    <![endif]-->


  </head>
  <body style="background-color: #EEE !important;">
    <div class="container" style="background-color: #EEE;">
        <div class="row">


<form id="cart_form" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">

<div class="control-group">
		<label class="control-label">Customer Name</label>
		<div class="controls">
			<label class="checkbox" id="customername_details"><?php echo $customer->first_name; ?></label>
		</div>
    </div>
    <div class="control-group">
        <label class="control-label">Company</label>
        <div class="controls">
            <label class="checkbox" id="customercompany_details"><?php echo $customer->company_name; ?></label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Address</label>
        <div class="controls">
            <label class="checkbox" id="customercompany_details"><?php echo $customer->address; ?></label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Contact No.</label>
        <div class="controls">
            <label class="checkbox" id="customermobile_details"><?php echo $customer->mobile; ?></label>
        </div>
    </div>
</form>
    </div>
    </div>
  </body>
</html>