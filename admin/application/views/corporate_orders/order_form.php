<?php
	$form_action = ($action == "add") ? site_url($this->router->fetch_class().'/add/') : site_url($this->router->fetch_class().'/update/');
?>
<form id="cart_form" class="form-horizontal" method="POST" action="<?=site_url($this->router->fetch_class().'/add/')?>" enctype="multipart/form-data">
	<input type="hidden" name="total_discount" id="total_discount" value="0.00" />
	<input type="hidden" name="total_price" id="total_price" value="0.00" />
	<input type="hidden" name="grand_total" id="grand_total" value="0.00" />

    <?php if ($this->session->flashdata('required_error')): ?>
      <div class="alert alert-error"> <?= $this->session->flashdata('required_error') ?></div>
    <?php endif; ?>
    <?php if (validation_errors()):?>
      <div class="alert alert-error"><?= validation_errors() ?></div>
    <?php endif ?>
    <?php if (isset($order_details)): ?>
    	<input type="hidden" name="order_id" value="<?=$order_details->order_id;?>" />
    	<h3>ORDER NO. <?=$order_details->order_ref?></h3>
    	<legend>Customer Details</legend>
		<div class="control-group">
		    <label class="control-label">Customer Name</label>
		    <div class="controls">
		        <label class="checkbox" id="customername_details"><?=$order_details->first_name.' '.$order_details->last_name?></label>
		        <input type="hidden" name="first_name" value="<?=$order_details->first_name?>">
		        <input type="hidden" name="last_name" value="<?=$order_details->last_name?>">
		    </div>
		</div>
		<div class="control-group">
		    <label class="control-label">Company</label>
		    <div class="controls">
		        <label class="checkbox" id="customercompany_details"><?=$order_details->company_name?></label>
		    </div>
		</div>
		<div class="control-group">
		    <label class="control-label">Address</label>
		    <div class="controls">
		        <label class="checkbox" id="customercompany_details"><?=$order_details->address?></label>
		    </div>
		</div>
		<div class="control-group">
		    <label class="control-label">Contact No.</label>
		    <div class="controls">
		        <label class="checkbox" id="customermobile_details"><?=$order_details->mobile?></label>
		        <input type="hidden" name="mobile" value="<?=$order_details->mobile?>">
		    </div>
		</div>
	<?php else: ?>
		<?php $this->load->view('corporate_orders/partials/customer_info') ?>  
    <?php endif ?>
    
    <legend>Order Details</legend>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Product Name</th>
                <th>Unit Price</th>
                <th><div class="text-center">Qty</div></th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $key => $product): ?>
            	<?php if (isset($order_details)): ?>            		
                	<?php $item = Corporate_orders_model::LoadOrderItem($order_details->order_id, $product->id); ?>
            	<?php endif ?>
                <tr id="row_<?= $product->id?>">
                    <td><?= ($key+1)?></td>
                    <td><?= $product->product_name?></td>
                    <td><?= $product->price?></td>
                    <td class="col_span1">
                        <input type="hidden" name="product_id[]" value="<?= $product->id?>" />
                        <input type="hidden" name="product_name[]" value="<?= $product->product_name?>" />
                        <input type="hidden" name="product_price[]" value="<?= $product->price?>" class="product_price" />
                        <input type="text" name="product_qty[]" value="<?= ((isset($item))?$item->qty:0) ?>" 
                            class="span2 text-right product_qty" />
                    </td>
                    <td><input type="text" class="total_price span2 text-right" id="qty_<?= $product->id?>" value="0.00" readonly name="product_subtotal[]"/></td>
                </tr>
                
            <?php endforeach; ?>
                <tr>
                    <td colspan="4"><div class="text-right"><strong>Discount:</strong></div></td>
                    <td><input type="text" class="span1 text-right" name="corporate_discount" value="0" /> %</td>
                </tr>
        </tbody>
    </table>
    
    <legend>Order Section</legend>
    
    <table class="">
        <tr>
            <td colspan="5">
                <label class="radio inline">
                    <input name="order_section" value="premium box" type="radio" checked=""> Premium Box
                </label>
                <label class="radio inline">
                    <input name="order_section" value="red box" type="radio"> Red Box
                </label>
            </td>
        </tr>
        <tr>
            
            <td width="110"><label class="checkbox inline">
                    <input name="hot_stamping" value="1" type="checkbox" class="hot_stamping" /> Hot-stamping
                </label>
            </td>
            <td width="300"><input type="text" name="logo_id" id="logo_id" placeholder="Logo ID" maxlength="100" /></td>
            <td><div style="float: left; padding-right: 5px;">Sales manager </div>
                
                <input type="text" name="sales_manager" id="sales_manager" maxlength="100" class="span4" value="<?=$co_orderinfo2['sales_manager'];?>" />
            </td>
                
        </tr>
        <tr>
            <td><label class="checkbox inline">
                    Customization
                </label></td>
            <td width="300"><input type="number" step="any" name="customization_charge" id="customization_charge" value="<?=$co_orderinfo2['customization'];?>" placeholder="SGD" /></td>
            <td>Payment Mode <input type="text" name="payment_mode" maxlength="7" value="<?=$co_orderinfo2['payment_mode'];?>" /></td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <div class="align-center">
        <legend></legend>
      <button id="add_to_cart" type="submit" class="btn btn-primary" style="width:100px;"> Next </button>
      <a href="<?php echo site_url($this->router->fetch_class()); ?>" class="btn">Cancel</a>	
    </div>
</form>

<?php if (isset($order_details)): ?>
	<?php $this->load->view('corporate_orders/fulfillment_options_form') ?>	
<?php endif ?>

<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/js/select2/select2.css">
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/select2/select2.min.js" /></script>



<script type="text/javascript">
var corporate_discounts = [], total_discount = 0, total_price=0;
	function update_sub_total(){
		var sub_total = 0;
		$("input.total_price").each(function(i, el){
			sub_total += parseInt($(el).val());;
		});
		$(".sub_total").html(sub_total.toFixed(2));
		$("input#total_price").val(sub_total.toFixed(2));
		update_grand_total();
	}

	function update_total_qty(){
		var total_qty = 0;
		$("input.product_qty").each(function(i, el){
			total_qty += parseInt($(el).val());;
		});
		
		var discount = 0;
		for(var i=0; i< corporate_discounts.length; i++){
			if(total_qty >= corporate_discounts[i].min 
				&& total_qty <= corporate_discounts[i].max){
				
				discount = corporate_discounts[i].discount;
				break;
			}
		}
		$(".total_discount").text((parseInt(discount * 100)) + '%');
		$("input#total_discount").val(discount);
		update_sub_total();
		//console.log("DISCOUNT = " + discount);
	}

	function update_grand_total(){
		var total_discount= Number($("input#total_discount").val());
		var sub_total = Number($("input#total_price").val());
		//console.log(sub_total, total_discount, sub_total * total_discount);
		var discount = sub_total * total_discount;
		$("input#grand_total").val((sub_total - discount).toFixed(2));
		$(".grand_total").text($("input#grand_total").val());
		$(".total_discount_amount").text('-' + (discount.toFixed(2)));
	}
$(document).ready(function(){
    
    $("#sales_manager").select2({
        //tags:["red", "green", "blue"]
        tags:[
            <?php 
            foreach ($sales_managers as $sm) {
                echo '"'.$sm->sales_manager.'",';
            }
            ?>
        ],
        maximumSelectionSize: 1
    });
    
    if ($('#newcustomer_radio').is(':checked')) {
        
            $('#existingcustomer_container').hide();
            $('#newcustomer_container').show();
    }
            
            
    $('.hot_stamping').click(function () {
        if ($(this).is(':checked')) {
            $('#logo_id').removeAttr('disabled', 'disabled');
            $('#customization_charge').removeAttr('disabled', 'disabled');
        } else {
             $('#logo_id').attr('disabled', 'disabled');
            $('#customization_charge').attr('disabled', 'disabled');
        }
    });
    
	/*$.get('<?= base_url()?>corporate_orders/ajax_corporate_discount', function(data){
		corporate_discounts = $.parseJSON(data);
		$(".product_qty").trigger('keyup');
	});
        */

    $('.customer_type').click(function(){
        var customer_type = $(this).val();
        if (customer_type == "existing") {
            $('#existingcustomer_container').show();
            $('#newcustomer_container').hide();
        } else {
            $('#existingcustomer_container').hide();
            $('#newcustomer_container').show();
        }
    });
        
   // $("#search_customer").select2();
    
    $('#select_customer').click(function(){
        var customer_id = $('#search_customer').val();
        
        
        
        
        /*$.post("<?php echo BASE_URL; ?>corporate_orders/ajax_get_customer_details/",{customer_id:customer_id},function(result){      
            
            var customer = jQuery.parseJSON(result);
            
            
            $('#customername_details').html(customer.first_name);
            $('#customercompany_details').html(customer.company_name);
            $('#customermobile_details').html(customer.mobile);
            $('#customeremail_details').html(customer.email);
        });
        */
       $("#myiframe").attr("src", "<?php echo BASE_URL; ?>corporate_orders/display_customer/"+customer_id);
    });

    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
	$(".product_qty").on('keyup', function(e){
		var value = $(this).val();
		if(numberRegex.test(value)) {
		   	var price = $(this).parent('td').find('.product_price').val();
			var total = value * parseFloat(price);
			$(this).closest('tr').find('.total_price').val(total.toFixed(2));

			update_total_qty();
			

		}
	});
    $('form#cart_form #add_to_cart').click(function(e){        
        //e.preventDefault();
        
        var error_text = '';
        var hasError = false;
        var chktotalqty = 0;
        
        $('.product_qty').each(function(){
            var thevalue = $(this).val();
            chktotalqty = chktotalqty + thevalue;
        });
        
        if (chktotalqty == 0){
                hasError = true;
                error_text += "Please enter a quantity.\n";
        }
        
        if(hasError == false) {
                return true;
                //$('#jobpost_form').submit();	
        }
        else{
            alert(error_text);

            return false;
        }
		//return false;
		
    });
});
</script>