<?php
	$form_action = ($action == "add") ? base_url().$this->router->fetch_class().'/add/' : base_url().$this->router->fetch_class().'/update/';
?>

<form class="form-horizontal" method="POST" 
		action="<?= $form_action?>" enctype="multipart/form-data" />
		<input type="hidden" name="total_discount" id="total_discount" value="0.00">
		<input type="hidden" name="total_price" id="total_price" value="0.00">
		<input type="hidden" name="grand_total" id="grand_total" value="0.00">
	<?php
	if ($this->session->flashdata('required_error')){
		echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
	}

	if (validation_errors()){
		echo '<div class="alert alert-error">'.validation_errors().'</div>';
	}
	if ($action == "edit"){
	//echo '<input type="hidden" name="id" value="'.$entries->id.'" />';
	}
	?>
	<?= $this->load->view('corporate_orders/partials/customer_info')?>
	<table class="table ">
		<thead>
			<tr>
				<th></th>
				<th class="span6">Product Name</th>
				<th class="">Unit Price</th>
				<th class="span2"><div class="text-center">Qty</div></th>
				<th class="span2">Total Price</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($products as $key => $product): ?>
				<tr id="row_<?= $product->id?>">
					<td><?= ($key+1)?></td>
					<td><?= $product->product_name?></td>
					<td><?= $product->price?></td>
					<td class="col_span1">
						<input type="hidden" name="product_id[]" value="<?= $product->id?>" />
						<input type="hidden" name="product_name[]" value="<?= $product->product_name?>" />
						<input type="hidden" name="product_price[]" value="<?= $product->price?>" class="product_price" />
						<input type="text" name="product_qty[]" value="<?= isset($product_qty) ? $product_qty[$key] : 0; ?>" 
							class="span2 text-right product_qty" />
					</td>
					<td><input type="text" class="total_price span2 text-right" id="qty_<?= $product->id?>" value="0.00" readonly name="product_subtotal[]"/></td>
				</tr>
			<?php endforeach; ?>	
		</tbody>
	</table>
	<div class="align-center">
		<legend></legend>
	  <button type="submit" class="btn btn-primary" style="width:100px;">Next</button>
	</div>
</form>

<script>
	var corporate_discounts = [], total_discount = 0, total_price=0;
	function update_sub_total(){
		var sub_total = 0;
		$("input.total_price").each(function(i, el){
			sub_total += parseInt($(el).val());;
		});
		$(".sub_total").html(sub_total.toFixed(2));
		$("input#total_price").val(sub_total.toFixed(2));
		update_grand_total();
	}

	function update_total_qty(){
		var total_qty = 0;
		$("input.product_qty").each(function(i, el){
			total_qty += parseInt($(el).val());;
		});
		
		var discount = 0;
		for(var i=0; i< corporate_discounts.length; i++){
			if(total_qty >= corporate_discounts[i].min 
				&& total_qty <= corporate_discounts[i].max){
				
				discount = corporate_discounts[i].discount;
				break;
			}
		}
		$(".total_discount").text((parseInt(discount * 100)) + '%');
		$("input#total_discount").val(discount);
		update_sub_total();
		//console.log("DISCOUNT = " + discount);
	}

	function update_grand_total(){
		var total_discount= Number($("input#total_discount").val());
		var sub_total = Number($("input#total_price").val());
		console.log(sub_total, total_discount, sub_total * total_discount);
		var discount = sub_total * total_discount;
		$("input#grand_total").val((sub_total - discount).toFixed(2));
		$(".grand_total").text($("input#grand_total").val());
		$(".total_discount_amount").text('-' + (discount.toFixed(2)));
	}

	$(function(){
		var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
		$(".product_qty").on('keyup', function(e){
			var value = $(this).val();
			if(numberRegex.test(value)) {
			   	var price = $(this).parent('td').find('.product_price').val();
				var total = value * parseFloat(price);
				$(this).closest('tr').find('.total_price').val(total.toFixed(2));

				update_total_qty();
				

			}
		});

		$.get('<?= base_url()?>corporate_orders/ajax_corporate_discount', function(data){
			corporate_discounts = $.parseJSON(data);
			$(".product_qty").trigger('keyup');
		});

	});
</script>
