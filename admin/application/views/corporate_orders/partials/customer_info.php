<legend>Customer Details</legend>
<div class="control-group">
    <label class="control-label">Details:</label>
    <div class="controls">
        <label class="radio inline">
            <input name="customer_type" id="existingcustomer_radio" class="customer_type" value="existing" type="radio" <?php echo set_radio('customer_type', 'existing', TRUE); ?>> Existing Customer
        </label>
        <label class="radio inline">
            <input name="customer_type" id="newcustomer_radio" class="customer_type" value="new" type="radio" <?php echo set_radio('customer_type', 'new'); ?>> New Customer
        </label>
   </div>
</div>
<div id="existingcustomer_container">
    <div class="control-group">
        <label class="control-label">Select Customer</label>
        <div class="controls">
            <select id="search_customer" name="search_customer" style="width: 400px;">
            	<?php foreach ($corporate_customers as $key => $value): ?>
            		<option value="<?= $value->id?>">
            			<?= ($value->first_name .' '. $value->last_name) ?>
            		</option>
            	<?php endforeach ?>
          	</select>
            &nbsp;<button type="button" id="select_customer" class="btn">GO</button>
            <br />
        </div>
    </div>
    <div style="border-bottom: 1px #CCC dashed; width: 90%; margin:0 auto 20px auto; padding: 10px 0 20px 0; text-align: center;"></div>
    <iframe id="myiframe" width="800" height="200" frameborder="0" style="background-color: #EEE;" scrolling="no"></iframe>
</div>
<div id="newcustomer_container" class="hide">
    <div class="control-group">
      <label class="control-label"><span class="red-text">*</span> Customer Name</label>
      <div class="controls">
        <input type="text" name="first_name" maxlength="200" value="<?php echo set_value('first_name'); ?>" placeholder="First Name">
        <input type="text" name="last_name" maxlength="200" value="<?php echo set_value('last_name'); ?>" placeholder="Last Name"> 
     </div>
      
    </div>
    <div class="control-group">
        <label class="control-label">Company</label>
        <div class="controls">
            <input type="text" name="company_name" maxlength="200" value="<?php echo set_value('company_name'); ?>" placeholder="" class="span5">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Address</label>
        <div class="controls">
            <input type="text" name="address" maxlength="200" value="<?php echo set_value('address'); ?>" placeholder="Address" class="span5">
            <input type="text" name="postal_code" maxlength="200" value="<?php echo set_value('postal_code'); ?>" placeholder="Postal Code" class="span2">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><span class="red-text">*</span> Email</label>
        <div class="controls">
            <input type="text" name="email" maxlength="200" value="<?php echo set_value('email'); ?>" class="span5">
        </div>
    </div>
   	<div class="control-group">
        <label class="control-label"><span class="red-text">*</span> Contact No.</label>
        <div class="controls">
            <input type="text" name="mobile" maxlength="200" value="<?php echo set_value('mobile'); ?>" placeholder="">                
        </div>
    </div>
</div>