

<form class="form-horizontal" method="POST" action="<?=($action == "add")?base_url().$this->router->fetch_class().'/insert/':base_url().$this->router->fetch_class().'/update/'?>" enctype="multipart/form-data" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }
  
  if ($action == "edit")
    echo '<input type="hidden" name="id" value="'.$entries->country_id.'" />';
  ?>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Region</label>
    <div class="controls">
      <select name="region_id">
        <?php
        if ($regions_list){
          foreach ($regions_list as $regions_list){
            echo "<option value='$regions_list->region_id'";
            if ($action == "edit"){
              if ($regions_list->region_id == $entries->region_id)
                echo " selected";

            }
            echo ">$regions_list->region</option>\n";
          }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Country</label>
    <div class="controls">
      <input type="text" name="country" maxlength="100" value="<?=($action == "add")?set_value('country'):$entries->country?>">
    </div>
  </div>
  
  
  

  <div class="control-group">
      <label class="control-label" for="inputEmail">Status</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="status" value="1"
          <?
          if ($action == "add"){
            echo ' checked="checked"';
          }
          else{
            if ($entries->status == 1)
              echo ' checked="checked"';           
          }          
          ?>
           />Active
          
        </label>
        <label class="radio">
          
          <input type="radio" name="status" value="0"
          <?
          if ($action == "edit"){
            
            if ($entries->status == 0)
              echo ' checked="checked"';           
          }          
          ?>
           />Inactive
        </label>
      </div>
    </div>
    

    

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Save</button>
      <a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn">Cancel</a>
    </div>
</form>
