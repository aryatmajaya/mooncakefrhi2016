<?php if($this->session->flashdata('success_notification')): ?>
	<div class="alert alert-success"><?= $this->session->flashdata('success_notification') ?></div>
<?php endif; ?>

<hr>
<table class="table table-striped table-condensed">
	<thead>
		<th></th>
		<th>Title</th>
		<th>MID</th>
		<th>Discount</th>
		<th>Sort Order</th>
		<th>Status</th>
        <th>Default</th>
        <th></th>
	</thead>
	<tbody>
		<?php foreach ($entries as $key => $value): ?>
			<tr>
				<td><?= ($key + 1)?></td>
				<td><?php echo $value->title;?></td>
				<td><?php echo $value->mid;?></td>
                <td><?php echo $value->discount;?>%</td>
				<td><?php echo $value->sort_order;?></td>
                <td><?php echo $value->status?'Active':'Not Active';?></td>
                <td><?php echo $value->default?'Default':'';?></td>
				<td>
					<?= $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role, $value->id, site_url("discount_module")); ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>