<hr>
	<?php if (validation_errors()):?>
      <div class="alert alert-error"><?= validation_errors() ?></div>
    <?php endif ?>
	
    
    <h3><?php echo $value['title'];?></h3>
    
    <form class="form form-horizontal" role="form" method='POST'>	        
        <div class="control-group">
            <label class="control-label">MID</label>
            <div class="controls">
                <div class="input-append">
                    <input type="text" name="mid" value="<?php echo $value['mid'];?>">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Discount</label>
            <div class="controls">
                <div class="input-append">
                    <input type="text" class="" name="discount" value="<?php echo $value['discount'];?>">
                    <span class="add-on">%</span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Sort Order</label>
            <div class="controls">
                <input type="number" name="sort_order" value="<?php echo $value['sort_order'];?>">
            </div>
        </div>
         <div class="control-group">
            <label class="control-label">Status</label>
            <div class="controls">
            	<div class="radio">
                    <label>
                        <input type="radio" name="status" value="1" <?php echo $value['status']==1?'checked':'';?>> Active
                    </label>
				</div>
				<div class="radio">
                    <label>
                        <input type="radio" name="status" value="0" <?php echo $value['status']==0?'checked':'';?>> Not Active
                    </label>
				</div>
            </div>
        </div>
         <div class="control-group">
            <label class="control-label">Default</label>
            <div class="controls">
            	<div class="checkbox">
            		<label><input type="checkbox" name="default" <?php echo $value['default']?'checked':'';?> value="1"> Set as default</label>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary" name="submit_form" value="submit_form">Save</button>
                <a href="<?php echo site_url($this->router->fetch_class()); ?>" class="btn btn-default">Cancel</a>	
            </div>
        </div>
</form>