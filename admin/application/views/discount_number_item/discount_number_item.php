<?php if($this->session->flashdata('success_notification')): ?>
	<div class="alert alert-success"><?= $this->session->flashdata('success_notification') ?></div>
<?php endif; ?>

<?php
$this->check_permission->Check_Button_Add(' Discount',$current_app_info->add_role);
?>
<hr>
<table class="table table-striped table-condensed">
	<thead>
		<th></th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Total Item</th>
		<th>Discount</th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($entries as $key => $value): ?>
			<tr>
				<td><?= ($key + 1)?></td>
				<td><?= date("d F Y",strtotime($value->start_date)); ?></td>
				<td><?= date("d F Y",strtotime($value->end_date)); ?></td>
                <td><?= $value->total_item ?></td>
				<td><?= $value->discount ?>%</td>
				<td>
					<?= $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role, $value->discount_id); ?>
					<?= $this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$value->discount_id); ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<script>
	$(function(){
		$(".delete-button").on('click', function(e){
			var conf = confirm("Do you want to delete the discount?");
			if(conf){
				$.post('<?= base_url(). $this->router->fetch_class()?>/delete/'+$(this).val(), function(data){
					location.href="<?= base_url() . $this->router->fetch_class()?>";
				});
			}
		});
	});
</script>