<hr>
	<?php if (validation_errors()):?>
      <div class="alert alert-error"><?= validation_errors() ?></div>
    <?php endif ?>
	
    <form class="form form-horizontal" role="form" method='POST'>

        <div class="control-group">
            <label class="control-label">Start date</label>
            <div class="controls">
                <div class="input-append">
                    <input type="text" class="from_date" readonly name="start_date" value="<?php echo $value['start_date'];?>">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">End date</label>
            <div class="controls">
                <div class="input-append">
                    <input type="text" class="to_date" readonly name="end_date" value="<?php echo $value['end_date'];?>">
                    
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Total Item</label>
            <div class="controls">
                <input type="number" name="total_item" value="<?php echo $value['total_item'];?>">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Discount</label>
            <div class="controls">
                <div class="input-append">
                    <input type="text" class="" name="discount" value="<?php echo $value['discount'];?>">
                    <span class="add-on">%</span>
                </div>
            </div>
        </div>
            
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary" name="submit_form" value="submit_form">Save</button>
                <a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn btn-default">Cancel</a>	
            </div>
        </div>
</form>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script>
	$('.from_date').datepicker({
        showOn: "both",
        dateFormat: "dd MM yy",
        changeMonth: true,
        changeYear: true,
        buttonText: '<i class="icon-calendar"></i>',
        onClose: function( selectedDate ) {
            $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
        }
    });

    $('.to_date').datepicker({
        showOn: "both",
        dateFormat: "dd MM yy",
        changeMonth: true,
        changeYear: true,
        buttonText: '<i class="icon-calendar"></i>',
        onClose: function( selectedDate ) {
            $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
        }
    });

    $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
</script>