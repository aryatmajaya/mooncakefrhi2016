<?php if($this->session->flashdata('message_success')): ?>
	<div class="alert alert-success"><?= $this->session->flashdata('message_success') ?></div>
<?php endif; ?>

<hr>
	<?php if (validation_errors()):?>
      <div class="alert alert-error"><?= validation_errors() ?></div>
    <?php endif ?>
	
    <form class="form form-horizontal" role="form" method='POST'>

        <div class="control-group">
			<?php
                if($products){
                    echo "<p><strong>Product Assign</strong><p>";
                    foreach($products as $product){
                        echo '<div class="checkbox">
                          <label><input type="checkbox" name="products[]" value="'.$product->id.'" '.(in_array($product->id,$discount_products)?"checked='checked'":"").'>'.$product->product_name.'</label>
                        </div>';
                    }
                }
            ?>
        </div>
            
        <div class="control-group">
            <button type="submit" class="btn btn-primary" name="submit_form" value="submit_form">Save</button>
            <a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn btn-default">Cancel</a>	
        </div>
</form>