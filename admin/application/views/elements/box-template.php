<?php

?>
<!DOCTYPE html>
<html>
  <head>
    <title><?php echo PROJECT_TITLE; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	   
     <!-- jQuery -->
     <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
     
     <!-- end jQuery -->


    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-1024.css" rel="stylesheet" media="screen">

    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" media="screen">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/bootstrap/js/html5shiv.js"></script>
    <![endif]-->


  </head>
  <body>
    

  	



	<div class="row" id="the_content">
		
		<?php
		
		
		if (ENVIRONMENT == "development"){
			echo '<div class="span8">';
			echo 'controller: '.$this->router->fetch_class();
			echo '<br />function: '.$this->router->fetch_method();
			echo '<br />view: '.$view_page;
			echo '</div>';

		}
		?>

		
		<div class="well-small" style="border:1px solid #C2CF81; background-color: #F8F8F8;width: 740px; float:left; margin-left: 10px;">
			
			
			<?php
			$this->load->view($view_page);
			?>
		</div>
	</div>


	

    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-v2.3.1.js"></script>
    
  </body>
</html>	