<?php


if ($this->session->userdata('session_login') != 1){
  redirect(base_url(),'refresh');
}

if ($view_type == "xls"):
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=".$xls_filename.".xls");
endif;

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo PROJECT_TITLE; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <?php
    if ($view_type == "html"):
    ?>
    
     <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script> 
    <!-- end jQuery -->
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen"> 
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url(); ?>assets/jQuery-TE/jquery-te-1.4.0.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <!-- File Upload -->
    <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/bootstrap/js/html5shiv.js"></script>
    <![endif]-->

    <?php endif; ?>
  </head>
  <body>
    <div style="padding: 20px;">

  	
