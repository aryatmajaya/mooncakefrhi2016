<?php



if ($this->session->userdata('session_login') != 1){
  redirect(base_url(),'refresh');
}
else{
  if ($this->router->fetch_class() != "welcome" && $this->router->fetch_class() != "myaccount"){
    //This is for checking the current Application permission
    
    if($current_app_info == false) {
      redirect('welcome/');
    }
    
  }
  

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo PROJECT_TITLE; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script> 
    <!-- end jQuery -->
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-1024.css" rel="stylesheet" media="screen"> 
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url(); ?>assets/jQuery-TE/jquery-te-1.4.0.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <!-- File Upload -->
    <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/bootstrap/js/html5shiv.js"></script>
    <![endif]-->


  </head>
  <body>
    <div class="master-container">

  	<div class="container">
  		<div class="navbar">
  		  <div class="navbar-inner">
  		    <a class="brand" href="<?php echo base_url().'welcome/'; ?>"><img src="<?php echo base_url(); ?>assets/images/<?php echo HEADER_LOGO; ?>" /></a>
  		     <ul class="nav pull-right">
  		      <li>
              <div style="padding: 10px 10px 5px 0px; color: #CCC !important;">
              <a href="<?php echo base_url().'myaccount/'; ?>" style="color: #CCC !important;">Welcome <span><?=$this->session->userdata('user_fullname'). ' ('.$this->session->userdata('sess_user_account_type_name').')';?></a>
              </div>
              <div style="color: #CCC !important;">
              <?php echo 'Last login: '.date("F d, Y @ g:i A",strtotime($this->session->userdata('sess_log_date'))); ?>
              </div>
            </li>
  		    </ul> 
  		  </div>
  		</div>
  	</div>
