<?php 
	$lra_top_menu = $this->common->Allowed_Apps_Query($this->session->userdata('sess_user_account_type')); 

	$menu = array();
	foreach ($lra_top_menu as $value) {
		
		$menu[$value->app_group]['group_name'] = $value->group_name;
		$menu[$value->app_group]['child'][] = $value;
		if(str_replace("/","",$value->app_url) == $this->router->fetch_class()){
			$menu[$value->app_group]['class'] = 'active';
		}
	}
?>

<div class="navbar navbar-default">
  <div class="navbar-inner">
    <ul class="nav">
		<? if($lra_top_menu): ?>
		  	<? foreach ($menu as $menu_config): ?>
		  		<li class='dropdown <?= isset($menu_config['class'])? $menu_config['class']: '' ?>'>
		  			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= ucfirst($menu_config['group_name']) ?><b class="caret"></b></a>
		  			<?php if ($menu_config['child']): ?>
		  				<ul class="dropdown-menu">
		  				<?php foreach ($menu_config['child'] as $value): ?>
		  					<li class="<?= (str_replace("/","",$value->app_url) == $this->router->fetch_class()) ? 'active' : '' ?>"><a href="<?= site_url($value->app_url); ?>"<?php echo ($value->new_window==1)?' target="_blank"':'';?>><?= ucfirst($value->app_name) ?></a></li>
		  				<?php endforeach ?>
		  				 </ul>
		  			<?php endif ?>
		  		</li>
		  	<? endforeach; ?>
	  	<? endif; ?>
	  <li><a href="<?php echo site_url("home/logout");?>">Log Out</a></li>
	
    </ul>
  </div>
</div>