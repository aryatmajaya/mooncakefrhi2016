<!-- view: audit_templates_manage_tp_form.php -->
<?
$this->load->library('Ektron_Utilities');
$this->utilities->BreadCrumbs($bread_crumbs);
?>
<!-- FOR JQUERY SORTABLE -->
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

    

<form id="touch_point_form" class="form-inline" method="POST" action="<?=base_url().$this->router->fetch_class().'/add_tp_element_in_template/'?>" enctype="multipart/form-data" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }


  if ($this->session->flashdata('notification_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('notification_error').'</div>';
  }
  

  
  ?>
  <input type="hidden" name="touchpoint_id" value="<?=$tp_details->id?>" />
  <input type="hidden" name="template_id" value="<?=$tp_details->template_id?>" />
  <input id="the_caption" type="hidden" name="caption" value="" />
  <input id="the_element_type" type="hidden" name="element_type" value="" />
  <div>Add Element:
  <select id="element_type" name="element_type">
        <option value="">Select an Element</option>
        
        <option value="drop_list_option">Drop-list Option</option>
        <option value="general_description">General Description</option>
        <option value="calendar">Calendar</option>
        <option value="title_tags">Title Tags</option>
        <option value="content_box">Content Box</option>
        <option value="yn_question">Y/N Question</option>
        <option value="content_query">Content Query</option>
        <option value="insert_image">Insert Image or ID</option>
        
        
      </select>
      <button id="add_element" type="submit" class="btn btn-primary lra-hide-me" >Add</button>
</div>



<!-- <div style="background-color: #999; color: #FFF;border: 1px solid #CCC; padding: 10px;margin-top: 5px;">Element Preview</div>
<div id="element_container" style="border: 1px solid #CCC; padding: 10px;">
</div>       -->

  
</form>

<hr>

<?
// LOADING ALL ADDED ELEMENTS

if ($elements > 0){
  
  echo '<div class="accordion" id="accordion2">
  <div class="accordion-group">';

  foreach ($elements as $elements){
    echo "<div id=\"sortelement_$elements->id\" class=\"sortable_element\">\n";
    echo '<div id="caption-'.$elements->id.'" class="accordion-heading element_caption">';
      //echo '<a id="link-content'.$elements->id.'" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#content'.$elements->id.'"></a>';
     
      echo "<form id='touch_point_form' method='POST' action='".base_url().$this->router->fetch_class()."/update_tp_element_in_template/"."' />\n";
       
        echo "<div class='pull-left'><input type='hidden' name='element_id' value='$elements->id' />\n";
          echo "<input type='text' name='caption' value='$elements->caption' />\n";
          echo "<input type='hidden' name='touchpoint_id' value='$tp_details->id' />\n";
        
        //echo '<a id="link-content'.$elements->id.'" data-toggle="collapse" data-parent="#accordion2" href="#content'.$elements->id.'">test</a>';
        echo "</div>\n";
        echo "<div class='pull-right'>\n";
          echo '<a id="link-content'.$elements->id.'" class="btn btn-mini" data-toggle="collapse" data-parent="#accordion2" href="#content'.$elements->id.'"><i class="icon-folder-open"></i> Expand</a>';
          echo " <button type='submit' class='btn btn-mini btn-primary'>Update</button> <button class='btn btn-mini btn-danger delete_element' type='button' value='$elements->id'>Delete Element</button>\n";
        echo "</div>\n";
      echo "</form>";
    echo '</div>';
    echo "<div class=lra-clear></div>";
    echo '<div id="content'.$elements->id.'" class="accordion-body collapse lra-marginbottom10">
      <div class="accordion-inner element_fields">
        ';
        $content['touchpoint_id'] = $tp_details->id;
        $content['element_id'] = $elements->id;
        $content['entries'] = $this->audits_model->Get_Entries_of_an_Element($elements->id);
        $content['action'] = "manage";
      //var_dump($content['entries']);
      if ($elements->element_type)
        $this->load->view('form-creation/templates/'.$elements->element_type,$content);
        echo '
      </div>
    </div>';
    echo "</div>\n";
    
  }
  echo "</div>\n</div>";
}
?>
<!--<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-transitions-v2.3.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-collapse-v2.3.1.min.js" type="text/javascript"></script>
-->
<script type="text/javascript">
  
      



    
  $(document).ready(function() {
    //$('#testme').popover();
    $('.bs-tooltip').tooltip({html: true});

    $(".accordion").collapse();
    var anchor = window.location.hash.replace("#", "");
    //console.log(anchor);
    if (anchor){
      //$("#content"+anchor).collapse('show');
      $('#link-'+anchor).trigger('click');
    }
      //alert(anchor);
      
      //
    
    //$(".collapse").collapse('hide');
    //$("#" + anchor).collapse('show');
    $(".expand_caption").click(function(){
      $("span", this).toggle();
      
    });

    $(".element_caption -- disabled").on("click", function(){
      //alert($(this).attr('id'));
      var caption_id = $(this).attr('id').replace('caption-','');
      //alert(caption_id);
      $(".lra-marginbottom10").collapse('hide');
      $('#content'+caption_id).collapse('toggle');
      //alert($('#link-content'+caption_id).attr('href'));
      //$('#link-content'+caption_id).trigger('click');  
    });



    $("#add_element").hide();
    $("#element_type").change(function(){
      var selected_element = $(this).val();
       
      if (selected_element != "") {
        $("#the_caption").val($("#element_type").val());          
        $("#the_element_type").val(selected_element);

        $("#add_element").show();
        /*$.post("../../Show_Elements",{element_type:selected_element},function(result){
        
          //$("#the_caption").val($('#element_type option:selected').text()); //works in Chrome and FF
         //alert($("#element_type").text());
         $("#the_caption").val($("#element_type").val());
          
          $("#the_element_type").val(selected_element);

          $("#add_element").show();
        });*/
      } else {
        $("#add_element").hide();
      }
    });

    $("#touch_point_id").on("change",function(){
      //temp_touchpoint_id
      alert($('#touch_point_id option:selected').val());
    });
    
    // DELETE AN ELEMENT
    $(".delete_element").on("click",function(){
        
        var confirm_delete = confirm("Are you sure you want to delete this element?");
        if (confirm_delete == true){
            var element_id = $(this).val();
            $.post("<?php echo AJAX_URL; ?>audit_templates/ajax_delete_elements",{element_id:element_id},function(result){
              window.location.reload();
              /*$("#the_caption").val($('#element_type option:selected').text());
              $("#the_element_type").val(selected_element);

              $("#add_element").show();*/
            });
        }
       
         
    });
    
     $("#cancel-touch-point").on("click", function(event){
      alert('cancel');
      $("#category-container").remove();
    });

      
    
    $('form#touch_point_form #add_touch_point').on("click",function(e){
    e.preventDefault();
      var test = $('select option:selected').val();
      //alert('test');
      $('#the_notifications').html('');
      var error_text = '';
      var hasError = false;
      
      
      var category_droplist = $('#category_droplist option:selected').val();
      if(category_droplist == '') {      
        error_text += 'Category field is required.';
        hasError = true;
      }
      
      var touch_point_id = $('#touch_point_id option:selected').val();
      if(touch_point_id == '') {      
        error_text += 'Touch Point field is required.';
        hasError = true;
      }
    
    
      if(hasError == false) {
        console.log('no errors');
        
        $('#touch_point_form').submit();  
        return true;
      }
      else{
        console.log('has errors');
        //$('#the_notifications').html('<div class="alert alert-error">'+error_text+'</div>');
        //$('html').animate({scrollTop:0}, 'slow'); //IE, FF
          //$('body').animate({scrollTop:0}, 'slow'); //chrome, don't know if safary works
        alert(error_text);
        return false;

        
      }
      //return false;
    
    }); 

    //Element Sorter
    //$('.element_entry').css('cursor','pointer');
    
    $("#accordion2").sortable({ 
      connectWith: '#accordion2',
      //helper: fixHelper,
      opacity: 0.6, 
      cursor: 'move',
      items: '.sortable_element',
      //handle: '#move_element',      
      update: function() {
        var order = $(this).sortable("serialize"); 
        //alert(order);
        $.post("<?php echo AJAX_URL; ?>audit_templates/update_sortorder_elements", order, function(theResponse){   
          
        });                          
      }                 
    });
    //End Element Sorter
    
  });

  function deleteEntry(entry_id){
      var confirm_delete = confirm("Are you sure you want to delete?");
      if (confirm_delete == true)
        {
          var the_id = entry_id;
          //alert(the_id);
        $.post("<?php echo AJAX_URL; ?>audit_templates/ajax_delete_entry",{the_id:the_id},function(result){
          //$('#entry_' + the_id).remove();
          window.location.reload();          
        });

        }
  }
</script>
     

