<?
$this->check_permission->Check_Button_Add($add_caption,$current_app_info->add_role);
?>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<th>Template</th>
			<th>Date Created</th>
			
			<th>Published</th>
			
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->id."'>\n";
			echo "<td>".$ctr."</td>\n";
			
			echo "<td>".$entries->template_name."</td>\n";
			echo "<td>".$entries->date_created."</td>\n";
			
			echo "<td>";
			if ($entries->published == 1){
				echo '<span class="label label-alert">Yes</span>';
			}
			echo "</td>\n";

			echo "<td>";
				$this->check_permission->Check_Button_Manage(' Manage',1,$entries->id);

				$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id);

				$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);
				
			echo "</td>\n";

			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("<?php echo AJAX_URL; ?>audit_templates/ajax_delete_template",{template_id:the_id},function(result){
				window.location.reload();
				//$('#tr_' + the_id).remove();
				
				//$("#dialog").dialog("close");
			});
		  }
		

    });
</script>