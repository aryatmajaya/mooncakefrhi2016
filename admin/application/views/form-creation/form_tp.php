<?php
$this->utilities->BreadCrumbs($bread_crumbs);
?>

<form id="touch_point_form" class="form-inline" method="POST" action="<?=base_url().$this->router->fetch_class().'/add_tp_element_in_template/'?>" enctype="multipart/form-data" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  ?>
  
  
  
</form>

<table class="table table-striped table-condensed table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>Touch Points</th>
      
      <th>&nbsp;</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <?php
  if ($touchpoints) {
    echo "<tbody>\n";
    
    $ctr = 1;
    foreach ($touchpoints as $touchpoint){
      echo "<tr id='tr_".$touchpoint->id."'>\n";
      echo "<td>".$ctr."</td>\n";
      
      echo "<td>".$touchpoint->touch_point_name."</td>\n";
      
      echo "<td>";
      /*if ($entries->status == 1){
        echo '<span class="label label-success">Active</span>';
      } else {
        echo '<span class="label">Inactive</span>';
      }*/
      echo "</td>\n";

      echo "<td>";
      echo "<a href='".base_url().$this->router->fetch_class()."/form_tp_form/".$touchpoint->id."' class='btn btn-mini'>Open Form</a>";
      //$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->brand_id);

      //$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->brand_id);
        
      echo "</td>\n";

      echo "</tr>\n";
      $ctr++;
    }

    echo "</tbody>\n";
  } ?>
</table>




<script type="text/javascript">
  $(document).ready(function() {
    $("#element_type").on("change",function(){
      var selected_element = $(this).val();
        
      $.post("../../Show_Elements",{element_type:selected_element},function(result){
        //$("#touch_point_id").remove();
        //$("#add_touch_point").remove(); // removing the add touch point button
        //$("#element_container").html(result);
        
        $("#the_caption").val($('#element_type option:selected').text());
        $("#the_element_type").val(selected_element);

        
      });

    });

    $("#touch_point_id").on("change",function(){
      //temp_touchpoint_id
      alert($('#touch_point_id option:selected').val());
    });
    
    /*$("#add-touch-point").click(function(){
        
        $( "#dialog" ).dialog({
      resizable: false,
      height:140,
      modal: true,
      buttons: {
        "Delete all items": function() {
          $( this ).dialog( "close" );
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });*/
      /*
      $.post("../../editor_loader",{editor:'touch-point'},function(result){
        $("#lra-audit-content").append(result);
      });
  
    });*/
    
     $("#cancel-touch-point").on("click", function(event){
      alert('cancel');
      $("#category-container").remove();
    });
    
    $('form#touch_point_form #add_touch_point').on("click",function(e){
    e.preventDefault();
      var test = $('select option:selected').val();
      //alert('test');
      $('#the_notifications').html('');
      var error_text = '';
      var hasError = false;
      
      
      var category_droplist = $('#category_droplist option:selected').val();
      if(category_droplist == '') {      
        error_text += 'Category field is required.';
        hasError = true;
      }
      
      var touch_point_id = $('#touch_point_id option:selected').val();
      if(touch_point_id == '') {      
        error_text += 'Touch Point field is required.';
        hasError = true;
      }
    
    
      if(hasError == false) {
        console.log('no errors');
        
        $('#touch_point_form').submit();  
        return true;
      }
      else{
        console.log('has errors');
        //$('#the_notifications').html('<div class="alert alert-error">'+error_text+'</div>');
        //$('html').animate({scrollTop:0}, 'slow'); //IE, FF
          //$('body').animate({scrollTop:0}, 'slow'); //chrome, don't know if safary works
        alert(error_text);
        return false;

        
      }
      //return false;
    
    });   
    
  });

</script>
     

