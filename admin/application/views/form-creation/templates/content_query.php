<?php

if ($action != "answer") {
	?>
	
<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
	
	<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
	<input type='hidden' name='element_id' value='<?=$element_id?>' />
	<input type="hidden" name="element_type" value="employee_contribution" />
	
	<? $this->ektron_utilities->content_query_fields('add'); ?>

	</form>
	
	<div class\"lra-clear\">&nbsp;</div>
	<hr>
	
	<div id="employee_contribution_elements">

	<?php
	if ($entries > 0){
		$ctr = 1;
		foreach ($entries as $entry) {
			//echo "<div id='entry_$entry->id' class='element_entry'>$entry->field_name</div>";
			
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entry->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entry->id."\" />\n";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			
			if ($entry->entry_type == "yes_no")
				$this->ektron_utilities->content_query_fields('update',$entry->field_name,$entry->entry_type, $ctr++);			
			else
				$this->ektron_utilities->content_query_fields('update',$entry->dropdown_list,$entry->entry_type, $ctr++);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			
			echo "</div>";

			echo "</form>";

		}
		

	}
	?>
	</div>

<? 
} else { //FOR AUDITOR'S FORM
	if ($entries > 0){
		echo "<table class='table table-condensed table-hover table-no-border'>\n";
		$total_yes = 0;
		$total_no = 0;
		$total_entry = 0;
		foreach ($entries as $entry) {
			if ($entry->entry_type == "header_txt"){
				$ehcaption = explode("\n",$entry->dropdown_list);
				echo "<tr><td><strong>".(empty($ehcaption[0])?'':$ehcaption[0])."</strong></td><td><strong>".(empty($ehcaption[1])?'':$ehcaption[1])."</strong></td><td><strong>".(empty($ehcaption[2])?'':$ehcaption[2])."</strong></td></tr>";
			}
			else if ($entry->entry_type == "yes_no"){
					echo "<tr>\n";
					echo "<td>$entry->field_name</td>\n";
					echo "<td style=\"text-align: center;\">";

					echo "<select name='entry-$entry->id-answer_text' class='droplist'>\n";
					//FOR YES
					echo "<option value='yes'";
					if ($entry->answer_text == 'yes')
						echo ' selected';
					echo ">Yes</option>\n";

					//FOR NO
					echo "<option value='no'";
					if ($entry->answer_text == 'no')
						echo ' selected';
					echo ">No</option>\n";

					//FOR N/A
					echo "<option value='na'";
					if ($entry->answer_text == 'na')
						echo ' selected';
					echo ">N/A</option>\n";

					echo "</select>\n";
					echo "</td>\n";

					echo "<td><textarea name='entry-$entry->id-answer_full_text' class='span5'>$entry->answer_full_text</textarea></td>\n";
					echo "</tr>\n";

					//calculating
					if ($entry->answer_text != "na")
						$total_entry = $total_entry + 1;
					if ($entry->answer_text == "yes")
						$total_yes = $total_yes + 1;
					if ($entry->answer_text == "no")
						$total_no = $total_no + 1;

				}
				elseif ($entry->entry_type == "footer_txt"){
					

					$efcaption = explode("\n",$entry->dropdown_list);
					echo "<tr><td style=\"text-align: right; font-weight: bold;\">".(empty($efcaption[0])?'':$efcaption[0])."</td><td style=\"text-align: center;\">$total_yes</td><td>&nbsp;</td></tr>";
					echo "<tr><td style=\"text-align: right; font-weight: bold;\">".(empty($efcaption[1])?'':$efcaption[1])."</td><td style=\"text-align: center;\">$total_no</td><td>&nbsp;</td></tr>";
					echo "<tr><td colspan=\"3\"><div style=\"border-top: 3px double #999;\"></div></td></tr>";
					echo "<tr><td style=\"text-align: right; font-weight: bold;\">".(empty($efcaption[2])?'':$efcaption[2])."</td><td style=\"text-align: center;\">".number_format((($total_yes/$total_entry)*100),1)."%</td><td>&nbsp;</td></tr>";
				}
				
				
				
		}
		echo "</table>\n";
		
	}
}
?>

<script type="text/javascript">
	


    $(document).ready(function() {

		//$('.element_entry').css('cursor','pointer');
	/*
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};*/
		
		$("#employee_contribution_elements").sortable({ 
			connectWith: '#employee_contribution_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("../../update_sortorder_entries", order, function(theResponse){
					
					
				});															 
			}								  
		});

		//$('#gp_pointscale_description').hide();

    	<?php //if ($mode == "update") { ?>

    	$('.entry_type').on('change', function(){
    		var change_div = $(this).attr('id');
    		if ($(this).val() == "header_txt"){
    			//var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv');
			 
				//newTextBoxDiv.after().html('<label>Textbox # : </label><input type="text" name="textbox" id="textbox" value="" />');
				//newTextBoxDiv.appendTo("#TextBoxesGroup");
				var thenew_html = '<textarea name="dropdown_list" style="width:300px;" rows="4">Standard Description\nStatus\nLRA Comments</textarea>';	

				$("."+change_div).html(thenew_html);
				//$('.bs-tooltip-change-field').tooltip({html: true});
    		}
    		else if ($(this).val() == "footer_txt"){    			
				var thenew_html = '<textarea name="dropdown_list" style="width:300px;" rows="4">Number Passed:\nNumber Failed:\nPercent Compliant</textarea>';	

				$("."+change_div).html(thenew_html);
    		}
    		else
    			var thenew_html = '<input type="text" placeholder="Caption" maxlength="255" style="width:300px;" name="field_name" value="" />';

				$("."+change_div).html(thenew_html);
    	});

    	<? //} ?>
		
		//alert($('#sort_table >tbody >tr').length);	
		
	});
</script>