<?php



if ($action != "answer") {
	if ($entries == 0) {
?>

	<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
		
		<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
		<input type='hidden' name='element_id' value='<?=$element_id?>' />
		<input type="hidden" name="element_type" value="experience_narrative" />
		<? $this->ektron_utilities->gen_desc_create_fields('add','Narrative Commentary Goes Here'); ?>
		<hr>
	</form>
	<? } else {
		
		//foreach ($entries as $entry) {
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entries[0]->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entries[0]->id."\" />\n";

			echo "<div id=\"entry_".$entries[0]->id."\" class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			$this->ektron_utilities->gen_desc_create_fields('update',$entries[0]->field_name);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry(".$entries[0]->id.");\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";			
		//}	
	} 	
		
} else { // FOR AUDITOR'S FORM
	if ($entries > 0)
		echo "<div><textarea name='entry-".$entries[0]->id."-answer_full_text' placeholder='".$entries[0]->field_name."' class='span8' rows='7'>".$entries[0]->answer_full_text."</textarea></div>";

} ?>