<?php




if ($action != "answer") {
	?>
	<form id='the_form' class="form-inline" method='POST' action='<?=base_url().$this->router->fetch_class()."/add_tp_element_entries/"?>' />
	
	<input type='hidden' name='touchpoint_id' value='<?=$touchpoint_id?>' />
	<input type='hidden' name='element_id' value='<?=$element_id?>' />
	<input type="hidden" name="element_type" value="enhancers" />	
	<? $this->ektron_utilities->title_tags_create_fields('add'); ?>
	</form>
	<hr>
	<div id="enhancers_elements">

	<?php
	if ($entries > 0){
		foreach ($entries as $entry) {
			echo "<form class=\"form-inline lra-entry-form\" method=\"POST\" action=\"".base_url().$this->router->fetch_class()."/update_entries/"."\">";
			echo "<input type='hidden' name='element_id' value=\"".$entry->element_id."\" />\n";
			echo "<input type='hidden' name='touchpoint_id' value='$touchpoint_id' />\n";
			echo "<input type='hidden' name='entry_id' value=\"".$entry->id."\" />\n";

			echo "<div id='entry_$entry->id' class='element_entry row-fluid'>\n";
			echo "<div class=span11>";
			$this->ektron_utilities->title_tags_create_fields('update',$entry->field_name, $entry->entry_type);			
			echo "</div>\n";
			echo "<div class=\"span1\"><a href=\"javascript:deleteEntry($entry->id);\"><i class=\"icon-remove icon-white red-background\"></i></a></div>\n";
			//echo "<div class\"lra-clear\">&nbsp;</div>";
			echo "</div>";
			echo "</form>";

		}
		
	}
	?>
	</div>

<? } else { //FOR AUDITOR'S FORM
	if ($entries > 0){
		echo "<table>\n";
		foreach ($entries as $entry) {
			if ($entry->entry_type == "check_box"){
				echo "<tr><td colspan='2'><label class='checkbox'><input id='ec-$entry->id' class='enhancer_checkbox' value='5' type='checkbox'";
				if ($entry->answer_integer == 5)
					echo ' checked="checked"';
				echo " /> $entry->field_name</label>";
				echo "<input type='hidden' name='entry-$entry->id-answer_integer' class='hec-$entry->id' value='".(($entry->answer_integer == 0)?1:$entry->answer_integer)."' />\n";
				echo "</td></tr>\n";
			}
			else {
				echo "<tr><td>$entry->field_name</td><td><textarea name='entry-$entry->id-answer_full_text' class='span6'>$entry->answer_full_text</textarea></td></tr>\n";
			}
			
		}
		echo "</table>";
		
	}
}
?>


<script type="text/javascript">
	/*$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("ajax_delete",{the_id:the_id},function(result){
				$('#tr_' + the_id).remove();
				window.location.reload();
				
				
			});

		  }
		

    });*/


    $(document).ready(function() {
    	



		//$('.element_entry').css('cursor','pointer');
	/*
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};*/
		
		$("#enhancers_elements").sortable({ 
			connectWith: '#enhancers_elements',
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: '.element_entry',
			//handle: '#move_element',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				//alert(order);
				$.post("<?php echo AJAX_URL; ?>audit_templates/update_sortorder_entries", order, function(theResponse){		
					
				});													 
			}								  
		});

		//AUDITOR'S FORM
		$('.enhancer_checkbox').on('change', function(){    
          var the_obj = $(this).attr('id');
          //alert(the_obj);
          var check_status = this.checked ? true : false;
          if (check_status == true)
          	$('.h'+the_obj).val('5'); //check
          else
          	$('.h'+the_obj).val('1'); //uncheck
        });
		
		
		
	});
</script>