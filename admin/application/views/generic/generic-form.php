<?
$the_characters = array("_","|");
$the_characters2 = array("_","|",'id');

if ($action == "add")
  $action_url = site_url($this->router->fetch_class()).'/insert/';
elseif ($action == "edit"){
  if (isset($update_url))
    $action_url = site_url($this->router->fetch_class()).$update_url;
  else
    $action_url = site_url($this->router->fetch_class()).'/update/';
}
?>
<form class="form-horizontal" method="POST" action="<?=$action_url?>" enctype="multipart/form-data" />
  
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }

  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }

  if (isset($return_url))
    echo "<input type=\"hidden\" name=\"return_url\" value=\"$return_url\" />";

  if ($action == "edit")
    echo '<input type="hidden" name="id" value="'.$entries->id.'" />';
  ?>

  <?

  foreach($the_fields as $the_fields){
    if ($the_fields != "status|"){
      
      $field_type = explode('|',$the_fields);
       
      if ($field_type[1] == ""){

          echo '<div class="control-group">
        <label class="control-label" for="inputEmail">'.str_replace($the_characters2," ",ucwords($field_type[0])).'</label>
        <div class="controls">
          <input type="text" name="'.$field_type[0].'" value="';
          if ($action == "add")
            echo set_value($field_type[0]);
          else
            echo $entries->$field_type[0];
          echo '">
        </div>
      </div>';   
      } 
      elseif ($field_type[1] == "password"){

          echo '<div class="control-group">
        <label class="control-label">'.str_replace($the_characters2," ",ucwords($field_type[0])).'</label>
        <div class="controls">
          <input type="password" name="'.$field_type[0].'" value="" />
        </div>
      </div>';   
      }
      elseif ($field_type[1] == "dropdown") {
          echo '<div class="control-group">
    <label class="control-label">'.str_replace($the_characters2," ",ucwords($field_type[0])).'</label>
    <div class="controls">
      <select name="'.$field_type[0].'">';
        echo '<option></option>';
        if ($drop_list1){
          foreach ($drop_list1 as $drop_list1){
            echo "<option value='$drop_list1->list_id'";
            if ($action == "add"){
              if ($drop_list1->list_id == set_value($field_type[0]))
                echo " selected";              
            }elseif ($action == "edit"){
              if ($drop_list1->list_id == $entries->$field_type[0])
                echo " selected";
            }
            echo ">$drop_list1->list_name</option>\n";
          }
        }
        echo '
      </select>
    </div>
  </div>';   
      }
    }
    else {
      echo '<div class="control-group">
      <label class="control-label" for="inputEmail">Status</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="status" value="1"';

          if ($action == "add"){
            echo set_radio('status', '1', TRUE);
          }
          else{
            if ($entries->status == 1)
              echo ' checked="checked"';           
          }          
          
          echo '/>Active
          
        </label>
        <label class="radio">
          
          <input type="radio" name="status" value="0"';
          
          if ($action == "add"){
            echo set_radio('status', '0', TRUE);
          }
          elseif ($action == "edit"){
            
            if ($entries->status == 0)
              echo ' checked="checked"';           
          }          
          
          echo '/>Inactive
        </label>
      </div>
    </div>'; 
    }
  }
  ?>

  
  
  
  

  
    

    

    <div class="form-actions">
      <button type="submit" name="save" class="btn btn-primary">Save</button>
      <a href="<?php echo (isset($return_url))?site_url($this->router->fetch_class()).'/'.$return_url:site_url($this->router->fetch_class()); ?>" class="btn">Cancel</a>
    </div>
</form>
