<?php
/* This framework has sorting tool */


$the_characters = array("_","|");


  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }

$url = site_url($this->router->fetch_class());

if (isset($add_caption)){
	$this->check_permission->Check_Button_Add($add_caption,$current_app_info->add_role, $url);
}

?>

<?
if (isset($the_fields)){
?>
<table  id="sort_table" class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<?

			foreach ($the_fields as $field){
				echo "<th>".str_replace($the_characters," ",ucwords($field))."</th>\n";
			}
			//$the_fields = reset($the_fields);
			?>
			
			<th>&nbsp;</th>
			<th>Sort</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->id."'>\n";
			echo "<td>".$ctr."</td>\n";
			
			//var_dump($the_fields);
			
			foreach ($the_fields as $field){
				if ($field != "status"){
					$field_name = str_replace("|","",$field);
					echo "<td>".$entries->$field_name."</td>\n";
				}else{
					
					echo "<td>";
					if ($entries->status == 1){
						echo '<span class="label label-success">Active</span>';
					} else {
						echo '<span class="label">Inactive</span>';
					}
					echo "</td>\n";
				}
			}
			

			echo "<td>";
			$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id, $url);

			$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);

				
			echo "</td>\n";
			echo "<td id='move_td' style='background-color: #CCC; width: 30px; text-align: center;'><img src='".base_url()."assets/images/move_icon.png' id='move_icon'></td>\n";
			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>
<? } ?>

<!-- FOR JQUERY SORTABLE -->
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("<?php echo $url;?>/ajax_delete",{the_id:the_id},function(result){
				$('#tr_' + the_id).remove();
				window.location.reload();
				
				
			});

		  }
		

    });


    $(document).ready(function() {
		$('#move_td').css('cursor','pointer');
	
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
		
		$("#sort_table tbody").sortable({ 
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: 'tr:not(.tableHeader,.not_sortable)',
			handle: '#move_td',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				$.post("update_sortorder", order, function(theResponse){
					//$("#contentRight").html(theResponse);
					var num_ctr = 1;
					$('#sort_table >tbody >tr').each(function( index ) {

						$(this).closest('tr').children('td:first').html(num_ctr);
						num_ctr++
				  		//console.log(value);
					});
				}); 															 
			}								  
		});
		
		//alert($('#sort_table >tbody >tr').length);	
		
	});
</script>