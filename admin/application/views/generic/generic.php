<?php

$the_characters = array("_","|");


  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }

if (isset($add_caption))
	$this->check_permission->Check_Button_Add($add_caption,$current_app_info->add_role);

?>

<?
if (isset($the_fields)){
?>
<table id="theTable" class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<?

			foreach ($the_fields as $field){
				echo "<th>".str_replace($the_characters," ",ucwords($field))."</th>\n";
			}
			//$the_fields = reset($the_fields);
			?>
			
			<th>&nbsp;</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->id."'>\n";
			echo "<td>".$ctr."</td>\n";
			
			//var_dump($the_fields);
			
			foreach ($the_fields as $field){
				if ($field != "status"){
					$field_name = str_replace("|","",$field);
					echo "<td>".$entries->$field_name."</td>\n";
				}else{
					
					echo "<td>";
					if ($entries->status == 1){
						echo '<span class="label label-success">Active</span>';
					} else {
						echo '<span class="label">Inactive</span>';
					}
					echo "</td>\n";
				}
			}
			

			echo "<td>";
			$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id);

			$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);

				
			echo "</td>\n";

			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>
<? } ?>
<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("ajax_delete",{the_id:the_id},function(result){
				//$('#tr_' + the_id).remove();
				window.location.reload();
				//alert($('#theTable >tbody >tr').length);	
				/*
				$('#theTable >tbody >tr').each(function( index ) {
				  console.log( index + ": " + $(this).text() );
				});
				*/
			});

		  }
		

    });
</script>