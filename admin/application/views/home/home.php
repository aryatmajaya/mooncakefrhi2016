<!DOCTYPE html>
<html>
  <head>
    <title><?php echo PROJECT_TITLE; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	<link href="<?php echo base_url(); ?>assets/css/lra-styles.css" rel="stylesheet" media="screen">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-1024.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container">
    	<div class="span6 offset3 login-top text-center"><h3>&nbsp;</h3></div>
    	<div class="span6 offset3 well well-small">
            <h3 class="heading text-center"><?= PROJECT_TITLE ?> - Admin</h3>
            <hr>
    		<?php
    		if (validation_errors()){
    			echo '<div class="alert alert-error">'.validation_errors().'</div>';
    		}
    		if ($this->session->flashdata('login_error')){
    			echo '<div class="alert alert-error">Invalid Username or password.</div>';
    		}
    		if ($this->session->flashdata('inactive')){
    			echo '<div class="alert alert-error">Your account has been disabled.</div>';
    		}

    		?>

		    <form class="form form-horizontal" action="" method="POST">
			  <div class="control-group">
			    <label class="control-label" for="inputEmail">Username</label>
			    <div class="controls">
			      <input type="text" id="username" name="username" placeholder="Username">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="inputPassword">Password</label>
			    <div class="controls">
			      <input type="password" id="password" name="password" placeholder="Password">
			    </div>
			  </div>
			  <div class="control-group">
			    <div class="controls">
			      
			      <button type="submit" class="btn btn-primary">Sign in</button>
			    </div>
			  </div>
			</form>
		</div>
		

    </div>





    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>	