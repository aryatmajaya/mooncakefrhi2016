<!DOCTYPE html>
<html>
  <head>
    <title><?php echo PROJECT_TITLE; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	<link href="<?php echo base_url(); ?>assets/css/lra-styles.css" rel="stylesheet" media="screen">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-1024.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container">
    	<div class="login-top"></div>
    	<div class="span7 offset2 well well-small">
    		

		    <form class="form-horizontal" action="" method="POST">
			  <div class="control-group">
			    <label class="control-label" for="inputEmail">Enter encrypted password:</label>
			    <div class="controls">
			      <input type="text" id="password" name="password">
			    </div>
			  </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="inputEmail">Decoded Password:</label>
                            <div class="controls">
                                <?=isset($decoded_password)?$decoded_password:''?>
                            </div>
                        </div>
                        
                            
			  
			  <div class="control-group">
			    <div class="controls">
			      
			      <button type="submit" class="btn btn-primary">Generate</button>
			    </div>
			  </div>
			</form>
		</div>
		

    </div>





    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>	