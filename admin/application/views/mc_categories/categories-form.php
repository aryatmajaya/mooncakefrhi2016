
<form class="form-horizontal" method="POST" action="<?=($action == "add")?site_url('mc_product_categories/add'):site_url('mc_product_categories/update')?>" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }
  ?>

  <?php if(isset($entries)&& $entries->id): ?>
	<input type="hidden" name="category_id" value="<?= $entries->id ?>">
  <?php endif; ?>

<div class="control-group">
    <label class="control-label">Category Name</label>
    <div class="controls">
        <input type="text" name="form[name]" maxlength="200" value="<?=($action == "add")?set_value("form['name']"):$entries->name?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Category Name China</label>
    <div class="controls">
        <input type="text" name="form[china_name]" maxlength="200" value="<?=($action == "add")?set_value("form['china_name']"):$entries->china_name?>">
    </div>
</div>

<div class="control-group">
  <label class="control-label" for="">Status</label>
  <div class="controls">
     <label class="radio">
      <input type="radio" name="form[status]" value="1"
      <?
      if ($action == "add"){
        echo ' checked="checked"';
      }
      else{
        if ($entries->status == 1)
          echo ' checked="checked"';           
      }          
      ?>
       />Active
      
    </label>
    <label class="radio">
      
      <input type="radio" name="form[status]" value="0"
      <?
      if ($action == "edit"){
        
        if ($entries->status == 0)
          echo ' checked="checked"';           
      }          
      ?>
       />Inactive
    </label>
  </div>
</div>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Save</button>
      <a href="<?php echo site_url("mc_product_categories");?>" class="btn">Cancel</a>
    </div>
</form>