<?php
  if ($this->session->flashdata('success_notification')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<?php
$url = site_url("mc_product_categories");
$this->check_permission->Check_Button_Add(' Add Category', $current_app_info->add_role, $url);
?>
<hr>
<table class="table table-striped table-condensed table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>China Name</th>
            <th>Status</th>
			<th></th>
		</tr>
	</thead>
	<?php if ($entries): ?>
		<tbody>
			<?php foreach ($entries as $key => $value): ?>
				<tr>
					<td> <?= $value->id ?></td>
					<td> <?= $value->name ?></td>
                    <td> <?= $value->china_name ?></td>
					<td>
						<? if($value->status == 1): ?>
							<span data-id="<?= $value->id; ?>" class="label label-success tooltip-button close-button" data-toggle="tooltip" title data-original-title="Click to De-activate">Active</a>
						<? else: ?>
							<span data-id="<?= $value->id; ?>" class="label tooltip-button open-button" data-toggle="tooltip" title data-original-title="Click to Activate">Inactive</a>
						<? endif; ?>
					</td>
					<td class="text-right">
						<?php $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$value->id, $url); ?>
					</td>
				</tr>			
			<?php endforeach ?>
		</tbody>
	<?php endif ?>
</table>
<script type="text/javascript">
	$(function(){
		$('.close-button').click(function(e){
			e.preventDefault();
			var confirm_close = confirm("Are you sure you want to de-activate this product?");
			if (confirm_close == true){
				var the_id=$(this).data('id');
				$.post("<?php echo $url;?>/ajax_close_category",{the_id:the_id},function(result){
					window.location.reload();
				});
			}
		});
		
		$('.open-button').click(function(e){
			e.preventDefault();
			var the_id=$(this).data('id');
			$.post("<?php echo $url;?>/ajax_open_category",{the_id:the_id},function(result){
				   window.location.reload();
			});

		});

		$('.tooltip-button').tooltip();
	});
</script>