
<?php if($this->session->flashdata('success_notification')): ?>
<div class="alert alert-success"><?= $this->session->flashdata('success_notification') ?></div>
<?php endif; ?>

<?php
$url = site_url('mc_card_discount');
$this->check_permission->Check_Button_Add(' Card discount',$current_app_info->add_role, $url);
?>
<hr>
<table class="table table-striped table-condensed">
	<thead>
		<th></th>
		<th>Promo name</th>
		<th>Discount Code</th>
		<th>Discount</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Status</th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($entries as $key => $value): ?>
			<tr>
				<td><?= ($key + 1)?></td>
				<td><?= (($value->promo_name) ? $value->promo_name : '-') ?></td>
				<td><?= $value->discount_code ?></td>
				<td><?= $value->discount ?>%</td>
				<td><?= $value->start_date ?></td>
				<td><?= $value->end_date ?></td>
				<td><?= ($value->expired) ? 'Expired' : 'Active' ?></td>
				<td>
					<?= $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role, $value->id, $url); ?>
					<?= $this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$value->id); ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<script>
	$(function(){
		$(".delete-button").on('click', function(e){
			var conf = confirm("Do you want to delete the card discount?");
			if(conf){
				$.post('<?= site_url("mc_card_discount/delete/");?>'+$(this).val(), function(data){
					location.href="<?= site_url('mc_card_discount')?>";
				});
			}
		});
	});
</script>