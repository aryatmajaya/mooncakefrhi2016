<?php
  if ($this->session->flashdata('success_notification')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<?php
$url = site_url($this->router->fetch_class());
$this->check_permission->Check_Button_Add(' Add CMS', $current_app_info->add_role, $url);
?>
<hr>
<table class="table table-striped table-condensed table-hover">
	<thead>
		<tr>
			<th class="span1">#</th>
			<th class="span4" style="text-align:left;">Title</th>
			<th class="span2" style="text-align:left;">Slug/Unique Url</th>
			<th class="span1" style="text-align:left;">Published</th>
			<th class="span2" style="text-align:left;">Published Date</th>
			<th class="span2"></th>
		</tr>
	</thead>
	<?php if ($entries): ?>
		<tbody>
			<?php foreach ($entries as $key => $value): ?>
				<tr>
					<td> <?= ($key+1) ?></td>
					<td> <?= $value->title ?></td>
					<td> <?= $value->slug ?></td>
					<td> <?= $value->published ?></td>
					<td> <?= $value->published_date ?></td>
					<td class="text-right">
						<?php $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$value->id, $url); ?>
						<?= $this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$value->id); ?>
					</td>
				</tr>			
			<?php endforeach ?>
		</tbody>
	<?php endif ?>
</table>

<script>
	$(function(){
		$(".delete-button").on('click', function(e){
			var conf = confirm("Do you want to delete this card ?");
			if(conf){
				$.post('<?= site_url($this->router->fetch_class())?>/delete/'+$(this).val(), function(data){
					location.href="<?= site_url($this->router->fetch_class())?>";
				});
			}
		});
	});
</script>