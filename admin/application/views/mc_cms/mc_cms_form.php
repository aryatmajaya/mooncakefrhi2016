<div class="span12">
	<?php if (validation_errors()): ?>
		<div class="alert alert-error"><?= validation_errors()?></div>
	<?php endif ?>
	<form action="<?=($action == "add")?site_url($this->router->fetch_class()).'/add/':base_url($this->router->fetch_class()).'/update/'?>" class="form-horizontal" method="POST">	
		<?php if ($action == 'edit'): ?>
			<input type="hidden" name="cms[id]" value="<?= $cms->id?>"/>
		<?php endif ?>
		<div class="control-group">
			<label class="control-label" for="cms_title">Title</label>
			<div class="controls">
				<input type="text" id="cms_title" name="cms[title]" value="<?= ($action=='add')? set_value('cms[title]') : $cms->title ?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="category_id">Category</label>
			<div class="controls">
				<select name="cms[category_id]" id="category_id">
					<?php foreach ($categories as $key => $category): ?>
						<option value="<?= $category->id?>" 
							<?= ($action == 'edit') ? (($category->id == $cms->category_id) ? 'selected' : '') : "" ?> 
							><?= $category->name?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cms_slug">URL/Slug</label>
			<div class="controls">
				<input type="text" id="cms_slug" name="cms[slug]" value="<?= ($action=='add')? set_value('cms[slug]') : $cms->slug ?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cms_description">Description</label>
			<div class="controls">
				<textarea type="text" id="cms_description" name="cms[description]" rows="4" class="span6"><?= ($action=='add')? set_value('cms[description]') : $cms->description ?></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cms_html">HTML Content</label>
			<div class="controls">

				<textarea type="text" id="cms_html" name="cms[html_content]" rows="20" class="html-editor"><?= ($action=='add')? set_value('cms[html_content]') : $cms->html_content ?></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="">Is Published</label>
			<div class="controls">
				<select name="cms[published]" id="">
					<option value="1" >Publish</option>
					<option value="0">Un-publish</option>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cms_published_date">Published Date</label>
			<div class="controls">
				<div class="input-append">
					<input type="text" id="cms_published_date" class="date" name="cms[published_date]" value="<?= ($action=='add')? set_value('cms[published_date]') : $cms->published_date ?>">
				</div>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<button class="btn btn-primary">Submit</button>
			</div>
		</div>
	</form>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script>
	$('.date').datepicker({
        showOn: "both",
        dateFormat: "yy-m-d",
        changeMonth: true,
        changeYear: true,
        setDate: "yy-m-d",
        buttonText: '<i class="icon-calendar"></i>',
        onClose: function( selectedDate ) {
            $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
        }
    });

    $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
</script>