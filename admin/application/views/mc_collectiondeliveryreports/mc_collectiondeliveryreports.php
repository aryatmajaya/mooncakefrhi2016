<?
//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
?>
<div class="btn-group">
  <button type="button" id="open-search-tool" class="btn"><i class="icon-search"></i> Search</button>
  <button type="button" id="open-total-sales-tool" class="btn"><i class="icon-shopping-cart"></i> Total Sales</button>
  
</div>
<br /><br />

<div class="well well-small no-display" id="search-tool">
    <form class="form-inline" method="POST">
        <strong>Search By:</strong>
        <select name="search_field">
            <option value="mc_orders.order_ref">Order No.</option>
            <option value="mc_customers.mobile">Mobile</option>
            <option value="mc_customers.email">Email</option>
        </select>
        <input type="text" name="search" value="<?=set_value('search')?>" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
</div>

<div class="well well-small no-display" id="total-sales-tool">
    <form class="form-inline" method="POST" action="<?=BASE_URL?>mc_transactions/total_sales">
        <strong>Select Date:</strong>
        <input type="text" name="from_date" id="from_date" readonly="readonly" class="span2 from_date" />
        <input type="text" name="to_date" id="to_date" readonly="readonly" class="span2 to_date" />
        <input type="hidden" name="view_type" value="<?=$view_type?>" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
    <div>
        <span><strong>Total Sales: </strong></span>
        <span id="grand_total_shortcut"></span>
    </div>
</div>

<ul class="nav nav-tabs">
  <li<?=($view_type=="")?' class="active"':''?>>
    <a href="<?=base_url().$this->router->fetch_class()?>/index/all/">View All</a>
  </li>
  <li<?=($view_type=="paid-open")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/paid-open/'?>">Paid/Open</a></li>
  <li<?=($view_type=="process")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/process/'?>">Process</a></li>
  
  <li<?=($view_type=="collected")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/collected/'?>">Collected/Delivered</a></li>
  
  <li<?=($view_type=="cancelled")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/cancelled/'?>">Cancelled</a></li>
  <li<?=($view_type=="error")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/error'?>">Problem CC</a></li>
  <li<?=($view_type=="incomplete")?' class="active"':''?>><a href="<?=base_url().$this->router->fetch_class().'/index/incomplete/'?>">Incomplete</a></li>
</ul>
<?=(isset($pagination_links))?$pagination_links:'';?>
<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Date</th>
			<th>&nbsp;</th>
                        <th>Order Ref</th>
			<th>Customer</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
                $grand_total = 0;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->did."'>\n";
			echo "<td>".date('d.m.y',strtotime($entries->service_date))."</td>\n";
			echo "<td>".ucwords($entries->service_type)."</td>\n";
			
			echo "<td><a href=\"".base_url()."mc_collectiondeliveryreports/view/".$entries->did."\">".$entries->order_ref."</a></td>\n";
			echo "<td>$entries->first_name $entries->last_name</td>\n";
			//echo "<td>".ucwords($transactions->service_type)."</td>\n";
			//echo "<td>".date('d.m.y',strtotime($transactions->service_date))."</td>\n";
                        
                        
                        //$deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($transactions->order_ref);
                        //$total_qty = Mc_transactions_model::Get_Total_Qty($transactions->id);
                        //$total_qty = array_sum($total_qty);
                        //print_r($total_qty);
                        //$delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty->total_qty);
                        //$delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                        
                        //R1
                        //$total_amount = Mc_utilities::Discounted_Price($transactions->total,$transactions->discount) + $delivery_charge;
                        
                        //$total_item_amount = Mc_transactions_model::Get_Order_Total_Amount($transactions->id);
                        
                       // $total_amount = Mc_utilities::Discounted_Price($total_item_amount, $transactions->discount) + $delivery_charge;
                        
                       // $grand_total += $total_amount;
                        
//                        echo "<td>";
                        
			//if ($entries->status == 1){
			//	echo '<span class="label label-success">Active</span>';
			//} else {
                        /*$the_status = $this->mc_utilities->order_status($transactions->status);
				if ($the_status =="error")
                                    echo '<span class="label label-important">error<span>';
                                elseif ($the_status =="incomplete") {
                                    echo '<span class="label" style="width: 70px;">incomplete<span>';
                                }
                                elseif ($the_status =="paid-open") {
                                    echo '<span class="label label-success">paid<span>';
                                }
                                elseif ($the_status =="cancelled") {
                                    echo '<span class="label label-inverse" style="width: 60px;">cancelled<span>';
                                }
                                
                                elseif ($the_status =="process") {
                                    echo '<span class="label label-warning" style="width: 60px;">process<span>';
                                }
                                elseif ($the_status =="delivered") {
                                    echo '<span class="label label-info" style="width: 60px;">delivered<span>';
                                }
                                elseif ($the_status =="collected") {
                                    echo '<span class="label label-info" style="width: 60px;">collected<span>';
                                }
                                else
                                    echo $the_status;
                         * 
                         */
			//}
			//echo "</td>\n";

			
                    
                            
			

			echo "</tr>\n";
			$ctr++;
		}
                
                

		echo "</tbody>\n";
	} ?>
</table>
<?=(isset($pagination_links))?$pagination_links:'';?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        <? if (isset($total_sales)) { ?>
                $('#total-sales-tool').show();
        <? } ?>
        
        $('#grand_total_shortcut').text($('#grand-total').text());
        $('.from_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        
        $('#open-search-tool').click(function() {
           $('#search-tool').show();
           $('#total-sales-tool').hide();
        });
        
        $('#open-total-sales-tool').click(function() {
            $('#search-tool').hide();
            $('#total-sales-tool').show();
           
        });
    });
    
	
</script>