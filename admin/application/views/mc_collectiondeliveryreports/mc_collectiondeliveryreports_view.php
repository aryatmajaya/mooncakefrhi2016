<link href="<?=BASE_URL?>assets/css/mc_styles.css" rel="stylesheet" media="screen">

<div class="row">
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }

  
  ?>
<input type="hidden" name="order_id" value="<?=$entry->id?>" />
<input type="hidden" name="order_ref" value="<?=$entry->order_ref?>" />
<div style="width: 350px;" class="pull-left">
    <table>
        <tr><td width="150"><strong>Order Date:</strong></td><td width="200"><?=date('d.m.y',strtotime($entry->date_ordered))?></td></tr>
        <tr><td><strong>Order Ref:</strong></td><td><?=$entry->order_ref?></td></tr>
        <tr><td><strong>Approval Code:</strong></td><td><?=$entry->approval_code?></td></tr>
        <tr><td><strong>Discount:</strong></td><td><?=$entry->discount?>%</td></tr>

        <tr>
            <td><strong>Amount:</strong></td>
            <td><span id="amount"></span>              
            <?//=number_format(Mc_utilities::Discounted_Price($total->total,$entry->discount),2);?>
            </td>
        </tr>
        
        
        
        
    </table>


</div>
<div style="width: 350px;" class="pull-right">
    <table>
        <tr>
            <td width="120"><strong>Customer:</strong></td>
            <td width="230"><?=$entry->first_name.' '.$entry->last_name?></td>
        </tr>
        <tr><td><strong>Mobile #:</strong></td><td><?=$entry->mobile?></td></tr>
        <tr><td><strong>Alt Contact #:</strong></td><td><?=$entry->alt_contact?></td></tr>
        <tr><td><strong>Email:</strong></td><td><?=$entry->email?></td></tr>
    </table>
</div>
<div class="lra-clear"></div>
    
    <!-- START FULFILLMENT OPTIONS -->
    
    <div style="padding: 20px 0 0 0;">
        <h4>FULFILLMENT OPTIONS</h4>
    
            <?
            if ($delivery_items){
                $sub_total = 0;
                foreach ($delivery_items as $fo){
                    
                    $foi = Mc_transactions_model::Get_All_Fulfillment_Option_Items($fo->id);
                    ?>
                    <div class="well well-small">
                    <p><strong><?=ucwords($fo->service_type)?>: </strong><?=date("d F Y", strtotime($fo->service_date))?>
                    <?=($fo->time_slot)?', '.$fo->time_slot:''?>
                    </p>
                    <?
                    if ($fo->service_type == "delivery"){
                        echo '<p>';
                        echo '<strong>Address: </strong>'.$fo->address.', '.$fo->postal_code;
                        echo '<br /><strong>Recipient: </strong>'.$fo->delivered_to;
                        echo '<br /><strong>Contact No.: </strong>'.$fo->delivered_contact;
                        echo '</p>';
                    }
                    ?>
                    </div>
                    <table class="table table-bordered table-striped">
                        
                    <thead>
                    <tr>
                        <th>Items</th>                        
                        <th class="align-center">Qty</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($foi as $foi){
                    
                        echo "<tr><td>$foi->product_name</td>";                    
                        echo "<td class=\"align-center\">$foi->qty</td>";
                    }
                    ?>
                    </tbody>        
                    </table>
                    <?

                }

                
            ?>
            
            
            <? } ?>
            
        
</div>
<!-- End Fulfillment options -->


</div>
<!-- FOR JQUERY SORTABLE -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/jquery-ui/themes/base/jquery-ui.css" />

<script src="<?=base_url()?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

<script type="text/javascript">
$(document).ready(function(){
    $('#amount').text($('#total_amount').text());
    
    $('#status').change(function(){
        var selected_value = ($(this).val());
        
        //if ($('#current_status').val() == 7 && selected_value == 8){
        if (selected_value == 8){
            //if current is error/cc problem and the selected status is paid/open
            $('.mc-notify-customer').show();            
        }
        else
            $('.mc-notify-customer').hide();
    });
    
    $('form#transaction_form #update-button').on("click",function(e){
        e.preventDefault();
        var hasError = false;      
        
        //Confirm Yes or No
        var confirm_delete = confirm("Do you confirm to update this transaction?");
        if (confirm_delete == true){
            hasError = false;            
        }else{
            hasError = true;
        }
    
        if(hasError == false) {
          $('#transaction_form').submit();  
          return true;
        }
        else{
          return false;        
        }
        //return false;    
    });
    
});
</script>