<?php $form_action = site_url('mc_creditcards') . '/' . (($action == 'add') ? $action : 'update'); ?>
<hr>
<?= form_open($form_action, array('class'=>'form-horizontal'))?>
		<?php if($this->session->flashdata('required_error')):?>
			<div class="alert alert-error"><?= $this->session->flashdata('required_error') ?></div>
		<?php endif; ?>
		<?php if(validation_errors()):?>
			<div class="alert alert-error"><?= validation_errors() ?></div>
		<?php endif; ?>

		<?php
		if ($this->session->flashdata('required_error')){
		echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
		}
		?>

		<?php if(isset($creditcard) && $creditcard->id): ?>
			<input type="hidden" name="creditcard_id" value="<?= isset($creditcard) ? $creditcard->id : '' ?>">
		<?php endif; ?>

		<div class="control-group">
			<label class="control-label">Name</label>
			<div class="controls">
			  	<input type="text" name="form[name]" maxlength="200" value="<?=($action == "add")?set_value("form[name]"):$creditcard->name?>">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Label</label>
			<div class="controls">
				<input type="text" name="form[label]" maxlength="200" value="<?=($action == "add")?set_value("form[label]"):$creditcard->label?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Payment gateway</label>
			<div class="controls">
				<select name="form[payment_gateway_id]" id="">
					<option value="">Choose...</option>
					<?php foreach ($payment_gateways as $key => $value): ?>
						<option value="<?= $value->id?>" 
							<?= ($action=='edit') ? (($value->id == $creditcard->payment_gateway_id) ? 'selected' : '') : '' ?>><?= $value->name?></option>
					<?php endforeach ?>
				</select>
			  	<!-- <input type="text" name="form[label]" maxlength="200" value="<?=($action == "add")?set_value("form[label]"):$creditcard->label?>"> -->
			</div>
		</div>	

		<div class="control-group">
			<label class="control-label">Merchan ID</label>
			<div class="controls">
			  	<input type="text" name="form[mid]" maxlength="200" value="<?=($action == "add")?set_value("form[mid]"):$creditcard->mid?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Issue by (Alphanumeric only. No spaces)</label>
			<div class="controls">
			  	<input type="text" name="form[issued_by]" maxlength="200" value="<?=($action == "add")?set_value("form[issued_by]"):$creditcard->issued_by?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="">Status</label>
			<div class="controls">
				<label class="radio">
				<input type="radio" name="form[active]" value="1" 
					<?= ($action == "add") ? 'checked="checked"' : ((isset($creditcard) && $creditcard->active == 1)?' checked="checked"' : '') ?>/>Active
				</label>
				<label class="radio">
					<input type="radio" name="form[active]" value="0"
					<?= ($action == "edit") ? (($creditcard->active == 0) ? ' checked="checked"' : '') : ''?> />Inactive
				</label>
			</div>
	    </div>

    
  	<div class="clearfix"></div>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="<?php echo site_url('mc_creditcards'); ?>" class="btn">Cancel</a>
	</div>
</form>
