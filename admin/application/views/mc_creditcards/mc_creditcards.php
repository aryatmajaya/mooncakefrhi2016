<?php
  if ($this->session->flashdata('success_notification')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<?php
$url = site_url('mc_creditcards');
$this->check_permission->Check_Button_Add(' Add Credit Card', $current_app_info->add_role, $url);
?>
<hr>
<table class="table table-striped table-condensed table-hover">
	<thead>
		<tr>
			<th class="span1">#</th>
			<th class="span3" style="text-align:left;">Name</th>
			<th class="span3" style="text-align:left;">Payment Label</th>
			<th class="span2" style="text-align:left;">Merchant ID</th>
			<th class="span1" style="text-align:left;">Card type</th>
			<th class="span2">Status</th>
			<th></th>
		</tr>
	</thead>
	<?php if ($entries): ?>
		<tbody>
			<?php foreach ($entries as $key => $value): ?>
				<tr>
					<td> <?= ($key+1) ?></td>
					<td> <?= $value->name ?></td>
					<td> <?= $value->label ?></td>
					<td> <?= $value->mid ?></td>
					<td> <?= $value->issued_by ?></td>
					<td>
						<? if($value->active == 1): ?>
							<span data-id="<?= $value->id; ?>" class="label label-success tooltip-button close-button" data-toggle="tooltip" title data-original-title="Click to De-activate">Active</a>
						<? else: ?>
							<span data-id="<?= $value->id; ?>" class="label tooltip-button open-button" data-toggle="tooltip" title data-original-title="Click to Activate">Inactive</a>
						<? endif; ?>
					</td>
					<td class="text-right">
						<?php $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$value->id, $url); ?>
						<?= $this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$value->id); ?>
					</td>
				</tr>			
			<?php endforeach ?>
		</tbody>
	<?php endif ?>
</table>

<script>
	$(function(){
		$(".delete-button").on('click', function(e){
			var conf = confirm("Do you want to delete this card ?");
			if(conf){
				$.post('<?php echo $url;?>/delete/'+$(this).val(), function(data){
					location.href="<?php echo $url;?>";
				});
			}
		});
	});
</script>