<?
//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
?>
<?php
if ($this->session->flashdata('notification_status')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('notification_status').'</div>';
}

$url = site_url($this->router->fetch_class());
?>

<div>
    <form class="form-inline" method="POST">
        <strong>Search By:</strong>
        <select name="search_field">
            <option value="mc_customers.first_name">First Name</option>
            <option value="mc_customers.last_name">Last Name</option>
            <option value="mc_customers.email">Email</option>
            <option value="mc_customers.mobile">Mobile</option>
        </select>
        <input type="text" name="search" value="<?=set_value('search')?>" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
</div>


<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Customer ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Contact Numbers</th>
            <th><div class="text-center">Subscription</div></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
	<?php
	if ($entries) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->id."'>\n";
			echo "<td>".$entries->id."</td>\n";
			
			echo "<td>$entries->last_name, $entries->first_name</td>\n";
			echo "<td>$entries->email</td>\n";
			
			echo "<td>";
                        echo $entries->mobile;
                        echo ($entries->alt_contact)?'<br />'.$entries->alt_contact:'';
                        echo "</td>\n";
			echo "<td><div class=\"text-center\">".(($entries->subscribe)?'<span class="badge badge-success"><i class="icon-ok icon-white"></i></span>':'')."</div></td>\n";

			echo "<td>";
				$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id, $url);

				$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);				
			echo "</td>\n";
			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>

<script type="text/javascript">
    $('button#delete-button').click(function(){
        var confirm_delete = confirm("Are you sure you want to delete?");
        if (confirm_delete == true){
            var the_id=$(this).val();
            //alert(the_id);
            $.post("ajax_delete_customer",{the_id:the_id},function(result){
                    window.location.reload();
            });
        }
    });
</script>