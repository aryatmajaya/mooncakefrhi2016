

<form class="form-horizontal" method="POST" action="<?=($action == "add")?site_url($this->router->fetch_class().'/add'):site_url($this->router->fetch_class().'/update')?>" enctype="multipart/form-data" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }
  
  if ($action == "edit"){
    echo '<input type="hidden" name="id" value="'.$entry->id.'" />';
    
  }
  ?>
  <div class="control-group">
    <label class="control-label">First Name</label>
    <div class="controls">
      <input type="text" name="first_name" maxlength="200" value="<?=($action == "add")?set_value('first_name'):$entry->first_name?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Last Name</label>
    <div class="controls">
      <input type="text" name="last_name" maxlength="200" value="<?=($action == "add")?set_value('last_name'):$entry->last_name?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Mobile No.</label>
    <div class="controls">
      <input type="text" name="mobile" maxlength="200" value="<?=($action == "add")?set_value('mobile'):$entry->mobile?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Alt Contact No.</label>
    <div class="controls">
      <input type="text" name="alt_contact" maxlength="200" value="<?=($action == "add")?set_value('alt_contact'):$entry->alt_contact?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Email (Login)</label>
    <div class="controls">
      <input type="text" name="email" maxlength="200" value="<?=($action == "add")?set_value('email'):$entry->email?>">
    </div>
  </div>

<div class="control-group">
    <label class="control-label">Password</label>
    <div class="controls">
      <input type="text" name="password" maxlength="200" value="<?=($action == "add")?set_value('password'):$this->encrypt->decode($entry->password)?>" />
    </div>
  </div>

<div class="control-group">
    <label class="control-label">Confirm Password</label>
    <div class="controls">
      <input type="text" name="password2" maxlength="200" value="<?=($action == "add")?set_value('password2'):$this->encrypt->decode($entry->password)?>" />
    </div>
  </div>


  
  
  
    

    

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Save</button>
      <a href="<?php echo site_url($this->router->fetch_class()); ?>" class="btn">Cancel</a>
    </div>
</form>



  <h3>Order History</h3>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Order Date</th>
			<th>Order Ref</th>
			<th>Customer</th>
			
                        <th><div class="text-right">Amount</div></th>
			<th></th>
			<th>Status</th>
		</tr>
	</thead>
	<?php
	if ($transactions) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($transactions as $transactions){
			echo "<tr id='tr_".$transactions->id."'>\n";
			echo "<td>".date('d.m.y',strtotime($transactions->date_ordered))."</td>\n";
			
			echo "<td><a href=\"".site_url("mc_transactions/view/".$transactions->id)."\">".$transactions->order_ref."</a></td>\n";
			echo "<td>".$transactions->first_name.' '.$transactions->last_name."</td>\n";
			//echo "<td>".ucwords($transactions->service_type)."</td>\n";
			//echo "<td>".date('d.m.y',strtotime($transactions->service_date))."</td>\n";
			//echo "<td><span class=\"pull-right\">$ ".number_format(Mc_utilities::Discounted_Price($transactions->total,$transactions->discount),2)."</span></td>\n";
                        /*$deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($transactions->order_ref);
                        $total_qty = Mc_transactions_model::Get_Total_Qty($transactions->id);
                        if (isset($total_qty))
                            $total_qty = array_sum($total_qty);
                        else
                            $total_qty = null;
                        //print_r($total_qty);
                        //$delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty->total_qty);
                        $delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                        
                        //R1
                        //$total_amount = Mc_utilities::Discounted_Price($transactions->total,$transactions->discount) + $delivery_charge;
                        
                        $total_item_amount = Mc_transactions_model::Get_Order_Total_Amount($transactions->id);
                        
                        $total_amount = Mc_utilities::Discounted_Price($total_item_amount, $transactions->card_discount) + $delivery_charge;
                         */
                         $total_amount = ($transactions->total_amount - $transactions->discounted_amount);
                         $gst_amount = $total_amount * .07;
                         $grand_total = $total_amount + $gst_amount;
			echo "<td><span class=\"pull-right\">$ ".number_format($grand_total,2)."</span></td>\n";
			echo "<td></td>\n";
                        echo "<td>";
                        
			//if ($entries->status == 1){
			//	echo '<span class="label label-success">Active</span>';
			//} else {
                        $the_status = $this->mc_utilities->order_status($transactions->status);
				if ($the_status =="error")
                                    echo '<span class="label label-important">error<span>';
                                elseif ($the_status =="incomplete") {
                                    echo '<span class="label" style="width: 70px;">incomplete<span>';
                                }
                                elseif ($the_status =="paid-open") {
                                    echo '<span class="label label-success">paid<span>';
                                }
                                elseif ($the_status =="cancelled") {
                                    echo '<span class="label label-inverse" style="width: 60px;">cancelled<span>';
                                }
                                
                                elseif ($the_status =="in-process") {
                                    echo '<span class="label label-warning" style="width: 60px;">in process<span>';
                                }
                                elseif ($the_status =="delivered") {
                                    echo '<span class="label label-info" style="width: 60px;">delivered<span>';
                                }
                                elseif ($the_status =="collected") {
                                    echo '<span class="label label-info" style="width: 60px;">collected<span>';
                                }
                                else
                                    echo $the_status;
			//}
			echo "</td>\n";

			echo "<td>";
				//$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$transactions->id);

				//$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->property_id);
				
			echo "</td>\n";

			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>
