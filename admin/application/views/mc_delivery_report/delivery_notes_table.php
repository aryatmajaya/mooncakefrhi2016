<?php

if ($service_type == "corporate"):
        $fo = Mc_delivery_report_model::Get_All_Delivery_Options_By_OrderRef($order->order_ref);
    else:
        $fo = Mc_delivery_report_model::Get_All_Delivery_Options($delivery_id);
endif;
//var_dump($fo);

$subtotal = 0;
$discount = 0;
$discounted_amount = 0;
$number_item_discount = 0;
$number_item_discounted_amount = 0;
$gst_charge = 0;
$total_delivery_charge = 0;
$grandtotal = 0;

$total_qty =0;
foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal += $price_total;
	$total_qty += $item->qty;
}

if($order->promo_code_discount > 0){
	$discount = $order->promo_code_discount;
	$discounted_amount = ($subtotal*$order->promo_code_discount)/100;
}elseif($order->card_discount > 0){
	$discount = $order->card_discount;
	$discounted_amount = ($subtotal*$order->card_discount)/100;
}elseif($order->corporate_discount > 0){
	$discount = $order->corporate_discount;
	$discounted_amount = ($subtotal*$order->corporate_discount)/100;
}

/*if($order->number_item_discount){
	$number_item_discount = $order->number_item_discount;
	$number_item_discounted_amount = ($subtotal*$order->number_item_discount)/100;
}*/


if($order->ordering_method == "corporate"){
	$total_delivery_charge = $order->delivery_charge;
}else if($order->ordering_method != "corporate" && $fo){
	$total_delivery_charge = Utilities::Calculate_Delivery_Charges($total_qty, $fo[0]->service_type);
}



//$gst_charge = ($subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge) * (GST_PERCENT /100);

$grandtotal = $subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge + $gst_charge;

?>

<style type="text/css">
<!--
	table {
		width: 100%;
		border: none;
	}
	
	table .content{
		padding:10px;
	}
-->
</style>


<table cellpadding="0" cellspacing="0"><tr><td><?=strtoupper(str_replace('_',' ',$service_type))?> ORDER</td></tr></table>

<table cellpadding="0" cellspacing="0"><tr><td height="12"></td></tr></table>

<table style="width:100%;" cellpadding="0" cellspacing="0">
    <tr>
    	<td style="width:50%;" valign="top"><strong>Order No.</strong> : <?=$order->order_ref;?></td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0"><tr><td height="12"></td></tr></table>

<!-- orders detail -->
<table cellpadding="0" cellspacing="0">
    <tr>
        <td style="width:50%;" valign="top">
            <table cellpadding="0" cellspacing="0">
                <tr><td style="color:#f99e48; font-family:Arial" width="100%"><strong>ORDER DETAILS</strong></td></tr>
                <tr><td height="12" width="100%"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0">
                <tr><td style="font-size:13px; color:#6b6b6b">Order Date</td></tr>
                <tr><td><strong><?=date("d F Y", strtotime($order->date_ordered))?></strong></td></tr>
                <tr><td height="10"></td></tr>
                <tr><td style="font-size:13px; color:#6b6b6b">Customer</td></tr>
                <tr><td><strong><?=$order->first_name.' '.$order->last_name?></strong></td></tr>
                <tr><td height="10"></td></tr>
                <tr><td style="font-size:13px; color:#6b6b6b">Mobile</td></tr>
                <tr><td><strong><?=$order->mobile;?></strong></td></tr>
                <tr><td height="10"></td></tr>
                <tr><td style="font-size:13px; color:#6b6b6b">Email</td></tr>
                <tr><td><strong><?=$order->email?></strong></td></tr>
                <tr><td height="10"></td></tr>
            </table>
        </td>
        <td style="width:50%;" valign="top">
            <table cellpadding="0" cellspacing="0">
                <tr><td style="color:#f99e48;" width="100%"><strong>FULLFILLMENT</strong></td></tr>
                <tr><td height="12" width="100%"></td></tr>
            </table>

            <table cellpadding="0" cellspacing="0">
                <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Option</td></tr>
                <tr><td style="width:100%;"><strong><?php 
                if($fo[0]->service_type=="self_collection"){
                    echo "Self Collection";
                }elseif($fo[0]->service_type=="delivery"){
                    echo "Delivery";
                }else{
                    echo $fo[0]->service_type;	
                }
                ?></strong></td></tr>
                <tr><td height="10" style="width:100%;"></td></tr>
                <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Date</td></tr>
                <tr><td style="width:100%;"><strong><?php echo date("d F Y", strtotime($fo[0]->service_date));?></strong></td></tr>
                <tr><td height="10" style="width:100%;"></td></tr>
                
                <?php if($fo[0]->service_type == "delivery" && $fo[0]->time_slot){?>
                <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Time Slot</td></tr>
                <tr><td style="width:100%;"><strong><?php echo $fo[0]->time_slot;?></strong></td></tr>
                <tr><td height="10" style="width:100%;"></td></tr>
                <?php } ?>
                
                <?php if ($fo[0]->service_type == "delivery"){ ?>
                    <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Address</td></tr>
                    <tr><td style="width:100%;"><strong><?php echo $fo[0]->address.', '.$fo[0]->postal_code;?></strong></td></tr>
                    <tr><td height="10" style="width:100%;"></td></tr>
                    
                    <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Delivered To</td></tr>
                    <tr><td style="width:100%;"><strong><?php echo $fo[0]->delivered_to;?></strong></td></tr>
                    <tr><td height="10" style="width:100%;"></td></tr>
                    
                    <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Contact No</td></tr>
                    <tr><td style="width:100%;"><strong><?php echo $fo[0]->delivered_contact;?></strong></td></tr>
                    <tr><td height="10" style="width:100%;"></td></tr>
                    
                 <?php } ?>
                 
                 <?php if($order->ordering_method == "corporate" && $order->special_requirements){ ?>
                    <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Additional Request</td></tr>
                    <tr><td style="width:100%;"><strong><?php echo $order->special_requirements;?></strong></td></tr>
                    <tr><td height="10" style="width:100%;"></td></tr>
                 <?php }elseif($order->ordering_method != "corporate" && $fo[0]->additional_request){ ?>
                    <tr><td style="font-size:13px; color:#6b6b6b; width:100%;">Additional Request</td></tr>
                    <tr><td style="width:100%;"><strong><?php echo $fo[0]->additional_request;?></strong></td></tr>
                    <tr><td height="10" style="width:100%;"></td></tr>
                 <?php } ?>
                
            </table>
        </td>
    </tr>
</table>
 
<table cellpadding="0" cellspacing="0"><tr><td height="12"></td></tr></table>

<!-- Products Begin -->
<table border="1" style="border-collapse:collapse;" cellpadding="0" cellspacing="0">

    <tr>
        <th style="width:50%;"><div class="content">SELECTION OF MOONCAKES</div></th>
        <th style="width:10%;"><div class="content">Pcs/box</div></th>
        <th style="width:15%;"><div class="content">Price</div></th>
        <th style="width:10%;"><div class="content">Qty</div></th>
        <th style="width:15%;"><div class="content">Total (SGD)</div></th>
    </tr>
    
    <?php
    $total = 0;
    $total_qty = 0;
    $subtotal = 0;
    $ctr = 1;
	
	foreach ($items as $product) {
		$total = $product->price * $product->qty;

		$subtotal = $subtotal + $total;
		?>        
		<tr>
			<td style="width:50%"><div class="content"><?php echo $product->product_name; ?></div></td>
			<td style="width:10%;"><div class="content" style="text-align:right;"><?=$product->pc_per_box?></div></td>
			<td style="width:15%;"><div class="content" style="text-align:right;">$ <?php echo number_format($product->price, 2); ?></div></td>
			<td style="width:10%;"><div class="content" style="text-align:right;"><?php echo $product->qty;?></div></td>
			<td style="width:15%;"><div class="content" style="text-align:right;"><?php echo ($total>0)?'SGD '.number_format($total,2):'';?></div></td>
		</tr>
		<?php 
	} 
    ?>
</table>   

<table cellpadding="0" cellspacing="0"><tr><td height="12"></td></tr></table>

<table style="width:100%"> 	
    <tr>
        <td style="width:60%;">
        	<p style="font-size: 9px;">For enquiries, please contact us at T +65 6338 8785 E fairmontsingapore.mooncakes@fairmont.com</p>
            <span>Received the above in good order &amp; condition</span>
        </td>
        <td style="width:40%;" align="right">
        	<table style="width:100%;">
            	<tr>
                	<td align="right" style="width:50%;"><strong>Subtotal :</strong></td>
                    <td align="right" style="padding-left:10px; width:50%;">SGD <?php echo number_format($subtotal,2); ?></td>
                </tr>
                <tr>
                	<td align="right" style="width:50%;"><strong>Discount <?php echo $discount; ?>% :</strong></td>
                    <td align="right" style="padding-left:10px; width:50%;">SGD <?php echo '('.number_format($discounted_amount,2).')'; ?></td>
                </tr>
				<tr>
					<td align="right" style="width:50%;"><strong>Delivery Charge(s) :</strong></td>
					<td align="right" style="padding-left:10px; width:50%;">SGD <?php echo number_format($total_delivery_charge,2); ?></td>
				</tr>
                <tr>
                    <td align="right" style="width:50%;"><strong>Grand Total :</strong></td>
                    <td align="right" style="padding-left:10px; width:50%;">SGD <?php echo number_format($grandtotal,2); ?></td>
                </tr>
                
            </table>
        </td>
    </tr>
</table>  


        <table width="100%" style="font-size: 12px;">
            <tr>
                <td width="50%"><br /><br /><br /><br />
                    __________________________________________________<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name, Signature and Date
                </td>                        

                <td width="50%">&nbsp;</td>
            </tr>
        </table>
        <?
        if ($debug) {
        ?>
        <table width="100%" border="1" cellspacing="0" cellpadding="5" class="text_12px" class="pdfreport-table">
            <tr>
                <td width="50%" align="left" valign="top"><strong> Product Name </strong></td>
                <td width="20%" align="center" valign="top"><strong> Unit Price </strong></td>
                <td width="10%" align="center" valign="top"><strong> Qty </strong></td>
                <td width="20%" align="center" valign="top"><strong> Total </strong></td>
            </tr>
            <?
            $total_qty = 0;
            $subtotal = 0;
            
            if (isset($items)) {
                foreach ($items as $item) {
                    $price_total = $item->qty * $item->price;
                    ?>        
                    <tr>
                        <td><? echo $item->product_id.' - '.htmlentities($item->product_name); ?></td>
                        <td align="center">SGD <? echo number_format($item->price, 2); ?></td>
                        <td align="center"><? echo $item->qty; ?></td>
                        <td align="right">SGD <? echo number_format($price_total, 2); ?></td>
                    </tr>
                    <?
                    $total_qty = $total_qty + $item->qty;
                    $subtotal = $subtotal + $price_total;
                } 
            }
            ?>
                <tr>
                    <td colspan="3" align="right" style="border-left: none;"><strong>Sub-total:</strong></td>
                    <td align="right">SGD <? echo number_format($subtotal,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>Discount:</strong></td>
                    <td align="right"><?=$order->discount;?>%</td>
                </tr>
                <?
                $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($order->order_ref);
                $total_delivery_charge = 0;
                //if ($review_page == "on"){
                $total_delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                
                
                $grandtotal = $subtotal - ($subtotal * ($order->discount / 100));
                $grandtotal = $grandtotal + $total_delivery_charge;
                ?>
                <tr>
                    <td colspan="3" align="right"><strong>Delivery Charge(s): </strong></td>
                    <td align="right">SGD <?=number_format($total_delivery_charge,2); ?></td>
                 </tr>  
                 <? 
                    //}
                    ?>
                <tr>
                    <td colspan="3" align="right"><strong>Total Amount:</strong></td>
                    <td align="right">SGD <?=number_format($grandtotal,2);?></td>
                </tr>
        </table>
        <!-- END items table -->
        <? } ?>
        
    <!-- START FULFILLMENT OPTIONS -->
    
    <? 
    if ($debug) {
    
    if (isset($fo)){ ?>
    <h2 class="pdfreport-h2"><?=strtoupper($service_type)?> DETAILS</h2>
    <?
    foreach ($fo as $fo){
    ?>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" class="pdfreport-table">
        <tr>
            <td>
                <?
                echo '<strong>'.ucwords($fo->service_type).': </strong>'.date("d F Y", strtotime($fo->service_date)).(($fo->time_slot)?', '.$fo->time_slot:'');
                if ($fo->service_type == "delivery"){
                    echo '<br /><strong>Address:</strong> '.$fo->address.', '.$fo->postal_code;
                    echo '<br /><strong>Delivered To:</strong> '.$fo->delivered_to;
                    echo '<br /><strong>Contact No.:</strong> '.$fo->delivered_contact;
                }
                ?>
            </td>
        </tr>
    </table>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" class="pdfreport-table">
        
            <tr>
                <td><strong>Items</strong></td>
                <td align="center"><strong>Qty</strong></td>
            </tr>
            
       
            <? 
            //$delivery_items = Delivery_model::Get_Delivery_Option_Items($fo->id);
            $delivery_items = Mc_transactions_model::Get_All_Fulfillment_Option_Items($fo->id);
                   
            foreach ($delivery_items as $di) {
                if ($di->qty !=0 ) {
            ?>
                <tr>
                    <td><?=$di->product_name?></td>
                    <td align="center"><? echo $di->qty;?></td>
                </tr>
            <?
                }
            } ?>
        
    </table>
    
    <? } ?>
    <!-- END FULFILLMENT OPTIONS -->
    <? } //End if ($fo)?>
    <? } ?>

<!--<pagebreak orientation="P" margin-top="7mm" />-->
