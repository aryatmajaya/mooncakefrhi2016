<?php
$subtotal = 0;
$grandtotal = 0;

if ($service_type == "corporate"):
        $fo = Mc_delivery_report_model::Get_All_Delivery_Options_By_OrderRef($order->order_ref);
    else:
        $fo = Mc_delivery_report_model::Get_All_Delivery_Options($delivery_id);
endif;
//var_dump($fo);
?>

<!--<pagebreak orientation="P" margin-top="7mm" margin-left="10mm" margin-right="10mm" />-->

<?php if (isset($print)) { ?>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pdfreport-table2">
        <tr>
            <td width="30%"></td>
            <td align="center" width="40%"><img src="assets/images/pdf-report-logo.png" border="0"></td>
            <!--<td width="30%" align="right">ORDER DATE: <?=date("d F Y", strtotime($order->date_ordered))?></td>-->
            <td width="30%" align="right"></td>
        </tr>
        <tr>
            <td colspan="3" align="center"><strong><br /><?=strtoupper(str_replace('_',' ',$service_type))?> ORDER</strong></td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
    </table>

<? } ?>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pdfreport-table2">
            <tr>
                <td width="50%" align="left" valign="top" colspan="2"><strong>Order No.: </strong><?=$order->order_ref; //$bc->draw();?></td>
                <td width="50%" align="left" valign="top" colspan="2"><strong><?=($service_type == "delivery")?'Delivery':'Collection'?> Date: </strong><?=date("d F Y", strtotime($fo[0]->service_date)).(($fo[0]->time_slot)?', '.$fo[0]->time_slot:'')?></td>
            </tr>
            <tr>
                <td align="left" valign="top" colspan="2"><strong><u>Order By</u></strong></td>
              <td align="left" valign="top" colspan="2"><strong><u>
                  <?php 
                  //echo ($service_type == "self_collection" || $service_type == "corporate")?'':'Received By';
                  echo ($fo[0]->delivered_to)?'Received By':'';
                  ?>
                      </u></strong></td>
               
          </tr>
          <tr>
              <td width="15%" align="left" valign="top"><strong>Name</strong></td>
              <td width="35%" align="left" valign="top">: <?=$order->first_name.' '.$order->last_name?></td>
              <td width="17%" align="left" valign="top">
                  <strong>
                  <?php 
                  //echo ($service_type == "self_collection")?'':'Name';
                  echo ($fo[0]->delivered_to)?'Name':'';
                  ?>
                  </strong></td>
              <td width="33%" align="left" valign="top"><?=($fo[0]->delivered_to)?': '.$fo[0]->delivered_to:''?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Company</strong></td>
            <td align="left" valign="top">: <?=$order->company_name?></td>
            <td align="left" valign="top">
                <strong>
                    <?php 
                    //echo ($service_type == "self_collection" || $service_type == "corporate")?'':'Company';
                    echo ($fo[0]->delivered_to)?'Company':'';
                    ?>
                </strong></td>
            <td align="left" valign="top"><?=($fo[0]->company)?': '.$fo[0]->company:'';?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Address</strong></td>
            <td align="left" valign="top">: <?=$order->address;?></td>
            <td align="left" valign="top">
                <strong>
                    <?php 
                    //echo ($service_type == "self_collection" || $service_type == "corporate")?'':'Address';
                    echo ($fo[0]->delivered_to)?'Address':'';
                    ?>
                </strong>
            </td>
            <td align="left" valign="top"><?=($fo[0]->address)?': '.$fo[0]->address:'';?></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;  <?=$order->postal_code;?></td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;  <?=$fo[0]->postal_code;?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Contact No.</strong></td>
            <td align="left" valign="top">: <?=$order->mobile;?></td>
            <td align="left" valign="top">
                <strong>
                    <?php 
                    //echo ($service_type == "self_collection" || $service_type == "corporate")?'':'Contact No.';
                    echo ($fo[0]->delivered_to)?'Contact No.':'';
                    ?>
                </strong>
            </td>
            <td align="left" valign="top"><?=($fo[0]->delivered_contact)?': '.$fo[0]->delivered_contact:''; ?></td>
          </tr>
          
          
          <tr>
            
            <td align="left" valign="top" colspan="4">&nbsp;</td>
          </tr>
          
        </table>
    
        <br />
        <!-- products table -->
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="text_12px" class="pdfreport-table">
            <tr>
                <td width="5%" align="center" rowspan="2">&nbsp;</td>
                <td width="50%" align="center" rowspan="2"><strong>SELECTION OF MOONCAKES</strong></td>
                
                <td width="20%" align="center" colspan="2"><strong> Per Box </strong></td>
                
                <td width="10%" align="center" rowspan="2"><strong>Qty<br />(Boxes)</strong></td>
                <td width="15%" align="center" rowspan="2"><strong>Total<br />(SGD)</strong></td>
                
            </tr>
            <tr>
                
                <td align="center">Pcs</td>
                <td align="center">Price</td>
                </tr>
            <?php
            
            $total = 0;
            $total_qty = 0;
            $subtotal = 0;
            $ctr = 1;
            //var_dump($products);
            if (isset($products)) {
                foreach ($products as $product) {
                    //$price_total = $item->qty * $item->price;
                    //Version 1
                    //$ditem = Mc_delivery_report_model::Get_Specific_Delivery_Item($delivery_id, $product->id);
                    
                    //Version 2
                    $ditem = Mc_delivery_report_model::Get_Specific_Delivery_Item_ByOrderRef($order->order_ref, $product->id);
                    //var_dump($ditem);
                    
                    if ($ditem):
                    ?>        
                    <tr>
                        <td align="center"><?=$ctr++?></td>
                        <td><? echo $product->product_name; ?></td>
                        <td align="center"><?=$product->pc_per_box?></td>
                        
                        <td align="center">$ <?php echo number_format($product->price, 2); ?></td>
                        <td align="center">
                        <?
                        if (!empty($ditem->qty)) {
                            echo $ditem->qty;
                            $total_qty = $total_qty + $ditem->qty;
                        }
                        ?>
                        </td>
                        <?
                        if (!empty($ditem->oi_price))
                            $total = $ditem->qty * $ditem->oi_price;
                        else
                            $total = 0;
                        
                        
                        ?>
                        <td align="right"><?php echo ($total>0)?'SGD '.number_format($total,2):'';?></td>
                    </tr>
                    <?
                    //
                    $subtotal = $subtotal + $total;
                    endif;
                } 
            
            }
            ?>
                
                <tr>
                    <td colspan="2" style="border-left: none;border-right: none; border-top: none; border-bottom: none; font-size: 9px;"><!--Prices stated are inclusive of 7% GST-->&nbsp;</td>
                    
                    
                    <td colspan="3" align="right" style="border-right: none; border-left: none; border-bottom: none;"><strong>Subtotal :</strong></td>
                        
                    <td align="right">SGD <?php echo number_format($subtotal,2); ?></td>                    
                </tr>
                <tr>
                    <td colspan="2" style="border-left: none;border-right: none; border-top: none; border-bottom: none; font-size: 9px;">For enquiries, please contact us at T +65 6338 8785 E fairmontsingapore.mooncakes@fairmont.com</td>
                    
                    <?php
                    
                    
                    
                    $the_discount = 0;
                    
                    
                    if ($order->corporate_discount != 0.00):
                        $the_discount = $order->corporate_discount;
                    endif;

                    if ($order->card_discount != 0):
                        $the_discount = $order->card_discount;
                    endif;
                    
                    
                    $discounted_amount = $subtotal * ($the_discount/100);
                    ?>
                    <td colspan="3" align="right" style="border-right: none; border-left: none; border-bottom: none; border-top: none;"><strong>Discount : <?php echo $the_discount; ?>%</strong></td>
                    <td align="right">SGD <?php echo '('.number_format($discounted_amount,2).')'; ?></td>
                    
                    
                </tr>
                <?
                /* version 1
                $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($order->order_ref);
                $total_delivery_charge = 0;
                if ($review_page == "on"){
                $total_delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                 * 
                 */
                /* version 2
                if ($total_qty < QTY_FREE_DELIVERY)
                    $total_delivery_charge = DELIVERY_CHARGE;
                else
                    $total_delivery_charge = 0;
                * 
                 */
                
                
                
                /* Version 3 By James Castaneros */
                $delivery_charge = 0;
                if ($order->ordering_method == "corporate"):
                    if ($order->service_type == "delivery"):
                        if ($order->delivery_charge > 0): 
                            //If it has custom delivery charge
                            $delivery_charge = $order->delivery_charge;
                        else:
                            //If not then do the auto delivery charge (50sgd)
                            if ($total_qty < DELIVERY_COMPLIMENTARY_QTY)
                                $delivery_charge = DELIVERY_CHARGE;
                        endif;
                    endif;
                    
                    
                endif;
                
                
                $gst_amount = (($subtotal - $discounted_amount) + $delivery_charge) * (GST_PERCENT/100);
                //hack: remove gst charge for frhi
                $gst_amount = 0;
                $grandtotal = ($subtotal - $discounted_amount) + $delivery_charge + $gst_amount;
                //$grandtotal = $grandtotal + $total_delivery_charge;
                
                ?>
                
                <tr>
                    <td colspan="2" style="border-left: none;border-right: none; border-top: none; border-bottom: none;">Received the above in good order &amp; condition</td>
                    <td colspan="3" align="right" style="border-right: none; border-left: none; border-top: none; border-bottom: none;"><strong>Delivery Charge(s) :</strong>&nbsp;</td>
                    <td align="right">SGD <?php echo number_format($delivery_charge,2); ?></td>
                </tr>  
                
                 <? 
                    //}
                    ?>
                <tr>
                    <td colspan="2" style="border-left: none;border-right: none; border-top: none; border-bottom: none;">&nbsp;</td>
                    <td colspan="3" align="right" style="border-right: none; border-left: none; border-top: none; border-bottom: none;"><strong>GST Charge :</strong>&nbsp;</td>
                    <td align="right">SGD <?php echo number_format($gst_amount,2); ?></td>
                </tr>
                <tr>
                    <td colspan="5" align="right" style="border-right: none; border-left: none; border-top: none; border-bottom: none;"><strong>Grand Total :</strong>&nbsp;</td>
                    <td align="right">SGD <?php echo number_format($grandtotal,2); ?></td>
                </tr>
        </table>
        <!-- End products table -->
        <table width="100%" style="font-size: 12px;">
            <tr>
                <td width="50%"><br /><br /><br /><br />
                    __________________________________________________<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name, Signature and Date
                </td>                        

                <td width="50%">&nbsp;</td>
            </tr>
        </table>
        <?
        if ($debug) {
        ?>
        <table width="100%" border="1" cellspacing="0" cellpadding="5" class="text_12px" class="pdfreport-table">
            <tr>
                <td width="50%" align="left" valign="top"><strong> Product Name </strong></td>
                <td width="20%" align="center" valign="top"><strong> Unit Price </strong></td>
                <td width="10%" align="center" valign="top"><strong> Qty </strong></td>
                <td width="20%" align="center" valign="top"><strong> Total </strong></td>
            </tr>
            <?
            $total_qty = 0;
            $subtotal = 0;
            
            if (isset($items)) {
                foreach ($items as $item) {
                    $price_total = $item->qty * $item->price;
                    ?>        
                    <tr>
                        <td><? echo $item->product_id.' - '.htmlentities($item->product_name); ?></td>
                        <td align="center">SGD <? echo number_format($item->price, 2); ?></td>
                        <td align="center"><? echo $item->qty; ?></td>
                        <td align="right">SGD <? echo number_format($price_total, 2); ?></td>
                    </tr>
                    <?
                    $total_qty = $total_qty + $item->qty;
                    $subtotal = $subtotal + $price_total;
                } 
            }
            ?>
                <tr>
                    <td colspan="3" align="right" style="border-left: none;"><strong>Sub-total:</strong></td>
                    <td align="right">SGD <? echo number_format($subtotal,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>Discount:</strong></td>
                    <td align="right"><?=$order->discount;?>%</td>
                </tr>
                <?
                $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($order->order_ref);
                $total_delivery_charge = 0;
                //if ($review_page == "on"){
                $total_delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                
                
                $grandtotal = $subtotal - ($subtotal * ($order->discount / 100));
                $grandtotal = $grandtotal + $total_delivery_charge;
                ?>
                <tr>
                    <td colspan="3" align="right"><strong>Delivery Charge(s): </strong></td>
                    <td align="right">SGD <?=number_format($total_delivery_charge,2); ?></td>
                 </tr>  
                 <? 
                    //}
                    ?>
                <tr>
                    <td colspan="3" align="right"><strong>Total Amount:</strong></td>
                    <td align="right">SGD <?=number_format($grandtotal,2);?></td>
                </tr>
        </table>
        <!-- END items table -->
        <? } ?>
        
    <!-- START FULFILLMENT OPTIONS -->
    
    <? 
    if ($debug) {
    
    if (isset($fo)){ ?>
    <h2 class="pdfreport-h2"><?=strtoupper($service_type)?> DETAILS</h2>
    <?
    foreach ($fo as $fo){
    ?>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" class="pdfreport-table">
        <tr>
            <td>
                <?
                echo '<strong>'.ucwords($fo->service_type).': </strong>'.date("d F Y", strtotime($fo->service_date)).(($fo->time_slot)?', '.$fo->time_slot:'');
                if ($fo->service_type == "delivery"){
                    echo '<br /><strong>Address:</strong> '.$fo->address.', '.$fo->postal_code;
                    echo '<br /><strong>Delivered To:</strong> '.$fo->delivered_to;
                    echo '<br /><strong>Contact No.:</strong> '.$fo->delivered_contact;
                }
                ?>
            </td>
        </tr>
    </table>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" class="pdfreport-table">
        
            <tr>
                <td><strong>Items</strong></td>
                <td align="center"><strong>Qty</strong></td>
            </tr>
            
       
            <? 
            //$delivery_items = Delivery_model::Get_Delivery_Option_Items($fo->id);
            $delivery_items = Mc_transactions_model::Get_All_Fulfillment_Option_Items($fo->id);
                   
            foreach ($delivery_items as $di) {
                if ($di->qty !=0 ) {
            ?>
                <tr>
                    <td><?=$di->product_name?></td>
                    <td align="center"><? echo $di->qty;?></td>
                </tr>
            <?
                }
            } ?>
        
    </table>
    
    <? } ?>
    <!-- END FULFILLMENT OPTIONS -->
    <? } //End if ($fo)?>
    <? } ?>

<!--<pagebreak orientation="P" margin-top="7mm" />-->
