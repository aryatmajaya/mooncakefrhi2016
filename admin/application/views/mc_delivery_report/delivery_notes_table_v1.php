<?

$subtotal = 0;
 
$grandtotal = 0;
/*
foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal = $subtotal + $price_total;
}
$grandtotal = $subtotal - ($subtotal * ($order->discount / 100));
 * 
 */




?>

<pagebreak orientation="P" margin-top="7mm" />


<?php if (isset($print)) { ?>
    <table width="100%" class="pdfreport-header">
        <tr>
            <td><img src="assets/images/logo_edm.jpg" width="102" height="104" border="0"></td>
            <td align="right">The Legendary Raffles Singapore Mooncakes</td>
        </tr>
        
    </table>
<table width="100%" class="">
        
        <tr>
            <td colspan="2">
                <h1 class="pdfreport-h1"><?=strtoupper($service_type)?> NOTES</h1>
            </td>
        </tr>
    </table>
<? } ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pdfreport-table">
          <tr>
              <td width="20%" align="left" valign="top"><strong>Order Date: </strong></td>
              <td width="30%" align="left" valign="top"><?=date("d.m.y", strtotime($order->date_ordered))?></td>
              <td width="20%" align="left" valign="top"><strong>Customer: </strong></td>
              <td width="30%" align="left" valign="top"><?=$order->first_name.' '.$order->last_name?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Order Ref: </strong></td>
            <td align="left" valign="top"><?=$order->order_ref;?></td>
            <td align="left" valign="top"><strong>Mobile: </strong></td>
            <td align="left" valign="top"><?=$order->mobile;?></td>
          </tr>
          <tr>
            
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Discount: </strong></td>
            <td align="left" valign="top"><?=$order->discount;?>%</td>
            <td align="left" valign="top"><strong>Alt Contact #:</strong></td>
            <td align="left" valign="top"><?=(($order->alt_contact)?$order->alt_contact:'&nbsp;');?></td>
            
          </tr>
          <tr>
              <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top"><strong>Email: </strong></td>
            <td align="left" valign="top"><?=$order->email?></td>
<!--            <td align="left" valign="top"><strong>Amount: </strong></td>
            <td align="left" valign="top">SGD <?=number_format($grandtotal,2);?></td>-->
            
          </tr>
        </table>
    
        
        
    
        <br />
        <table width="100%" border="1" cellspacing="0" cellpadding="5" class="text_12px" class="pdfreport-table">
            <tr>

                <td width="50%" align="left" valign="top"><strong> Product Name </strong></td>
                <td width="20%" align="center" valign="top"><strong> Unit Price </strong></td>
                <td width="10%" align="center" valign="top"><strong> Qty </strong></td>
                <td width="20%" align="center" valign="top"><strong> Total </strong></td>
                
            </tr>
            <?
            $total_qty = 0;
            $subtotal = 0;
            
            if (isset($items)) {
                foreach ($items as $item) {
                    $price_total = $item->qty * $item->price;
                    ?>        
                    <tr>
                        <td><? echo htmlentities($item->product_name); ?></td>
                        <td align="center">SGD <? echo number_format($item->price, 2); ?></td>
                        <td align="center"><? echo $item->qty; ?></td>
                        <td align="right">SGD <? echo number_format($price_total, 2); ?></td>
                    </tr>
                    <?
                    $total_qty = $total_qty + $item->qty;
                    $subtotal = $subtotal + $price_total;
                } 
            
            }
            ?>
                <tr>
                    <td colspan="3" align="right"><strong>Sub-total:</strong></td>
                    <td align="right">SGD <? echo number_format($subtotal,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>Discount:</strong></td>
                    <td align="right"><?=$order->discount;?>%</td>
                </tr>
                <?
                $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($order->order_ref);
                $total_delivery_charge = 0;
                //if ($review_page == "on"){
                $total_delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                
                
                $grandtotal = $subtotal - ($subtotal * ($order->discount / 100));
                $grandtotal = $grandtotal + $total_delivery_charge;
                ?>
                <tr>
                    <td colspan="3" align="right"><strong>Delivery Charge(s): </strong></td>
                    <td align="right">SGD <?=number_format($total_delivery_charge,2); ?></td>
                 </tr>  
                 <? 
                    //}
                    ?>
                <tr>
                    <td colspan="3" align="right"><strong>Total Amount:</strong></td>
                    <td align="right">SGD <?=number_format($grandtotal,2);?></td>
                </tr>
        </table>
        <!-- END items table -->
        
    <!-- START FULFILLMENT OPTIONS -->
    
    <? 
    $fo = Mc_delivery_report_model::Get_All_Delivery_Options($delivery_id);
    
    if (isset($fo)){ ?>
    <h2 class="pdfreport-h2"><?=strtoupper($service_type)?> DETAILS</h2>
    <?
    foreach ($fo as $fo){
    ?>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" class="pdfreport-table">
        <tr>
            <td>
                <?
                echo '<strong>'.ucwords($fo->service_type).': </strong>'.date("d F Y", strtotime($fo->service_date)).(($fo->time_slot)?', '.$fo->time_slot:'');
                if ($fo->service_type == "delivery"){
                    echo '<br /><strong>Address:</strong> '.$fo->address.', '.$fo->postal_code;
                    echo '<br /><strong>Delivered To:</strong> '.$fo->delivered_to;
                    echo '<br /><strong>Contact No.:</strong> '.$fo->delivered_contact;
                }
                ?>
            </td>
        </tr>
    </table>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" class="pdfreport-table">
        
            <tr>
                <td><strong>Items</strong></td>
                <td align="center"><strong>Qty</strong></td>
            </tr>
            
       
            <? 
            //$delivery_items = Delivery_model::Get_Delivery_Option_Items($fo->id);
            $delivery_items = Mc_transactions_model::Get_All_Fulfillment_Option_Items($fo->id);
                   
            foreach ($delivery_items as $di) {
                if ($di->qty !=0 ) {
            ?>
                <tr>
                    <td><?=$di->product_name?></td>
                    <td align="center"><? echo $di->qty;?></td>
                </tr>
            <?
                }
            } ?>
        
    </table>
    
    <? } ?>
    <!-- END FULFILLMENT OPTIONS -->
    <? } //End if ($fo)?>

<!--<pagebreak orientation="P" margin-top="7mm" />-->


