<?
//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
?>
<div class="btn-group">
<!--  <button type="button" id="open-search-tool" class="btn"><i class="icon-search"></i> Search</button>
  <button type="button" id="open-total-sales-tool" class="btn"><i class="icon-shopping-cart"></i> Total Sales</button>-->
    
</div>
<div class="pull-left"></div>
<div class="pull-right">
    <button type="button" id="print-report-button" class="btn"><i class="icon-print"></i> Print Report</button>  
    <button type="button" id="print-deliverynotes-button" class="btn"><i class="icon-print"></i> Print <?=str_replace(' ','-',ucwords($service_type));?> Notes</button>  
</div>
<br /><br />

<div class="well well-small no-display" id="search-tool">
    <form class="form-inline" method="POST">
        <strong>Search By:</strong>
        <select name="search_field">
            <option value="mc_orders.order_ref">Order No.</option>
            <option value="mc_customers.mobile">Mobile</option>
            <option value="mc_customers.email">Email</option>
        </select>
        <div class="input-append">
            <input type="text" name="search" value="<?=set_value('search')?>" />
            <button type="submit" class="btn btn-primary add-on">GO</button>
        </div>
    </form>
</div>

<div class="well well-small" id="total-sales-tool">
    <form class="form-inline" method="POST" action="<?=BASE_URL.$this->router->fetch_class()?>/index/">
        <strong>Select Date:</strong>
        <div class="input-append">
            <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=$from_date?>" />
        </div>
        <div class="input-append">
            <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=$to_date?>" />
        </div>
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
</div>


<?=(isset($pagination_links))?$pagination_links:'';?>
<div id="print-report">
    <?php $this->load->view('mc_delivery_report/transaction_table'); ?>
</div>
<?=(isset($pagination_links))?$pagination_links:'';?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    
    //print report v2
    $('#print-report-button').click(function(){
        var print_url = '<?=BASE_URL.'mc_delivery_report/print_report_PDF/'.$service_type.'/'.(($from_date)?$from_date:'').(($to_date)?'/'.$to_date:'');?>';
        window.open(print_url);
    });
    
    
    $('#print-deliverynotes-button').click(function(){
        var delivery_ids = [];
        $('input[type="checkbox"].delivery_checkbox').each(function() {
            if (this.checked)
                delivery_ids.push($(this).val());
        });
        delivery_ids = delivery_ids.join("-");
        
        var print_url = '<?php echo site_url('mc_delivery_report/print_deliverynotes_PDF/'.$service_type);?>' + '/' + delivery_ids;
        window.open(print_url);
        
        
    });
        

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Delivery Report</title>');
        
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }

        
        $('.from_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
                $('.ui-datepicker-trigger').addClass('add-on btn').css({height:'auto'});
            }
        });
        
        $('.to_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
                $('.ui-datepicker-trigger').addClass('add-on btn').css({height:'auto'});
            }
        });

        $('.ui-datepicker-trigger').addClass('add-on btn').css({height:'auto'});
    }); 
</script>