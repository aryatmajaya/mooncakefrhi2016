<table class="table table-striped table-condensed pdfreport-table">
	<thead>
		<tr>
                    <th><?=ucwords($service_type);?> Date</th>
                    <th>Date Ordered</th>
                    <th>Order Ref</th>
                    <th>Customer</th>

                    <th><div class="text-right">Amount</div></th>
                    <?
                    if (isset($print_mode)) {
                            echo '<th>Delivered To</th>';
                            echo '<th>Address</th>';
                            echo '<th>Contact No.</th>';
                        }
                    
                    if (!isset($print_mode))
                        echo '<th></th>';
                    ?>
			
                        
		</tr>
	</thead>
	<?php
	if ($transactions) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
                $grand_total = 0;
		foreach ($transactions as $transactions){
			echo "<tr id='tr_".$transactions->id."'>\n";
                        echo "<td>".date('d.m.y',strtotime($transactions->service_date))."</td>\n";
			echo "<td>".date('d.m.y',strtotime($transactions->date_ordered))."</td>\n";
			
			echo "<td>";
                        
                        if (!isset($print_mode)) {
                            //echo '<a href="'.BASE_URL.$this->router->fetch_class().'/view_delivery_note/'.$transactions->delivery_id.'">'.$transactions->order_ref.'</a>';
                            echo '<a href="'.BASE_URL.'mc_transactions/view/'.$transactions->id.'">'.$transactions->order_ref.'</a>';
                        } else {
                            echo $transactions->order_ref;
                        }
                        echo "</td>\n";
			echo "<td>".$transactions->first_name." ".$transactions->last_name."</td>\n";
			
                        $delivery_amount = Mc_delivery_report_model::Get_Total_Delivery_Amount($transactions->delivery_id);
			echo "<td><span class=\"pull-right\">".number_format($delivery_amount->delivery_amount,2)."</span></td>\n";
                        if (isset($print_mode)) {
                            echo '<td>'.$transactions->delivered_to.'</td>';
                            echo '<td>'.$transactions->address.' '.$transactions->postal_code.'</td>';
                            echo '<td>'.$transactions->delivered_contact.'</td>';
                        }
                        
			
                        if (!isset($print_mode))
                            echo '<td><input type="checkbox" class="delivery_checkbox" name="delivery_id[]" value="'.$transactions->delivery_id.'" checked /></td>';
                        /*echo '<td>';
                        echo '<a href="'.FRONT_URL.'admin_tools/print_delivery_notes/'.$transactions->id.'" class="btn" target="_blank"><i class="icon-shopping-cart"></i></a>';
                        echo '</td>';
                         * 
                         */
			
			echo "</tr>\n";
			$ctr++;
		}
                
                

		echo "</tbody>\n";
	} ?>
</table>