<link href="<?=BASE_URL?>assets/css/mc_styles.css" rel="stylesheet" media="screen">

<div class="row">

  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }

   $this->load->view('mc_delivery_report/delivery_notes_table');
  ?>




    





</div>
<!-- FOR JQUERY SORTABLE -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/jquery-ui/themes/base/jquery-ui.css" />

<script src="<?=base_url()?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

<script type="text/javascript">
$(document).ready(function(){
    $('#amount').text($('#total_amount').text());
    
    $('#status').change(function(){
        var selected_value = ($(this).val());
        
        //if ($('#current_status').val() == 7 && selected_value == 8){
        if (selected_value == 8){
            //if current is error/cc problem and the selected status is paid/open
            $('.mc-notify-customer').show();            
        }
        else
            $('.mc-notify-customer').hide();
    });
    
    $('form#transaction_form #update-button').on("click",function(e){
        e.preventDefault();
        var hasError = false;      
        
        //Confirm Yes or No
        var confirm_delete = confirm("Do you confirm to update this transaction?");
        if (confirm_delete == true){
            hasError = false;            
        }else{
            hasError = true;
        }
    
        if(hasError == false) {
          $('#transaction_form').submit();  
          return true;
        }
        else{
          return false;        
        }
        //return false;    
    });
    
});
</script>