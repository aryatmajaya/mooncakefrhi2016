<?
//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
?>
<div class="pull-left">
    <button type="button" id="open-default-tool" class="btn"><i class="icon-th-list"></i> Sort <?=ucwords($service_type)?> Date</button>
    <button type="button" id="open-sort-tool" class="btn"><i class="icon-th-list"></i> Sort Order Date</button>
    
</div>
<div class="pull-right">
    <button type="button" id="print-report-button" class="btn"><i class="icon-print"></i> Print Report</button>  
    <button type="button" id="print-deliverynotes-button" class="btn"><i class="icon-print"></i> Print <?=str_replace('_','-',ucwords($service_type));?> Notes</button>  
</div>
<br /><br />



<div class="well well-small the-tools<?=($tool_type == "default-tool")?'':' no-display'?>" id="default-tool">
    <form class="form-inline" method="POST" action="<?=site_url($this->router->fetch_class())?>/index/">
        <strong>Select Date:</strong>
        <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=$from_date?>" />
        <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=$to_date?>" />
        <input type="hidden" name="tool_type" value="default-tool" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
</div>

<!-- Sort Tool -->
<div class="well well-small the-tools<?=($tool_type == "sort-date-ordered")?'':' no-display'?>" id="total-sort-tool">
    <form class="form-inline" method="POST" action="<?=site_url($this->router->fetch_class());?>">
        <strong>Order Date:</strong>
        <input type="text" name="from_date" id="from_date_ordered" readonly class="span2" value="<?=(empty($from_date))?date('d-M-Y'):$from_date?>" />
        <input type="text" name="to_date" id="to_date_ordered" readonly class="span2" value="<?=(empty($to_date))?date('d-M-Y'):$to_date?>" />
        <input type="hidden" name="tool_type" value="sort-date-ordered" />
        
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
</div>
<!-- End Sort Tool -->

<?=(isset($pagination_links))?$pagination_links:'';?>
<div id="print-report">
    <?php $this->load->view('mc_delivery_report/transaction_table'); ?>
</div>
<?=(isset($pagination_links))?$pagination_links:'';?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url("assets/js/jquery-ui/themes/base/jquery-ui.css");?>" />
<script src="<?php echo base_url("assets/js/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>

<!-- FancyBox -->
<script src="<?php echo base_url("assets/js/fancy-box-2.1.4/jquery.fancybox.js");?>"></script>
<link href="<?php echo base_url("assets/js/fancy-box-2.1.4/jquery.fancybox.css");?>" rel="stylesheet" media="screen" />
<!-- End FancyBox -->



<script type="text/javascript">
$(document).ready(function() {
    $('.iframe').fancybox({
        'type': 'iframe',
        'autoSize': false
    });
    
    $('#toggle_check').click(function() {
        
        if (this.checked) {
            $('input[type="checkbox"].delivery_checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $('input[type="checkbox"].delivery_checkbox').each(function() {
                this.checked = false;
            });
        }
            
        
    });
    //print report v1
    
    /*$('#print-report-button').click(function(){
        Popup($('#print-report').html());
        
    });
    */
    
    //print report v2
    $('#print-report-button').click(function(){
        var print_url = '<?=site_url($this->router->fetch_class().'/print_report_PDF/'.$service_type.'/'.(($from_date)?$from_date:'').(($to_date)?'/'.$to_date:'').'/'.$tool_type);?>';
        window.open(print_url);
    });
    
    
    $('#print-deliverynotes-button').click(function(){
        var delivery_ids = [];
        $('input[type="checkbox"].delivery_checkbox').each(function() {
            if (this.checked)
                delivery_ids.push($(this).val());
        });
        delivery_ids = delivery_ids.join("-");
        
        var print_url = '<?php echo site_url($this->router->fetch_class().'/print_deliverynotes_PDF/'.$service_type);?>' + '/' + delivery_ids;
        window.open(print_url);
        
        
    });
        

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Delivery Report</title>');
        
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }

        
        $('.from_date, #from_date_ordered').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date, #to_date_ordered').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        // FOR DEFAULT TOOL
        $('#open-default-tool').click(function() {
            $('.the-tools').hide();
            $('#default-tool').show();
        });
        
        // FOR SORT TOOL
        $('#open-sort-tool').click(function() {
            $('.the-tools').hide();
            $('#total-sort-tool').show();
        });
        // END SORT TOOL
        
    });
</script>