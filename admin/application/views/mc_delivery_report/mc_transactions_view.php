<link href="<?=BASE_URL?>assets/css/mc_styles.css" rel="stylesheet" media="screen">

<div class="row">
<form id="transaction_form" class="form-horizontal" method="POST" action="<?=site_url($this->router->fetch_class().'/update/');?>" enctype="multipart/form-data" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }

  
  ?>
<input type="hidden" name="order_id" value="<?=$entry->id?>" />
<input type="hidden" name="order_ref" value="<?=$entry->order_ref?>" />
<div style="width: 350px;" class="pull-left">
    <table>
        <tr><td width="150"><strong>Order Date:</strong></td><td width="200"><?=date('d.m.y',strtotime($entry->date_ordered))?></td></tr>
        <tr><td><strong>Order Ref:</strong></td><td><?=$entry->order_ref?></td></tr>
        <tr><td><strong>Approval Code:</strong></td><td><?=$entry->approval_code?></td></tr>
        <tr><td><strong>Discount:</strong></td><td><?=$entry->discount?>%</td></tr>

        <tr>
            <td><strong>Amount:</strong></td>
            <td><span id="amount"></span>              
            <?//=number_format(Mc_utilities::Discounted_Price($total->total,$entry->discount),2);?>
            </td>
        </tr>
        <tr><td valign="top"><strong>Status:</strong></td>
            <td>
                <input type="hidden" id="current_status" name="current_status" value="<?=$entry->status?>" />
                <select id="status" name="status">
                    <option></option>
                    <?php
                      foreach ($status_list as $key => $value){
                        echo "<option value=\"$key\"";

                          if ($entry->status == $key)
                            echo " selected";
                          
                        echo ">".(($value == "paid-open")?'PAID & OPEN':strtoupper($value))."</option>\n";
                      }                    
                    ?>
                  </select>
                <div id="email_prompt" class="form-inline mc-notify-customer">
                    Notify Customer? 
                    <label class="radio"><input type="radio" name="notify" value="yes" />Yes</label>
                    <label class="radio"><input type="radio" name="notify" value="no" checked />No</label>
                </div>
                    
            </td>
        </tr>
        <!--
        <tr>
            <td><strong>Payment Mode:</strong></td>
            <td>
                <select name="payment_mode">
                    <option value="credit card"<?=($entry->payment_mode == "credit card")?' selected':'';?>>Credit Card</option>
                    <option value="cash"<?=($entry->payment_mode == "cash")?' selected':'';?>>Cash</option>
                </select>
                
            </td>
        </tr>
        -->
        <?
                if ($entry->payment_mode == 'credit card'){
                    echo '<tr><td><strong>Credit Card Type:</strong></td>';
                    echo '<td>'.Mc_utilities::Credit_Card_Type($entry->payment_type).'<input type="hidden" name="payment_mode" value="'.$entry->payment_mode.'" /></td></tr>';
                }
        ?>
        <?
                if ($entry->status == 7)
                    echo '<tr><td>&nbsp;</td><td><div style="color: #FF0000; font-size:11px;"><strong>Error Message:</strong><br />'.$entry->error_message.'</div></td></tr>';
                ?>
        <tr>
            <td><strong>Remarks/Notes:</strong></td>
            <td><textarea name="admin_notes" rows="3"><?=$entry->admin_notes;?></textarea></td>
        </tr>
        
    </table>


</div>
<div style="width: 350px;" class="pull-right">
    <table>
        <tr><td width="120"><strong>Customer:</strong></td>
            <td width="230">
                <input type="hidden" name="customer_id" value="<?=$entry->customer_id?>" />
                <input type="text" name="first_name" class="pull-left" style="width:90px;" value="<?=$entry->first_name?>" /><input type="text" name="last_name" class="pull-left" style="width:100px;" value="<?=$entry->last_name?>" />
            </td>
        </tr>
        <tr><td><strong>Mobile #:</strong></td><td><input type="text" name="mobile" value="<?=$entry->mobile?>" /></td></tr>
        <tr><td><strong>Alt Contact #:</strong></td><td><input type="text" name="alt_contact" value="<?=$entry->alt_contact?>" /></td></tr>
        <tr><td><strong>Email:</strong></td><td><input type="text" name="email" readonly value="<?=$entry->email?>" /></td></tr>
    </table>
</div>
<div class="lra-clear"></div>
<div>
    <table>
        <tr>
            <td nowrap><strong>Browser Details:</strong></td>
            <td><?=($entry->browser_details == 'none')?'':'<br />'.$entry->browser_details;?></td>
        </tr>
    </table>
</div>
<br /><br />
<div class="text-center">
    <button id="update-button" type="submit" class="btn btn-primary">Update</button> 
    <a href="<?php echo site_url($this->router->fetch_class()); ?>" class="btn">Cancel</a>
  </div>
</form>
    <!-- START ORDERED ITEMS -->
    <div style="padding: 20px 0 0 0;">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>Unit Price</th>
            <th>Qty</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
            <?
            if ($items){
                $sub_total = 0;
                $total_qty = 0;
                foreach ($items as $item){

                    echo "<tr><td>$item->product_name</td>";
                    echo "<td style=\"text-align: right;\">$item->price</td>";
                    echo "<td style=\"text-align: center;\">$item->qty</td>";
                    $total_qty = $total_qty + $item->qty;
                    $total_unit = $item->qty*$item->price;
                    echo "<td style=\"text-align: right;\">".number_format($total_unit,2)."</td></tr>";
                    $sub_total = $sub_total + $total_unit;

                }

                
            ?>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Sub-total:</strong></td>
                <td style="text-align: right;">SGD <?=number_format($sub_total,2)?></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Discount:</strong></td>
                <td style="text-align: right;"><?=$entry->discount?>%</td>
            </tr>
            <?
            
                $total_delivery_charge = 0;
                //if ($review_page == "on"){
                $total_delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                $grand_total = $sub_total - ($sub_total * ($entry->discount / 100));
                $grand_total = $grand_total + $total_delivery_charge;
                ?>
                <tr>
                    <td colspan="3" style="text-align: right;"><strong>Delivery Charge(s): </strong></td>
                    <td style="text-align: right">SGD <?=number_format($total_delivery_charge,2); ?></td>
                 </tr>  
                 <? 
                    //}
                    ?>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Total Amount:</strong></td>
                <td style="text-align: right;"><span id="total_amount">SGD <?=number_format($grand_total,2)?></span></td>
            </tr>
            <? } ?>
            
        </tbody>
        
    </table>






</div>
    <!-- END ORDERED ITEMS -->
    <!-- START FULFILLMENT OPTIONS -->
    
    <div style="padding: 20px 0 0 0;">
        <h4>FULFILLMENT OPTIONS</h4>
    
            <?
            if ($fo){
                $sub_total = 0;
                foreach ($fo as $fo){
                    
                    $foi = Mc_transactions_model::Get_All_Fulfillment_Option_Items($fo->id);
                    ?>
                    <div class="well well-small">
                    <p><strong><?=ucwords($fo->service_type)?>: </strong><?=date("d F Y", strtotime($fo->service_date))?>
                    <?=($fo->time_slot)?', '.$fo->time_slot:''?>
                    </p>
                    <?
                    if ($fo->service_type == "delivery"){
                        echo '<p>';
                        echo '<strong>Address: </strong>'.$fo->address.', '.$fo->postal_code;
                        echo '<br /><strong>Recipient: </strong>'.$fo->delivered_to;
                        echo '<br /><strong>Contact No.: </strong>'.$fo->delivered_contact;
                        echo '</p>';
                    }
                    ?>
                    </div>
                    <table class="table table-bordered table-striped">
                        
                    <thead>
                    <tr>
                        <th>Items</th>                        
                        <th class="align-center">Qty</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($foi as $foi){
                    
                        echo "<tr><td>$foi->product_name</td>";                    
                        echo "<td class=\"align-center\">$foi->qty</td>";
                    }
                    ?>
                    </tbody>        
                    </table>
                    <?

                }

                
            ?>
            
            
            <? } ?>
            
        
</div>
<!-- End Fulfillment options -->


</div>
<!-- FOR JQUERY SORTABLE -->
<link rel="stylesheet" href="<?=base_url()?>assets/js/jquery-ui/themes/base/jquery-ui.css" />

<script src="<?=base_url()?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

<script type="text/javascript">
$(document).ready(function(){
    $('#amount').text($('#total_amount').text());
    
    $('#status').change(function(){
        var selected_value = ($(this).val());
        
        //if ($('#current_status').val() == 7 && selected_value == 8){
        if (selected_value == 8){
            //if current is error/cc problem and the selected status is paid/open
            $('.mc-notify-customer').show();            
        }
        else
            $('.mc-notify-customer').hide();
    });
    
    $('form#transaction_form #update-button').on("click",function(e){
        e.preventDefault();
        var hasError = false;      
        
        //Confirm Yes or No
        var confirm_delete = confirm("Do you confirm to update this transaction?");
        if (confirm_delete == true){
            hasError = false;            
        }else{
            hasError = true;
        }
    
        if(hasError == false) {
          $('#transaction_form').submit();  
          return true;
        }
        else{
          return false;        
        }
        //return false;    
    });
    
});
</script>