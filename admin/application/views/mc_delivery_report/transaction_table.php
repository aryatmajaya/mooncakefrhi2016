<table class="table table-striped table-condensed pdfreport-table">
	<thead>
		<tr>
                    <th><?=ucwords($service_type);?> Date</th>
                    <th>Date Ordered</th>
                    <th>Order Ref</th>
                    <th>Customer</th>
                    <th>
                    	<?php if($this->input->get("sort")=="status" && $this->input->get("order")=="asc"){ ?>
                            <a href="?sort=status&order=desc" class="sort asc">Status</a>
                        <?php }elseif($this->input->get("sort")=="status" && $this->input->get("order")=="desc"){ ?>
                            <a href="?sort=status&order=asc" class="sort desc">Status</a>
                        <?php }else{ ?>
                            <a href="?sort=status&order=asc">Status</a>
                        <?php } ?>
                    </th>

                    <th><div class="text-right">Amount</div></th>
                    <?
                    if (isset($print_mode)) {
                            echo '<th>Delivered To</th>';
                            echo '<th>Address</th>';
                            echo '<th>Contact No.</th>';
                        }
                    
                    if (!isset($print_mode))
                        echo '<th><input type="checkbox" id="toggle_check" name="toggle_check" checked /></th>';
                    ?>
			
                        
		</tr>
	</thead>
	<?php
	if ($transactions) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
                $grand_total = 0;
                
		foreach ($transactions as $transactions){
			echo "<tr id='tr_".$transactions->id."'>\n";
                        echo "<td>".date('d.m.y',strtotime($transactions->service_date))."</td>\n";
			echo "<td>".date('d.m.y',strtotime($transactions->date_ordered))."</td>\n";
			
			echo "<td>";
                        
                        if (!isset($print_mode)) {
                            echo '<a href="'.site_url('mc_transactions/boxview/'.$transactions->id).'" class="iframe">'.$transactions->order_ref.'</a>';
                        } else {
                            echo $transactions->order_ref;
                        }
                        echo "</td>\n";
			echo "<td>".$transactions->first_name." ".$transactions->last_name."</td>\n";
			
                        echo "<td>";
                        $the_status = $this->mc_utilities->order_status($transactions->status);
			echo '<span class="label label-'.$this->mc_utilities->get_order_status_class($transactions->status) . '">'. strtoupper($the_status) .'<span>';
			echo "</td>\n";
                        
                        /* LET's Compute - Version 1*/
                        $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($transactions->order_ref);
                        $total_qty = Mc_transactions_model::Get_Total_Qty($transactions->id);
                        
                        //var_dump($deliveries);

                        if (isset($total_qty))
                            $total_qty = array_sum($total_qty);
                        else
                            $total_qty = null;

                        if ($transactions->ordering_method == 'corporate'):
                            $delivery_charge = $transactions->delivery_charge;
                        else:
                            $delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                        endif;
                        
                        
                        $total_item_amount = Mc_transactions_model::Get_Order_Total_Amount($transactions->id);
                        if (isset($transactions->total_amount))
                            $gst_charge = ($transactions->total_amount - $transactions->discounted_amount + $delivery_charge) * (GST_PERCENT / 100);
                        else
                            $gst_charge = 0;
                        
                        if (isset($transactions->total_amount))
                            $total_amount_no_gst = ($transactions->total_amount - $transactions->discounted_amount) + $delivery_charge;
                        else
                            $total_amount_no_gst = 0;
                        
                        //$total_amount = $total_amount_no_gst + $gst_charge;
						$total_amount = $total_amount_no_gst;
                        /* end Computation */
                        
                        
                        
			
                        //$delivery_amount = Mc_delivery_report_model::Get_Total_Delivery_Amount($transactions->delivery_id);
			echo "<td><span class=\"pull-right\">".number_format($total_amount,2)."</span></td>\n";
                        
                        if (isset($print_mode)) {
                            echo '<td>'.$transactions->delivered_to.'</td>';
                            echo '<td>'.$transactions->address.' '.$transactions->postal_code.'</td>';
                            echo '<td>'.$transactions->delivered_contact.'</td>';
                        }
                        
			
                        if (!isset($print_mode))
                            echo '<td><input type="checkbox" class="delivery_checkbox" name="delivery_id[]" value="'.$transactions->delivery_id.'" checked /></td>';
                        /*echo '<td>';
                        echo '<a href="'.FRONT_URL.'admin_tools/print_delivery_notes/'.$transactions->id.'" class="btn" target="_blank"><i class="icon-shopping-cart"></i></a>';
                        echo '</td>';
                         * 
                         */
			
			echo "</tr>\n";
			$ctr++;
		}
                
                

		echo "</tbody>\n";
	} ?>
</table>