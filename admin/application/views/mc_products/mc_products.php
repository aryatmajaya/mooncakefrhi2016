<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<?php
$url = site_url("mc_products");
$this->check_permission->Check_Button_Add(' Product',$current_app_info->add_role, $url);
?>
<hr>
<table class="table table-striped table-condensed table-hover" id="sort_table">
	<thead>
		<tr>
			<th class="span6">Mooncake Type</th>
			<th class="span3">Category</th>
			<th class="span1">Qty/Box</th>
			<th class="span1">Price</th>
			<th class="span1" colspan="2">&nbsp;</th>
		</tr>
	</thead>
	
	<? if ($entries): ?>
		<tbody>
		<? $ctr = 1; ?>
		<? foreach ($entries as $entry): ?>
			<tr id='tr_<?= $entry->id ?>'>
			<td><?= nl2br($entry->product_name) ?></td>
			<td><?= $entry->category_name ?></td>
			<td><?= $entry->pc_per_box ?></td>
			<td><?= $entry->price ?></td>
			
			<td>
			<? if($entry->status == 1): ?>
				<span data-id="<?= $entry->id; ?>" class="label label-success tooltip-button close-button" data-toggle="tooltip" title data-original-title="Click to De-activate">Active</span>
			<? else: ?>
				<span data-id="<?= $entry->id; ?>" class="label tooltip-button open-button" data-toggle="tooltip" title data-original-title="Click to Activate">Inactive</span>
			<? endif; ?>
			</td>

			<td>
			<?= $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entry->id, $url); ?>
			</td>
			<td class='move_td' style='background-color: #CCC; width: 30px; text-align: center;'><img src='<?=base_url()?>assets/images/move_icon.png' id='move_icon'></td>
			</tr>
			<? $ctr++; ?>
		<? endforeach; ?>

		</tbody>
	<? endif; ?>
</table>
<!-- FOR JQUERY SORTABLE -->
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<!-- END jQuery Sortable -->

<script type="text/javascript">
	$(function(){



    $('.close-button').click(function(e){
    	e.preventDefault();
       	var confirm_close = confirm("Are you sure you want to de-activate this product?");
        if (confirm_close == true){
            var the_id=$(this).data('id');
            $.post("<?php echo $url;?>/ajax_close_product",{the_id:the_id},function(result){
				window.location.reload();
            });
        }
    });
    
    $('.open-button').click(function(e){
    	e.preventDefault();
        var the_id=$(this).data('id');
        $.post("<?php echo $url;?>/ajax_open_product",{the_id:the_id},function(result){
               window.location.reload();
        });

    });
	
	
	
	$('.hide-button').click(function(e){
    	e.preventDefault();
       	var confirm_hide = confirm("Are you sure you want to hide this product on frontend?");
        if (confirm_hide == true){
            var the_id=$(this).data('id');
            $.post("<?php echo $url;?>/ajax_hide_in_front",{the_id:the_id},function(result){
				window.location.reload();
            });
        }
    });
    
    $('.show-button').click(function(e){
    	e.preventDefault();
        var the_id=$(this).data('id');
        $.post("<?php echo $url;?>/ajax_show_in_front",{the_id:the_id},function(result){
               window.location.reload();
        });

    });



    $('.tooltip-button').tooltip();
    $("#sort_table tbody").sortable({ 
			//helper: fixHelper,
			opacity: 0.6, 
			cursor: 'move',
			items: 'tr:not(.tableHeader,.not_sortable)',
			handle: '.move_td',			
			update: function() {
				var order = $(this).sortable("serialize"); 
				console.log($(this));
				$.post("<?php echo $url;?>/update_app_sortorder", order, function(theResponse){
					//$("#contentRight").html(theResponse);
				}); 															 
			}								  
		});
    });
</script>