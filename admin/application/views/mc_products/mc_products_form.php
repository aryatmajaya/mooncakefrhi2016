
<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<form class="form-horizontal" method="POST" id="product_form" name="product_form" action="<?=($action == "add")?site_url('mc_products/add'):site_url('mc_products/update');?>"/>
	
	<?php
	if ($this->session->flashdata('required_error')){
		echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
	}
	
	if ($action == "edit"){
		echo '<input type="hidden" name="id" value="'.$product->id.'" />';
	}
	?>
	
    <div class="control-group">
		<label class="control-label">Product Name</label>
		<div class="controls">
			<textarea name="product[product_name]" required rows="4" cols="50"><?=($action == "add")?set_value('product[product_name]'):$product->product_name?></textarea>
		</div>
	</div>
    
    <div class="control-group">
		<label class="control-label">Product Name China</label>
		<div class="controls">
			<input type="text" name="product[product_name_china]" required rows="4" cols="50" value="<?=($action == "add")?set_value('product[product_name_china]'):$product->product_name_china?>">
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label">Product Price</label>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on">S$</span>
				<input type="text" class="span2" name="product[price]" placeholder="00.00" required value="<?=($action == "add")?set_value('product[price]'):$product->price?>">
			</div>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Qty per box</label>
		<div class="controls">
			<input type="text" class="span1" name="product[pc_per_box]" required value="<?=($action == "add")?0:$product->pc_per_box?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Category</label>
		<div class="controls">
			<select name="product[category_id]">
				<option value="">Select Category</option>
				<?php foreach ($categories as $key => $category): ?>
					<option value="<?= $category->id ?>" 
						<?= ($action == "add" || $product->category_id == $category->id)? ' selected="selected"': ""; ?>
					><?= $category->name ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inputEmail">Status</label>
		<div class="controls">
			<label class="radio">
				<input type="radio" name="product[status]" value="1"
				<?= ($action == "add") ? 'checked="checked"' : (($product->status == 1)? 'checked="checked"' : '')?>
				 />Active
			</label>
			<label class="radio">
				<input type="radio" name="product[status]" value="0" <?= ($action == "edit" && $product->status == 0)? 'checked="checked"' : ''?>/>Inactive
			</label>
		</div>
	</div>
    
    <div class="control-group">
		<label class="control-label" for="inputEmail">Show on frontend</label>
		<div class="controls">
			<label class="radio">
				<input type="radio" name="product[show_in_front]" value="1"
				<?= ($action == "add") ? 'checked="checked"' : (($product->show_in_front == 1)? 'checked="checked"' : '')?>
				 />Show
			</label>
			<label class="radio">
				<input type="radio" name="product[show_in_front]" value="0" <?= ($action == "edit" && $product->show_in_front == 0)? 'checked="checked"' : ''?>/>Hide
			</label>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="">Image</label>
		<div class="controls">
			<span class="btn btn-success fileinput-button">
				<i class="glyphicon glyphicon-plus"></i>
					<span>Add files...</span>
					<!-- The file input field used as target for the file upload widget -->
					<input id="fileupload" type="file" data-url="<?php echo site_url("mc_products/ajax_upload");?>" multiple>
			</span>
			<div class="clearfix"></div>
			<div id="progress" class="progress progress-striped active hide" style="margin-top:10px; width:50%;">
				<div class="bar" style="width: 0%;"></div><span class="percent">100%</span>
			</div>
			<div id="imgpreview" style="display:inline-block; margin-top:10px;">
				<?php if ($product && $product->images): ?>
					<?php $product_images = json_decode($product->images); ?>
					<?php foreach ($product_images as $image_path): ?>
						<div class="product_thumbnail">
							<a class="close remove-image" href="#">&times;</a>
							<img src="<?= base_url()?>../img/products/<?= $image_path ?>" alt="" width="80" class="img-polaroid">
							<input type="hidden" name="product[product_images][]" value="<?= $image_path ?>">
						</div>
					<?php endforeach; ?>
				<?php endif ?>
				
			</div>
		</div>
	</div>
    
	<div class="control-group">
		<label class="control-label" for="">Is New?</label>
		<div class="controls">
			<label class="radio">
		<input type="radio" name="product[is_new]" value="1"
			<?= ($action == "add") ? 'checked="checked"' : (($product->is_new == 1)? 'checked="checked"' : '')?>
				 />New
	</label>
	<label class="radio">
			<input type="radio" name="product[is_new]" value="0" <?= ($action == "edit" && $product->is_new == 0)? 'checked="checked"' : ''?>/>Old
	</label>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">Short Description
		<small><i>(This will appear in the product listing)</i></small></label>
		<div class="controls editor-wrapper">
			<textarea name="product[short_description]" class="span5" id="" cols="60" rows="6"><?= ($product) ? trim($product->short_description):'' ?></textarea>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">HTML Description
		<small><i>(This will appear in the product's detail/page)</i></small></label>
		<div class="controls editor-wrapper">
			<textarea name="product[description]" id="editor" cols="30" rows="20"><?= ($product) ? trim($product->description) : ''?></textarea>
		</div>
	</div>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="<?php echo site_url("mc_products"); ?>" class="btn">Cancel</a>
	</div>
</form>

<script>
	var httpHost = '//<?= $_SERVER["HTTP_HOST"]?>';
	$(function(){
		$('#fileupload').fileupload(
			{
				dataType: 'json',
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .bar').css(
						'width',
						progress + '%'
					);
				},
				start: function(e, data){
					console.log("START->");
					console.log(e, data);
					$("#progress").removeClass('hide');
				},
				done: function(e, data){
					
					if(data.textStatus == 'success'){
						$("#progress").addClass('hide');
						
						$(data.result.files).each(function(i, e){
							$("#imgpreview").append('<img src="'+httpHost+e.thumbnailUrl+'"/>');
							$("#product_form").append('<input type="hidden" name="product[product_images][]" value="'+e.tempFolder + e.name +'">');
						})

					}
				},
				fail: function(e, data){
					$("#progress").append("Error uploading the file.");
				}
			}
		);

		$(".product_thumbnail a.remove-image").on('click', function(e){
			e.preventDefault();
			var answer  = confirm("Do you want to delete this image? Press OK to do so. Otherwise, press Cancel.");
			if(answer){
				$(this).closest('.product_thumbnail').remove();
			}
		});
	})
</script>
