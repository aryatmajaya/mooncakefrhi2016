<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<?php
$url = site_url('mc_promocodes');
$this->check_permission->Check_Button_Add(' Promocode',$current_app_info->add_role, $url);
?>
<hr>
<table class="table table-striped table-condensed">
	<thead>
		<th></th>
		<th>Promo code</th>
		<th>Credit Card</th>
		<th>Discount</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Status</th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($promocodes as $key => $value): ?>
			<tr>
				<td><?= ($key + 1)?></td>
				<td><?= $value->promocode ?></td>
				<td><?= $value->card_name ?></td>
				<td><?= $value->discount ?>%</td>
				<td><?= $value->start_date ?></td>
				<td><?= $value->end_date ?></td>
				<td><?= ($value->expired) ? 'Expired' : 'Active' ?></td>
				<td>
					<?= $this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role, $value->id, $url); ?>
					<?= $this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$value->id); ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<script>
	$(function(){
		$(".delete-button").on('click', function(e){
			var conf = confirm("Do you want to delete the promocode?");
			if(conf){
				$.post('<?= site_url('mc_promocodes');?>/delete/'+$(this).val(), function(data){
					location.href="<?= site_url('mc_promocodes');?>";
				});
			}
		});
	});
</script>