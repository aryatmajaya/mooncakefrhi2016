<hr>

<?php if (validation_errors()):?>
      <div class="alert alert-error"><?= validation_errors() ?></div>
    <?php endif ?>
<form action="" class="form form-horizontal" role="form" method='POST'>
	<?php if (isset($promocode) && isset($promocode->id)): ?>
		<input type="hidden" name="promocode[id]" id="id" value="<?= $promocode->id ?>">
	<?php endif ?>
	<div class="control-group">
	    <label class="control-label">Promo code</label>
	    <div class="controls">
	        <input type="text" name="promocode[promocode]" value="<?=isset($promocode) ? $promocode->promocode : ''?>">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label">Credit Card</label>
	    <div class="controls">
	    	<select name="promocode[credit_card_id]" id="">

	    		<option value="">Choose...</option>
	    		<?php foreach ($creditcards as $key => $value): ?>
	    			<option value="<?= $value->id?>"
						<?= (isset($promocode) && ($value->id == $promocode->credit_card_id)) ? 'selected' : '' ?>
	    			><?= $value->name ?></option>
	    		<?php endforeach ?>
	    	</select>
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label">Discount</label>
	    <div class="controls">
	    	<div class="input-append">
	        	<input type="text" class="" name="promocode[discount]" value="<?=isset($promocode) ? $promocode->discount : ''?>">
	        	<span class="add-on">%</span>
	    	</div>
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label">Start date</label>
	    <div class="controls">
	    	<div class="input-append">
	        	<input type="text" class="from_date" name="promocode[start_date]" value="<?=isset($promocode) ? $promocode->start_date : ''?>">
	    	</div>
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label">End date</label>
	    <div class="controls">
	    	<div class="input-append">
	        	<input type="text" class="to_date" name="promocode[end_date]" value="<?=isset($promocode) ? $promocode->end_date : ''?>">
	        	
	    	</div>
	    </div>
	</div>
	<div class="control-group">
	    <div class="controls">
	    	<button type="submit" class="btn btn-primary">Save</button>
	    	<a href="<?php echo site_url('mc_promocodes'); ?>" class="btn btn-default">Cancel</a>	
	    </div>
	</div>
</form>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script>
	$('.from_date').datepicker({
        showOn: "both",
        dateFormat: "dd-M-yy",
        changeMonth: true,
        changeYear: true,
        setDate: "dd-M-yy",
        buttonText: '<i class="icon-calendar"></i>',
        onClose: function( selectedDate ) {
            $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
        }
    });

    $('.to_date').datepicker({
        showOn: "both",
        dateFormat: "dd-M-yy",
        changeMonth: true,
        changeYear: true,
        buttonText: '<i class="icon-calendar"></i>',
        onClose: function( selectedDate ) {
            $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
        }
    });

    $(".icon-calendar").parent('button').addClass('add-on').css('height','auto');
</script>