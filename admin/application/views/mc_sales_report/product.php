
<?

//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
$allowed_collection_date = array('in-process', 'completed');
$order_type_array = array ('online', 'corporate');
?>


<?php
if ($view_type =="html"):
?>    







<div class="well well-small" id="total-sales-tool">
    <form class="" method="POST" action="<?=site_url("mc_sales_report/product");?>">
        <div class="span5">
        <!--<input type="hidden" name="view_type" value="html" />-->
        <div class="control-group">
            <label class="control-label" for="inputEmail">Order Date:</label>
            <div class="controls">
                <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=$from_date?>" />
                <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=$to_date?>" />
            </div>
        </div>
            <div class="control-group">
            <label class="control-label" for="inputPassword">Status:</label>
            <div class="controls">
                <?php
                    $ctr = 1;
                    $last_key = end(array_keys($status_list));
                    foreach ($status_list as $key => $value){
                        if (!in_array($key, array(4,6,7))):
                            if ($ctr == 1):
                                echo '<div class="pull-left" style="padding-right: 10px;">';
                                
                            endif;
                            echo '<label class="checkbox">';
                            echo '<input type="checkbox" name="order_status[]" value="'.$key.'"';
                            if (in_array($key, $order_status))
                                echo ' checked';
                            echo ' />'.strtoupper($value);
                            echo '</label>';
                            
                            if ($ctr == 3):
                                echo '</div>';
                                $ctr = 0;
                            endif;
                            
                            $ctr++;
                            
                            if ($key == $last_key):
                                echo '</div>';
                            endif;
                            
                            
                        endif;
                            /*echo "<option value=\"$key\"";
                              if ($order_status == $key)
                                echo " selected";
                            echo ">".(($value == "paid-open")?'PAID & OPEN':strtoupper($value))."</option>\n";
                             * 
                             */
                          }                    
                        ?>
                    
                </div>
            </div>
        </div>
        <div class="span5">
        <div class="control-group">
            <label class="control-label" for="inputPassword">Order Type:</label>
            <div class="controls">
                 
                    <?php
                    foreach ($order_type_array as $ot):
                        echo '<label class="checkbox">';
                            echo '<input type="checkbox" name="order_type[]" value="'.$ot.'"';
                            if (in_array($ot, $order_type))
                                echo ' checked';
                            echo ' />'.strtoupper($ot);
                            echo '</label>';
                        //echo '<option';
                        /*if ($order_type == $ot)
                            echo ' selected';
                        echo '>'.$ot.'</option>';
                         * 
                         */
                    endforeach;
                    
                    ?>
                
            </div>
        </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">Export :</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" name="view_type" value="xls" />Excel Format
                    </label>
                </div>
            </div>
        </div>
        
        <div class="span2" style="padding-top: 50px;">
            
            <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary">GO</button>
            </div>
            </div>
        </div>
        
        
<!--        <input type="hidden" name="view_type" value="<?=$view_type?>" />
        <input type="hidden" id="status_type" name="status_type" value="<?=$view_type?>" />-->
        
    </form>
    <div class="clearfix"></div>
</div>
<!-- End Report Filter -->
<?php endif; ?>


<?=(isset($pagination_links))?$pagination_links:'';?>
<table class="" border="1">
		<tr>
			<td width="220" nowrap><strong>Product</strong></td>
                        <?php
			$startTime = strtotime($from_date);
                        $endTime = strtotime($to_date);

                        // Loop between timestamps, 24 hours at a time
                        for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
                          $thisDate = date('d-M', $i); // 2010-05-01, 2010-05-02, etc
                          echo '<td align="center"><strong>'.$thisDate.'</strong></td>';
                        }
                        ?>
                        <td width="50" align="center"><strong>Products</strong></td>
                        
		</tr>
	<?php
	if ($products) {
                //$order_status = $this->mc_utilities->order_status();
            
		
		$ctr = 1;
                
		foreach ($products as $product){
                    $total_product = 0;
		    echo "<tr>\n";
                    echo "<td>";
                    //echo "($product->prod_id) ";
                    echo "$product->product_name</td>";
                    for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
                        $total = 0;
                          $thisDate = date('Y-m-d', $i); // 2010-05-01, 2010-05-02, etc
                          echo '<td align="center" nowrap>';
                          //echo '<div class="text-center">';
                          $report = mc_sales_report_model::GetSalesReportPerDate($product->prod_id, $thisDate, $order_status, $order_type);
                          //var_dump($report);
                          //echo '<div class="text-center">';
                        if ($report):
                            foreach($report as $r):
                                echo $r->the_date.'00 - '.$r->total_qty.'<br />';
                                $total += $r->total_qty;
                            endforeach;
                            //'</div>';
                        endif;
                            
                        echo 'Total: '.$total;
                        //echo '</div>';
                          echo '</td>';
                        
                        $total_product += $total;
                    }
                    echo '<td align="center">'.$total_product.'</td>';
                    echo "</tr>\n";
                    $ctr++;
		}
                
        

	} ?>
</table>
<?=(isset($pagination_links))?$pagination_links:'';?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        
        
        
        
        
        $('.from_date').datepicker({
            //showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            //buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date').datepicker({
            //showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            
            //buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        
        
    });
    
	
</script>