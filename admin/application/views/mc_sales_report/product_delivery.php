
<?

//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
$allowed_collection_date = array('in-process', 'completed');
$order_type_array = array ('online', 'corporate');
?>


<?php
if ($view_type =="html"):
?>    

<div class="well well-small" id="total-sales-tool">
    <form class="" method="POST" action="<?php echo $action;?>">
        <div class="span5">
        <div class="control-group">
            <label class="control-label" for="inputEmail"><?php echo $title;?> Date:</label>
            <div class="controls">
                <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=$from_date?>" />
                <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=$to_date?>" />
            </div>
        </div>
            <div class="control-group">
            <label class="control-label" for="inputPassword">Status:</label>
            <div class="controls">
                <?php
                    $ctr = 1;
                    $last_key = end(array_keys($status_list));
                    foreach ($status_list as $key => $value){
                        if (!in_array($key, array(4,6,7))):
                            if ($ctr == 1):
                                echo '<div class="pull-left" style="padding-right: 10px;">';
                                
                            endif;
                            echo '<label class="checkbox">';
                            echo '<input type="checkbox" name="order_status[]" value="'.$key.'"';
                            if (in_array($key, $order_status))
                                echo ' checked';
                            echo ' />'.strtoupper($value);
                            echo '</label>';
                            
                            if ($ctr == 3):
                                echo '</div>';
                                $ctr = 0;
                            endif;
                            
                            $ctr++;
                            
                            if ($key == $last_key):
                                echo '</div>';
                            endif;
                            
                            
                        endif;
                            /*echo "<option value=\"$key\"";
                              if ($order_status == $key)
                                echo " selected";
                            echo ">".(($value == "paid-open")?'PAID & OPEN':strtoupper($value))."</option>\n";
                             * 
                             */
                          }                    
                        ?>
                    
                </div>
            </div>
        </div>
        <div class="span5">
        <div class="control-group">
            <label class="control-label" for="inputPassword">Order Type:</label>
            <div class="controls">
                 
                    <?php
                    foreach ($order_type_array as $ot):
                        echo '<label class="checkbox">';
                            echo '<input type="checkbox" name="order_type[]" value="'.$ot.'"';
                            if (in_array($ot, $order_type))
                                echo ' checked';
                            echo ' />'.strtoupper($ot);
                            echo '</label>';
                        //echo '<option';
                        /*if ($order_type == $ot)
                            echo ' selected';
                        echo '>'.$ot.'</option>';
                         * 
                         */
                    endforeach;
                    
                    ?>
                
            </div>
        </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">Export :</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" name="view_type" value="xls" />Excel Format
                    </label>
                </div>
            </div>
        </div>
        
        <div class="span2" style="padding-top: 50px;">
            
            <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary">GO</button>
            </div>
            </div>
        </div>
        
        
<!--        <input type="hidden" name="view_type" value="<?=$view_type?>" />
        <input type="hidden" id="status_type" name="status_type" value="<?=$view_type?>" />-->
        
    </form>
    <div class="clearfix"></div>
</div>
<!-- End Report Filter -->
<?php endif; ?>


<?=(isset($pagination_links))?$pagination_links:'';?>

<?php
$startTime = strtotime($from_date);
$endTime = strtotime($to_date);
?>			

<?php //print_r($deliveries);?>

<table cellspacing="0" cellpadding="3" border="1" class="">
		<tr>
			<td width="220" nowrap><strong>Product</strong></td>
           	<?php
				for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
				  $thisDate = date('d-M', $i); // 2010-05-01, 2010-05-02, etc
				  echo '<td align="center"><strong>'.$thisDate.'</strong></td>';
				}
			?>
            <td width="50" align="center"><strong>Total Products</strong></td>
		</tr>
		<?php if($products){ ?>
			<?php foreach($products as $product){ ?>
            	<tr>
                	<td><?php echo $product->product_name;?></td>
                    <?php $total = 0; for ($i = $startTime; $i <= $endTime; $i = $i + 86400){ ?>
                    	<?php $add = isset($deliveries[$product->product_id][$i])?$deliveries[$product->product_id][$i]:"0";?>
						<td><?php echo $add; $total+= $add; ?></td>
                    <?php } ?>
                    <td><?php echo $total;?></td>
                </tr>
			<?php } ?>
		<?php } ?>
</table>
<?=(isset($pagination_links))?$pagination_links:'';?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {     
        $('.from_date').datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date').datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        
        
    });
    
	
</script>