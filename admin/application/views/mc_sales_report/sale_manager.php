
<?

//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
$allowed_collection_date = array('in-process', 'completed');
$order_type_array = array ('online', 'corporate');
?>


<?php
if ($view_type =="html"):
?>    

<!-- Report Filter -->



<div class="well well-small" id="total-sales-tool">
    <form class="" method="POST" action="<?= site_url("mc_sales_report/sale_manager");?>">
        <div class="span5">
        
            <!--<input type="hidden" name="view_type" value="html" />-->
            <div class="control-group">
                <label class="control-label" for="inputEmail">Delivery Date:</label>
                <div class="input-append">
                    <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=$from_date?>" />
                    <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=$to_date?>" style="margin-left: 10px;" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">Status:</label>
                <div class="controls">
                <?php
                    $ctr = 1;
                    $last_key = end(array_keys($status_list));
                    foreach ($status_list as $key => $value){
                        if (!in_array($key, array(4,6,7))):
                            if ($ctr == 1):
                                echo '<div class="pull-left" style="padding-right: 10px;">';
                                
                            endif;
                            echo '<label class="checkbox">';
                            echo '<input type="checkbox" name="order_status[]" value="'.$key.'"';
                            if (in_array($key, $order_status))
                                echo ' checked';
                            echo ' />'.strtoupper($value);
                            echo '</label>';
                            
                            if ($ctr == 3):
                                echo '</div>';
                                $ctr = 0;
                            endif;
                            
                            $ctr++;
                            
                            if ($key == $last_key):
                                echo '</div>';
                            endif;
                            
                            
                        endif;
                            /*echo "<option value=\"$key\"";
                              if ($order_status == $key)
                                echo " selected";
                            echo ">".(($value == "paid-open")?'PAID & OPEN':strtoupper($value))."</option>\n";
                             * 
                             */
                          }                    
                        ?>
                    
                </div>
            </div>
        
        </div>
        <div class="span4">
            <!--<div class="control-group">
                <label class="control-label" for="inputPassword">Order Type:</label>
                <div class="controls">
                    <select name="order_type">
                        <?php
                        foreach ($order_type_array as $ot):
                            echo '<option';
                            if ($order_type == $ot)
                                echo ' selected';
                            echo '>'.$ot.'</option>';
                        endforeach;

                        ?>
                    </select>
                </div>
            </div>-->
            <div class="control-group">
                <label class="control-label" for="inputPassword">Sales Manager:</label>
                <div class="controls">
                    <select name="sales_manager">
                        <option value="View All">View All</option>
                        <?php
                        foreach ($managers as $sm):
							if($sales_manager == $sm->sales_manager){
								echo '<option selected="selected">'.$sm->sales_manager.'</option>';
							}else{
                            	echo '<option>'.$sm->sales_manager.'</option>';
							}
                        endforeach;

                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">Export :</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" name="view_type" value="xls" />Excel Format
                    </label>
                </div>
            </div>
        
        </div>
        <div class="span2" style="padding-top: 50px;">
            
            <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary">GO</button>
            </div>
            </div>
        </div>
        
        
        
        
        
        
        <!--<input type="hidden" name="view_type" value="<?=$view_type?>" />-->
        <!--<input type="hidden" id="status_type" name="status_type" value="<?=$view_type?>" />-->
        
    </form>
    <div class="clearfix"></div>
</div>
<!-- End Report Filter -->
<?php endif; ?>


<?=(isset($pagination_links))?$pagination_links:'';?>
<table class="" border="1" cellspacing="0" cellpadding="3">
		<tr>
                    <td><strong>#</strong></td>
                    <td><strong>Sales Manager Name</strong></td>
                    <td><strong>Delivery Date</strong></td>
                    <td><strong>Order No</strong></td>
                    <td><strong>Order Status</strong></td>
                    <td><strong>Company Name</strong></td>
                    <td><strong># of items</strong></td>
                    <td><strong>Gross</strong></td>
                    <td><strong>Discount (%)</strong></td>
                    <td><strong>Discount</strong></td>
                    <td><strong>Delivery</strong></td>
                    <?php /*<td><strong>GST</strong></td>*/ ?>
                    <td><strong>Net</strong></td>
		</tr>
	<?php
	if ($reports) {
                //$order_status = $this->mc_utilities->order_status();
            
		
		$ctr = 1;
                $sales_manager = "";
                $total_gross = 0;
                $total_discount = 0;
                $total_discount_amount = 0;
                $total_delivery_amount = 0;
                $total_gst_amount = 0;
                $total_net_amount = 0;
                
		foreach ($reports as $report){
                    if (strtolower(trim($sales_manager)) != strtolower(trim($report->sales_manager))) {
                        $sales_manager = $report->sales_manager;
                        echo "<tr bgcolor=\"#EEE\"><td colspan=\"7\"></td>";
                        echo "<td align=\"right\" nowrap><strong>SGD ".number_format($total_gross,2)."</strong></td>";
                        echo "<td align=\"right\" nowrap><strong>".number_format($total_discount,2)."%</strong></td>";
                        echo "<td align=\"right\" nowrap><strong>SGD ".number_format($total_discount_amount,2)."</strong></td>";
                        echo "<td align=\"right\" nowrap><strong>SGD ".number_format($total_delivery_amount,2)."</strong></td>";
                        //echo "<td align=\"right\" nowrap><strong>SGD ".number_format($total_gst_amount,2)."</strong></td>";
                        echo "<td align=\"right\" nowrap><strong>SGD ".number_format($total_net_amount,2)."</strong></td>";
                        echo "</tr>";
                        $total_gross = 0;
                        $total_discount = 0;
                        $total_discount_amount = 0;
                        $total_delivery_amount = 0;
                        $total_gst_amount = 0;
                        $total_net_amount = 0;
                    }
                    
                    
                    
                    
                    $discount_amount = number_format($report->gross,2,'.','') * ($report->corporate_discount/100);
                    $gst_charge = ((number_format($report->gross,2,'.','') - number_format($discount_amount,2,'.','')) + $report->delivery_charge) * (GST_PERCENT / 100);
                    //$net = ((number_format($report->gross,2,'.','') - number_format($discount_amount,2,'.','')) + $report->delivery_charge) + $gst_charge;
					$net = ((number_format($report->gross,2,'.','') - number_format($discount_amount,2,'.','')) + $report->delivery_charge);
                    
                    
                    $total_gross += number_format($report->gross,2,'.','');
                    $total_discount += number_format($report->corporate_discount,2,'.','');
                    $total_discount_amount += number_format($discount_amount,2,'.','');
                    $total_delivery_amount += number_format($report->delivery_charge,2,'.','');
                    $total_gst_amount += number_format($gst_charge,2,'.','');
                    $total_net_amount += number_format($net,2,'.','');
                    
		    echo "<tr>\n";
                    echo "<td align=\"right\">$ctr</td>\n";
                    echo "<td>$report->sales_manager</td>\n";
                    echo "<td>".date("d-M-y", strtotime($report->service_date))."</td>\n";
                    echo "<td>$report->order_ref</td>\n";
                    echo "<td>".strtoupper(mc_utilities::order_status($report->status))."</td>\n";
                    echo "<td>$report->company_name</td>\n";
                    echo "<td align=\"center\">$report->numitems</td>\n";
                    echo "<td align=\"right\">SGD ".number_format($report->gross,2)."</td>\n";
                    echo "<td align=\"right\">".number_format($report->corporate_discount,2)."%</td>\n";
                    echo "<td align=\"right\">SGD ".number_format($discount_amount,2)."</td>\n";
                    echo "<td align=\"right\">SGD ".number_format($report->delivery_charge,2)."</td>\n";
                    //echo "<td align=\"right\">SGD ".number_format($gst_charge,2)."</td>\n";
                    echo "<td align=\"right\">SGD ".number_format($net,2)."</td>\n";
                    
                    

                    
                    echo "</tr>\n";
                    
                    
                    
                    $ctr++;
		}
                
        

	} ?>
</table>
<?=(isset($pagination_links))?$pagination_links:'';?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        
        
        
        
        
        $('.from_date').datepicker({
            //showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            //buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date').datepicker({
            //showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            
            //buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        
        
    });
    
	
</script>