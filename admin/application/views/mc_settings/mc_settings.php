<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
?>
<div class="span6">
    <h4>Block Collection Dates</h4>
    <form name="bc_form" id="bc_form" method="POST" action="add_bc" class="form-inline">
        <div class="input-append">
            <input type="text" name="bc_date" id="bc_date" readonly="readonly" class="span2" />
        </div>
        <button id="add_bc" type="submit" class="btn btn-primary">Add</button></h4>
    </form>

    <table class="table table-striped table-condensed table-hover">
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>Date</th>
    			<th>&nbsp;</th>
    		</tr>
    	</thead>
    	<?php
    	if ($bc) {
    		echo "<tbody>\n";
    		$ctr = 1;
    		foreach ($bc as $bc){
    			echo "<tr id='tr_".$bc->id."'>\n";
    			echo "<td>".$ctr."</td>\n";
    			echo "<td>".date("d F Y",strtotime($bc->block_date))."</td>\n";			
    			echo "<td>";
    			$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$bc->id);
    			echo "</td>\n";
    			echo "</tr>\n";
    			$ctr++;
    		}
    		echo "</tbody>\n";
    	} ?>
    </table>
</div>

<!-- Block Delivery Dates -->
<div class="span6">
    <h4>Block Delivery Dates</h4>
    <form name="bd_form" id="bd_form" method="POST" action="add_bd" class="form-inline">
        <div class="input-append">
            <input type="text" name="bd_date" id="bd_date" readonly="readonly" class="span2" />
        </div>
        <button id="add_bd" type="submit" class="btn btn-primary">Add</button></h4>
    </form>
    <table class="table table-striped table-condensed table-hover">
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>Date</th>
    			<th>&nbsp;</th>
    		</tr>
    	</thead>
    	<?php
    	if ($bd) {
    		echo "<tbody>\n";
    		
    		$ctr = 1;
    		foreach ($bd as $bd){
    			echo "<tr id='tr_".$bd->id."'>\n";
    			echo "<td>".$ctr."</td>\n";
    			
    			echo "<td>".date("d F Y",strtotime($bd->block_date))."</td>\n";			

    			echo "<td>";
    			//$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id);

    			$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$bd->id);
                            
                                    
    				
    			echo "</td>\n";

    			echo "</tr>\n";
    			$ctr++;
    		}

    		echo "</tbody>\n";
    	} ?>
    </table>
</div>
<!-- Promotion Rules -->
<!-- <div class="span12">
    <hr />
    <h4>Promotion Rules</h4>
    <form name="bd_form" id="bd_form" method="POST" action="update_promotion_rules" class="form-inline">

        <table class="table table-striped table-condensed table-hover">
            <thead>
                <tr>
                    <th >Promotion</th>
                    <th >From</th>                            
                    <th >To</th>
                    <th >Discount</th>
                    
                </tr>
            </thead>
                <?php
                if ($pr) {
                        echo "<tbody>\n";
                        $ctr = 1;
                        foreach ($pr as $pr){
                                echo "<tr id='tr_".$pr->id."'>\n";
                                echo "<td>\n";
                                echo ucwords($pr->promo_name);
                                echo "<input type=\"hidden\" name=\"promo[$pr->id]\" value=\"$pr->id\" />";
                                echo "</td>\n";
                                echo "<td><div class='input-append'><input type=\"text\" name=\"date_start[$pr->id]\" class=\"from_date\" value=\"".date("d F Y",strtotime($pr->date_start))."\" readonly=\"readonly\" /></div></td>\n";
                                echo "<td><div class='input-append'><input type=\"text\" name=\"date_end[$pr->id]\" class=\"to_date\" value=\"".date("d F Y",strtotime($pr->date_end))."\" readonly=\"readonly\" /></div></td>\n";
                                echo "<td><div class='input-append'><input type=\"text\" name=\"discount[$pr->id]\" class=\"span1 text-center\" value=\"".$pr->discount."\" /><span class='add-on'>%</span></div></td>\n";
                                echo "</tr>\n";
                                $ctr++;
                        }
                        echo "</tbody>\n";
                } ?>
        </table>
        <div class="form-actions text-center">
          <button type="submit" class="btn btn-primary">Update</button>      
        </div>
    </form>
</div> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $('button#delete-button').click(function(){
        var confirm_close = confirm("Are you sure you want to delete?");
        if (confirm_close == true){
            var the_id=$(this).val();
            $.post("ajax_delete_date",{the_id:the_id},function(result){                    
                window.location.reload();                    
            });
        }
    });
    
    

$(document).ready(function(){
    $('#bc_date').datepicker({
        showOn: "both",
        dateFormat: "dd MM yy",
        
        buttonText: '<i class="icon-calendar"></i>'        
    });
    $('#bd_date').datepicker({
        showOn: "both",
        dateFormat: "dd MM yy",
        
        buttonText: '<i class="icon-calendar"></i>'        
    });
    
    $('.from_date, .to_date').datepicker({
        showOn: "both",
        dateFormat: "dd MM yy",
        changeMonth: true,
        changeYear: true,
        buttonText: '<i class="icon-calendar"></i>'        
    });
    
    $('form#bc_form #add_bc').click(function(e){        
        //e.preventDefault();        
        var error_text = '';
        var hasError = false;
        
        var bc_date = $('#bc_date').val();
        if (bc_date == ''){
                hasError = true;
                error_text += "Date is required.";
        }        
        if(hasError == false) {
                return true;         
        }
        else{
            alert(error_text);
            return false;
        }
    });
    
    $('form#bd_form #add_bd').click(function(e){        
        //e.preventDefault();        
        var error_text = '';
        var hasError = false;
        
        var bd_date = $('#bd_date').val();
        if (bd_date == ''){
                hasError = true;
                error_text += "Date is required.";
        }        
        if(hasError == false) {
                return true;         
        }
        else{
            alert(error_text);
            return false;
        }
    });

    $(".icon-calendar").parent('button').addClass('add-on btn').css({height:'auto'});
    
    
});
</script>