<?
//$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
$allowed_collection_date = array('in-process', 'completed');
?>
<?= $this->load->view('mc_transactions/partials/filter_tools') ?>

<ul class="nav nav-tabs">
  <li<?=($view_type=="")?' class="active"':''?>>
    <a href="<?=site_url($this->router->fetch_class());?>/index/all/">View All</a>
  </li>
  <li<?=($view_type=="in-process")?' class="active"':''?>><a href="<?=site_url($this->router->fetch_class()).'/index/in-process/'; ?>">In-Process</a></li>
  <li<?=($view_type=="completed")?' class="active"':''?>><a href="<?=site_url($this->router->fetch_class()).'/index/completed/'; ?>">Completed</a></li>
  
  <li<?=($view_type=="collected")?' class="active"':''?>><a href="<?=site_url($this->router->fetch_class()).'/index/collected/'?>">Collected/Delivered</a></li>
  
  <li<?=($view_type=="cancelled")?' class="active"':''?>><a href="<?=site_url($this->router->fetch_class()).'/index/cancelled/'?>">Cancelled</a></li>
  <li<?=($view_type=="error")?' class="active"':''?>><a href="<?=site_url($this->router->fetch_class()).'/index/error'?>">Problem CC</a></li>
  <li<?=($view_type=="incomplete")?' class="active"':''?>><a href="<?=site_url($this->router->fetch_class()).'/index/incomplete/'?>">Incomplete</a></li>
</ul>
<?=(isset($pagination_links))?$pagination_links:'';?>
<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Order Date</th>
            <?=(in_array($view_type, $allowed_collection_date))?'<th>Collection Date</th>':''?>
			<th>Order Ref</th>
			<th>Customer</th>
			<th><div class="text-right">Amount</div></th>
			<th></th>
			<th>
            	<?php if($this->input->get("sort")=="status" && $this->input->get("order")=="asc"){ ?>
            		<a href="?sort=status&order=desc" class="sort asc">Status</a>
                <?php }elseif($this->input->get("sort")=="status" && $this->input->get("order")=="desc"){ ?>
                	<a href="?sort=status&order=asc" class="sort desc">Status</a>
                <?php }else{ ?>
                	<a href="?sort=status&order=asc">Status</a>
                <?php } ?>
            </th>
            <th>Type</th>
			<th></th>
			<? 
            if ($this->session->userdata('sess_user_account_type_name') == "Super Admin")
                echo '<th>(Item Stats)</th>';
            ?>
		</tr>
	</thead>
	<?php
	if ($transactions) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
        $grand_total = 0;
		foreach ($transactions as $transactions){
			echo "<tr id='tr_".$transactions->id."'>\n";
			echo "<td>".date('d.m.y',strtotime($transactions->date_ordered))."</td>\n";
			if (in_array($view_type, $allowed_collection_date)):
                            echo "<td>".date('d.m.y',strtotime($transactions->service_date))."</td>\n";
                        endif;
			echo "<td><a href=\"".site_url("mc_transactions/view/".$transactions->id)."\">".$transactions->order_ref."</a></td>\n";
			echo "<td>".$transactions->first_name.' '.$transactions->last_name."</td>\n";
			 
            $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($transactions->order_ref);
            //var_dump($deliveries);
            $total_qty = Mc_transactions_model::Get_Total_Qty($transactions->id);
            
            if (isset($total_qty))
                $total_qty = array_sum($total_qty);
            else
                $total_qty = null;
            
            $delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty); //print_r($delivery_charge);
            $total_item_amount = Mc_transactions_model::Get_Order_Total_Amount($transactions->id); //print_r($total_item_amount);
            $gst_charge = ($transactions->total_amount - $transactions->discounted_amount + $delivery_charge) * (GST_PERCENT / 100);            //print_r($gst_charge);
            $total_amount_no_gst = ($transactions->total_amount - $transactions->discounted_amount) + $delivery_charge;
            //$total_amount = $total_amount_no_gst + $gst_charge;
			$total_amount = $total_amount_no_gst;
            
            
            
            
            $grand_total += number_format($total_amount,2);
            
            //$grand_total = $transactions->total_amount - $transactions->discounted_amount;
                        
            echo "<td>";
            //echo '<!-- '.number_format(common::Compute_Total_Amount($transactions->order_ref), 2).' -->';
            echo "<span class=\"pull-right\">$ ".number_format($total_amount,2)."</span></td>\n";
            echo "<td></td>\n";
            echo "<td>";
            
            $the_status = $this->mc_utilities->order_status($transactions->status);
			echo '<span class="label label-'.$this->mc_utilities->get_order_status_class($transactions->status) . '">'. strtoupper($the_status) .'<span>';
            
			//}
			echo "</td>\n";
            echo "<td>" . ucfirst($transactions->ordering_method) . "</td>";
            echo '<td>';
            //echo '<a href="'.FRONT_URL.'admin_tools/print_delivery_notes/'.$transactions->id.'" class="btn" target="_blank"><i class="icon-shopping-cart"></i></a>';
            echo '</td>';
            if ($this->session->userdata('sess_user_account_type_name') == "Super Admin") {                     
                $raw_total = Mc_transactions_model::Get_Raw_Total_Qty($transactions->id);
                echo "<td>";
                    echo '<span class="'.(($total_qty != $raw_total->total_qty)?'badge badge-important':'').'">';
                    echo $total_qty.'/'.$raw_total->total_qty;
                    echo '</span>';
                echo "</td>\n";
            }
			echo "</tr>\n";
			$ctr++;
		}
                
        if (isset($total_sales)) {
            echo '<tr class="info">';
            if (in_array($view_type, $allowed_collection_date))
                echo "<td>&nbsp;</td>\n";
            echo '<td colspan="3"><span class="pull-right"><strong>TOTAL:</strong></span></td>';
            echo '<td><span id="grand-total" class="pull-right">$ '.number_format($grand_total,2).'</span></td>';
            echo '<td>&nbsp;</td>';

            if ($this->session->userdata('sess_user_account_type_name') == "Super Admin") {                     
                echo "<td>&nbsp;</td>\n";
            }

            echo '</tr>';
        }

		echo "</tbody>\n";
	} ?>
</table>
<?php echo (isset($pagination_links))?$pagination_links:''; ?>
<div class="result"></div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/themes/base/jquery-ui.css" />
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#print-report-button').click(function(){
            var print_url = '<?=site_url($this->router->fetch_class().'/print_report_PDF/'.$view_type.'/'.$page_number.'/'.$tool_type.'_'.((!empty($from_date) && !empty($to_date))?$from_date.'_'.$to_date:''));?>';
            window.open(print_url);
        });
        
        <? if (isset($total_sales)) { ?>
                $('#total-sales-tool').show();
        <? } ?>
        
        $('#grand_total_shortcut').text($('#grand-total').text());
        $('.from_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            setDate: "dd-M-yy",
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".to_date" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        
        $('.to_date').datepicker({
            showOn: "both",
            dateFormat: "dd-M-yy",
            changeMonth: true,
            changeYear: true,
            
            
            buttonText: '<i class="icon-calendar"></i>',
            onClose: function( selectedDate ) {
                $( ".from_date" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        
        $('#open-search-tool').click(function() {
           $('#search-tool').show();
           $('#total-sales-tool').hide();
        });
        
        $('#open-total-sales-tool, #open-total-sales-tool2').click(function() {
            $('#search-tool').hide();
            $('#total-sales-tool').show();
           if (this.id == "open-total-sales-tool")
                $('#status_type').val('in-process');
            else
                $('#status_type').val('completed');
        });
    });
    
	
</script>