<?php

$subtotal = 0;
$discount = 0;
$discounted_amount = 0;
$number_item_discount = 0;
$number_item_discounted_amount = 0;
$gst_charge = 0;
$total_delivery_charge = 0;
$grandtotal = 0;

$total_qty = 0;
foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal += $price_total;
	$total_qty += $item->qty;
}

if($entry->promo_code_discount > 0){
	$discount = $entry->promo_code_discount;
	$discounted_amount = ($subtotal*$entry->promo_code_discount)/100;
}elseif($entry->card_discount > 0){
	$discount = $entry->card_discount;
	$discounted_amount = ($subtotal*$entry->card_discount)/100;
}elseif($entry->corporate_discount > 0){
	$discount = $entry->corporate_discount;
	$discounted_amount = ($subtotal*$entry->corporate_discount)/100;
}

/*if($entry->number_item_discount){
	$number_item_discount = $entry->number_item_discount;
	$number_item_discounted_amount = ($subtotal*$entry->number_item_discount)/100;
}*/

if($entry->ordering_method == "corporate"){
	$total_delivery_charge = $entry->delivery_charge;
}else if($entry->ordering_method != "corporate" && $fo){
	$total_delivery_charge = Mc_utilities::Calculate_Delivery_Charges($total_qty, $fo[0]->service_type);
}

//$gst_charge = ($subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge) * (GST_PERCENT /100);

$grandtotal = $subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge + $gst_charge;


?>


<link href="<?php echo base_url("assets/css/mc_styles.css");?>" rel="stylesheet" media="screen">

<div class="row">
<form id="transaction_form" class="form-horizontal" method="POST" action="<?=site_URL($this->router->fetch_class()).'/update/';?>" enctype="multipart/form-data" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }

  
  ?>
<input type="hidden" name="order_id" value="<?=$entry->id?>" />
<input type="hidden" name="order_ref" value="<?=$entry->order_ref?>" />
<div style="width: 350px;" class="pull-left">
    <table>
        <tr><td width="150"><strong>Order Date:</strong></td><td width="200"><?=date('d.m.y',strtotime($entry->date_ordered))?></td></tr>
        <tr><td><strong>Order Ref:</strong></td><td><?=$entry->order_ref?></td></tr>
        <tr><td><strong>Approval Code:</strong></td><td><?=$entry->approval_code?></td></tr>
        <!-- <tr><td><strong>Pay Using:</strong></td><td><?=mc_utilities::PayUsing($entry->pay_using)?></td></tr> -->
        <tr><td><strong>Discount:</strong></td><td><?=($entry->corporate_discount + $entry->card_discount)?>%</td></tr>
        <?php /*<tr><td><strong>GST</strong></td><td><span id="gst_charge">...</span></td></tr>*/ ?>
        <tr>
            <td><strong>Amount:</strong></td>
            <td><span id="amount">...</span>              
            <?//=number_format(Mc_utilities::Discounted_Price($total->total,$entry->discount),2);?>
            </td>
        </tr>
        <tr><td valign="top"><strong>Status:</strong></td>
            <td>
                <input type="hidden" id="current_status" name="current_status" value="<?=$entry->status?>" />
                <select id="status" name="status">
                    <option></option>
                    <?php
                      foreach ($status_list as $key => $value){
                        echo "<option value=\"$key\"";

                          if ($entry->status == $key)
                            echo " selected";
                          
                        echo ">".(($value == "paid-open")?'PAID & OPEN':strtoupper($value))."</option>\n";
                      }                    
                    ?>
                  </select>
                <div id="email_prompt" class="form-inline mc-notify-customer">
                    Notify Customer? 
                    <label class="radio"><input type="radio" name="notify" value="yes" />Yes</label>
                    <label class="radio"><input type="radio" name="notify" value="no" checked />No</label>
                </div>
            </td>
        </tr>
        <?
            if ($entry->payment_mode == 'credit card'){
                echo '<tr><td><strong>Credit Card Type:</strong></td>';
                echo '<td>'.Mc_utilities::Credit_Card_Type($entry->payment_type).'<input type="hidden" name="payment_mode" value="'.$entry->payment_mode.'" /></td></tr>';
            }
        ?>
        <?
            if ($entry->status == 7)
                echo '<tr><td>&nbsp;</td><td><div style="color: #FF0000; font-size:11px;"><strong>Error Message:</strong><br />'.$entry->error_message.'</div></td></tr>';
        ?>
        <tr>
            <td><strong>Remarks/Notes:</strong></td>
            <td><textarea name="admin_notes" rows="3"><?=$entry->admin_notes;?></textarea></td>
        </tr> 
    </table>
</div>
<div style="width: 350px;" class="pull-right">
    <table>
        <tr><td width="120"><strong>Customer:</strong></td>
            <td width="230">
                <input type="hidden" name="customer_id" value="<?=$entry->customer_id?>" />
                <input type="text" name="first_name" class="pull-left" style="width:90px;" value="<?=$entry->first_name?>" /><input type="text" name="last_name" class="pull-left" style="width:100px;" value="<?=$entry->last_name?>" />
            </td>
        </tr>
        <tr><td><strong>Mobile #:</strong></td><td><input type="text" name="mobile" value="<?=$entry->mobile?>" /></td></tr>
        <tr><td><strong>Alt Contact #:</strong></td><td><input type="text" name="alt_contact" value="<?=$entry->alt_contact?>" /></td></tr>
        <tr><td><strong>Email:</strong></td><td><input type="text" name="email" value="<?=$entry->email?>" /></td></tr>
    </table>
</div>
<div class="lra-clear"></div>
<div>
    <table>
        <tr>
            <td nowrap><strong>Browser Details:</strong></td>
            <td><?=($entry->browser_details == 'none')?'':'<br />'.$entry->browser_details;?></td>
        </tr>
    </table>
</div>
<br /><br />
<div class="text-center">
    <button id="update-button" type="submit" class="btn btn-primary">Update</button> 
    <a href="<?php echo site_url($this->router->fetch_class()); ?>" class="btn">Cancel</a>
  </div>
</form>
    <!-- START ORDERED ITEMS -->
    <div style="padding: 20px 0 0 0;">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>Unit Price</th>
            <th>Qty</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
            <?
            if ($items){ 
                $sub_total = 0;
                $total_qty = 0; ?>
                <? foreach ($items as $item): ?>
                    <tr><td><?= html_entity_decode($item->product_name) ?></td>
                    <td style="text-align: right"><?= $item->price ?></td>
                    <td style="text-align: center"><?= $item->qty ?></td>
                    <?php $total_qty = $total_qty + $item->qty; 
                    $total_unit = $item->qty*$item->price;?>
                    <td style="text-align: right"><?= number_format($total_unit,2)?></td></tr>
                    <?php $sub_total = $sub_total + $total_unit; ?>
                <? endforeach;?>
                <?php
               
                
				?>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Sub-total:</strong></td>
                <td style="text-align: right;">SGD <?=number_format($sub_total,2)?></td>
            </tr>
            
			<?php if($discount){ ?>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Discount (<?php echo $discount;?>%)</strong></td>
                <td style="text-align: right;">SGD (<?php echo number_format($discounted_amount,2); ?>)</td>
            </tr>
            <?php } ?>
            
            <tr>
            	<td colspan="3" style="text-align: right;"><strong>Delivery Charge(s): </strong></td>
                <td style="text-align: right">SGD <?=number_format($total_delivery_charge,2); ?></td>
            </tr>
			
			<?php
             /*<tr>
                <td colspan="3" style="text-align: right;"><strong>GST Charge: </strong></td>
                <td style="text-align: right"><span id="gst_amount">SGD <?=number_format($gst_charge,2); ?></span></td>
             </tr> */
            ?>
            
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Total Amount:</strong></td>
                <td style="text-align: right;"><span id="total_amount">SGD <?=number_format($grandtotal,2)?></span></td>
            </tr>
            <? } ?>
        </tbody>
    </table>
</div>
    <!-- END ORDERED ITEMS -->
    <!-- START FULFILLMENT OPTIONS -->
    <form id="fulfillment_form" class="form-horizontal" method="POST" action="<?=site_url($this->router->fetch_class()).'/update_fulfillmentdate/';?>" enctype="multipart/form-data" />
    <input type="hidden" name="order_id" value="<?=$entry->id?>" />
    <input type="hidden" name="order_ref" value="<?=$entry->order_ref?>" />
    <div style="padding: 20px 0 0 0;">
        <h4>FULFILLMENT OPTIONS</h4>
            <?
            if ($fo){
                $sub_total = 0;
                foreach ($fo as $fo){
                    
                    //$foi = Mc_transactions_model::Get_All_Fulfillment_Option_Items($fo->id);
                    $foi = Mc_transactions_model::Get_All_Fulfillment_Option_Items_by_OrderRef($fo->order_ref);
                    ?>
                    <div class="well well-small">
                    <p>
                        <strong>
							<?php 
							if($fo->service_type=="self_collection"){
								echo "Self Collection: ";
							}elseif($fo->service_type=="delivery"){
								echo "Delivery: ";
							}
							?>
                        </strong>
                        <input type="text" value="<?=date("d F Y", strtotime($fo->service_date))?>" name="temp_date" id="temp_date" class=" temp_date span2" readonly>
                        <input name="delivery_date_field" id="delivery_date_field" type="hidden" value="<?=$fo->service_date?>">
                        <button type="submit" class="btn btn-primary">Update</button>
                        
                        <?php if($fo->service_type=="delivery"){ ?>
                            <br />
                            <?=($fo->time_slot)?$fo->time_slot:''?>
                    	<?php } ?>
                    </p>
                    <?
                    if ($fo->service_type == "delivery"){
                        echo '<p>';
                        echo '<strong>Address: </strong>'.$fo->address.', '.$fo->postal_code;
                        echo '<br /><strong>Recipient: </strong>'.$fo->delivered_to;
                        echo '<br /><strong>Contact No.: </strong>'.$fo->delivered_contact;
                        echo '</p>';
                    }
                    ?>
                    
                    <?php if(isset($order_corporate->special_requirements) && $order_corporate->special_requirements){?>
                    	<p><strong>Special Requirements:</strong> <?php echo $order_corporate->special_requirements ;?></p>
                    <?php } ?>
                    
                    <?php if($fo->additional_request){ ?>
                    	<p><strong>Additional Request:</strong> <?php echo $fo->additional_request;?></p>
                    <?php } ?>
                    
                    
                    </div>
                    <?php /*
                        <table class="table table-bordered table-striped">
                            
                        <thead>
                        <tr>
                            <th>Items</th>                        
                            <th class="align-center">Qty</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        foreach ($foi as $foi){
                        
                            echo "<tr><td>$foi->product_name</td>";                    
                            echo "<td class=\"align-center\">$foi->qty</td>";
                        }
                        ?>
                        </tbody>        
                        </table>*/
					?>
                    
                    <?

                }

                
            ?>
            
            
            <? } ?>
            
        
</div>
        </form>
<!-- End Fulfillment options -->


</div>

<link rel="stylesheet" href="<?php echo base_url("assets/js/jquery-ui/themes/base/jquery-ui.css");?>" />
<script src="<?php echo base_url("assets/js/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>





<script type="text/javascript">
$(document).ready(function(){
    $('#amount').text($('#total_amount').text());
    $('#gst_charge').text($('#gst_amount').text());
    
    $('#status').change(function(){
        var selected_value = ($(this).val());
        
        //if ($('#current_status').val() == 7 && selected_value == 8){
        if (selected_value == 8){
            //if current is error/cc problem and the selected status is paid/open
            $('.mc-notify-customer').show();            
        }
        else
            $('.mc-notify-customer').hide();
    });
    
    $('form#transaction_form #update-button').on("click",function(e){
        e.preventDefault();
        var hasError = false;      
        
        //Confirm Yes or No
        var confirm_delete = confirm("Do you confirm to update this transaction?");
        if (confirm_delete == true){
            hasError = false;            
        }else{
            hasError = true;
        }
    
        if(hasError == false) {
          $('#transaction_form').submit();  
          return true;
        }
        else{
          return false;        
        }
        //return false;    
    });
    
    $('.temp_date').datepicker({
                    showOn: "focus",
                    dateFormat: "dd MM yy",
                    altField: "#delivery_date_field",
                    altFormat: "yy-mm-dd",     
                    
                    
                    //showOn: "both",
                    
                    changeMonth: true,
                    changeYear: true
                    //setDate: "dd-M-yy"

                    //buttonText: '<i class="icon-calendar"></i>'

                    
                    //buttonImageOnly: true
                    //beforeShowDay: BlockDeliveryDaysConfig
            });
    
});
</script>