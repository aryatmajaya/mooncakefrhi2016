<div class="pull-left">
    <button type="button" id="open-search-tool" class="btn"><i class="icon-search"></i> Search</button>
    <!--<button type="button" id="open-total-sales-tool" class="btn"><i class="icon-shopping-cart"></i> Total Sales (In-Process)</button>
    <button type="button" id="open-total-sales-tool2" class="btn"><i class="icon-shopping-cart"></i> Total Sales (Completed)</button>-->
</div>
<div class="pull-right">
    <button type="button" id="print-report-button" class="btn"><i class="icon-print"></i> Print Report</button>  
</div>
<br /><br />

<div class="well well-small no-display" id="search-tool">
    <form class="form-inline" method="POST">
        <strong>Search By:</strong>
        <select name="search_field">
            <option value="o.order_ref">Order No.</option>
            <option value="c.mobile">Mobile</option>
            <option value="c.email">Email</option>
        </select>
        <input type="text" name="search" value="<?=set_value('search')?>" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
</div>

<?php

if (in_array($view_type, array('in-process','completed','collected'))) {
?>

<div class="well well-small" id="total-sales-tool">
    <form class="form-inline" method="POST" action="<?=site_url("mc_transactions/total_sales");?>">
        <strong>Select Date:</strong>
        <input type="text" name="from_date" id="from_date" readonly class="span2 from_date" value="<?=(empty($from_date))?'01-Jun-2014':$from_date?>" />
        <input type="text" name="to_date" id="to_date" readonly class="span2 to_date" value="<?=(empty($to_date))?date('d-M-Y'):$to_date?>" />
        <input type="hidden" name="view_type" value="<?=$view_type?>" />
        <input type="hidden" id="status_type" name="status_type" value="<?=$view_type?>" />
        <button type="submit" class="btn btn-primary">GO</button>
    </form>
    <div>
        <span><strong>Total Sales: </strong></span>
        <span id="grand_total_shortcut"></span>
    </div>
</div>
<?php } ?>