<? //echo $view_type.' '.$allowed_collection_date; ?>
<table class="table table-striped table-condensed pdfreport-table">
    <thead>
        <tr>
            <th>Date Ordered</th>
            <th>Collection Date</th>
            <th>Order Ref</th>
            <th>Customer</th>
            <th><div class="text-right">Amount</div></th>
            <th>Status</th>
            <th>Type</th>
        </tr>
    </thead>
	<?php
	if ($transactions) {
                //$order_status = $this->mc_utilities->order_status();
            
		echo "<tbody>\n";
		
		$ctr = 1;
                $grand_total = 0;
                
		foreach ($transactions as $transactions){
			echo "<tr id='tr_".$transactions->id."'>\n";
                        
			echo "<td>".date('d.m.y',strtotime($transactions->date_ordered))."</td>\n";
                        echo "<td>".date('d.m.y',strtotime($transactions->service_date))."</td>\n";
			echo "<td>";
                        
                        if (!isset($print_mode)) {
                            //echo '<a href="'.BASE_URL.$this->router->fetch_class().'/view_delivery_note/'.$transactions->delivery_id.'">'.$transactions->order_ref.'</a>';
                            echo '<a href="'.site_url('mc_transactions/boxview/'.$transactions->id).'" class="iframe">'.$transactions->order_ref.'</a>';
                        } else {
                            echo $transactions->order_ref;
                        }
                        echo "</td>\n";
			echo "<td>".$transactions->first_name." ".$transactions->last_name."</td>\n";
                        
                        /* LET's Compute - Version 1*/
                        $deliveries = Mc_transactions_model::Get_Total_Delivery_Qty($transactions->order_ref);
                        $total_qty = Mc_transactions_model::Get_Total_Qty($transactions->id);
                        
                        //var_dump($deliveries);

                        if (isset($total_qty))
                            $total_qty = array_sum($total_qty);
                        else
                            $total_qty = null;

                        $delivery_charge = Mc_utilities::Calculate_Delivery_Charges($deliveries, $total_qty);
                        $total_item_amount = Mc_transactions_model::Get_Order_Total_Amount($transactions->id);
                        $gst_charge = ($transactions->total_amount - $transactions->discounted_amount + $delivery_charge) * (GST_PERCENT / 100);
                        $total_amount_no_gst = ($transactions->total_amount - $transactions->discounted_amount) + $delivery_charge;
                        $total_amount = $total_amount_no_gst + $gst_charge;
                        /* end Computation */
                        
                        
                        
			
                        //$delivery_amount = Mc_delivery_report_model::Get_Total_Delivery_Amount($transactions->delivery_id);
			echo "<td align=\"right\"><span class=\"pull-right\">$ ".number_format($total_amount,2)."</span></td>\n";
                        echo "<td>";
            
            $the_status = mc_utilities::order_status($transactions->status);
			echo '<span class="label label-'.mc_utilities::get_order_status_class($transactions->status) . '">'. strtoupper($the_status) .'<span>';
            
			//}
			echo "</td>\n";
            echo "<td>" . ucfirst($transactions->ordering_method) . "</td>";
                        
                        /*echo '<td>';
                        echo '<a href="'.FRONT_URL.'admin_tools/print_delivery_notes/'.$transactions->id.'" class="btn" target="_blank"><i class="icon-shopping-cart"></i></a>';
                        echo '</td>';
                         * 
                         */
			
			echo "</tr>\n";
                    $grand_total += number_format($total_amount,2);
			$ctr++;
		}
                echo '<tr>';
                echo '<td colspan="4" align="right"><strong>TOTAL: </strong></td>';
                echo '<td align="right">$ '.number_format($grand_total,2).'</td>';
                echo '<td colspan="2">&nbsp;</td>';
                echo '</tr>';

		echo "</tbody>\n";
	} ?>
</table>