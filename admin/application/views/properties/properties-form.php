

<form class="form-horizontal" method="POST" action="<?=($action == "add")?base_url().$this->router->fetch_class().'/add/':base_url().$this->router->fetch_class().'/update/'?>" enctype="multipart/form-data" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  if ($action == "edit"){
    echo '<input type="hidden" name="id" value="'.$entries->property_id.'" />';
    
  }
  ?>
  <div class="control-group">
    <label class="control-label">Brand</label>
    <div class="controls">
      <select name="brand_id">
        <option></option>
        <?php
        if ($brands_list){
          foreach ($brands_list as $brands_list){
            echo "<option value='$brands_list->brand_id'";
            if ($action == "add"){              
              echo set_select('brand_id',$brands_list->brand_id);
            } elseif ($action == "edit"){
              if ($brands_list->brand_id == $entries->brand_id)
                echo " selected";

            }
            echo ">$brands_list->brand_name</option>\n";
          }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Region</label>
    <div class="controls">
      <select id="region_id" name="region_id">
        <option></option>
        <?php
        if ($regions_list){
          foreach ($regions_list as $regions_list){
            echo "<option value='$regions_list->region_id'";
            if ($action == "add"){

              echo set_select('region_id',$regions_list->region_id);
            } elseif ($action == "edit"){
              if ($regions_list->region_id == $entries->region_id)
                echo " selected";

            }
            echo ">$regions_list->region</option>\n";
          }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Country</label>
    <div class="controls">
      <select  id="country_id" name="country_id">
        
        <?php
        if ($countries_list){
          foreach ($countries_list as $countries_list){
            echo "<option value='$countries_list->country_id'";
            if ($action == "add"){

              echo set_select('country_id',$countries_list->country_id);
            } elseif ($action == "edit"){
              if ($countries_list->country_id == $entries->country_id)
                echo " selected";

            }
            echo ">$countries_list->country</option>\n";
          }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">Other</label>
    <div class="controls">
      <select name="other_id">
        <option></option>
        <?php
        if ($others_list){
          foreach ($others_list as $others_list){
            echo "<option value='$others_list->id'";
            if ($action == "add"){
              echo set_select('other_id',$others_list->id);
            } elseif ($action == "edit"){
              if ($others_list->id == $entries->other_id)
                echo " selected";
            }
            echo ">$others_list->caption</option>\n";
          }
        }
        ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="inputEmail">Property Code</label>
    <div class="controls">
      <input type="text" maxlength="15" class="" name="property_code" value="<?=($action == "add")?set_value('property_code'):$entries->property_code?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="inputEmail">Hotel Name</label>
    <div class="controls">
      <input type="text" maxlength="255" class="span6" name="hotel_name" value="<?=($action == "add")?set_value('hotel_name'):$entries->hotel_name?>">
    </div>
  </div>

  <!-- <div class="control-group">
    <label class="control-label" for="inputEmail">Logo Image</label>
    <div class="controls">
      <input type="file" id="logo_image" name="logo_image" />
    </div>
  </div> -->

  <div class="control-group">
    <label class="control-label" for="inputEmail">Address</label>
    <div class="controls">
      <input type="text" class="span6" maxlength="255" name="address" value="<?=($action == "add")?set_value('address'):$entries->address?>">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="inputEmail">City</label>
    <div class="controls">
      <input type="text" maxlength="100" name="city" value="<?=($action == "add")?set_value('city'):$entries->city?>" />
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" for="inputEmail">State</label>
    <div class="controls">
      <input type="text" maxlength="100" name="state" value="<?=($action == "add")?set_value('state'):$entries->state?>" />
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="inputEmail">Zip Code</label>
    <div class="controls">
      <input type="text" maxlength="20" name="zip_code" value="<?=($action == "add")?set_value('zip_code'):$entries->zip_code?>" />
    </div>
  </div>

  
  
  <div class="control-group">
      <label class="control-label" for="inputEmail">Status</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="status" value="1"
          <?
          echo set_radio('status');
          if ($action == "add"){

            echo set_radio('status', '1', TRUE);
              
          }
          else{
            if ($entries->status == 1)
              echo ' checked="checked"';           
          }          
          ?>
           />Active
          
        </label>
        <label class="radio">
          
          <input type="radio" name="status" value="0"
          <?
          if ($action == "add"){
            echo set_radio('status', '0', TRUE);
          }
          elseif ($action == "edit"){
            
            if ($entries->status == 0)
              echo ' checked="checked"';           
          }          
          ?>
           />Inactive
        </label>
      </div>
    </div>
  

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn">Cancel</a>
  </div>

</form>

<script type="text/javascript">
$(document).ready(function(){
  $('#region_id').on('change', function(){
    var region_id = $('#region_id option:selected').val();

    $.post("<?=AJAX_URL?>ajax_query/dynamic_dropdown_country/",{region_id:region_id},function(result){        
        
        $('#country_id').html(result);
        //$("#the_caption").val($('#element_type option:selected').text());
        //$("#the_element_type").val(selected_element);

        
    });

  });
});
</script>