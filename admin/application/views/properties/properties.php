<?
$this->check_permission->Check_Button_Add(' Add Property',$current_app_info->add_role);
?>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<th>Region</th>
			<th>Country</th>
			<th>Hotel Name</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->country_id."'>\n";
			echo "<td>".$ctr."</td>\n";
			
			echo "<td>".$entries->the_region_name."</td>\n";
			echo "<td>".$entries->the_country_name."</td>\n";
			echo "<td>".$entries->hotel_name."</td>\n";
			echo "<td>";
			if ($entries->status == 1){
				echo '<span class="label label-success">Active</span>';
			} else {
				echo '<span class="label">Inactive</span>';
			}
			echo "</td>\n";

			echo "<td>";
				$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->property_id);

				$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->property_id);
				
			echo "</td>\n";

			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("ajax_delete",{property_id:the_id},function(result){
				window.location.reload();
				//$('#tr_' + the_id).remove();
				
				//$("#dialog").dialog("close");
			});
		  }
		

    });
</script>