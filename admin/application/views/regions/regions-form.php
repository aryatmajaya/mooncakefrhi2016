

<form class="form-horizontal" method="POST" action="<?=($action == "add")?base_url().$this->router->fetch_class().'/add/':base_url().$this->router->fetch_class().'/update/'?>" enctype="multipart/form-data" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  


  if ($action == "edit")
    echo '<input type="hidden" name="id" value="'.$entries->region_id.'" />';
  ?>

  <div class="control-group">
    <label class="control-label" for="inputEmail">Region</label>
    <div class="controls">
      <input type="text" name="region" value="<?=($action == "add")?set_value('region'):$entries->region?>">
    </div>
  </div>
  
  
  

  <div class="control-group">
      <label class="control-label" for="inputEmail">Status</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="status" value="1"
          <?
          echo set_radio('status');
          if ($action == "add"){

            echo set_radio('status', '1', TRUE);
              
          }
          else{
            if ($entries->status == 1)
              echo ' checked="checked"';           
          }          
          ?>
           />Active
          
        </label>
        <label class="radio">
          
          <input type="radio" name="status" value="0"
          <?
          if ($action == "add"){
            echo set_radio('status', '0', TRUE);
          }
          elseif ($action == "edit"){
            
            if ($entries->status == 0)
              echo ' checked="checked"';           
          }          
          ?>
           />Inactive
        </label>
      </div>
    </div>
    

    

    <div class="form-actions">
      <button type="submit" name="save" class="btn btn-primary">Save</button>
      <a href="<?php echo base_url().$this->router->fetch_class(); ?>" class="btn">Cancel</a>
    </div>
</form>
