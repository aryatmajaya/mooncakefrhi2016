<?php
  if ($this->session->flashdata('success_notification')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('success_notification').'</div>';
  }
 ?>


<?php
$url = site_url('usergroups');
$this->check_permission->Check_Button_Add(' Add User Group',$current_app_info->add_role, $url);
?>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<th class="span10">User Group</th>
			
			<th>&nbsp;</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->group_id."'>\n";
			echo "<td>".$ctr."</td>\n";
			echo "<td>".$entries->group_name."</td>\n";
			echo "<td>";
				if ($current_app_info->group_id == 1){
					echo '<a href="'.$url.'/edit/'.$entries->group_id.'" class="btn btn-mini"><i class="icon-pencil"></i> Edit</a>';
				}
				else{
					$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->group_id, $url);	
				}
				$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->group_id);
			echo "</td>\n";
			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("<?php echo $url;?>/ajax_delete",{group_id:the_id},function(result){
				//$('#tr_' + the_id).remove();
				window.location.reload();
				//$("#dialog").dialog("close");
			});
		  }
		

    });
</script>