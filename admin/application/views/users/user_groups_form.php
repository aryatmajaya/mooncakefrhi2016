<form class="form-horizontal" method="POST" action="<?=($action == "add")?site_url('usergroups/add'):site_url('usergroups/update');?>" />
  <?php
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  if ($action == "edit"){
    //$this->load->model('applications_model');
    //$this->load->model('common');
    echo '<input type="hidden" name="id" value="'.$entries->group_id.'" />';
  }
    
  ?>
  

  <div class="control-group">
    <label class="control-label" for="inputEmail">Group Name</label>
    <div class="controls">
      <input type="text" maxlength="100" class="span6" name="group_name" value="<?=($action == "add")?set_value('group_name'):$entries->group_name?>">
    </div>
  </div>

  

  <div class="control-group">
    <label class="control-label">Application Permissions</label>
    <div class="controls">
        <table class="table table-striped table-hover table-condensed">
          <tbody>
        <?php

        if ($applications_list){
          $current_app_group = "";
          foreach ($applications_list as $applications_list){
            if ($current_app_group != $applications_list->app_group){
               echo "<tr class='info'>\n";
               echo "<td colspan='5'><strong>".ucwords($applications_list->group_name)."</strong></td>";
               echo "</tr>\n";
               $current_app_group = $applications_list->app_group;
            }

            

            echo "<tr>\n";
            echo "<td><label class='checkbox'><input name='app_id[]' value='$applications_list->id' type='checkbox'";
            if ($action == 'add'){
              echo set_checkbox('app_id[]', $applications_list->id);
            }
            elseif ($action == 'edit'){
              $app_permissions = $this->applications_model->get_app_group_permissions($applications_list->id,$entries->group_id);
              
              if ($app_permissions && $app_permissions->app_id == $applications_list->id)
                echo ' checked';
            }

            echo "> $applications_list->app_name</label></td>";
            echo "<td>\n";

            echo "<label class='checkbox'><input name='add_role[$applications_list->id]' value='1' type='checkbox'";
            if ($action == 'add'){
              
              echo set_checkbox("add_role[$applications_list->id]", '1');
            }
            elseif ($action == 'edit'){
              
              if ($app_permissions && $app_permissions->add_role == 1)
                echo ' checked';
            }
            echo "> Add</label>";
            echo "</td>\n";

            echo "<td>";
            echo "<label class='checkbox'><input name='view_role[$applications_list->id]' value='1' type='checkbox'";
            if ($action == 'add'){
              echo set_checkbox('view_role[]', '1');
            }
            elseif ($action == 'edit'){
              
              if ($app_permissions && $app_permissions->view_role == 1)
                echo ' checked';
            }
            echo "> View</label>\n";
            echo "</td>";
            echo "<td>";
            
            echo "<label class='checkbox'><input name='edit_role[$applications_list->id]' value='1' type='checkbox'";
            if ($action == 'add'){
              echo set_checkbox('edit_role[]', '1');
            }
            elseif ($action == 'edit'){              
              if ($app_permissions && $app_permissions->edit_role == 1)
                echo ' checked';
            }
            echo "> Edit</label>";
            echo "</td>\n";

            echo "<td>";
            echo "<label class='checkbox'><input name='delete_role[$applications_list->id]' value='1' type='checkbox'";
            if ($action == 'add'){
              echo set_checkbox('delete_role[]', '1');
            }
            elseif ($action == 'edit'){
              
              if ($app_permissions && $app_permissions->delete_role == 1)
                echo ' checked';
            }
            echo "> Delete</label>\n";
            echo "</td>\n";
            echo "</tr>\n";

            
            
          }
        }
        ?>
          </tbody>
        </table>
    </div>
  </div>

   

  
  
  

  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="<?php echo site_url('usergroups');?>" class="btn">Cancel</a>
  </div>

</form>
