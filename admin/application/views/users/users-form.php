
<form class="form-horizontal" method="POST" action="<?=($action == "add")?site_url('users/add'):site_url('users/update');?>" />
  <?php
  if ($this->session->flashdata('required_error')){
    echo '<div class="alert alert-error">'.$this->session->flashdata('required_error').'</div>';
  }
  
  if (validation_errors()){
    echo '<div class="alert alert-error">'.validation_errors().'</div>';
  }
  
  if ($action == "edit"){
    echo '<input type="hidden" name="id" value="'.$entries->id.'" />';
    echo '<input type="hidden" name="orig_username" value="'.$entries->username.'" />';
  }
    
  ?>
<!--  <ul id="myTab" class="nav nav-tabs">
  <li class="active">
    <a href="#account_profile" data-toggle="tab">Account Profile</a>
  </li>
  <li><a href="#property_permissions" data-toggle="tab">Property Permissions</a></li>
  
</ul>-->

<div id="myTabContent" class="tab-content">
<div class="tab-pane active" id="account_profile">
  <div class="control-group">
    <label class="control-label">Username</label>
    <div class="controls">
      <input type="text" name="username" value="<?=($action == "add")?set_value('username'):$entries->username?>" autocomplete="off">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">Password</label>
    <div class="controls">
      <input type="text" name="password" value="<?=($action == "add")?set_value('password'):$this->encrypt->decode($entries->password)?>">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">First Name</label>
    <div class="controls">
      <input type="text" name="first_name" value="<?=($action == "add")?set_value('first_name'):$entries->first_name?>" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">Last Name</label>
    <div class="controls">
      <input type="text" name="last_name" value="<?=($action == "add")?set_value('last_name'):$entries->last_name?>" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">Email</label>
    <div class="controls">
      <input type="text" name="email" value="<?=($action == "add")?set_value('email'):$entries->email?>" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">Company Name</label>
    <div class="controls">
      <input type="text" name="company_name" value="<?=($action == "add")?set_value('company_name'):$entries->company_name?>" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">Account Type</label>
    <div class="controls">
      <select name="account_type">
      	<option></option>
      	<?
      	if ($user_groups_list>0){
            if ($this->session->userdata('sess_user_account_type') == 1) {
                foreach($user_groups_list as $user_groups_list){

                    echo "<option value='$user_groups_list->group_id'";
                    if ($action == "add"){
                        echo set_select('account_type',$user_groups_list->group_id);
                    } elseif ($action == 'edit'){
                        if ($user_groups_list->group_id == $entries->account_type)
                                echo " selected";
                    }
                    echo ">$user_groups_list->group_name</option>\n";
                }
            } else {
                foreach($user_groups_list as $user_groups_list){
                    if ($user_groups_list->group_id >=2 ) {
                        echo "<option value='$user_groups_list->group_id'";
                        if ($action == "add"){
                            echo set_select('account_type',$user_groups_list->group_id);
                        } elseif ($action == 'edit'){
                            if ($user_groups_list->group_id == $entries->account_type)
                                    echo " selected";
                        }
                        echo ">$user_groups_list->group_name</option>\n";
                    }
                }
            }
      	}
      	?>

      </select>
    </div>
  </div>
  

  <div class="control-group">
      <label class="control-label">Status</label>
      <div class="controls">
         <label class="radio">
          <input type="radio" name="status" value="1"
          <?
          echo set_radio('status');
          if ($action == "add"){

            echo set_radio('status', '1', TRUE);
              
          }
          else{
            if ($entries->status == 1)
              echo ' checked="checked"';           
          }          
          ?>
           />Active
          
        </label>
        <label class="radio">
          
          <input type="radio" name="status" value="0"
          <?
          if ($action == "add"){
            echo set_radio('status', '0', TRUE);
          }
          elseif ($action == "edit"){
            
            if ($entries->status == 0)
              echo ' checked="checked"';           
          }          
          ?>
           />Inactive
        </label>
      </div>
    </div>
</div>  
<div class="tab-pane fade" id="property_permissions">
  <div class="control-group">
    <!-- <label class="control-label">Property Permissions</label> -->
    <div class="" style="padding: 0px;">
      
      <select id="client_id" name="client_id">
        <option value="0">Select a Client</option>
        <?
        if ($clients_list > 0){
          foreach ($clients_list as $clients_list) {
            echo "<option value='$clients_list->id'>$clients_list->client_name</option>\n";
          }
        }
        ?>
      </select>
      
      
      <div id="properties_container" class="">
      </div>
        <?php
        /*if ($properties_list){
          $current_brand = "";
          foreach ($properties_list as $properties_list){
            if ($current_brand != $properties_list->brand_name){
              echo "<legend>$properties_list->brand_name</legend>\n";
              $current_brand = $properties_list->brand_name;
            }
              
            echo "<label class='checkbox'><input name='property_id[]' value='$properties_list->property_id' type='checkbox'";
            if ($action == 'add'){
              echo set_checkbox('property_id[]', $properties_list->property_id);
            }
            elseif ($action == 'edit'){
              $property_permissions = $this->common->get_app_property_permissions($properties_list->property_id,$entries->id);
              
              if ($property_permissions && $property_permissions->property_id == $properties_list->property_id)
                echo ' checked';
            }
            
            echo "> $properties_list->hotel_name</label>\n";

            
          }
        }*/
        ?>
      
    </div>
  </div>
</div>
</div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="<?php echo site_url('users'); ?>" class="btn">Cancel</a>
  </div>

</form>

<script src="<?php echo base_url("assets/bootstrap/js/bootstrap-tab-v2.3.1.js");?>" type="text/javascript"></script>
<script type="text/javascript">
function Check_All(){
    //console.log('check na!');
  }

  

$(document).ready(function(){
  
  $('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  $('#client_id').on('change', function(){
    var client_id = $('#client_id option:selected').val();
    
    $.post("<?php echo AJAX_URL; ?>users/ajax_display_properties/",{client_id:client_id,user_id:<?=($action == "edit")?$entries->id:'0'?>,action:'<?=$action?>'},function(result){        
        $('#properties_container').html(result);

        //If User clicks on the brand checkbox, it will select all properties under that brand
        $('.check_all_regions').on('change', function(){    
          
          var the_parent = $(this).parents('.brand_container').attr('id');
          //console.log(the_parent);
          var check_status = this.checked ? true : false;
          $('.'+the_parent+':checkbox').prop('checked', check_status);
          
        });

        //If User clicks on the region checkbox, it will select all properties under that region
        $('.check_all_properties').on('change', function(){    
          //console.log('checkbox');
          var the_parent = $(this).parents('.span3').attr('id');
          //console.log(the_parent);
          var check_status = this.checked ? true : false;
          $('.'+the_parent+':checkbox').prop('checked', check_status);
          /*$('#'+the_parent).find(':checkbox').each(function(){
              $(this).attr('checked');
          }); */
        });
        //$("#the_caption").val($('#element_type option:selected').text());
        //$("#the_element_type").val(selected_element);

        
      });
  });

  

  

});
</script>

