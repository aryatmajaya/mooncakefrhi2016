<?php
  if ($this->session->flashdata('notification_status')){
    echo '<div class="alert alert-success">'.$this->session->flashdata('notification_status').'</div>';
  }
  
  
  ?>


<?php
$url = site_url("users");
$this->check_permission->Check_Button_Add(' Add User',$current_app_info->add_role, $url);
?>
<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>#</th>
			<th>Username</th>
			<th>Full Name</th>
			<th>User Type</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<?php
	if ($entries) {
		echo "<tbody>\n";
		
		$ctr = 1;
		foreach ($entries as $entries){
			echo "<tr id='tr_".$entries->id."'>\n";
			echo "<td>".$ctr."</td>\n";
			
			echo "<td>".$entries->username."</td>\n";
			echo "<td>".$entries->first_name." ".$entries->last_name."</td>\n";
			echo "<td>".$entries->group_name."</td>\n";
			echo "<td>";
			if ($entries->status == 1){
				echo '<span class="label label-success">Active</span>';
			} else {
				echo '<span class="label">Inactive</span>';
			}
			echo "</td>\n";

			echo "<td>";
				$this->check_permission->Check_Button_Edit(' Edit',$current_app_info->edit_role,$entries->id, $url);

				$this->check_permission->Check_Button_Delete(' Delete',$current_app_info->delete_role,$entries->id);

				
			echo "</td>\n";

			echo "</tr>\n";
			$ctr++;
		}

		echo "</tbody>\n";
	} ?>
</table>

<div class="result"></div>

<script type="text/javascript">
	$('button#delete-button').click(function(){
		var confirm_delete = confirm("Are you sure you want to delete?");
		if (confirm_delete == true)
		  {
		  	var the_id=$(this).val();
		  	//alert(the_id);
			$.post("<?php echo $url;?>/ajax_delete",{user_id:the_id},function(result){
				//$('#tr_' + the_id).remove();
				window.location.reload();
			});
		  }
		

    });
</script>