$(function(){
	$('#customer-tab a').click(function (e) {
  		e.preventDefault()
  		$("#customer-type").val($(this).attr("data-value"));
	})
	
	$('#staff_form').submit(function(){
		$("#loading-staff-form").show();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			dataType: "json",
			success: function(data) {
				console.log(data);
				$("#loading-staff-form").hide();
			}
		});
		
		return false;	
	})
})