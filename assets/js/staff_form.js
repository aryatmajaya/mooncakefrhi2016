angular.module('staffForm', ['ui.bootstrap','ngSanitize','ngAnimate','angular-loading-bar']);

/* CONFIG BEGIN */

angular.module('staffForm').value('cfg',{
	api: 'http://mooncake-frhi-2016.dev/',
	//api: 'http://celebrationscentral.com.sg/',
	//gst_persen: 7.00
	gst_persen:0
});


angular.module('staffForm').filter('html', function($sce) {
	return function(val) {
    	return $sce.trustAsHtml(val);
  	};
});

/* CONFIG END */


/* CONTROLLER BEGIN */
angular.module('staffForm')
	.controller('staffFormController', function ($scope, staffFormService, $log, cfg, $uibModal) {
		
		$scope.time_slots_self_collection=['10am-12pm','12pm-2pm','2pm-4pm','4pm-6pm','6pm-8pm'];
		$scope.time_slots_delivery=['Morning (9am-12pm)','Afternoon (2pm-5pm)'];				
		$scope.fullfillment_type = "self_collection";
		$scope.customer_type = "existing";
		$scope.customer_new={
			first_name:"",
			last_name:"",
			email:"",
			contact:"",
			company:"",
			address:""
		};
		$scope.customer_id=0;
		$scope.customer_selected = false;
		$scope.fullfillment_self_collection={
			date:"",
			time_slot:"",
			additional_request:""
		};
		$scope.fullfillment_delivery={
			date:"",
			time_slot:"",
			recipient:"",
			contact:"",
			address:"",
			postal_code:"",
			additional_request:""
		};
		$scope.payment_mode="";
		$scope.special_requirements="";
		$scope.sales_manager="";
		$scope.product_quantities={};
		$scope.subtotal = 0;
		$scope.discount = 0;
		$scope.discount_amount = 0;
		$scope.delivery_charge = 0;
		$scope.gst_persen = cfg.gst_persen;
		$scope.gst = 0;
		$scope.grandtotal = 0;
		$scope.order_confirmation = false;
		$scope.searchType="";
		$scope.searchKeywords="";
		$scope.customers="";
		$scope.success=false;

		var products_promise = staffFormService.getProducts();
		products_promise.then(function(datas){
			$scope.products=datas;
			for(var category_id in datas){
				if(datas[category_id].products){
					$scope.product_quantities[category_id]={};
					for(var product_id in datas[category_id].products){
						$scope.product_quantities[category_id][product_id]=0;
					}
				}
			}
						
		});
		
		$scope.update_quantity = function(category_id, product_id, plus){
			var old_data = $scope.product_quantities[category_id][product_id];
			if(plus){
				old_data++;
			}else{
				old_data--;
			}
			
			if(old_data<0){
				old_data=0;
			}
			
			$scope.product_quantities[category_id][product_id] = old_data;
			
			$scope.update_summary();
		};
		
		$scope.update_summary = function(){
			
			var amount = 0;
			if($scope.product_quantities){
				for (var category_id in $scope.product_quantities){
					for(var product_id in $scope.product_quantities[category_id]){
						if($scope.product_quantities[category_id][product_id]!==0){
							amount += ($scope.product_quantities[category_id][product_id] * $scope.products[category_id].products[product_id].price);
						}

					}
				}
			}
			
			$scope.subtotal = amount;
			
			if($scope.discount){
				$scope.discount_amount = ($scope.subtotal*$scope.discount)/100;
			}else{
				$scope.discount_amount = 0;	
			}
			
			if(!$scope.delivery_charge){
				$scope.delivery_charge = 0;
			}
			if(!$scope.discount){
				$scope.discount = 0;
			}
			
			$scope.gst = (parseFloat($scope.subtotal) - parseFloat($scope.discount_amount) + parseFloat($scope.delivery_charge))*cfg.gst_persen/100;
			$scope.grandtotal = parseFloat($scope.subtotal) - parseFloat($scope.discount_amount) + parseFloat($scope.delivery_charge) + $scope.gst;
		};
		
		$scope.change_customer_type = function(param){
			$scope.customer_type = param;
		};
		$scope.change_fullfillment_type = function(param){
			$scope.delivery_charge = 0;
			$scope.update_summary();
			$scope.fullfillment_type = param;
		};
		
		
    	$scope.searchCustomer = function(searchType, searchKeywords){
    		var customers_promise = staffFormService.getCustomers(searchType, searchKeywords);
			customers_promise.then(function(data){
				$scope.customers = data;
			});
    	};
		
		
		
		$scope.selectCustomer = function(customer_id){
			var customer_promise = staffFormService.getCustomer(customer_id);
			$scope.customer_id = customer_id;
			customer_promise.then(function(data){
				$scope.customer_selected = {
					customer_id: data.id,
					first_name: data.first_name,
					last_name: data.last_name,
					email: data.email,
					contact: data.mobile,
					company: data.company_name,
					address: data.address
				};
			});
		};
		
		
		//// fullfillment
		$scope.dateOptions = {
    		formatYear: 'yy',
    		startingDay: 1
  		};
		
		$scope.delivery_date = {
    		opened: false
  		};
		$scope.self_collection_date = {
			opened: false
		};	
		
		$scope.delivery_date_open = function(){
			$scope.delivery_date.opened = true;
	  	};
		$scope.self_collection_date_open = function(){
			$scope.self_collection_date.opened = true;
	  	};
		
		
		
		$scope.open_modal_info = function (type, text) {
			 $uibModal.open({
		  		animation: true,
		  		templateUrl: 'modal_info.html',
		  		controller: 'modalInfoCtrl',
		  		resolve: {
					type: function () {
						return type;
					},
					text: function () {
						return text;
					}
		  		}
			});
	  	};
		
		$scope.set_confirmation = function(){
			$scope.order_confirmation = !$scope.order_confirmation;
		};
		
		
		
		$scope.continue = function(){
						
			if($scope.customer_type==="existing"){
				if(!$scope.customer_id){
					$scope.open_modal_info("error","Please select a customer.");
				}else{
					if($scope.subtotal<=0){
						$scope.open_modal_info("error","Sub total is zero, please make sure you input the correct order.");
					}else if($scope.fullfillment_type === "self_collection" && (
						!$scope.fullfillment_self_collection.date || 
						!$scope.fullfillment_self_collection.time_slot)
					){
						$scope.open_modal_info("error","Please fill the reqiured field on Self Collection Fullfillment.");
					}else if($scope.fullfillment_type === "delivery" && (
						!$scope.fullfillment_delivery.date || 
						!$scope.fullfillment_delivery.time_slot || 
						!$scope.fullfillment_delivery.recipient || 
						!$scope.fullfillment_delivery.contact || 
						!$scope.fullfillment_delivery.address || 
						!$scope.fullfillment_delivery.postal_code)
					){
						$scope.open_modal_info("error","Please fill the reqiured field on Delivery Information Fullfillment.");
					}else if(!$scope.payment_mode){
						$scope.open_modal_info("error","Please select payment type.");
					}else{

						if($scope.product_quantities){
							
							$scope.products_selected = {};
							
							for (var category_id in $scope.product_quantities){
								for(var product_id in $scope.product_quantities[category_id]){
									if($scope.product_quantities[category_id][product_id]>=1){
										
										if(!angular.isDefined($scope.products_selected[category_id])){
											$scope.products_selected[category_id] = {
												category_title: $scope.products[category_id].name,
												items:{}
											};
										}
										
										if(!angular.isDefined($scope.products_selected[category_id].items[product_id])){
											$scope.products_selected[category_id].items[product_id] = {};
										}
										
										$scope.products_selected[category_id].items[product_id].product_name = $scope.products[category_id].products[product_id].product_name;
										$scope.products_selected[category_id].items[product_id].product_price = $scope.products[category_id].products[product_id].price;
										$scope.products_selected[category_id].items[product_id].qty = $scope.product_quantities[category_id][product_id];
										$scope.products_selected[category_id].items[product_id].total = ($scope.product_quantities[category_id][product_id] * $scope.products[category_id].products[product_id].price);
									}
			
								}
							}
							
							
							///// make category and product has a row
							$scope.products_conf = [];
							var no=1;
							for(var category_id in $scope.products_selected){
								$scope.products_conf.push({
									type: 'heading',
									title: $scope.products_selected[category_id].category_title
								});
								
								for(var product_id in $scope.products_selected[category_id].items){
									$scope.products_conf.push({
										type: 'data',
										product_name: $scope.products_selected[category_id].items[product_id].product_name,
										product_price: $scope.products_selected[category_id].items[product_id].product_price,
										qty: $scope.products_selected[category_id].items[product_id].qty,
										total: $scope.products_selected[category_id].items[product_id].total,
										no: no		
									});	
									
									no++;
								}
							}
							////////
						}
						///////////
								
						$scope.set_confirmation();	
					}
				}
			}else if($scope.customer_type==="new"){
				if(!$scope.customer_new.first_name || !$scope.customer_new.last_name || !$scope.customer_new.email || !$scope.customer_new.contact){
					$scope.open_modal_info("error","Please complete the customer form.");
				}else{
					var isEmailExistsPromise = staffFormService.isEmailExists($scope.customer_new.email);
					isEmailExistsPromise.then(function(data){
						if(data==='true'){
						//if(1===2){
							$scope.open_modal_info("error","Customer email is already exists.");
						}else{
							if($scope.subtotal<=0){
								$scope.open_modal_info("error","Sub total is zero, please make sure you input the correct order.");
							}else if($scope.fullfillment_type === "self_collection" && (
								!$scope.fullfillment_self_collection.date || 
								!$scope.fullfillment_self_collection.time_slot)
							){
								$scope.open_modal_info("error","Please fill the reqiured field on Self Collection Fullfillment.");
							}else if($scope.fullfillment_type === "delivery" && (
								!$scope.fullfillment_delivery.date || 
								!$scope.fullfillment_delivery.time_slot || 
								!$scope.fullfillment_delivery.recipient || 
								!$scope.fullfillment_delivery.contact || 
								!$scope.fullfillment_delivery.address || 
								!$scope.fullfillment_delivery.postal_code)
							){
								$scope.open_modal_info("error","Please fill the reqiured field on Delivery Information Fullfillment.");
							}else if(!$scope.payment_mode){
								$scope.open_modal_info("error","Please select payment type.");
							}else{
								
								if($scope.product_quantities){
							
							$scope.products_selected = {};
							
							for (var category_id in $scope.product_quantities){
								for(var product_id in $scope.product_quantities[category_id]){
									if($scope.product_quantities[category_id][product_id]>=1){
										
										if(!angular.isDefined($scope.products_selected[category_id])){
											$scope.products_selected[category_id] = {
												category_title: $scope.products[category_id].name,
												items:{}
											};
										}
										
										if(!angular.isDefined($scope.products_selected[category_id].items[product_id])){
											$scope.products_selected[category_id].items[product_id] = {};
										}
										
										$scope.products_selected[category_id].items[product_id].product_name = $scope.products[category_id].products[product_id].product_name;
										$scope.products_selected[category_id].items[product_id].product_price = $scope.products[category_id].products[product_id].price;
										$scope.products_selected[category_id].items[product_id].qty = $scope.product_quantities[category_id][product_id];
										$scope.products_selected[category_id].items[product_id].total = ($scope.product_quantities[category_id][product_id] * $scope.products[category_id].products[product_id].price);
									}
			
								}
							}
							
							
							///// make category and product has a row
							$scope.products_conf = [];
							var no=1;
							for(var category_id in $scope.products_selected){
								$scope.products_conf.push({
									type: 'heading',
									title: $scope.products_selected[category_id].category_title
								});
								
								for(var product_id in $scope.products_selected[category_id].items){
									$scope.products_conf.push({
										type: 'data',
										product_name: $scope.products_selected[category_id].items[product_id].product_name,
										product_price: $scope.products_selected[category_id].items[product_id].product_price,
										qty: $scope.products_selected[category_id].items[product_id].qty,
										total: $scope.products_selected[category_id].items[product_id].total,
										no: no		
									});	
									
									no++;
								}
							}
							////////
						}
						///////////
								
								$scope.set_confirmation();	
							}
						}
					});
				}
			}
			
			
			
		};
		
		$scope.submit = function(){
				
				$scope.data_insert = {
					customer_type        : $scope.customer_type,
					customer_id          : $scope.customer_id,
					customer_new	     : $scope.customer_new,
					products		     : $scope.product_quantities,
					payment_mode	     : $scope.payment_mode,
					special_requirements : $scope.special_requirements,
					sales_manager	     : $scope.sales_manager,
					fullfillment_type    : $scope.fullfillment_type,
					self_collection      : $scope.fullfillment_self_collection,
					delivery	         : $scope.fullfillment_delivery,
					discount	         : $scope.discount,
					discount_amount	     : $scope.discount_amount,
					grandtotal           : $scope.grandtotal,
					subtotal             : $scope.subtotal,
					delivery_charge      : $scope.delivery_charge,
					gst_persen			 : $scope.gst_persen,
					gst					 : $scope.gst
				};
				
			
				staffFormService.insert($scope.data_insert).then(function(){
					// change to default data			
					$scope.fullfillment_type = "self_collection";
					$scope.customer_type = "existing";
					$scope.customer_new={
						first_name:"",
						last_name:"",
						email:"",
						contact:"",
						company:"",
						address:""
					};
					$scope.customer_id=0;
					$scope.customer_selected = {};
					$scope.fullfillment_self_collection={
						date:"",
						time_slot:"",
						additional_request:""
					};
					$scope.fullfillment_delivery={
						date:"",
						time_slot:"",
						recipient:"",
						contact:"",
						address:"",
						postal_code:"",
						additional_request:""
					};
					$scope.payment_mode="";
					$scope.special_requirements="";
					$scope.sales_manager="";
					$scope.subtotal = 0;
					$scope.discount = 0;
					$scope.discount_amount = 0;
					$scope.delivery_charge = 0;
					$scope.gst_persen = cfg.gst_persen;
					$scope.grandtotal = 0;
					$scope.order_confirmation = false;
					$scope.products_selected = {};
					$scope.data_insert="";
					$scope.customers="";
					$scope.searchType="";
					$scope.searchKeywords="";
					$scope.gst=0;
					//$scope.products[0].active=true;
					
					for(var category_id in $scope.products){
						if($scope.products[category_id].products){
							$scope.product_quantities[category_id]={};
							for(var product_id in $scope.products[category_id].products){
								$scope.product_quantities[category_id][product_id]=0;
							}
						}
					}
									
					
					
					/////////
					
					
					$scope.success=true;
				});
			
		};
		
		
		$scope.back = function(){
			$scope.success=false;
		};
});


angular.module('staffForm').controller('modalInfoCtrl', function ($scope, $uibModalInstance, type, text) {
	
	$scope.type = type;
	
	if(type==="error"){
		$scope.title = "Error";
	}else{
		$scope.title = "Success";	
	}
	
	$scope.text = text;

	$scope.ok = function () {
		$uibModalInstance.close();
	};

});

/* CONTROLLER END */



/* SERVICE BEGIN */

angular.module('staffForm').service('staffFormService',staffFormService);
function staffFormService($http, $q, cfg){
	
	this.getProducts = function() {
		var deferred = $q.defer();
		$http({
			url		: cfg.api + 'api/staff-form/get-products',
		}).
		success(function(data) {
			deferred.resolve(data);
		}).
		error(function() {
			deferred.reject();
		});

		return deferred.promise;
	};
	
	this.getCustomers = function(searchType, searchKeywords){
		var deferred = $q.defer();
		$http({
			url		: cfg.api + 'api/staff-form/get-customers',
			params	: {
				search_type : searchType,
				search_keyword : searchKeywords
			}
		}).
		success(function(data) {
			deferred.resolve(data);
		}).
		error(function() {
			deferred.reject();
		});

		return deferred.promise;
	};
	
	this.getCustomer = function(customer_id){
		var deferred = $q.defer();
		$http({
			url		: cfg.api + 'api/staff-form/get-customer/'+customer_id,
		}).
		success(function(data) {
			deferred.resolve(data);
		}).
		error(function() {
			deferred.reject();
		});

		return deferred.promise;
	};
	
	this.isEmailExists = function(email){
		var deferred = $q.defer();
		$http({
			url	   : cfg.api + 'api/staff-form/is-email-exists',
			params : {email: email}
		}).
		success(function(data) {
			deferred.resolve(data);
		}).
		error(function() {
			deferred.reject();
		});

		return deferred.promise;
	};
	
	this.insert = function(data){
		var deferred = $q.defer();
		$http({
			method	: "POST",
			url		: cfg.api + 'api/staff-form/insert',
			data	: data
		}).
		success(function(data) {
			deferred.resolve(data);
		}).
		error(function() {
			deferred.reject();
		});

		return deferred.promise;
	};

}

/* SERVICE END */


/* DIRECTIVE BEGIN */
angular.module('staffForm').directive('numberformat', function () {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
				return ctrl.$modelValue.toFixed(2);
            });
        }
    };
});


angular.module('staffForm').directive('validNumber', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return; 
      }

      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
            var val = '';
        }
        var clean = val.replace( /[^0-9]+/g, '');
        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });

      element.bind('keypress', function(event) {
        if(event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});

/* DIRECTIVE END */