<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
 * OTHER CONSTANTS
 */
define('DELIVERY_CHARGE', 50);
define('DELIVERY_COMPLIMENTARY_QTY', 50);

/**
 * ORDER CONSTANTS
 */
define('ORDER_COLLECTED', 2);
define('ORDER_DELIVERED', 3);
define('ORDER_CANCELLED', 4);
define('ORDER_CARDERROR', 5);
define('ORDER_INCOMPLETE', 6);
define('ORDER_FAILED', 7);
define('ORDER_SUCCESS_PAID', 8);
define('ORDER_PROCESS', 9);
define('ORDER_OFFLINE_INCOMPLETE', 10);

//SANDBOX URL
//define('TELEMONEY_URL', 'https://uat.telemoney.com.sg/easypay2/paymentpage.do');
define('TELEMONEY_URL', 'https://test.wirecard.com.sg/easypay2/paymentpage.do');
//LIVE URL
//define('TELEMONEY_URL', 'https://securepayments.telemoneyworld.com/easypay2/paymentpage.do');

define('TELEMONEY_DBS', '20140701001');
define('TELEMONEY_MID', '20140701001');
define('TELEMONEY_RETURN_PATH', 'order/order_confirmation'); //Later will used in order.php as base_url() . RETURN_PATH
define('TELEMONEY_UPDATE_PATH', 'order/update_order');

define('GST_PERCENT', 7.00);
/*
* Define the admin email address here
*/
define('ADMIN_EMAIL', 'admin@celebrationscentral.com.sg');




/* End of file constants.php */
/* Location: ./application/config/constants.php */