<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
//$route['default_controller'] = "close";
$route['404_override'] = '';
$route['delivery-collection'] = "delivery_collection";
$route['promotions'] = "promo";
$route['contact-us'] = "contact";
$route['terms-and-conditions'] = "terms_and_conditions";

// staff form
$route['api/staff-form/get-products'] = 'staff_form/get_products_api';
$route['api/staff-form/get-customers'] = 'staff_form/get_customers_api';
$route['api/staff-form/get-customer/(:num)'] = 'staff_form/get_customer_api/$1';
$route['api/staff-form/insert'] = 'staff_form/insert_data_api';
$route['api/staff-form/is-email-exists'] = 'staff_form/is_email_exists_api';

/* End of file routes.php */
/* Location: ./application/config/routes.php */