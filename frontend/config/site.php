<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Centralize configuration for `frontend` application.
 * 
 * Being used in:
 * - send email confirmation
 */

$config['site']['name'] = 'Exquisite Mooncak Selection';
$config['site']['admin_email'] = 'fairmontsingapore.mooncakes@fairmont.com';
$config['site']['logo'] = '';
$config['site']['collection_address'] = 'Fairmont Singapore’s Mooncake Booth, Level 2, Fairmont Singapore, 80 Bras Basah Road Singapore 189560';
$config['site']['enquiry_phone_number'] = '';
$config['site']['test_email'] = 'aryatmajaya@gmail.com';

// This is used in Sales > Corporate Orders , Print Slips
$config['site']['select_of_product'] = '';

// This is used in Cart > Checkout
$config['site']['minimum_delivery_charge'] = 500; // in current currency (SGD)
$config['site']['minimum_delivery_quantity'] = 50;
$config['site']['delivery_charge_upper'] = 0; // in current currency (SGD)
$config['site']['delivery_charge_lower'] = 50.00; // in current currency (SGD)
