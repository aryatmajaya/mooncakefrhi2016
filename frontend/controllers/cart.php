<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends My_Controller {
	function __construct(){
		
		$this->allowed = array(
			'add',
			'index',
			'update',
			'login',
			'remove'
		);

		parent::__construct();

		$this->load->library('cart');
		$this->load->model('Card_discount_model');

		$this->load->model('creditcards_model');
		$this->load->model('Products_model');
		$this->load->model('orders_model');
		$this->load->model('delivery_model');
	}

	function index(){  
    
		$this->load->model('Payment_gateways_model');
		$this->load->model('creditcards_model');
		
		$session_discount = $this->session->userdata("session_discount");
		if(!$session_discount){
			
			$this->load->model("discount_model");
			$data_default = $this->discount_model->getTheDefaultDiscount();
	
			if($data_default && $data_default->class!="farcard"){
				$this->load->library("discount_module/".$data_default->class);
				$this->session->set_userdata("session_discount",array(
						"type"=>$this->{$data_default->class}->code,
						"discount"=>$this->{$data_default->class}->discount
					)
				);
			}
	
		}
		
		//Clear previous order details except the cart.
		$this->clear_fulfillments();
		$this->session->set_userdata('order_ref', NULL);

    	$payment_gateway = $this->Payment_gateways_model->get_all();
    	$data['cards'] = $this->creditcards_model->get_all();
    	
		$data['cart_contents'] = $this->get_cart_contents();
		$this->set_total_discount();
		$this->clear_fulfillments(); //Clear fulfillments first
		
		$this->load->model("discount_model");
		$data['discount_modules'] = $this->discount_model->get_all();
		
		$this->load->view("header",array(
			"header_title" => "My Cart"
		));
        $this->load->view('cart', $data);  
		$this->load->view("footer");
    }

    function add(){
    	$this->load->library('session');
    	$this->load->library('cart');
    	$this->load->model('products_model');
    	
    	//Respect the name of the products especially accented characters
    	$this->cart->product_name_rules = '^.'; 
		
    	$items = $this->input->post('items');
		    	
    	if($items){
			foreach($items as $item_id=>$item){
				$product = products_model::getProductById($item_id);
				if($product){ 
					$cart_contents = $this->cart->contents();
					$existing_rowid = false;
					
					/* Check if item is already existing in the list */
					foreach ($cart_contents as $key => $value) {
						if($value['id'] == $product->id){
							$existing_rowid = true; // Okay already product exist.
							$data = array(
								'rowid'	=> $value['rowid'],
								'qty'	=> $value['qty'] + $item['qty']
							);
						}
					}
	
					if($existing_rowid ){ 				// Update the product then by adding qty.
						$this->cart->update($data);
						$cart_success_message  = "The selected item has been added to your basket";
					}else{								// Else just add the product in the cart
						$data = array(
							'id' 	=> $product->id,
							'qty'	=> $item['qty'],
							'price' => $product->price,
							'name' 	=> $this->fixtags($product->product_name)
						);
						$this->cart->insert($data);
						$cart_success_message  = "The selected items has been added to your basket";
					}
					$this->session->set_flashdata('cart_success',$cart_success_message );
				}
			}
    	}
    	redirect("products");
    }

    function update(){
    	$this->load->library('cart');
    	$products = $this->input->post('products');

    	//Respect the name of the products especially accented characters
    	$this->cart->product_name_rules = '^.'; 
    	
    	$remaining_fulfillments = $this->session->userdata('fulfillment_products');

    	foreach ($products as $key => $value) {
    		$data = array(
               'rowid' => $key,
               'qty'   => $value['qty']
            );
            $this->cart->update($data);
    	}

    	// initialize discount and store this later to session.
    	//$session_discount = array('discount' => 0 );
//
//		$card_id = $this->input->post('cards');
//		$card = $this->creditcards_model->get($card_id);
//		if($card){
//			$session_discount['type'] = 'card';
//		}
//		$this->session->set_userdata('session_card', $card);
//		// Compute discounts for orders min of 2 boxes
//		// Get the card's discount. This will be treated as default discount if the user 
//		// did not type promo code
//		$card_discount = $this->creditcards_model->get_discount($card_id);
//		
//		if($card_discount){
//			$session_discount['discount'] = $card_discount->discount;
//		}
//
//		// Check if cardtype has promotions by promo code by validating the promocode input;
//		$promocode = $this->input->post("promocode$card_id");
//                
//		if($promocode && !empty($promocode)){
//			$valid_promo = $this->creditcards_model->validate_promocode($card_id, $promocode);
//			
//                        //echo 'valid promo';
//                        //echo print_r($valid_promo);
//			$promo_flash = 'promocode_invalid';
//			$promo_flash_value = 'Invalid';
//
//			if($valid_promo){
//				$promo_flash = 'promocode_valid';
//				$promo_flash_value = 'Valid';
//
//				$session_discount['discount'] = $valid_promo->discount;
//				$session_discount['promocode'] = $valid_promo->promocode;
//			}
//			$session_discount['promocode_attempt'] = $promocode;
//			$this->session->set_flashdata($promo_flash, $promo_flash_value);
//			$this->session->set_flashdata('promo_attempt', $promocode);
//		}
//
//		if($this->cart->total_items() < 2){
//			$session_discount['discount'] = 0;
//		}
//
//		$this->session->set_userdata('session_discount', $session_discount);
//		
//		$this->set_total_discount();
		
		
		if($this->input->post("discount_module")){
			$this->load->library("discount_module/".$this->input->post("discount_module"));
			if(!$this->{$this->input->post("discount_module")}->update_proccess()){
				$this->session->set_flashdata("discount_module_error",$this->{$this->input->post("discount_module")}->message);
				redirect("cart");
			}
		}

		$this->session->set_userdata('fulfillment_products', NULL);
		$this->session->set_userdata('current_fulfillments', NULL);
		$this->session->set_userdata('display_fulfillments', NULL);
		
		if($this->input->post('checkout')){
			redirect($this->input->post('checkout'));
		}

    	redirect($_SERVER['HTTP_REFERER']);
    }

    function remove($cart_id){
    	$cart_contents = $this->cart->contents();
    	$data = array(
    		'rowid' => $cart_id,
    		'qty'=>0
    	);

    	$this->cart->update($data);

    	redirect($_SERVER['HTTP_REFERER'] . "#calculateform");
        //redirect('cart');
    }

    function checkout() {
    	//$this->session->set_userdata('collection_caps', NULL);
    	/*if(!$this->session->userdata('session_card')){
    		redirect('cart');
    	}*/
    	
    	$data['cart_contents'] = $this->get_cart_contents();
    	if($this->input->server('REQUEST_METHOD') == 'POST'){

    		$fulfillment = $this->input->post('fulfillment');
    		
    		$process_fulfillment = true;
                //echo $fulfillment['collection_type']; die();
                if ($fulfillment['collection_type'] == "delivery") {
                    if (empty($fulfillment['delivery_date']) || empty($fulfillment['address']) || empty($fulfillment['postal_code'])) {
                        $process_fulfillment = false;
                    }
                    
                } else if ($fulfillment['collection_type'] == "self_collection") {
                    if (empty($fulfillment['selfcollect_date'])) {
                        $process_fulfillment = false;
                    }
                }
                
                if ($process_fulfillment) {
                    $this->process_fulfillment($fulfillment);
                }

    		redirect('cart/checkout#fulfilment-form');
    	}else{
    		$this->save_order();

    		$remaining_fulfillments = $this->session->userdata('fulfillment_products');
    		
			if(!$remaining_fulfillments){

				$this->initialize_fulfillments($data['cart_contents']);
			}
		
			if(!$data['cart_contents']){
				redirect($this->uri->rsegment(1));
			}
    	}

    	 //block dates
        $data['bc'] = Delivery_model::get_all_block_dates('collection'); //block collection dates
        $data['bd'] = Delivery_model::get_all_block_dates('delivery'); //block delivery dates

        $this->load->view("header",array(
			'header_title' => "Checkout"
		));
        $this->load->view('cart/checkout', $data);
		$this->load->view('footer');
    }

    function cancel_fulfillment_item(){
    	$idx = $this->input->get('idx');
    	$display_fulfillments = $this->session->userdata('display_fulfillments');
    	$products = $display_fulfillments[$idx]['products'];
    	foreach ($products as $id => $value) {
    		if($value == 0) continue;
    		$this->remove_fulfillment($id);
    	}
		
		//Reduce collection_caps by 1 per type
		$collection_caps = $this->session->userdata('collection_caps');
    	if($collection_caps){
    		$type = $display_fulfillments[$idx]['collection_type'];
    		$collection_caps[$type]--;
    	}

    	unset($display_fulfillments[$idx]);

    	$this->session->set_userdata('collection_caps', $collection_caps);
    	$this->session->set_userdata('display_fulfillments', $display_fulfillments);

    	redirect('cart/checkout#fulfilment-form');
    }

    private function remove_fulfillment($product_id){
    	$remaining_fulfillments = $this->session->userdata('fulfillment_products');
		$current_fulfillments = $this->session->userdata('current_fulfillments');

		$temp_current_fulfillments = $current_fulfillments;
	/*	echo $product_id;
		print_r($remaining_fulfillments);
		print_r($temp_current_fulfillments);
		die();*/
		foreach ($temp_current_fulfillments as $key => $value) {
			if($value['product_id'] != $product_id) continue;

			$id = $value['product_id'];
			$remaining_fulfillments[$id]['value'] += $value['value'];
			unset($current_fulfillments[$key]);
		}		
			
		$this->session->set_userdata('current_fulfillments', $current_fulfillments);
		$this->session->set_userdata('fulfillment_products', $remaining_fulfillments);
    }

    function proceed_checkout(){
    	/*if($this->session->userdata('reached_proceed_checkout') 
    		&& $this->session->userdata('reached_proceed_checkout') != NULL){
    		$this->session->set_userdata('reached_proceed_checkout', NULL);
    		redirect('cart/');
    	}*/

    	$this->session->set_userdata('reached_proceed_checkout', true);
    	$this->save_delivery();
    	
    	$data['cart_contents'] = $this->get_cart_contents();

		$this->load->view("header",array(
			"header_title" => "Order Summary"
		));
        $this->load->view('cart/review', $data);
		$this->load->view('footer');
    }

    function cancel_checkout(){

    	$this->session->set_userdata('current_fulfillments', NULL);
    	$this->session->set_userdata('display_fulfillments', NULL);
		$this->session->set_userdata('fulfillment_products', NULL);
		$this->session->set_userdata('session_card', NULL);
		$this->session->set_userdata('session_discount', NULL);
		$this->session->set_userdata('total_discount', 0);
		$this->session->set_userdata('order_ref', NULL);
		sleep(1);
		redirect('products');
    }

    function proceed_payment(){

    	if($this->session->userdata('order_ref') && $this->session->userdata('current_fulfillments')){
    		$current_fulfillments = $this->session->userdata('current_fulfillments');
	    	$order_ref = $this->session->userdata('order_ref');
	    	$user_id = $this->session->userdata('user_id');
	    	//$card = $this->session->userdata('session_card');
			
	    	$this->load->model('delivery_model');
			
			$session_discount = $this->session->userdata('session_discount');
			
			$this->load->model("discount_model");
			$discount_module = $this->discount_model->getByClass($session_discount['type']);

	    	$order_details = $this->orders_model->get_order_details($order_ref);
	    	if($order_details){
	    		//Proceed to payment
	    		$delivery_charge = $this->delivery_model->get_total_delivery_charge($order_ref);
				
				switch(ENVIRONMENT) {
                	case 'local':
                	case 'local-ary':
                	case 'local-adi':
                	case 'development':
                	case 'staging':
                		$mid = TELEMONEY_MID;
                	break;
                	default:
						//$mid = TELEMONEY_MID;
                		$mid = $discount_module->mid;
                	break;
                }
				
	    		if($mid){
	    			$return_url = site_url(TELEMONEY_RETURN_PATH);
		    		$update_url = site_url(TELEMONEY_UPDATE_PATH);
		    		$gst_charge = ($order_details->total_amount - $order_details->discounted_amount + $delivery_charge) * (GST_PERCENT / 100);
		    		//hack: remove gst charge for frhi
		    		$gst_charge = 0;
		    		$total_charge = ($order_details->total_amount - $order_details->discounted_amount + $delivery_charge) + $gst_charge;
		    		$payment_url = TELEMONEY_URL . '?mid='. $mid .'&ref='.$order_ref.'&amt='.$total_charge.
		    						'&cur=SGD&transtype=sale&rcard=64&returnurl='.$return_url.'&statusurl=' . $update_url;
					$this->clear_fulfillments();
					
					//die($payment_url);
                                        
                                /*
                                 * This to log and check if we are sending valid data to telemoney
                                 * This is used for debugging purposes
                                 */
                                $insert_log = array('url' => $payment_url);
                                orders_model::log_transaction($insert_log);
                                /* End */
					
					
					//die($payment_url);
                                        
		    		redirect($payment_url);
	    		}
	    	}
    	}
    	//redirect(site_url());
    }

    private function save_delivery(){
    	if($this->session->userdata('order_ref') && $this->session->userdata('current_fulfillments')){

    		$current_fulfillments = $this->session->userdata('current_fulfillments');
	    	$order_ref = $this->session->userdata('order_ref');
	    	$user_id = $this->session->userdata('user_id');
	    	$card = $this->session->userdata('session_card');
	    	$this->load->model('delivery_model');
	    	$this->delivery_model->cleanUp_delivery($order_ref);
	    	$order_details = $this->orders_model->get_order_details($order_ref);
	    	if($order_details){

	    		foreach ($current_fulfillments as $key => $value) {
	    			
					$delivery_data = array(
						'order_ref' => $order_ref,
						'customer_id' => $user_id,
						'service_type' => $value['info']['type'],
						'service_date' => date('Y-m-d', strtotime($value['info']['date'])),
						'time_slot' => $value['info']['collection_slot'],
						'address' => $value['info']['address'],
						'postal_code' => $value['info']['postal_code'],
						'delivered_to' => $value['info']['recipient'],
						'delivered_contact' => $value['info']['contact_number'],
						'additional_request' => $value['info']['additional_request'],
					);

					$this->delivery_model->add_delivery($delivery_data);
					$latest_delivery_id = $this->db->insert_id();
					$product = $this->Products_model->getProductById($value['product_id']);

					$insert_item_data = array(
                        'delivery_id' => $latest_delivery_id,
                        'order_ref' => $order_ref,
                        'product_id' => $product->id,
                        'product_name' => $product->product_name,
                        'qty' => $value['value']
                    );
            		$this->delivery_model->add_delivery_item($insert_item_data);
	    		}
	    	}
    	}
    }

    private function get_cart_contents(){
		
		$cart_contents = $this->cart->contents();
		
		$ids = array();
		foreach ($cart_contents as $key => $value) {
			$ids[] = $value['id'];
		}

		$products = array();
		if(!empty($ids)){
			$this->load->model('Products_model');
			$products = Products_model::getProductsInId($ids);
		}

		foreach ($products as $product_key => $product_value) {
			foreach ($cart_contents as $cart_key => $cart_value) {
				if($cart_value['id'] == $product_value->id){
					$cart_contents[$cart_key]['product_detail'] = $product_value;
				}
			}
		}

		return $cart_contents;
	}

    private function fixtags($text){
		$text = htmlspecialchars($text);
		return $text;
	}

	private function save_order(){
		$this->load->model('orders_model');

		$browser_details = $_SERVER['HTTP_USER_AGENT'];
		$session_card = $this->session->userdata('session_card');
		$session_discount = $this->session->userdata('session_discount');

		$discounted_price = ($session_discount['discount'] / 100) * $this->cart->total();
		$order_ref = $this->session->userdata('order_ref');
		
		$this->load->library("discount_module/".$session_discount['type']);
		
		$insert_data = array(
			'customer_id' => $this->session->userdata('user_id'),  
			//'card_type' => $session_card->issued_by,
			//'card_discount' => $session_discount['discount'],
			//'promo_code' => isset($session_discount['promocode']) ? $session_discount['promocode'] : '',
			//'promo_code_discount' => 0,
			//'delivery_charge' => ($this->cart->total_items() < 50) ? 50 : 0,   
			'error_message' => '',
			'total_amount' => $this->cart->total(),
			//'discounted_amount' => $discounted_price,
			'admin_notes' => '',
			'browser_details' => (isset($browser_details)?$browser_details:'none'),
			'status' => ORDER_INCOMPLETE // 6 means 'Incomplete'
		);
		
		/// hack begin //////
		if($session_discount['type']=="farcard"){
			$insert_data['card_type'] = $this->{$session_discount['type']}->title;
			$insert_data['promo_code'] = $session_discount['code'];
			$insert_data['promo_code_discount'] = $session_discount['discount'];
		}else{
			$insert_data['card_type'] = $this->{$session_discount['type']}->title;
			$insert_data['card_discount'] = $session_discount['discount'];
		}
		
		$this->load->model("total/model_discount_number_item","model_discount_number_item");
		$number_item_discount_item = $this->model_discount_number_item->getItem();
		if($number_item_discount_item){
			$insert_data['number_item_discount'] = $number_item_discount_item->discount;	
		}
		
		$this->load->library("total/discount");
		$this->load->library("total/discount_number_item");
		$total = $this->cart->total();
		$total_data = array();
		
		$this->discount->getTotal($total_data,$total);
		$this->discount_number_item->getTotal($total_data,$total);
		
		$discounted_price = $this->cart->total()-$total;
		
		$insert_data['discounted_amount'] = $discounted_price;
		/// hack end //////

		if(!$order_ref){
			$the_sequence_id = $this->orders_model->get_last_id();
			$order_ref = date("ymdHi").sprintf("%05d",$the_sequence_id+1); //create order_ref
			$this->session->set_userdata('order_ref', $order_ref);

			$insert_data['order_ref'] = $order_ref;
			$this->orders_model->add_order($insert_data);
		}else{
			$order = $this->orders_model->get_order_id($order_ref, $this->session->userdata('user_id'));	
			if(!$order){
				$insert_data['order_ref'] = $order_ref;
				$this->orders_model->add_order($insert_data);
			}else{
				$this->orders_model->update_order('order_ref', $order_ref, $insert_data);	
			}
			$this->orders_model->save_order_items($order->id);
		}
		
		
		
	}

	function clear_orders(){
		$this->clear_order_details();
		redirect(site_url());
	}

	private function initialize_fulfillments($cart_contents){
		$fulfillment_products = $this->session->userdata('fulfillment_products');

		if(empty($fulfillment_products)){
			
			$fulfillments = array();
			foreach ($cart_contents as $cart_key => $cart_value) {
				$fulfillments[$cart_value['id']]['value'] = $cart_value['qty'];
			}
			$this->session->set_userdata('fulfillment_products', $fulfillments);
		}

		$current_fulfillments = $this->session->userdata('current_fulfillments');
		if(!$current_fulfillments){
			$current_fulfillments = array();
			$this->session->set_userdata('current_fulfillments', $current_fulfillments);
		}

		$display_fulfillments = $this->session->userdata('display_fulfillments');
		if(!$display_fulfillments){
			$display_fulfillments = array();
			$this->session->set_userdata('current_fulfillments', $display_fulfillments);
		}

	}

	private function process_fulfillment($fulfillment){
		if(!isset($fulfillment['products'])) return false;

		$remaining_fulfillments = $this->session->userdata('fulfillment_products');
		$current_fulfillments = $this->session->userdata('current_fulfillments');
		$display_fulfillments = $this->session->userdata('display_fulfillments', $display_fulfillments);
		$collection_caps = $this->session->userdata('collection_caps');
		
		$capp_delivery_count = 0;
		$fulfillment_insert = false;
		if($fulfillment['collection_type'] =='delivery' && $collection_caps['delivery'] >= 2){
			$this->session->set_flashdata('fulfillment_error', 'You already have 2 deliveries in you fulfillment. 
																	Maximum allowed for delivery is 2.');
			return;
		}

		if($fulfillment['collection_type'] =='self_collection' && $collection_caps['self_collection'] >= 3){
			$this->session->set_flashdata('fulfillment_error', 'You already have 3 self collection in you fulfillment. 
																	Maximum allowed for self collection is 3.');
			return;	
		}

		foreach ($fulfillment['products'] as $product_id => $value) {
			if($value == 0 ) continue;

			// This need to filter if value is more the remaining items.
			if($value > $remaining_fulfillments[$product_id]['value']){				
				$this->session->set_flashdata('fulfillment_error', 'One of your fulfillment input is more than the remaining deliverable item');
				break;
			}

			 // Time in self collection is set as collection slot
			
			if($fulfillment['collection_type'] == 'delivery'){
				$at_date = $fulfillment['delivery_date'];	
                $collection_slot = $fulfillment['collection_slot'];
				$additional_request = $fulfillment['delivery_additional_request'];
			} else {
                $collection_slot = $fulfillment['time'];
				$at_date = $fulfillment['selfcollect_date'];
				$additional_request = $fulfillment['selfcollect_additional_request'];
            }
			
			$new_array = array(
				'product_id' => $product_id,
				'value' => $value,
				'info' => array(
					'type' => $fulfillment['collection_type'],
					'date' => $at_date,
					'address' => $fulfillment['address'],
					'postal_code' => $fulfillment['postal_code'],
					'recipient' => $fulfillment['recipient'],
					'contact_number' => $fulfillment['contact_number'],
					'collection_slot' =>$fulfillment['collection_slot'],
					'additional_request' =>$additional_request
				)
			);
			
			//Check for duplicates in delivery
			if($fulfillment['collection_type'] == 'delivery'){
				$index = $this->get_delivery_duplicate_index($new_array);
				if($index >= 0){
					$remaining_fulfillments[$product_id]['value'] -= $value;
					$oldvalue = $current_fulfillments[$index]['value'];
					$newvalue = $value + $oldvalue;
					$current_fulfillments[$index]['value'] = $newvalue;
				}else{
					$remaining_fulfillments[$product_id]['value'] -= $value;
					if($capp_delivery_count == 0){
						$capp_delivery_count++;
						
					}
					$current_fulfillments[] = $new_array;
					$fulfillment_insert = true;
				}
			}else{ //Self collect
				$remaining_fulfillments[$product_id]['value'] -= $value;
				$current_fulfillments[] = $new_array;
				$fulfillment_insert = true;
			}
			
		}
		if($fulfillment_insert ){
			$this->process_collection_cap($fulfillment['collection_type']);
			$display_fulfillments[] = $fulfillment;
		}
		
		$this->session->set_userdata('display_fulfillments', $display_fulfillments);
		$this->session->set_userdata('current_fulfillments', $current_fulfillments);
		$this->session->set_userdata('fulfillment_products', $remaining_fulfillments);
		
	}

	private function get_delivery_duplicate_index($filldata){
		$current_fulfillments = $this->session->userdata('current_fulfillments');
		$index = -1;
		if(count($current_fulfillments) > 0){
			foreach ($current_fulfillments as $key => $value) {
				if($value['product_id'] == $filldata['product_id'] && 
					$value['info']['type'] == 'delivery' && $filldata['info']['type']){
					
					$old_info = $value['info'];
					$new_info = $filldata['info'];

					unset($new_info['recipient'], $new_info['contact_number'],
							$old_info['recipient'], $old_info['contact_number']);

					if(implode(',', $old_info) == implode(',', $new_info)){
						$index = $key;
						break;
					}
				}
			}	
		}

		return $index;
	}

	private function process_collection_cap($type){
		$collection_caps = $this->session->userdata('collection_caps');
		if(!$collection_caps){
			$collection_caps = array(
				'delivery' => 0,
				'self_collection' => 0
			);
		}
		
		$collection_caps[$type]++;
		
		$this->session->set_userdata('collection_caps', $collection_caps);

	}

	private function addfulfillment($product_id, $fulfillment){
		$current_fulfillments = $this->session->userdata('current_fulfillments');
		
		if(!$current_fulfillments){
			$current_fulfillments = array();
		}

		foreach ($current_fulfillments as $key => $value) {
			if($value['id'] == $product_id){
				if($value['info']['type'] == $fulfillment['collection_type'] &&
					$value['date'] == $fulfillment['date']){
					$current_fulfillments[$key]['value'] += $fulfillment['products'][$product_id];
				}
			}else{
				$new_fulfillment = array(
						'key'=>$product_id,
						'value'=>$fulfillment['products'][$product_id],
						'info'=>array(
							'type' => $fulfillment['collection_type'],
							'date' => $fulfillment['date'],
							'address' => $fulfillment['address'],
							'postal_code' => $fulfillment['postal_code'],
							'recipient' => $fulfillment['recipient'],
							'contact_number' => $fulfillment['contact_number'],
							'collection_slot' =>$fulfillment['collection_slot']
						)
					);

				$current_fulfillments[] = $new_fulfillment;
			}

		}

		$this->session->set_userdata('current_fulfillments', $current_fulfillments);
		die(print_r($this->session->userdata('current_fulfillments')));
	}

	private function set_total_discount(){
		//$card = $this->session->userdata('session_card');
		$session_discount = $this->session->userdata('session_discount');
		if($session_discount){
			$total_discount = $session_discount['discount'] / 100 * $this->cart->total();
			$this->session->set_userdata('total_discount', $total_discount);
		}else{
			$this->session->set_userdata('total_discount', 0);
		}
	}


	private function send_confirmation($order_ref = null){
        /* 
         * Notes: This method is being used by CMS admin (when status are changed from error to paid)
         * and the Order page (Front-end)
         * where it will send an email to the customer for order confirmation page.
         */
        if ($order_ref){
            Utilities::Send_Confirmation($order_ref);
        }
    }

    private function clear_fulfillments(){
    	$this->session->set_userdata('current_fulfillments', NULL);
    	$this->session->set_userdata('display_fulfillments', NULL);
		$this->session->set_userdata('fulfillment_products', NULL);
		$this->session->set_userdata('collection_caps', NULL);
    }

    private function clear_order_details(){
    	$this->session->set_userdata('order_ref', NULL);
		$this->session->set_userdata('session_card', NULL);
		$this->session->set_userdata('session_discount', NULL);
		
		$this->load->library('cart');
		$this->cart->destroy();
    }

    private function get_delivery_charges($delivery_items, $total_qty){
        $delivery_charge = 50;
        if ($delivery_items){
            $dctr = 1;
            $total_delivery_charge = 0;
            foreach($delivery_items as $delivery_charges){
                if ($total_qty < 50){
                    $total_delivery_charge = $total_delivery_charge + $delivery_charge;
                }
                elseif ($total_qty >= 50  && $dctr == 2){
                    //echo 'test';
                    $total_delivery_charge = $delivery_charge;
                }
                $dctr++;
            }
            return $total_delivery_charge;
        }
    }


    function login(){
    	
		if($this->session->userdata('user_id')) redirect($this->input->server('REFERER'));
		$this->form_validation->set_rules('login[username]', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('login[password]', 'Password', 'required|xss_clean');

		extract($_POST);
		$data = array(
			'username' => $login['username'],
			'password' => $login['password']
		);

		if(isset($request_path) && $request_path == 'cart'){
			//$proceed_to_url = site_url('cart/checkout#fulfillmentsection');
			$this->session->set_userdata('proceed_to_url', site_url('cart/checkout#fulfillmentsection'));
		}

		if ($this->form_validation->run() == FALSE)
		{		
			$this->session->set_flashdata('login_error', validation_errors());
		}
		else
		{
			
			$this->load->model('customers_model');
			
			$user_id = $this->customers_model->check_login($login['username'], $login['password']);
			
			if ($user_id == 'inactive'){
				//login failed
				$this->session->set_flashdata('inactive', TRUE);
				
			}
			elseif (!$user_id){
				//login failed
				$this->session->set_flashdata('login_error', "Bad credentials.");
			}
			else{
				if($this->session->userdata('proceed_to_url')){
					//$proceed_url = $this->session->userdata('proceed_to_url');
					//this->session->set_userdata('proceed_to_url', NULL);
					//redirect($proceed_url);
					redirect("cart");
				}
				//redirect($this->input->server('REFERER'));
			}
		}
		
		redirect('cart#login-register');
	}

    //Test 
    function test(){
    	$order_ref ='140713184200056';
    	$this->load->model('delivery_model');
    	$delivery_charge = $this->delivery_model->get_total_delivery_charge($order_ref);
    	
    	echo "<pre>";
    	print_r($delivery_charge);
    	echo "</pre>";
    	die();
    }
}

/* End of file cart.php */
/* Location: ./frontend/controllers/cart.php */