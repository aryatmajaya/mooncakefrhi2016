<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Close extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function index_cafe_noel(){ 
    	$url = $this->input->server('HTTP_REFERER');
    	$extract = parse_url($url);
    	if(empty($extract['path'])){
    		 redirect('http://www.celebrationscentral.com.sg/cafenoel');
    	}
    	
       redirect('http://www.celebrationscentral.com.sg/home/index2');
    }
    
    function index() { 

        $data['header_title'] = "Home";
        $data['view'] = "home/close";

        $this->load->model('Products_model');
        $data['new_products_line_up'] = Products_model::getNewProducts(3);

        $this->load->view('home/close', $data);        
    }
}

/* End of file home.php */
/* Location: ./frontend/controllers/home.php */