<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
	function index(){
		$this->load->view('header',array(
			"header_title" => "Contact Us"
		));
        $this->load->view('contact_us'); 
		$this->load->view('footer');       
    }
}