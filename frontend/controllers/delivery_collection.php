<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_collection extends CI_Controller {
	function index(){ 
        $this->load->model('mc_cms_model');
        $data['content'] = $this->mc_cms_model->get_by_slug('delivery_collection');
        
        $data['view'] = "delivery_collection/default";
    	$this->load->view("header",array(
			"header_title" => "Delivery/Collection",
			"menu_active"  => "delivery-collection"
		));
        $this->load->view('delivery_collection', $data);        
 		$this->load->view("footer");
    }
}