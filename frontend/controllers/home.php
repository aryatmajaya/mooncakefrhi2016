<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    function index() { 
        $this->load->model('Products_model');
        $data['new_products_line_up'] = Products_model::getNewProducts(3);
		
		$this->load->view('header',array(
			"header_title" => "Home",
			"menu_active"  => "home"
		));
        $this->load->view('home', $data);
		$this->load->view('footer');     
    }
}