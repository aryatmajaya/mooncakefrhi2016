<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount extends My_Controller {
	function __construct()
	{
		$this->allowed = array();

		parent::__construct();
		$this->load->model('customers_model');
	}

	function index(){ 
        $customer = $this->customers_model->get_user($this->session->userdata('user_id'));
        $data['customer'] = $customer;
		$this->load->view("header",array(
			"header_title" => "My Account"
		));
        $this->load->view('my_account', $data);
		$this->load->view('footer');  
    }

    function save(){
    	extract($_POST);
        
    	$this->form_validation->set_rules('customer[first_name]', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer[last_name]', 'Lastname', 'required|xss_clean');
		$this->form_validation->set_rules('customer[email]', 'Email', 'required|valid_email');
		if(!empty($customer['password'])){
                    
                    
			$this->form_validation->set_rules('customer[password]', 'Password', 'matches[confirm_password]|min_length[8]');
			//$this->form_validation->set_rules('customer[confirm_password]', 'Confirm password', 'xss_clean');
		}
		
		
		if ($this->form_validation->run())
		{
                    if (!empty($customer['password'])) {
                        $this->load->library('encrypt');
                        $customer['password'] = $this->encrypt->encode($customer['password']);
                    }
			
			$this->customers_model->update($customer, $this->session->userdata('user_id'));
			$this->session->set_flashdata('account_success', 'Account saved!');
		}else{
			
			$this->session->set_flashdata('account_error', 'Error saving. Please check your data before saving');	
		}

		

		redirect($this->uri->segment(1));
    }
}

/* End of file myaccount.php */
/* Location: ./frontend/controllers/myaccount.php */