<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

    
    function index() {
        redirect(site_url());
    }
    
    function order_confirmation() {
		
        $order_ref = $this->session->userdata('order_ref');
		
        $this->load->model('orders_model');
        $this->load->model('customers_model');
        $this->load->model('delivery_model');
        
        $data['order_detail'] = orders_model::get_order_details($order_ref);
        
        if($data['order_detail'] && $data['order_detail']->status == ORDER_SUCCESS_PAID){
            $data['order_items'] = orders_model::get_ordered_items($data['order_detail']->id);
            $data['customer'] = customers_model::get_user($data['order_detail']->customer_id);
            $data['delivery_charges'] = delivery_model::Get_Total_Delivery_Qty($data['order_detail']->order_ref);
			
			$data['fullfilments'] =  delivery_model::get_delivery_option3($order_ref);
			
			$this->config->load('site');
            $site_config = $this->config->item('site');
            $data['site_config'] = $site_config;

            $this->clear_order_details();
			$this->clear_cart();
            $this->clear_fulfillments();

            $this->load->view("header",array(
				'header_title' =>  "Transaction Success"
			));
			$this->load->view('order/success_summary', $data);
			$this->load->view('footer');
			
        } else {     
            redirect('order/failed');
        }
    }
    
    
	function order_confirmation_v1(){
            /* This has been disable due to error data being displayed */
    	$order_ref = $this->session->userdata('order_ref');
		
		$this->load->model('orders_model');
		$this->load->model('customers_model');
		$this->load->model('delivery_model');
		$data['order_detail'] = $this->orders_model->get_order_details($order_ref);
		
		if($data['order_detail'] && $data['order_detail']->status == ORDER_SUCCESS_PAID){
                    $data['order_items'] = $this->orders_model->get_ordered_items($order_detail->id);
                    $data['customer'] = $this->customers_model->get_user($order_detail->customer_id);
                    $data['delivery_charges'] = $this->delivery_model->Get_Total_Delivery_Qty($order_detail->order_ref);

                    $this->clear_order_details();
					$this->clear_cart();
                    $this->clear_fulfillments();
					
					$this->load->view("header",array(
						'header_title' => "Success Summary"
					));
					$this->load->view("order/success_summary",$data);;
                    $this->load->view("footer");
		} else {     
			redirect('order/failed/');
		}
		
    }
    
    function order_confirmation_test($order_ref) {
        if ($order_ref) {
		
		$this->load->model('orders_model');
		$this->load->model('customers_model');
		$this->load->model('delivery_model');
		$data['order_detail'] = orders_model::get_order_details($order_ref);
		var_dump($data['order_detail']);
		if($data['order_detail'] && $data['order_detail']->status == ORDER_SUCCESS_PAID){
                    $data['order_items'] = orders_model::get_ordered_items($data['order_detail']->id);
                    //var_dump($data['order_items']);
                    echo print_r($data['order_items']);
                    $data['customer'] = customers_model::get_user($data['order_detail']->customer_id);
                    $data['delivery_charges'] = delivery_model::Get_Total_Delivery_Qty($data['order_detail']->order_ref);

                    $this->clear_order_details();
					$this->clear_cart();
                    $this->clear_fulfillments();

					$this->load->view("header",array(
						'header_title' => "Success Summary"
					));
                    $this->load->view("order/success_summary",$data);
                    $this->load->view('footer');
		} else {     
                    redirect('order/failed/');
		}
        } else {
            redirect(site_url());
        }
    }

    function update_order(){
		
	   if (isset($_POST['TM_RefNo'])){
			$this->load->model('orders_model');
			$order_ref = $_POST['TM_RefNo'];
			$payment_type = $_POST['TM_PaymentType'];

			if ($_POST['TM_Status'] == "YES") {
				$update_data = array(
					'status' => ORDER_SUCCESS_PAID,
					'payment_type' => $payment_type,
					'approval_code' => $_POST['TM_ApprovalCode'],
					'payment_mode' => 'credit card'
				);

				$this->orders_model->update_order('order_ref', $order_ref, $update_data);
				
				$this->send_confirmation($order_ref);
								
			} else {
				$the_error = $_POST['TM_Error'];
				$the_error_msg = $_POST['TM_ErrorMsg'];

				$update_data = array(
					'status' => ORDER_FAILED,
					'the_error' => $the_error,
					'error_message' => $the_error_msg,
					'payment_type' => $payment_type,
					'payment_mode' => 'Credit Card'
				);

				$this->orders_model->update_order('order_ref', $order_ref, $update_data);
			}
	   }
	   else{
	   		$this->session->set_flashdata("payment_error", "Cannot process payment. Unknow error.");
			redirect(site_url());
	   }

	   //file_put_contents("/celebrations_2014/log.txt", json_encode($_POST, TRUE));
    }

    function summary(){
    	//$data['cart_contents'] = $this->get_cart_contents();
		$this->load->view("header",array(
			'header_title' => "Success Summary"
		));
		$this->load->view("order/success_summary");
        $this->load->view('footer');
    }

    function failed(){
    	//$data['cart_contents'] = $this->get_cart_contents();
    	if(!$this->session->userdata('order_ref')){
			redirect("");
		}
        $order_ref = $this->session->userdata('order_ref');
		
		$this->load->model('orders_model');

		$data['order_detail'] = $this->orders_model->get_order_details($order_ref);
		
		$this->clear_order_details();
		$this->clear_fulfillments();

        $this->load->view("header",array(
			'header_title' => "Transaction Failed"
		));
        $this->load->view("order/failed",$data);
        $this->load->view('footer');   
    }

    private function clear_order_details(){
    	$this->session->set_userdata('order_ref', NULL);
		$this->session->set_userdata('card_discount', NULL);
		$this->session->set_userdata('FAR_discount', NULL);
		$this->session->set_userdata('collection_caps', NULL);
    }
	
	private function clear_cart(){
		$this->load->library('cart');
		$this->cart->destroy();
	}

    private function clear_fulfillments(){
    	$this->session->set_userdata('current_fulfillments', NULL);
		$this->session->set_userdata('fulfillment_products', NULL);
		$this->session->set_userdata('collection_caps', NULL);
    }
    
    function send_confirmation_test2($order_ref) {
        if ($order_ref){
            $this->send_confirmation($order_ref);
        }
    }
    
    

    public function send_confirmation($order_ref = null){
        /* 
         * Notes: This method is being used by CMS admin (when status are changed from error to paid)
         * and the Order page (Front-end)
         * where it will send an email to the customer for order confirmation page.
         */
        if ($order_ref){
			Utilities::Send_Confirmation($order_ref);
        }
    }
    
    function send_confirmation_test($order_ref = null){
        /* 
         * Notes: This will send to the tester
         */
        if ($order_ref){
            Utilities::Send_Confirmation($order_ref, 'james@cprvision.com');
        }
    }
    
    
    function manual_send_confirmation($order_ref = null){
        /* 
         * Created by James Castaneros - 31 July 2014
         * Notes: This method is a tool to send confirmation manually by CPR Vision
         */
        if ($order_ref){
            Utilities::Send_Confirmation($order_ref);
        }
    }
    
    function send_notification($order_ref = null) {
        /* Created By James Castaneros - 01 August 2014
         * This tool is used by Admin thru Transaction module (mc_transactions)
         * 
         */
        if ($order_ref){
            if (Utilities::Send_Confirmation($order_ref)) {
                echo 'Notification sent successfully!';
            } else {
                echo 'Error in Sending Notification'.$order_ref;
            }
            
        } else {
            echo 'Order Ref is required.';
        }
        
    }
    
    function view_confirmation($order_ref)
    {
    
        $ndata = Utilities::Send_Confirmation($order_ref, NULL, 'view');
        //var_dump($ndata); die();
        echo $ndata['message'];
    }



    function ajax_test() {
        echo 'ajax test';
    }
    
}