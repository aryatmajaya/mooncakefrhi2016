<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {
	function index(){ 
		$this->load->model('products_model');
		
		$this->load->view("header",array(
			"header_title" => "Mooncake Selection",
			"menu_active"  => "products"
		));
        $this->load->view('products', array(
			'product_line_up' => $this->products_model->getProductGroupedByCategories()
		));
		
		$this->load->view("footer");
    }
}