<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends CI_Controller {
	function index(){ 
		$this->load->view("header",array(
			'header_title' => "Promotions",
			'menu_active'  => 'promotions'
		));
        $this->load->view('promotions');
		$this->load->view('footer');
    }
}