<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff_form extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		header('Access-Control-Allow-Origin: http://www.celebrationscentral.com.sg');
	}
	
	function index(){

		$data = array(

		);

		$this->load->view("staff_form/default", $data);
	}

	public function insert_data_api(){
		
		
		$postdata = file_get_contents("php://input");
    	$request = json_decode($postdata);
		
		//print_r($request);
		//die();
		
		$this->load->model("common");
		$this->load->model("products_model");
		$this->load->model("delivery_model");
		$this->load->model("customers_model");
		$this->load->model("orders_model");
				
		if($request->customer_type=="new"){
			$this->common->insert("mc_customers",array(
				'password' => '*corporate-order-only*',
				'first_name' => $request->customer_new->first_name,
				'last_name' => $request->customer_new->last_name,
				'company_name' => $request->customer_new->company,
				'address' => $request->customer_new->address,
				//'postal_code' => '',
				'mobile' => $request->customer_new->contact,
				'email' => $request->customer_new->email,
				'status' => 1,
				'corporate' => 1,
				//'created_by' => ''
			));
			
			$customer_id = $this->db->insert_id();
		}else{
			$customer_id = $request->customer_id;	
		}
		
        //SAVING ORDER TO DATABASE
		
		
        $the_sequence_id = $this->orders_model->GetMaxID();
        $order_ref = 'FC'.sprintf("%06d",$the_sequence_id+1);
        
        $order_data = array(
                'order_ref' =>  $order_ref,
                'customer_id' => $customer_id,
                'corporate_discount' => $request->discount,
                'total_amount' => $request->subtotal,
                'delivery_charge' => $request->delivery_charge,
                'discounted_amount' => $request->discount_amount,
                'status' => 10, //offline incomplete
                'ordering_method' => 'corporate',
				'is_booth'=>1
                //'admin_notes' => '',
                //'admin_ordered_by' => $this->session->userdata('user_id')
        );

        $this->orders_model->add_order_corporate($order_data);
        $order_id = $this->db->insert_id();
        
		
		$products_items = array();
		foreach($request->products as $category_id=>$product) {
			foreach($product as $product_id=>$qty){
				if($qty){
					$product_info = $this->products_model->getProductById($product_id);
					$products_items[$product_id] = array(
						"product_name"=>$product_info->product_name,
						"price"=>$product_info->price,
						"qty"=>$qty,
						"total"=>$product_info->price*$qty
					);	
				}
			}
		}
		
		
        //SAVING ORDER ITEMS TO DATABASE  
		if($products_items){
			foreach($products_items as $product_id=>$product){
				$items_data = array(
					'order_id' => $order_id,
					'product_id' => $product_id,
					'product_name' => $product['product_name'],
					'qty' => $product['qty'],
					'price' => $product['price']
				);
				$this->common->insert("mc_orders_items", $items_data);
			}
		}
        //END
        
        //SAVING FULFILLMENT OPTIONS TO DATABASE
        
		if($request->fullfillment_type=="delivery"){
			$service_date=date("Y-m-d",strtotime($request->delivery->date));
			$time_slot=$request->delivery->time_slot;
			$address=$request->delivery->address;
			$postal_code=$request->delivery->postal_code;
			$delivered_to=$request->delivery->recipient;
			$delivered_contact=$request->delivery->contact;
		}else{
			$service_date=date("Y-m-d",strtotime($request->self_collection->date));
			$time_slot=$request->self_collection->time_slot;
			$address="";
			$postal_code="";
			$delivered_to="";
			$delivered_contact="";
		}
		
        $fulfilment_data = array(
            'order_ref' => $order_ref,
            'customer_id' => $customer_id,
            'service_type' => $request->fullfillment_type,
            'service_date' => $service_date,
            'time_slot' => $time_slot,
            'address' => $address,
            'postal_code' => $postal_code,
            'delivered_to' => $delivered_to,
            'delivered_contact' => $delivered_contact
        );            
		
        $this->delivery_model->add_delivery($fulfilment_data);
        $delivery_id = $this->db->insert_id();

        
        
        //SAVING FULFILLMENT ITEMS TO DATABASE
		if($products_items){
			foreach($products_items as $product_id=>$product){
				$items_data = array(
					'delivery_id' => $delivery_id,
					'order_ref' => $order_ref,
					'product_id' => $product_id,
					'product_name' => $product['product_name'],
					'qty' => $product['qty'],
				);
				$this->delivery_model->add_delivery_item($items_data);
			}
		}
		        
        //END
        
        //SAVING COPORATE ORDER INFO TO DATABASE
        $co_orderinfo2 = $this->session->userdata('co_orderinfo2');
        
        $info2_data = array(
            'order_id' => $order_id,
			'special_requirements' => $request->special_requirements,
            //'notes1' => (!empty($co_orderinfo2['notes1']))?$co_orderinfo2['notes1']:' ',
            //'notes2' => (!empty($co_orderinfo2['notes2']))?$co_orderinfo2['notes2']:' ',
            //'order_section' => $co_orderinfo2['order_section'],
            //'hot_stamping' => $co_orderinfo2['hot_stamping'],
            //'logo_id' => $co_orderinfo2['logo_id'],
            'sales_manager' => $request->sales_manager,
            //'customization' => $co_orderinfo2['customization'],
            'payment_mode' => $request->payment_mode
        );
        $this->common->insert("mc_orders_corporate", $info2_data);
        
        //END
        
        //Now send a mail notification
		
		$order_info = $this->orders_model->order_summary($order_ref);
		$order_corporate = $this->orders_model->get_order_corporate($order_info->id);
		$fo = $this->delivery_model->get_delivery_option3($order_ref);
		
		//print_r($order_info);
		
		$this->load->library('email');
		$this->config->load('site');
        $site_config = $this->config->item('site');
				
		$config=array();
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['crlf'] = '\r\n';
		$config['newline'] = '\r\n';

		$this->email->initialize($config);

		$this->email->from(ADMIN_EMAIL, $site_config['name']);
		$this->email->to($order_info->email);
		
		//bcc to admin
		$this->email->bcc(array(
			'fairmontsingapore.mooncakes@fairmont.com',
			'aryatmajaya@gmail.com',
			'marcus@cprvision.com'
			//'cafenoel@fairmont.com'
			//'roy@cprvision.com',
			//$site_config['test_email'],
			//ADMIN_EMAIL
		));
		$this->email->subject(sprintf('Order Confirmation #%s - %s', $order_ref, $site_config['name']));
		
		$email_data = array(
			"subtotal"=>$request->subtotal,
			'products_items'=>$products_items,
			"order"=>$order_info,
			"order_corporate" => $order_corporate,
			"fo"=>$fo,
			"discount"=>$request->discount,
			"discount_amount"=>$request->discount_amount,
			"delivery_charge"=>$request->delivery_charge,
			"gst_persen"=>$request->gst_persen,
			"gst"=>$request->gst,
			"subtotal"=>$request->subtotal,
			"grandtotal"=>$request->grandtotal,
			"site_config"=>$site_config
		);
		
		
		
		$the_message = $this->load->view('staff_form/confirmation_email', $email_data, true);            
		
		//echo $the_message;
		
		$this->email->message(html_entity_decode($the_message));
		
		$this->email->send();

	}
	
	public function get_products_api(){
		
		$this->load->model("products_model");
		$products = $this->products_model->getProductGroupedByCategoriesEdited();
		
		
		echo json_encode($products);	
	}
	
	public function get_customers_api(){
		$this->load->model("customers_model");
		
		$data=array(
			"limit"=>10
		);
		
		switch ($this->input->get("search_type")){
			case "first_name":
				$data['filter_first_name'] = $this->input->get("search_keyword");
			break;
			case "last_name":
				$data['filter_last_name'] = $this->input->get("search_keyword");
			break;
			case "email":
				$data['filter_email'] = $this->input->get("search_keyword");
			break;
			case "mobile":
				$data['filter_mobile'] = $this->input->get("search_keyword");
			break;
		}
		
		$results = $this->customers_model->get_users($data);
		echo json_encode($results);

	}
	
	public function get_customer_api($customer_id){
		$this->load->model("customers_model");	
		$customer_info = $this->customers_model->get_user($customer_id);
		$result=array();
		if($customer_info){
			$result = array(
				"id"=>$customer_info->id,
				"email"=>$customer_info->email,
				"first_name"=>$customer_info->first_name,
				"last_name"=>$customer_info->last_name,
				"company_name"=>$customer_info->company_name,
				"address"=>$customer_info->address,
				"postal_code"=>$customer_info->postal_code,
				"mobile"=>$customer_info->mobile
			);
		}
		if($result){
			echo json_encode($result);	
		}
	}
	
	public function is_email_exists_api(){
		
		$email = $this->input->get("email");
		
		$this->load->model("customers_model");
		$row = $this->customers_model->get_user_by_email_cor($email);
		if($row){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
	}
	
}