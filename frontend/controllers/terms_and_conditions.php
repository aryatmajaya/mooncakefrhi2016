<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms_and_conditions extends CI_Controller {
	function index(){ 
		$this->load->view("header",array(
			'header_title' => "Promotions",
			'menu_active'  => 'promotions'
		));
        $this->load->view('terms_and_conditions');
		$this->load->view('footer');
    }
}