<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends My_Controller {

	function __construct()
	{
		$this->allowed = array(
			'changepassword',
			'forgotpassword',
			'login',
			'register',
			'logout'
		);

		parent::__construct();
	}

	function forgotpassword(){
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->load->model('customers_model');
			$email = $this->input->post('email');

			$user = $this->customers_model->get_user_by_email($email);
			if($user){
				if(Utilities::Send_Password($user->email, $user->password)){
					$this->session->set_flashdata("forgot_password_success", "Please check your email for the password.");
				}
			}else{
				$this->session->set_flashdata("forgot_password_error", "Sorry, your email is not registered.");
			}
		}

		$this->load->view("header",array(
			'header_title'=>'Forgot Password'
		));
		$this->load->view('user/forgotpassword');
		$this->load->view('footer');
	}

	function register(){	
		$this->form_validation->set_rules('register[first_name]', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('register[last_name]', 'Lastname', 'required|xss_clean');
		$this->form_validation->set_rules('register[mobile]', 'Mobile number', 'xss_clean|alpha_dash');
		//$this->form_validation->set_rules('register[alt_contact]', 'Mobile number', 'xss_clean|alpha_dash');
		$this->form_validation->set_rules('register[email]', 'Email', 'required|valid_email|is_unique[mc_customers.email]');
		$this->form_validation->set_rules('register[password]', 'Password', 'required|matches[confirm_password]|min_length[8]');
		//$this->form_validation->set_rules('register[confirm_password]', 'Confirm password', 'required|xss_clean');

		extract($_POST);
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('register_error', validation_errors());
		}
		else
		{	
			$this->load->model('customers_model');
			$password = $register['password'];
			$email = $register['email'];
			if($this->customers_model->add_user($register)){
				unset($register);
				sleep(1);
				if($this->customers_model->check_login($email, $password)){
					redirect($this->input->server('REFERER'));
				}else{
					
					$this->session->set_flashdata('login_error', "Login Error");
				}
			}else{
				$this->session->set_flashdata('register_error', "Problem adding user. Please check your form.");
			}
		}


		if(isset($register)){
			$data['register'] = $register;
		}
		
		
		$this->load->view("header",array(
			'header_title' => 'My Account'
		));
		$this->load->view('login',$data);
		$this->load->view('footer');
		
	}	
	
	function login(){
		if($this->session->userdata('user_id')) redirect($this->input->server('REFERER'));
		$this->form_validation->set_rules('login[username]', 'Login Email', 'valid_email|required|xss_clean');
        $this->form_validation->set_rules('login[password]', 'Password', 'required|xss_clean'); 

		extract($_POST);
		if(isset($request_path) && $request_path == 'cart'){
			//$proceed_to_url = base_url() . 'cart/checkout#fulfillmentsection';
			$this->session->set_userdata('proceed_to_url', site_url('cart/checkout#fulfillmentsection'));
		}

		if($this->input->server("REQUEST_METHOD") == 'POST'){
			if(!$this->form_validation->run()){
				$data = array(
					'username' => isset($login['username']) ? $login['username'] : '',
					'password' => isset($login['password']) ? $login['password'] : '',
				);	
				$this->session->set_flashdata('login_error', validation_errors());
				redirect('user/login');
			}else{
				
				$this->load->model('customers_model');
				$user_id = $this->customers_model->check_login($login['username'], $login['password']);
				if($user_id){
					if($this->session->userdata('proceed_to_url')){
						$proceed_url = $this->session->userdata('proceed_to_url');
						$this->session->set_userdata('proceed_to_url', NULL);
						redirect($proceed_url);
					}
					redirect($this->input->server('REFERER'));
				}
				
				//login failed
				$data = array(
					'username' => $login['username'],
					'password' => $login['password']
				);
					
				$this->session->set_flashdata('login_error', "Incorrect username and/or password.");
				redirect('user/login');
			}
		}
		
		$this->load->view("header",array(
			'header_title' => 'Welcome'
		));
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function logout() {
		$this->session->sess_destroy();
		sleep(1);
		redirect(site_url());
	}
}