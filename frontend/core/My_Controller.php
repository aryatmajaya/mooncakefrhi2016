<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	//This class is being extended by controllers that requires authorization access
	class MY_Controller extends CI_Controller {
		var $allowed = array();

		function __construct()
		{
			parent::__construct();
			
			if(! in_array($this->router->fetch_method(), $this->allowed) ){
				if ( ! $this->session->userdata('user_id'))
				{ 
					$this->session->set_userdata('request_uri', $_SERVER['REQUEST_URI']);
					sleep(1);
					redirect('/user/login');
				}
				
			}
		}
	}