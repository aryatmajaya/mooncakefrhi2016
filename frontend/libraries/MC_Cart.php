<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MC_Cart extends CI_Cart {
	var $card_discount = null;
	var $promo_discount = null;
	var $grand_total = 0;
	public function __construct($params = array()){
		parent::__construct();
	}

	private function compute_net_total(){
		//PROMO DISCOUNT
		$cart_total = $this->_cart_contents['cart_total'];
		$net_total = $this->total();
		$promo_discount = isset($this->_cart_contents['promo_discount']) ? $this->_cart_contents['promo_discount'] : false ; 
		if($promo_discount){
			$net_total = $this->total() - (($promo_discount['percent'] / 100) * $cart_total);	
		}
		
		//CARD DISCOUNT
		$card_discount = isset($this->_cart_contents['card_discount']) ? $this->_cart_contents['card_discount'] : false ;
		if($card_discount){
			$net_total = $net_total - (($card_discount['percent'] / 100) * $net_total);
			
		}

		$this->_cart_contents['cart_net_total'] = $net_total;

		//$this->_save_cart();
	}

	public function set_card_discount($card_name = null, $value = 0){
		if($card_name == null) return false;
		$this->_cart_contents['card_discount'] = array(
			'name' => $card_name,
			'percent'=>$value
		);

		$this->compute_net_total();
	}

	public function set_promo_discount($promo_name = null, $value = 0){
		if($promo_name == null) return false;

		$this->_cart_contents['promo_discount'] = array(
							'name' => $promo_name,
							'percent'=>$value
						);
		$this->compute_net_total();
	}

	function net_total(){
		$this->compute_net_total();
		return $this->_cart_contents['cart_net_total'];
	}

	function _save_cart(){
		//	unset($this->_cart_contents['cart_net_total']);
		parent::_save_cart();
	}

	function contents()
	{
		$cart = $this->_cart_contents;

		// Remove these so they don't create a problem when showing the cart table
		unset($cart['total_items']);
		unset($cart['cart_total']);
		unset($cart['cart_net_total']);
		unset($cart['promo_discount']);
		unset($cart['card_discount']);
		
		return $cart;
	}
}