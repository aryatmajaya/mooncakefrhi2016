<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MC_Discount {
	var $_discounts = array();
	var $CI;
	
	function __construct(){
		// Set the super object to a local variable for use later
		$this->CI =& get_instance();

		// Are any config settings being passed manually?  If so, set them
		$config = array();
		if (count($params) > 0)
		{
			foreach ($params as $key => $val)
			{
				$config[$key] = $val;
			}
		}

		// Load the Sessions class
		$this->CI->load->library('session', $config);

		// Grab the shopping cart array from the session table, if it exists
		if ($this->CI->session->userdata('discounts') !== FALSE)
		{
			$this->_discounts = $this->CI->session->userdata('discounts');
		}

		log_message('debug', "Discount Class Initialized");
	}
}