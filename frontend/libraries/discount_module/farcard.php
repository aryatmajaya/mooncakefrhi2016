<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Farcard{
	
	var $title = "The Far Card";
	var $code = "farcard";
	var $message = "";
	
	public function discountOption(){
		$CI =& get_instance();
		$data = array(
			"title"=>$this->title,
			"code"=>$this->code
		);
		
		$CI->load->model("discount_model");
		$data['default'] = $CI->discount_model->getTheDefaultDiscount();
		
		return $CI->load->view("discount_module/farcard/option.php",$data,true);
	}
	
	public function update_proccess(){
	
		$CI =& get_instance();
		
		$CI->load->library('cart');
		if($CI->cart->total_items() < 2 ){
			$this->message = "Minimum pre-order of 2 boxes required.";
			return false;
		}else{
		
			$CI->load->model("discount/farcard_model");
			$data = $CI->farcard_model->get_discount($CI->input->post("farcard_validation"));
			
			
			$data_session_discount = $CI->session->userdata('session_discount');
			if(!isset($data_session_discount['code'])){
				if($CI->input->post("farcard_validation")==""){
					$this->message = "Code cannot be empty!";
					return false;
				}elseif($data){
					$session_discount = array(
						'type' => $CI->input->post("discount_module"),
						'discount' => $data->discount,
						'code'=>$CI->input->post("farcard_validation")
					);
					$CI->session->set_userdata('session_discount', $session_discount);
					return true;
				}else{
					$this->message = "Code is not valid";
					return false;
				}
			}else{
				return true;	
			}
		}
	}
		
}