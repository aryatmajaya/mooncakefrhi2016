<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class No_discount{
	var $title = "No Discount (Buy 1 Mooncake)";
	var $code = "no_discount";
	var $discount = 0; // in %
	var $message = "";
	
	public function discountOption(){
		$CI =& get_instance();
		$data = array(
			"title"=>$this->title,
			"code"=>$this->code
		);
		
		$CI->load->model("discount_model");
		$data['default'] = $CI->discount_model->getTheDefaultDiscount();
		
		return $CI->load->view("discount_module/no_discount/option.php",$data,true);
	}
	
	public function update_proccess(){
		$CI =& get_instance();
		$CI->load->model("card_discount_model");
		$discount = $this->discount;
		$session_discount = array(
			'type' => $CI->input->post("discount_module"),
			'discount' => $discount
		);
		$CI->session->set_userdata('session_discount', $session_discount);
		return true;
	}
}