<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Other_credit_card{
	var $title = "All other credits cards";
	var $code = "other_credit_card";
	var $discount = 0; // in %
	var $message = "";
	
	public function discountOption(){
		$CI =& get_instance();
		$data = array(
			"title"=>$this->title,
			"code"=>$this->code
		);
		
		$CI->load->model("discount_model");
		$data['default'] = $CI->discount_model->getTheDefaultDiscount();
		
		return $CI->load->view("discount_module/other_credit_card/option.php",$data,true);
	}
	
	public function update_proccess(){
		$CI =& get_instance();
		
		$CI->load->library('cart');
		if($CI->cart->total_items() < 2 ){
			$this->message = "Minimum pre-order of 2 boxes required.";
			return false;
		}else{
			$CI->load->model("card_discount_model");
			$discount = $CI->card_discount_model->get_discount_by_discount_code($this->code);
			
			$session_discount = array(
				'type' => $CI->input->post("discount_module"),
				'discount' => $discount
			);
			$CI->session->set_userdata('session_discount', $session_discount);
			return true;
		}
	}
}