<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
 * Last modified : May 31, 2013
 * By James Earl Castaneros
 */
class Utilities {
    function Apply_Discount_r1(){
        $discount_type = $this->session->userdata('discount_type');
        if ($discount_type == "" || $discount_type == "none" ){
            if ($totalqty >= 50)
                $discount_type = 'bulk discount';
            else
                $discount_type = 'general discount';
        }
        
        
        $this->load->model('orders_model');
        $the_discount = orders_model::Get_Discount($discount_type);        
         
         if ($the_discount != false){
            $this->session->set_userdata('discount_type',$the_discount->promo_name);
            $this->session->set_userdata('discount',$the_discount->discount);
            return $the_discount->discount;
         }
         else{
            $this->session->set_userdata('discount_type','none');
            $this->session->set_userdata('discount',0);
            return FALSE;
         }
             
        
    }
    
    function Apply_Discount($totalqty = NULL){
        
        if ($this->session->userdata('sess_cardtype') == 'uob'){
            $discount_type = "uob discount";
        }else{ //if other cards
            if ($totalqty >= 50){
                $discount_type = 'bulk discount';
            }else{
                $discount_type = 'general discount';
            }
        }
        
        if ($this->session->userdata('discount_type') == 'far member discount'){
            $discount_type = 'far member discount';
        }
        
        
        $this->load->model('orders_model');
        $the_discount = orders_model::Get_Discount($discount_type);   
        
        if ($the_discount != false){
            $this->session->set_userdata('discount_type',$the_discount->promo_name);
            $this->session->set_userdata('discount',$the_discount->discount);
            return $the_discount->discount;
         }
         else{
            $this->session->set_userdata('discount_type','none');
            $this->session->set_userdata('discount',0);
            return FALSE;
         }
    }
    
    function Check_FAR_validity_period(){
                $date_today = date("Y-m-d");
                $sql =  "SELECT id FROM mc_promotion_rules".
                        " WHERE promo_name='far member discount'".
                        " AND '$date_today' BETWEEN date_start AND date_end";
                $query = $this->db->query($sql);

                if($query->num_rows() > 0){
                   return true;               
                }
                else
                    return false;
    }
    
    function Check_UOB_validity_period(){
                $date_today = date("Y-m-d");
                $sql =  "SELECT id FROM mc_promotion_rules".
                        " WHERE promo_name='uob discount'".
                        " AND '$date_today' BETWEEN date_start AND date_end";
                $query = $this->db->query($sql);

                if($query->num_rows() > 0){
                   return true;               
                }
                else
                    return false;
    }

    function BreadCrumbs($the_links){
		echo "<ul class=\"breadcrumb\">\n";
		  foreach ($the_links as $bc){
		    $bc_title = explode("|",$bc);
		    echo "<li class=\"active\">";
		    if ($bc_title[1] != "")
		    	echo "<a href=\"$bc_title[1]\">";
		    echo $bc_title[0];
		    if ($bc_title[1] != "")
		    	echo "</a>";
		    echo "<span class='divider'> /</span></li>\n";
		    
		  }
		echo "</ul>\n";	
		
    }
        
        

    function order_status($array_id = NULL){
            $order_status = array(
                1 => "Open",
                2 => "Collected",
                3 => "Delivered",
                4 => "Cancelled",
                5 => "Credit Card Problem",
                6 => "Incomplete",
                7 => "Error",
                8 => "Paid"
                );
            if ($array_id != "")
                return $order_status[$array_id];
            else
                return $order_status;
		
    }


    function sendXML($xml, $url) {
		
        if(!$ch=curl_init())
        {
                $this->_error="Curl is not initialized.";
                return false;
        }
        else
        {
                $result = "";
                curl_setopt($ch, CURLOPT_URL,$url); 
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=utf-8"));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                if(curl_error($ch) != "")
                {
                        echo "Error with Curl installation: " . curl_error($ch) . "<br>";
                        return false;
                }
                else
                {
                        curl_close($ch);
                        return $result;
                        //echo $result;
                }
        }
    }
    
    function Send_Password($email, $password){
        $decoded_password = $this->encrypt->decode($password);
        //NOW SENDING EMAIL
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['crlf'] = '\r\n';
        $config['newline'] = '\r\n';

        $this->email->initialize($config);

        $this->email->from('admin@rafflesmooncakes.com', 'Raffles Singapore Mooncakes');
        $this->email->to($email);

        $this->email->subject('Reset your Raffles Mooncakes password');

        $the_message = "<html>" .
                "<body>" .
                "<head>" .
                "<p><strong>Dear Customer,</strong></p>" .
                "<p>You requested for your password to be sent to your email.</p>" .
                "<p>Here is your password for use on www.raffles.com/singapore/moon-cakes:</p>" .
                "<p>$decoded_password</p>".
                "<p><br /><br />The Raffles Singapore Team</p>";
                //"<hr><p><i>Please do not reply to this message; this email was sent from an unmonitored email address. This message is a service email related to your use of www.raffles.com/singapore/moon-cakes. For general inquiries or to request support with your account, please email us at <a href='mailto:raffleshotel.mooncakes@raffles.com'>raffleshotel.mooncakes@raffles.com</a>.</i></p>";

        $the_message .= "</head>" .
                "</body>" .
                "</html>";
        $this->email->message($the_message);
        //$this->email->send();
        
        $subject = 'Your Raffles Mooncakes password';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers
        $headers .= 'From: Raffles Singapore Mooncakes <admin@rafflesmooncakes.com>' . "\r\n";
        $headers .= 'Reply-To: admin@rafflesmooncakes.com' . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";

        mail($email, $subject, $the_message, $headers);
    }
    
    function Send_Confirmation($order_ref){
        //GETTING ORDER DATA
        $this->load->model('orders_model');        
            
        $data['order_ref'] = $order_ref;
        $data['order'] = Orders_model::Order_Summary($order_ref);
        
        
        if ($data['order']){
            $data['items'] = Orders_model::Get_Ordered_Items($data['order']->id);
            
            $this->load->model('delivery_model');
            $data['delivery_charges'] = delivery_model::Get_Total_Delivery_Qty($order_ref);
            $data['fo'] = delivery_model::Get_Delivery_Option($order_ref);

            //NOW SENDING EMAIL
            $this->load->library('email');
            $config['mailtype'] = 'html';
            //$config['charset'] = 'iso-8859-1';
            $config['charset'] = 'utf-8';
            $config['crlf'] = '\r\n';
            $config['newline'] = '\r\n';

            $this->email->initialize($config);

            $this->email->from('admin@rafflesmooncakes.com', 'Raffles Singapore Mooncakes');
            $this->email->to($data['order']->email);

            $this->email->subject('Order Confirmation #' . $order_ref . ' - Raffles Singapore Mooncakes');

            $the_message = $this->load->view('front/confirmation_email', $data, true);

            //echo $the_message;
            $this->email->message(html_entity_decode($the_message));

            if (ENVIRONMENT != 'development'){
                //$this->email->send();
                $subject = 'Order Confirmation #' . $order_ref . ' - Raffles Singapore Mooncakes';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                // Additional headers
                $headers .= 'From: Raffles Singapore Mooncakes <admin@rafflesmooncakes.com>' . "\r\n";
                $headers .= 'Reply-To: admin@rafflesmooncakes.com' . "\r\n";
                $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
            
                mail($data['order']->email, $subject, $the_message, $headers);
                mail("raffleshotelmooncake@raffles.com", $subject, $the_message, $headers);

            }
        }

        
    }
    
    function Calculate_Delivery_Charges($delivery_items, $total_qty){
        if ($delivery_items){
            $dctr = 1;
            $total_delivery_charge = 0;
            foreach($delivery_items as $delivery_charges){
                if ($total_qty < 50){
                    $total_delivery_charge = $total_delivery_charge + DELIVERY_CHARGE;
                }
                elseif ($total_qty >= 50  && $dctr == 2){
                    //echo 'test';
                    $total_delivery_charge = DELIVERY_CHARGE;
                }
                $dctr++;
            }
            return $total_delivery_charge;
        }
    }
        
   

	
	
}

/* End of file Utilities.php */