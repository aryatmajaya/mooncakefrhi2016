<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Delivery_charge {
	public function getTotal(&$total_data, &$total){
		$ci =& get_instance();
		
		$ci->config->load('site');
		$site_config = $ci->config->item('site');
		$displayFullfil = $ci->session->userdata('display_fulfillments');
		if($displayFullfil != NULL && $displayFullfil[0]['collection_type'] == 'delivery'){
			
			$delivery_charge = ($ci->cart->total_items() < $site_config['minimum_delivery_quantity']) ? $site_config['delivery_charge_lower'] : $site_config['delivery_charge_upper'];    
			
			$total += $delivery_charge;
			
			$total_data[] = array(
				"title" => "Delivery charge",
				"text" => "SGD ".number_format($delivery_charge,2),
				"class" => array()
			);
		}
	}
}