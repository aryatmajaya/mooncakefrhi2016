<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Discount {
	public function getTotal(&$total_data, &$total){
		$ci =& get_instance();
		
		
		if($ci->session->userdata("session_discount")){
			$session_discount = $ci->session->userdata("session_discount");
			$discount_persen = $session_discount['discount'];
			

			$total_cart = $ci->cart->total();
			$total_discount = ($total_cart * $discount_persen)/100;
			
			$total -= $total_discount;
			
			$total_data[] = array(
				"title" => "Discount (".$discount_persen."%)",
				"text" => "SGD -".number_format($total_discount,2),
				"class" => array("text-danger")
			);
		
		}
	}
}