<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Discount_number_item {
	public function getTotal(&$total_data, &$total){
		$ci =& get_instance();
		
		$total_item = $ci->cart->total_items();
		$total_cart = $ci->cart->total();
		
		$ci->load->model("total/model_discount_number_item","model_discount_number_item");
		$item = $ci->model_discount_number_item->getItem();
		
		if($item){
			
			$discount_amount = (($total_cart*$item->discount)/100);
			
			$total -= $discount_amount;
			
			$total_data[] = array(
				"title" => "Quantity Discount (".$item->discount."%)",
				"text" => "SGD -".number_format($discount_amount,2),
				"class" => array("text-danger")
			);	
		}
		
		
	}
}