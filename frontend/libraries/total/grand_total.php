<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Grand_total {
	public function getTotal(&$total_data, &$total){
		$ci =& get_instance();
		
		$total_data[] = array(
			"title" => "Grand total",
			"text" => "SGD ".number_format($total,2),
			"class" => array()
		);
	}
}