<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Gst_charge {
	public function getTotal(&$total_data, &$total){
		$ci =& get_instance();
		
		
		$gst_charge = ($total) * (GST_PERCENT/100);
		$total += $gst_charge;
		$total_data[] = array(
			"title" => "GST Charge",
			"text" => "SGD ".number_format($gst_charge,2),
			"class" => array()
		);
	}
}