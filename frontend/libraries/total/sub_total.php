<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class sub_total {
	public function getTotal(&$total_data, &$total){
		$ci =& get_instance();
		
		$total += $ci->cart->total();
		$total_data[] = array(
			"title" => "Sub total",
			"text" => "SGD ".number_format($total,2),
			"class"=>array()
		);
		
		
	}
}