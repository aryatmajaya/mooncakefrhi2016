<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
 * Last modified : May 31, 2013
 * By James Earl Castaneros
 */
class Utilities {
    function Apply_Discount_r1(){
        $discount_type = $this->session->userdata('discount_type');
        if ($discount_type == "" || $discount_type == "none" ){
            if ($totalqty >= 50)
                $discount_type = 'bulk discount';
            else
                $discount_type = 'general discount';
        }
        
        
        $this->load->model('orders_model');
        $the_discount = orders_model::Get_Discount($discount_type);        
         
         if ($the_discount != false){
            $this->session->set_userdata('discount_type',$the_discount->promo_name);
            $this->session->set_userdata('discount',$the_discount->discount);
            return $the_discount->discount;
         }
         else{
            $this->session->set_userdata('discount_type','none');
            $this->session->set_userdata('discount',0);
            return FALSE;
         }
    }
    
    function Apply_Discount($totalqty = NULL){
        
        if ($this->session->userdata('sess_cardtype') == 'uob'){
            $discount_type = "uob discount";
        }elseif ($this->session->userdata('sess_cardtype') == 'citibank'){
            $discount_type = "citibank discount";
        }else{ //if other cards
            if ($totalqty >= 50){
                $discount_type = 'bulk discount';
            }else{
                $discount_type = 'general discount';
            }
        }
        
        if ($this->session->userdata('discount_type') == 'far member discount'){
            $discount_type = 'far member discount';
        }
        
        
        $this->load->model('orders_model');
        $the_discount = orders_model::Get_Discount($discount_type);   
        
        if ($the_discount != false){
            $this->session->set_userdata('discount_type',$the_discount->promo_name);
            $this->session->set_userdata('discount',$the_discount->discount);
            return $the_discount->discount;
         }
         else{
            $this->session->set_userdata('discount_type','none');
            $this->session->set_userdata('discount',0);
            return FALSE;
         }
    }
    
    function Check_FAR_validity_period(){
                $date_today = date("Y-m-d");
                $sql =  "SELECT id FROM mc_promotion_rules".
                        " WHERE promo_name='far member discount'".
                        " AND '$date_today' BETWEEN date_start AND date_end";
                $query = $this->db->query($sql);

                if($query->num_rows() > 0){
                   return true;               
                }
                else
                    return false;
    }
    
    function Check_UOB_validity_period(){
                $date_today = date("Y-m-d");
                $sql =  "SELECT id FROM mc_promotion_rules".
                        " WHERE promo_name='uob discount'".
                        " AND '$date_today' BETWEEN date_start AND date_end";
                $query = $this->db->query($sql);

                if($query->num_rows() > 0){
                   return true;               
                }
                else
                    return false;
    }
    
    function Check_Citibank_validity_period(){
                $date_today = date("Y-m-d");
                $sql =  "SELECT id FROM mc_promotion_rules".
                        " WHERE promo_name='citibank discount'".
                        " AND '$date_today' BETWEEN date_start AND date_end";
                $query = $this->db->query($sql);

                if($query->num_rows() > 0){
                   return true;               
                }
                else
                    return false;
    }


    function BreadCrumbs($the_links){
		echo "<ul class=\"breadcrumb\">\n";
		  foreach ($the_links as $bc){
		    $bc_title = explode("|",$bc);
		    echo "<li class=\"active\">";
		    if ($bc_title[1] != "")
		    	echo "<a href=\"$bc_title[1]\">";
		    echo $bc_title[0];
		    if ($bc_title[1] != "")
		    	echo "</a>";
		    echo "<span class='divider'> /</span></li>\n";
		    
		  }
		echo "</ul>\n";	
		
    }
        
        

    function order_status($array_id = NULL){
            $order_status = array(
                1 => "Open",
                2 => "Collected",
                3 => "Delivered",
                4 => "Cancelled",
                5 => "Credit Card Problem",
                6 => "Incomplete",
                7 => "Error",
                8 => "Paid"
                );
            if ($array_id != "")
                return $order_status[$array_id];
            else
                return $order_status;
		
    }


    function sendXML($xml, $url) {
		
        if(!$ch=curl_init())
        {
                $this->_error="Curl is not initialized.";
                return false;
        }
        else
        {
                $result = "";
                curl_setopt($ch, CURLOPT_URL,$url); 
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=utf-8"));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                if(curl_error($ch) != "")
                {
                        echo "Error with Curl installation: " . curl_error($ch) . "<br>";
                        return false;
                }
                else
                {
                        curl_close($ch);
                        return $result;
                        //echo $result;
                }
        }
    }
    
    function Send_Password($email, $password){
        $decoded_password = $this->encrypt->decode($password);
        //NOW SENDING EMAIL
        $the_message = "<html>" .
                "<body>" .
                "<head>" .
                "<p><strong>Dear Guest,</strong></p>" .
                "<p>Greetings from Fairmont Singapore & Swiss&ocirc;tel The Stamford!</p>" .
                "<p>We've received a password request to be sent to your email. Please return to our website, <a href=\"http://www.celebrationscentral.com.sg\">www.celebrationscentral.com.sg</a>, and log in using the following information. Here's your password: ". $decoded_password ."</p>" .
                "<p>Warm Regards,<br />Fairmont Singapore & Swiss&ocirc;tel The Stamford</p>" .
                "</head>" .
                "</body>" .
                "</html>";
        //$this->email->message($the_message);

        $subject = 'Password Request for Celebrations Central';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // Additional headers
        $headers .= 'From: Fairmont Singapore & Swiss�tel The Stamford <'.ADMIN_EMAIL.'>' . "\r\n";
        $headers .= 'Reply-To: '. ADMIN_EMAIL . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";

        mail($email, $subject, $the_message, $headers);
    }
    
    function Send_Confirmation($order_ref, $priority_email = NULL, $method = 'email'){
        /*
         * Note: If $priority_email is not null the the confirmation will be sent
         * to the $priority_email and not to the customer. (This is usually use by CMS admin)
         */
        //GETTING ORDER DATA
        $this->load->model('orders_model');
            
        $data['order_ref'] = $order_ref;
        $data['order'] = $this->orders_model->order_summary($order_ref);
        
        if ($data['order']){
            $data['items'] = $this->orders_model->get_ordered_items($data['order']->id);
            
            $this->load->model('delivery_model');
            $delivery_charges = $this->delivery_model->get_total_delivery_qty($order_ref);
			
			//$data['delivery_qty'] = $delivery_charges[0]->dctotal_qty;
			$data['delivery_qty'] = 0;

            $data['fo'] = $this->delivery_model->get_delivery_option3($order_ref);

            //NOW SENDING EMAIL
            $this->load->library('email');
            $config['mailtype'] = 'html';
            //$config['charset'] = 'iso-8859-1';
            $config['charset'] = 'utf-8';
            $config['crlf'] = '\r\n';
            $config['newline'] = '\r\n';
			
            $this->email->initialize($config);

            $this->email->from(ADMIN_EMAIL, 'Exquisite Mooncake Selection from Szechuan Court');
            $this->email->to($data['order']->email);

            $this->email->subject('Order Confirmation #' . $order_ref . ' - Mooncakes from Fairmont Singapore & Swissotel The Stamford');

            $the_message = $this->load->view('order/confirmation_email', $data, true);
            
            $this->email->message(html_entity_decode($the_message));
            
            /*$subject = 'Order Confirmation #' . $order_ref . ' - Exquisite Mooncake Selection from Szechuan Court';
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

            // Additional headers
            $headers .= 'From: Exquisite Mooncake Selection from Szechuan Court <'.ADMIN_EMAIL.'>' . "\r\n";
            $headers .= 'Reply-To: ' . ADMIN_EMAIL . "\r\n";
            $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";*/
			
            if (ENVIRONMENT != 'development'){
				
                $subject = 'Order Confirmation #' . $order_ref . ' - Mooncakes from Fairmont Singapore & Swissotel The Stamford';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                // Additional headers
                $headers .= 'From: Exquisite Mooncake Selection from Szechuan Court <'.ADMIN_EMAIL.'>' . "\r\n";
                $headers .= 'Reply-To: ' . ADMIN_EMAIL . "\r\n";
                $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
                $headers .= "Bcc: aryatmajaya@gmail.com, marcus@cprvision.com, fairmontsingapore.mooncakes@fairmont.com" . "\r\n";
            
                if (!empty($priority_email)) {
                	mail($priority_email, $subject, $the_message, $headers);
                } else {
                    
                    if ($method == 'email') {
                    
                        if(mail($data['order']->email, $subject, $the_message, $headers)){
							$this->orders_model->setNotificationSent($order_ref);	
						}
                        //LOGGING THE MAIL NOTIFICATION
                        $this->load->model('logs_model');
                        logs_model::mail_notification_log($order_ref, $data['order']->email);

                        return TRUE;
                    } else {
                        $ndata['email'] = $data['order']->email;
                        $ndata['header'] = $headers;
                        $ndata['subject'] = $subject;
                        $ndata['message'] = $the_message;
                        return $ndata;
                    }
                }
            }else{ // Production
                $this->email->send();
                //mail($data['order']->email, $subject, $the_message, $headers);
            }
			
			echo $this->email->print_debugger();
			
        }
    }
    
    
    
    /*function Calculate_Delivery_Charges($delivery_items, $total_qty){
        if ($delivery_items){
            $dctr = 1;
            $total_delivery_charge = 0;
            foreach($delivery_items as $delivery_charges){
                if ($total_qty < 50){
                    $total_delivery_charge = $total_delivery_charge + DELIVERY_CHARGE;
                }
                elseif ($total_qty >= 50  && $dctr == 2){
                    //echo 'test';
                    $total_delivery_charge = DELIVERY_CHARGE;
                }
                $dctr++;
            }
            return $total_delivery_charge;
        }
    }*/
	
	function Calculate_Delivery_Charges($cart_total, $service_type) {
        $site_config = get_instance()->config->item('site');
        $charges = 0;
        
        if($service_type=="delivery"){
			if ($cart_total < $site_config['minimum_delivery_quantity']) {
				$charges = $site_config['delivery_charge_lower'];
			} else {
				$charges = $site_config['delivery_charge_upper'];
			}
			return $charges;
		}else{
			return 0;	
		}
    }
        
   

	
	
}

/* End of file Utilities.php */