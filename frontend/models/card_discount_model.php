<?php

class Card_discount_model extends CI_Model {
	/**
	 * TODO: Depricate
	 */
	/*function get_discount($tag){

		$sql = "SELECT * FROM mc_ccard_promo WHERE bank_tag=? AND status=1 AND expired=0 AND (NOW() BETWEEN date_start AND date_end)";
		$results = $this->db->query($sql, array($tag));

		if($results->num_rows() > 0){
			return $results->row(0);
		}

		return false;
	}*/

	function get_discount_by_discount_code($discount_code){
		$this->db->where("discount_code", $discount_code);
		$this->db->where("status",1);
		$this->db->where("expired",0);
		$this->db->where("NOW() BETWEEN start_date AND end_date");
		
		$query = $this->db->get("mc_ccard_promo");
		$data = $query->row();
		if($data){
			return $data->discount;
		}else{
			return 0;	
		}
	}
	
	function get_card_promotions(){
		$sql = "SELECT a.name, a.mid, b.discount 
				FROM mc_credit_cards a 
				LEFT JOIN mc_ccard_promo b ON b.credit_card_id = a.id
				WHERE a.active=? AND b.expired=? 
				AND (NOW() BETWEEN b.date_start AND b.date_end)";

		$results = $this->db->query($sql, array(1, 0));

		if($results->num_rows() > 0){
			return $results->result();
		}

		return false;
	}

}