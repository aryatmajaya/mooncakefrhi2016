<?php
class Common extends CI_Model {
	
	
	
	function insert($table_name, $insert_data)
	{
		$this->db->set('date_created', 'NOW()',false);
		//$this->db->set('modified_by', '0',false);
		$this->db->set($insert_data);
 
		$this->db->insert($table_name);
	}
	
	function update($table_name, $field_name, $field_id, $update_data)
	{
		$this->db->where($field_name, $field_id);
		$this->db->update($table_name, $update_data);
	}

	function delete($table_name, $delete_data)
	{
		
		$this->db->delete($table_name, $delete_data);
	}

	function dynamic_query($sql_statement){
		$query = $this->db->query($sql_statement);
		if($query->num_rows() > 0){
			   return $query->result(); 
		}
		else
			return false;
	}

	function get_record_by_id($table_name, $the_field_id, $the_id){
		//if (is_numeric($the_id)){

			$sql = 	"SELECT * FROM $table_name".
					" WHERE $the_field_id=?".
					" AND $table_name.deleted = 0";
			$query = $this->db->query($sql, array($the_id));

			if($query->num_rows() > 0){
			   return $query->row(); 
			}
			else
				return false;
		//}
		//else
		//	return false;
	}

	function get_all_records($table_name)
	{
		$sql = "SELECT * from $table_name ORDER BY DESC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}

	function get_all_records2($table_name, $sort_by, $sort)
	{
		/* controllers using:
		clients
		*/
		$sql = "SELECT * FROM $table_name WHERE deleted = 0 ORDER BY ? ?";
		$query = $this->db->query($sql, array($table_name,$sort_by,$sort));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}
	}
        
    function get_all_records3($table_name, $sort_by, $sort)	{
        $sql = "SELECT * FROM $table_name WHERE status = 1 ORDER BY ? ?";
        $query = $this->db->query($sql, array($table_name,$sort_by,$sort));

        if($query->num_rows() > 0){
           return $query->result(); 
        }
    }
    
    function get_a_record($table_name, $field_name, $field_value, $sort_by, $sort) {
        $this->db->select('*')
                ->from($table_name)
                ->where($field_name, $field_value)
                ->order_by($sort_by, $sort);
        
        $query = $this->db->get();
       
        if($query->num_rows() > 0){
           return $query->row(); 
        } else {
            return FALSE;
        }
            
    }

	function get_table_list($table_name, $sort_by, $sort){

		$sql = "SELECT * FROM $table_name WHERE status = 1 AND deleted = 0 ORDER BY $sort_by $sort";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
		   return $query->result(); 
		}

	}

	function get_app_property_permissions($property_id,$user_id){
		//This is to get the app permission of a specific app_id and group_id
		$sql = 	"SELECT *".
				" FROM user_property_permissions".
				" WHERE user_property_permissions.property_id = ?".
				" AND user_property_permissions.user_id = ?";
				
		$query = $this->db->query($sql, array($property_id,$user_id));

		if($query->num_rows() > 0){
		   return $query->row(); 
		}
	}

	function Allowed_Apps_Query($user_group_id){
		//This query is to fetch the permitted applications based on the user_group_id
		$sql = 	"SELECT user_group_app_permissions.id,
						user_group_app_permissions.group_id,
						user_group_app_permissions.app_id,
						applications.id,applications.app_name,
						applications.app_group,
						applications.app_url,
                                                applications.new_window,
                                                application_groups.id,
						application_groups.group_name".
				" FROM user_group_app_permissions".
				" LEFT JOIN applications ON user_group_app_permissions.app_id = applications.id".
				" LEFT JOIN application_groups ON applications.app_group = application_groups.id".
				" WHERE user_group_app_permissions.group_id = ?".
				" AND applications.status = 1".
				" AND applications.deleted = 0".
				" ORDER BY application_groups.position,applications.position ASC";

		$query = $this->db->query($sql, array($user_group_id));

		if($query->num_rows() > 0){
		   return $query->result(); 
		}		
	}
        
        /* Computation of Total Amount */
               
        
        function Compute_Total_Amount($order_ref, $service_type, $custom_delivery_charge = NULL, $ordering_method = 'online')
        {
            //1st - compute total products
            $this->db->select('SUM(oi.qty*oi.price) as total, SUM(oi.qty) as total_qty')
                    ->from('mc_orders o')
                    ->join('mc_orders_items oi','oi.order_id = o.id', 'left')
                    ->where('o.order_ref', $order_ref)
                    ;
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $total = $query->row(); 
            } else {
                return FALSE;
            }
            
            //Get Delivery Charge
            
            if ($ordering_method == 'corporate'):
                $delivery_charge = $custom_delivery_charge;
            else:
            
                if ($service_type == "self_collection"):
                    $delivery_charge = 0;
                else:
                    if ($custom_delivery_charge > 0):
                        $delivery_charge = $custom_delivery_charge;
                    else:
                        if ($total->total_qty < 50)
                            $delivery_charge = 50;
                        else
                            $delivery_charge = 0;
                    endif;
                endif;
            endif;
                
            
            //2nd - Get Order Discount
            $this->db->select('card_discount, corporate_discount')
                    ->from('mc_orders o')
                    ->where('o.order_ref', $order_ref)
                    ;
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $discount = $query->row(); 
            } else {
                return FALSE;
            }
            
            
            
            
            $total_amount = $total->total;
            $discount_amount = $total->total * (($discount->card_discount + $discount->corporate_discount)/100);
            $gst_amount = (($total_amount - $discount_amount) + $delivery_charge) * (GST_PERCENT/100);
            
            $grand_total = (($total_amount - $discount_amount) + $delivery_charge) + $gst_amount;
            return $grand_total;
            
        }
        
        function Instant_Compute_Total($subtotal_amount, $discount_amount, $delivery_charge) {
            $subtotal_amount = ($subtotal_amount - $discount_amount) + $delivery_charge;
            $gst_amount = $subtotal_amount * (GST_PERCENT/100);
            $total_amount = $subtotal_amount + $gst_amount;
            return $total_amount;
        }

	function mail_notification_log($order_ref = NULL, $email = NULL)
        {
            if($order_ref){
                $this->db->set('order_ref', $order_ref);
                $this->db->set('email', $email);
                $this->db->set('referral', $_SERVER['HTTP_REFERER']);
                $this->db->insert('mc_mail_notification_logs');
            }
        }
	
		
}
?>