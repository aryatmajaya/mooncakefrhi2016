<?php

class Creditcards_model extends CI_Model {
	private $table = 'mc_credit_cards';
	private $table_promocode = "mc_far_promocodes";
	private $table_discount = "mc_ccard_promo";

	function get_all(){
		$sub_sql = "(SELECT COUNT(b.id) FROM ".$this->table_promocode." b WHERE b.credit_card_id = a.id)";
		$sql = "SELECT a.*, $sub_sql as has_promo FROM ".$this->table." a 
				WHERE a.active=1 ORDER BY sort_position ASC";

		$results = $this->db->query($sql);

		if($results->num_rows() > 0){
			return $results->result();
		}

		return false;
	}

	function get($id){
		$sql = "SELECT * FROM ".$this->table."  
				WHERE id =? AND
				active=1";

		$results = $this->db->query($sql, array($id));

		if($results->num_rows() > 0){
			return $results->row(0);
		}

		return false;
	}

	function get_card_discount($id){
		$sql = "SELECT a.name, a.mid, a.id, b.discount, a.issued_by
				FROM mc_credit_cards a 
				LEFT JOIN " . $this->table_discount . " b ON b.credit_card_id = a.id
				WHERE a.active=? AND b.expired=? 
				AND a.id=?
				AND (NOW() BETWEEN b.start_date AND b.end_date)";

		$results = $this->db->query($sql, array(1, 0, $id));

		if($results->num_rows() > 0){
			return $results->row(0);
		}

		return false;
	}


	function validate_promocode($card_id, $promocode){
		$sql = "SELECT a.id, a.promocode, a.discount FROM " . $this->table_promocode ." a
				WHERE a.credit_card_id = ? 
				AND a.promocode = ?
				AND (NOW() BETWEEN a.start_date AND a.end_date)";

		$results = $this->db->query($sql, array($card_id, $promocode));

		if($results->num_rows() > 0){
			return $results->row(0);
		}

		return false;
	}	

	function get_discount($card_id){

		$sql = "SELECT * FROM ".$this->table_discount." 
				WHERE credit_card_id = ? 
				AND status=1 AND expired=0 
				AND (NOW() BETWEEN start_date AND end_date)";

		$results = $this->db->query($sql, array($card_id));

		if($results->num_rows() > 0){
			return $results->row(0);
		}

		return false;
	}

}