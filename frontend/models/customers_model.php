<?php
class Customers_model extends CI_Model {
	
	
	function check_login($username, $password)
	{
		$this->load->library('encrypt');
		//$secured_password = sha1($password);
		$secured_password = $this->encrypt->encode($password);
		
		$sql = 	"SELECT *".
				" FROM mc_customers".
				" WHERE email = ?".
				" AND deleted = 0 AND status = 1 AND corporate = 0";
		$result = $this->db->query($sql, array($username));
		
		if ($result->num_rows() == 1)
		{
			if ($this->encrypt->decode($secured_password) == $this->encrypt->decode($result->row(0)->password)) {
				if ($result->row(0)->status == 1){
					$userdata = array(
						'user_id' => $result->row(0)->id,
						'email' => $result->row(0)->email,
						"mobile" => $result->row(0)->mobile,
						'name' => $result->row(0)->first_name . ' ' . $result->row(0)->last_name,
						'first_name' => $result->row(0)->first_name,
						'last_name' => $result->row(0)->last_name,
						'last_login' => $result->row(0)->last_login
					);
					
					$this->session->set_userdata($userdata);				
					
					$this->db->set('last_login', 'NOW()', FALSE);				
					$this->db->where('id',  $result->row(0)->id);
					$this->db->update('users');

					return $result->row(0)->id;
				}
				else{
					return 'inactive';
				}
			} else {
				return false;
			}		
		} else {
			return false;
		}
	}

	function add_user($insert_data){
		$this->load->library('encrypt');
		
		$insert_data['password'] = $this->encrypt->encode($insert_data['password']);
		$this->db->set('date_created', 'NOW()', false);
		$this->db->set($insert_data);
 
		return $this->db->insert('mc_customers');
	}

	function get_user($user_id){
		$result = $this->db->query("SELECT * FROM mc_customers WHERE id=?", array($user_id));
		if($result->num_rows() > 0){
			return $result->row(0);
		}
		return false;
	}

	function get_user_by_email($email){
		$result = $this->db->query("SELECT * FROM mc_customers WHERE corporate=0 AND email=?", array($email));
		if($result->num_rows() > 0){
			return $result->row(0);
		}
		return false;
	}

	function update($data, $id){
		$this->db->where('id', $id);
		$this->db->update('mc_customers', $data);
	}

	function get_users($data=array()){
		
		if(isset($data['filter_first_name'])){
			$this->db->where("first_name like '%".$data['filter_first_name']."%'");
		}
		
		if(isset($data['filter_last_name'])){
			$this->db->where("last_name like '%".$data['filter_last_name']."%'");
		}
		
		if(isset($data['filter_email'])){
			$this->db->where("email like '%".$data['filter_email']."%'");
		}
		
		if(isset($data['filter_mobile'])){
			$this->db->where("mobile like '%".$data['filter_mobile']."%'");
		}
		
		$this->db->where("status","1");
		
		if(isset($data['limit'])){
			$this->db->limit($data['limit']);
		}
		
		$query = $this->db->get("mc_customers");
		return $query->result();
		
	}

	function add_user_corporate($data=array()){
		$now = getdate();

		$data['password'] = "*corporate-order-only*";
		$data['date_created'] = date("Y-m-d H:i:s",$now[0]);
		$data['date_modified'] = date("Y-m-d H:i:s",$now[0]);
	}

}
?>