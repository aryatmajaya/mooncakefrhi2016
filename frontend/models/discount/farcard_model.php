<?php

class Farcard_model extends CI_Model {
	function get_discount($code){
		$this->db->where("promocode", $code);
		$this->db->where("status",1);
		$this->db->where("expired",0);
		$this->db->where("NOW() BETWEEN start_date AND end_date");
		
		$results = $this->db->get("mc_far_promocodes");

		if($results->num_rows() > 0){
			return $results->row(0);
		}

		return false;
	}
}