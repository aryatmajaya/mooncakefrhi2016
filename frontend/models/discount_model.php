<?php

class Discount_model extends CI_Model {
	function get_all(){
		$this->db->order_by("sort_order","asc");
		$query = $this->db->get_where("discount_module",array("status"=>1));
		return $query->result();
	}
	
	function getByClass($code){
		$query = $this->db->get_where("discount_module",array("status"=>1,"class"=>$code));
		return $query->row();
	}
	
	function getTheDefaultDiscount(){
		$query = $this->db->get_where("discount_module",array("default"=>1));
		$data = $query->row();
		if($data){
			return $data;
		}else{
			return false;	
		}
	}
}