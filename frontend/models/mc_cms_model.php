<?php
class Mc_cms_model extends CI_Model {
	static $table = "mc_cms";
	static $category_table = 'mc_cms_category';

	function get_all(){
		$sql = "SELECT a.* FROM " . self::$table . " a 
				WHERE a.active=1 AND a.published=1";

		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0){
			return $query->result();
		}

		return false;
	}

	function get($id){
		$sql = "SELECT a.* FROM " . self::$table . " a 
				
				WHERE a.id = ? ";

		$query = $this->db->query($sql, array($id));
		
		if($query->num_rows() > 0){
			return $query->row(0);
		}

		return false;
	}

	function get_by_slug($slug){
		$sql = "SELECT a.* FROM " . self::$table . " a 
				
				WHERE a.slug = ? ";

		$query = $this->db->query($sql, array($slug));
		
		if($query->num_rows() > 0){
			return $query->row(0);
		}

		return false;
	}

	function get_categories(){
		$sql = "SELECT a.* FROM " . self::$category_table . " a 
				WHERE a.active=1";

		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0){
			return $query->result();
		}

		return false;
	}

}