<?php
class Orders_model extends CI_Model {
		function __construct(){
			parent::__construct();

			$this->load->library('cart');
		}
	
		function add_order($insert_data){
			$order_id = $this->get_order_id($insert_data['order_ref'], $this->session->userdata('user_id'));
			if(!$order_id){
				$this->db->set('date_ordered', 'NOW()', FALSE);            
				$this->db->set($insert_data);
				$this->db->insert('mc_orders');
				$order_id = $this->db->insert_id();
			}
			
			$this->save_order_items($order_id);
		}
		
		function add_order_corporate($insert_data){
			$order_id = $this->get_order_id($insert_data['order_ref'], $insert_data['customer_id']);
			if(!$order_id){
				$this->db->set('date_ordered', 'NOW()', FALSE);            
				$this->db->set($insert_data);
				$this->db->insert('mc_orders');
				$order_id = $this->db->insert_id();
			}
			
			$this->save_order_items($order_id);
		}
		
		function add_order_item_R1($insert_item_data){
			$this->db->set('date_created', 'NOW()', FALSE);            
			$this->db->set($insert_item_data);
			$this->db->insert('mc_orders_items');
		}

		
		function update_order($field_name, $field_id, $update_data){
			$this->db->where($field_name, $field_id);
			$this->db->update('mc_orders', $update_data);
		}

		function get_order_id($order_ref, $customer_id){            
				$date_today = date("Y-m-d");
				$sql =  "SELECT id FROM mc_orders".
						" WHERE order_ref='$order_ref'".
						" AND customer_id=$customer_id";
				$query = $this->db->query($sql);

				if($query->num_rows() > 0){
				   return $query->row();               
				}
				else
					return false;
		}
		
		function delete_ordered_items($order_id){
			$this->db->where('order_id', $order_id);
			$this->db->delete('mc_orders_items');
		}
		
		function get_order_details($order_ref){
			$this->db->select('*');
			$this->db->from('mc_orders');
			$this->db->where('order_ref', $order_ref);
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				return $query->row();               
			}
			else
				return false;  
			 
		}
		
		function setNotificationSent($order_ref){
			$this->db->where('order_ref', $order_ref);
			$this->db->update('mc_orders', array(
				'notification_sent' => 1
			)); 
		}
		
		function order_summary($order_ref){
			$this->db->select('
				o.id, 
				o.date_ordered, 
				o.order_ref, 
				o.approval_code, 
				o.promo_code_discount,
				o.card_discount, 
				o.corporate_discount,
				o.ordering_method,
				c.first_name,
				c.last_name,
				c.mobile, 
				c.alt_contact,
				c.email,
				o.discounted_amount, 
				o.delivery_charge
			');
			
			$this->db->select('IFNULL(oc.special_requirements, "") as special_requirements', false);
			
			$this->db->from('mc_orders o');
			$this->db->from('mc_orders_corporate as oc', 'oc.order_id = o.id', 'left');
			$this->db->join('mc_customers c','c.id = o.customer_id', 'left');
			
			$this->db->where('o.order_ref', $order_ref);
			
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				return $query->row();               
			 }
			 else
				 return false;            
		}
		
		function get_order_corporate($order_id){
			$query = $this->db->get_where("mc_orders_corporate", array(
				"order_id" => $order_id
			));
			
			return $query->row();
		}
		
	function get_ordered_items($order_id){
		$this->db->select('*');
		$this->db->from('mc_orders_items');
		$this->db->where('order_id', $order_id);
		$this->db->group_by('product_id');

		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();               
		 }
		 else
			 return false;            
	}
	
	function get_last_id(){
		$this->db->select_max('id');
		$query = $this->db->get('mc_orders');
		if($query->num_rows() > 0){
			return $query->row()->id;               
		 }
		 else
			 return false;
	}
	
	function check_ordered_item_exists($order_id, $product_id) {
		$this->db->select('id')
				->from('mc_orders_items')
				->where('order_id', $order_id)
				->where('product_id', $product_id);
		$chk_query = $this->db->get();
		if($chk_query->num_rows() > 0){
			return $chk_query->row()->id;               
		} else
			return FALSE;
	}

	function save_order_items($order_id){
		$cart_contents = $this->cart->contents();
		foreach ($cart_contents as $key => $value) {
			$insert_data = array(
				'order_id'=>$order_id,
				'product_name'=>$value['name'],
				'product_id'=>$value['id'],
				'qty'=>$value['qty'],
				'price'=>$value['price']
			);

			$this->add_order_item($order_id, $value['id'], $insert_data);
		}
	}

	function add_order_item($order_id, $product_id, $insert_item_data){
		   
		$chkorder = $this->check_ordered_item_exists($order_id, $product_id);
		//echo $chkorder;
		if ($chkorder) {
			$this->db->where('id', $chkorder);
			$this->db->update('mc_orders_items', $insert_item_data);
		} else {

			$this->db->set('date_created', 'NOW()', FALSE);            
			$this->db->set($insert_item_data);
			$this->db->insert('mc_orders_items');
		}         
		
	}
        
        function log_transaction($insert_data){
            /* THIS IS to check if we are passing valid data to telemoney */
            $this->db->set('date_created', 'NOW()', FALSE);            
            $this->db->set($insert_data);
            $this->db->insert('transaction_logs');
			
        }
		
	
	function GetMaxID(){
		$this->db->select_max('id');
		$query = $this->db->get('mc_orders');
		if($query->num_rows() > 0){
			return $query->row()->id;               
		 }
		 else
			 return false;
	}
		
}
?>