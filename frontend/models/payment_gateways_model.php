<?php
/**
* Payment Merchant records
*  Table fields
*	- name
*	- merchant_id (for backdoor notifcation)
*	- merchant_secret_key 
*	- merchant_payment_url
*	- local_success_url
*	- local_cancel_url
*	- local_s2s_url
*	- credit_cards_accepted (lowecase_underscorespace and commas_separated- ex: bankone,bank_two,bank_three,bankfour)
*/

class Payment_gateways_model extends CI_Model {

	function get_all() {
		$this->db->select('*');
		$gateways = $this->db->get('mc_payment_gateways');

		if($gateways->num_rows() > 0){
			return $gateways->row(0);
		}

		return false;
	}

}