<?php
class Products_model extends CI_Model {

	function getProductGroupedByCategories(){
		$sql = "SELECT id, name as category_name, china_name as category_china_name FROM mc_product_categories WHERE status = 1 ORDER BY sort_order ASC";

		$query_categories = $this->db->query($sql);
		$product_line_up = false;
		if($query_categories->num_rows() > 0){
			$product_line_up = $query_categories->result();
           	foreach ($product_line_up as $key => $value) {
            	$sql = "SELECT id, product_name, short_description, price, pc_per_box, images, is_new FROM mc_products WHERE status = 1 AND category_id = ? ORDER BY sort_order ASC";
            	$product_query = $this->db->query($sql, array($value->id));
            	$product_line_up[$key]->products = $product_query->result();
            }; 
        }

        return $product_line_up;
	}
	
	function getProductGroupedByCategoriesEdited(){
		
		$this->db->order_by("sort_order","asc");
		$query_categories = $this->db->get_where("mc_product_categories",array("status"=>1));
		$data_categories = $query_categories->result();
		
		$categories=array();
		foreach($data_categories as $category){
			
			$this->db->order_by("sort_order","asc");
			$query_products = $this->db->get_where("mc_products",array("status"=>1,"category_id"=>$category->id));
			
			$data_products = $query_products->result();
			$products=array();
			foreach($data_products as $product){
				$products[$product->id] = array(
					"id"=>$product->id,
					"product_name"=>$product->product_name,
					"price"=>$product->price,
					"pc_per_box"=>$product->pc_per_box,
					"images"=>$product->images,
					"is_new"=>$product->is_new
				);	
			}
			
			
			if($products){
				$categories[$category->id] = array(
					"id"=>$category->id,
					"name"=>$category->name,
					"products"=>$products
				);
			}
		}
		

        return $categories;
	}

	function getNewProducts($count) {
		$sql = "SELECT id, product_name, short_description, is_new, images FROM mc_products WHERE status = 1 AND is_new = 1 ORDER BY sort_order ASC  LIMIT $count";
		$product_query = $this->db->query($sql);

		if($product_query->num_rows() > 0){
			return $product_query->result();
		}

		return false;
	}

	function getProductById($id){
		
		$this->db->select('id, price, product_name, short_description');

		$product_query = $this->db->get_where('mc_products', array('id'=>$id, 'status'=>1));
		if($product_query->num_rows() > 0){
			$products = $product_query->result();
			return $products[0];
		}

		return false;
		
	}


	function getProductsInId($ids){
		if(!is_array($ids)){
			return false;
		}

		if(sizeof($ids) == 0){
			return array();
		}

		$this->db->where_in('id', $ids);
		$product_query = $this->db->get('mc_products');

		if($product_query->num_rows() > 0){
			$products = $product_query->result();
			return $products;
		}

		return false;

	}
}
	
	
	