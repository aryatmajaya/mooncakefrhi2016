<?php
	$total_qty = 0;
	foreach ($$order_items as $key => $value) {
		$total_qty += $value->qty;
	}
?>
<div class="container bt-ca">
	<div class="row">
		<div class="sub-page-title col-xs-12">
			<h1><?= $header_title ?></h1>
		</div><!-- sub-page-title -->
	</div><!-- row -->
</div><!-- container -->

<div class="sections">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-success header text-center">Your order has been successfully processed.</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">&nbsp;</div>
			<h4 class="text-center">Order Details</h4>
			<br>
		</div>
		<div class="row">
			<div class="col-md-5 col-sm-5">
				<table>
					<tr>
						<th>Order date:</th>
						<td><?= $order_detail->date_ordered?></td>
					</tr>
					<tr>
						<th>Order reference:</th>
						<td><?= $order_detail->order_ref?></td>
					</tr>
					<tr>
						<th>Approval code:</th>
						<td><?= $order_detail->approval_code?></td>
					</tr>
					<tr>
						<th>Discount:</th>
						<td><?= $order_detail->card_discount?>%</td>
					</tr>
					<tr>
						<th>Delivery Charge:</th>
						<td><?= $total_delivery_charge?>%</td>
					</tr>
					<tr>
						<th>Amount:</th>
						<td>SGD <?= number_format($order_detail->total_amount - $order_detail->discounted_amount + $total_delivery_charge, 2) ?></td>
					</tr>
				</table>
			</div>
			<div class="col-md-5 col-sm-5 col-md-offset-1">
				<table>
					<tr>
						<th>Customer:</th>
						<td><?= $customer->first_name . ' ' . $customer->last_name ?></td>
					</tr>
					<tr>
						<th>Mobile:</th>
						<td><?= $customer->mobile ?></td>
					</tr>
					<tr>
						<th>Alt contact:</th>
						<td><?= $customer->alt_contact ?></td>
					</tr>
					<tr>
						<th>Email:</th>
						<td><?= $customer->email ?></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<hr>
			<table class="table table-bordered">
				<thead>
					<td class="col-md-1">#</td>
					<th class="col-md-9">Item</th>
					<th>Quantity</th>
				</thead>
				<tbody>
					
					<?php foreach ($order_items as $key => $value): ?>
						<tr>
							<td>
								<?= $key+1 ?>
							</td>
							<td>
								<?= $value->product_name ?>
							</td>
							<td>
								<?= $value->qty ?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="2"><span class="text-right">Total Qty:</span></th>
						<td><?= $total_qty ?></td>
					</tr>
				</tfoot>
			</table>
			<hr>
		</div>
	</div>
</div>