<?php 

class Model_discount_number_item extends CI_Model {
	function getItem(){
		$cart_contents = $this->cart->contents();
		$products_discount = $this->getProductsChecked();
		
		
		$total_item = 0;
		
		if($cart_contents){
			foreach($cart_contents as $content){
				if(in_array($content['id'],$products_discount)){
					$total_item += $content['qty'];
				}
			}
		}
		
		$this->db->order_by("total_item","DESC");
		
		$this->db->where("status",1);
		$this->db->where("NOW() BETWEEN start_date AND end_date");
		$this->db->where("total_item <=",$total_item);
		$query = $this->db->get("discount_number_item");
		$data = $query->row();
		
		if($data){
			return $data;	
		}else{
			return false;	
		}
	}
	
	function getProductsChecked(){
		$query = $this->db->get("discount_number_item_products");
		
		$datas=$query->result();
		$returns=array();
		foreach($datas as $data){
			$returns[] = $data->product_id;	
		}
		
		return $returns;
	}
}