<?php

class Total_model extends CI_Model {
	function get_all(){
		$this->db->order_by("sort_order","asc");
		$query = $this->db->get_where("total",array("status"=>1));
		return $query->result();
	}
}