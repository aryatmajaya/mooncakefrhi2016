<?php $CI =& get_instance(); ?>
<?php if ($this->session->flashdata('cart_success')): ?>
	<div class="alert alert-success" role="alert"><?= $this->session->flashdata('cart_success')?><span aria-hidden="true">&times;</span></div>
<?php endif ?>

<div class="sub-page-title">
	<div class="bg-flower"></div>
 	<h1><?= $header_title ?></h1>
</div>


<div class="sections">

    <!-- end cart listing -->
    <?php if ($this->session->flashdata('basket_error')): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('basket_error');?></div>
    <?php endif ?>
    
    <?php if (sizeof($cart_contents) > 0): ?>
        <form action="<?= site_url("cart/update");?>" method="POST" name="calculateform" id="calculateform">
            
            <div class="lined-section">
            	<table class="table">
                	<thead>
                    	<tr>
                        	<th>Description</th>
                            <th>Quantity per Box</th>
                            <th>Price</th>
                            <th width="1">Quantity</th>
                            <th width="1"></th>
                        </tr>
                    </thead>
                	<tbody>
						<?php foreach ($cart_contents as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php
                                    //chinese text use short description
                                    if(isset($value['product_detail']->short_description) && !empty($value['product_detail']->short_description)) {
                                        $cntext = '<br><span class="title-cn">'.$value['product_detail']->short_description.'</span>';
                                    } else {
                                        $cntext = false;
                                    }
                                    ?>
                                    <?= nl2br($value['product_detail']->product_name)?><?= ($cntext) ? $cntext : '' ?><?= ($value['product_detail']->is_new == 1) ? '<sup>New</sup>' : '' ?>
                                </td>
                                <td>
									<?php if ($value['product_detail']->pc_per_box > 1): ?>
                                        <?= $value['product_detail']->pc_per_box ?>pcs per box
                                    <?php endif ?>
                                </td>
                                <td>$<?= intval($value['price'])?></td>
                                <td>
                                	<input type="hidden" name="products[<?= $key ?>][id]" value="<?= $value['id'] ?>">
                                    <input type="text" class="form-control product-qty" name="products[<?= $key?>][qty]" placeholder="0" value="<?= $value['qty']?>" style="width:88px;">
                                </td>
                                <td>
                                	<a href="<?= site_url("cart/remove/".$value['rowid']);?>" class="btn btn-danger remove-product">&times;</a>
                                </td>
                            </tr><!-- row -->
                        <?php endforeach ?>
                	</tbody>
                </table>
            </div><!-- lined-section -->
            
            
            <?php if($this->session->flashdata("discount_module_error")){?>
                <div role="alert" class="alert alert-danger" style="margin-top:20px;">
                    <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <p><?php echo $this->session->flashdata("discount_module_error");?></p>
                </div>
            <?php } ?>
            
            <div class="row lined-section payment-option">
                <div class="col-xs-12 col-sm-8">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-lg-2">
                            <p>Pay using:</p>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <?php
                                //This $card_discount holds the creditcard info  
                                $card_discount = $this->session->userdata('session_card'); 
                                // This $session_discount holds the discount parameters for promocode of card discount
                                $session_discount = $this->session->userdata('session_discount');
                                $total_discount = $this->session->userdata('total_discount');
                            ?>
                            
                            <?php foreach($discount_modules as $module){ ?>
								<?php
                                    $CI->load->library("discount_module/".$module->class);
                                    echo $CI->{$module->class}->discountOption();
                                ?>
                            <?php } ?>
                            
                            <div class="discount-note">
                            	<ul class="list-unstyled">
                                	<?php /*<li>* Additional 5% for OCBC/BOS Cardholder and The Far Card members</li>*/ ?>
                                    <li>* Minimum pre-order of 2 boxes required.</li>
                            	</ul>
                            </div>
                                
                                
                            <?php /*foreach ($cards as $ckey => $cvalue): ?>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="cards" required value="<?= $cvalue->id ?>" 
                                        <?= ($card_discount && $card_discount->id == $cvalue->id) ? 'checked' : '' ; ?>
                                        class="last payment-option" >
                                        <?= $cvalue->name?>
                                    </label>
                                    <?php if ($cvalue->has_promo): ?>
                                        <div class="clearfix"></div>
                                        <div class=" promocode-wrapper col-md-8 col-ld-8 col-sm-8 
                                            <?= (!isset($card_discount) || empty($card_discount) || ($card_discount->id != $cvalue->id)) ? 'hidden' : '' ?>"
                                            >
                                            <? $success = (isset($session_discount['promocode']) && $session_discount['promocode']) ? 'has-success' : 'has-error'; ?>
                                            <?php if (!isset($session_discount['promocode_attempt'])): ?>
                                             <?php $success = ''; ?>
                                            <?php endif ?>
                                            <div class="form-group has-feedback <?= $success?>">
                                                <div>Promo Code:</div>
                                                <input name="promocode<?=$cvalue->id;?>" 
                                                    type="text" class="form-control promocode-input" 
                                                    value="<?= ($session_discount && isset($session_discount['promocode_attempt'])) ? $session_discount['promocode_attempt'] : '' ?>">
                                                <?
                                                    $icon = ($success != '') ? 'glyphicon-ok' : 'glyphicon-remove';
                                                    if(empty($session_discount['promocode_attempt'])){
                                                        $icon ='';
                                                    }

                                                ?>
                                                <span class="glyphicon <?= $icon?> form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    <?php endif ?>
                                </div>
                            <?php endforeach*/ ?>
                        </div>
                        <div id="card_error"></div>
                    </div>
                </div>
            </div><!-- payment-option -->
            
            <div class="lined-section payment-option">
                <!-- <div class="col-xs-12 col-sm-8"></div> -->
                <?php
                    $calculate_label = "Calculate";
                    if($this->session->userdata('session_discount')){
                        $calculate_label = "Re-Calculate";
                    }
                ?>
                <div class="text-right">
                    <p><button type="submit" class="btn btn-default"><?= $calculate_label?></button></p>
                    <?php if (!$this->session->userdata('user_id')){ ?><p>Please Login or Register below to proceed</p><?php } ?>
                </div>
            </div>
             <!-- end of calculate form/cart list -->
            <div class="row lined-section total">
                <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
                    <?php
                    /*<div class="row">
                        <div class="col-xs-6"><p class="total-txt-label">Sub Total:</p></div>
                        <?php
                            $total_discount =$this->session->userdata('total_discount');
                            $card = $this->session->userdata('session_discount');
                            $total_discount_percent = ($card) ?  $card['discount'] : 0;
                                                            $gst_charge = ($this->cart->total() - $total_discount) * (GST_PERCENT/100);
                                                            //hack: remove gst charge for frhi
                                                            $gst_charge = 0;
                                                            $overall_amount = $this->cart->total() - $total_discount + $gst_charge;
                        ?>
                        <div class="col-xs-6"><p class="total-txt-value">SGD <?= $this->cart->format_number($this->cart->total()) ?></p></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6"><p class="total-txt-label">Discount: <?= $total_discount_percent?>%</p></div>
                        <div class="col-xs-6"><p class="total-txt-value text-danger">SGD -<?= number_format($total_discount, 2)?></p></div>
                    </div>
                                            <!-- <div class="row">
                                                <div class="col-xs-6"><p class="total-txt-label">GST Charge:</p></div>
                                                <div class="col-xs-6"><p class="total-txt-value">SGD <?=number_format($gst_charge, 2) ?></p></div>
                                            </div> -->
                    <div class="row">
                        <div class="col-xs-6"><p class="total-txt-label">Total Amount:</p></div>
                        <div class="col-xs-6"><p class="total-txt-value">SGD <?=number_format($overall_amount,2) ?></p></div>
                    </div>*/
                    ?>
                    <?php $this->load->view("cart/partials/total");?>
                </div>
                <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
                    <div class="row btn-grp-total">
                        <?php if ($this->session->userdata('user_id')): ?>
                            <div class="col-xs-12 col-sm-6 pull-right"><button type="submit" class="btn btn-default btn-block btn-total" name="checkout" value="<?= site_url("cart/checkout");?>">Checkout</button></div>
                        <?php endif ?>
                        <div class="col-xs-12 col-sm-6 pull-right">
                            <a href="<?= site_url("products");?>" class="btn btn-primary btn-block btn-total">Continue Shopping</a>
                        </div>

                    </div>
                </div>
            </div><!-- total -->
        </form>
    <?php else: ?>
        <div class="row lined-section">
            <div class="col-xs-12 col-sm-12">
                <div class="text-center form-title">Your Cart is empty. Please make an order <a href="<?= site_url("products");?>">here</a></div>
            </div>
        </div>
    <?php endif ?>		
    <!-- end cart listing -->
    <!-- User's Registration and Login form -->
    <?php if (!$this->session->userdata('user_id')): ?>
        <div class="row login-section last basket-form" name="login-register" id="login-register">
            <? if (validation_errors()): ?>
                <div class="alert alert-danger"><?= validation_errors() ?></div>
            <? endif; ?>
            <? if ($this->session->flashdata('register_error')): ?>
                <div class="alert alert-danger"><p><?=$this->session->flashdata('register_error')?></p></div>
            <? endif; ?>
            <? if ($this->session->flashdata('login_error')): ?>
                <div class="alert alert-danger"><p>Invalid Username or password</p></div>
            <? endif; ?>
            
            <div class="col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-5 col-lg-push-7">
            	<div class="basket-form-sections last">
                <p>If you have already made a purchase this year, just login.</p>
                <div class="form-title">Existing Customer?</div>
                <form action="<?= site_url("cart/login");?>" name="login" class="form-horizontal log-in" role="form" method="POST">
                    <input type="hidden" name="request_path" value="cart">
                  <div class="form-group">
                    <label for="uName" class="col-xs-12 col-sm-5 col-md-4 col-lg-3 control-label">Login Email<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                      <input type="text" class="form-control" name="login[username]" value="<?= isset($username)?$username:''; ?>" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password" class="col-xs-12 col-sm-5 col-md-4 col-lg-3 control-label">Password<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                      <input type="password" class="form-control" id="password" name="login[password]" value="" >
                      <button class="btn btn-default mt20 col-xs-6 " value="login" name="form_action">Login</button>
                      <a href="<?php echo site_url("user/forgotpassword");?>" class="btn-forgot col-xs-6">Forgot Password?</a>
                    </div>
                  </div>
                </form>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-pull-6 col-md-6 col-md-pull-6 col-lg-5 col-lg-pull-5">
            	<div class="basket-form-sections">
                <div class="form-title">New Customer?</div>
                                    <p>We respect your privacy and we will use the information provided to process your order and communicate with you via email upon confirmation of your purchase.</p>
                <form name="register" class="form-horizontal" role="form" method="POST"
                    action="<?=site_url("user/register");?>">
                    <input type="hidden" name="request_path" value="cart">
                  <div class="form-group">
                    <label for="fName" class="col-xs-12 col-sm-6 control-label">First Name<span class="hidden-xs">:</span><span class="required">*</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" class="form-control" id="fName" name="register[first_name]" value="<?= isset($register) ? $register['first_name'] : ''; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="lName" class="col-xs-12 col-sm-6 control-label">Last Name<span class="hidden-xs">:</span><span class="required">*</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" class="form-control" id="lName" name="register[last_name]" value="<?= isset($register) ? $register['last_name'] : ''; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="mNumber" class="col-xs-12 col-sm-6 control-label">Mobile Number<span class="hidden-xs">:</span><span class="required">*</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="tel" class="form-control" id="mNumber" name="register[mobile]" value="<?= isset($register) ? $register['mobile'] : ''; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="altNumber" class="col-xs-12 col-sm-6 control-label">Alt Contact Number<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="tel" class="form-control" id="altNumber" name="register[alt_contact]" value="<?= isset($register) ? $register['alt_contact'] : ''; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Email<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="email" class="form-control" id="email" name="register[email]" value="<?= isset($register) ? $register['email'] : ''; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Password<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="password" class="form-control" id="password" name="register[password]" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="cPassword" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Confirm Password<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-6">
                      <input type="password" class="form-control" id="cPassword" name="confirm_password" value="">
                      
                    </div>
                  </div>
                                        <div class="checkbox">
                                                <input type="checkbox" class="" id="subscribe" name="register[subscribe]" value="1">I would like to receive exclusive news and updates from Fairmont Singapore &amp; Swiss&ocirc;tel The Stamford.
                    
                                        </div>
                                        <div class="form-group">
                    <label for="password" class="col-xs-12 col-sm-6 control-label">&nbsp;</label>
                    <div class="col-xs-12 col-sm-6">
                        <button class="btn btn-default mt20" value="register" name="form_action">Register</button>
                    </div>
                                        </div>
                  
                </form>
            </div>
            </div>
            
        </div>
    <?php endif ?>
</div><!-- section -->

<script>
	$('#calculateform').validate({
		rules: {
			cards: 'required'
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "cards" ) {
				error.insertAfter("#card_error");
			} else {
				error.insertAfter(element);
			}
		}
	});

	$("input.payment-option").on('change', function(e){
		$('.promocode-wrapper').addClass('hidden');
                $('.promocode-input').val('');
		var that = this;
		setTimeout(function(){
			$(that).closest('.radio').find('.promocode-wrapper').removeClass('hidden');
		}, 10);
	});

	window.setTimeout(function(){
		//$('.alert').alert('close');
	}, 1000);
</script>