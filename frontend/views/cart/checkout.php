<?php $CI =& get_instance();?>
<div class="sub-page-title">
	<div class="bg-flower"></div>
	<h1><?= $header_title ?></h1>
</div>

<div class="sections">

    <div class="lined-section customer">
        <p class="title-a">Customer Information</p>
        
        <div class="row">
            <?php
                $mobile = $this->session->userdata('mobile');
                $members_name = $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name') ;
                $members_phone = $this->session->userdata('mobile');
                $members_email = $this->session->userdata('email');
            ?>
            <div class="col-xs-12 col-sm-4"><span>Name:</span> <span class="info"><?= $members_name?></span></div>
            <div class="col-xs-12 col-sm-4"><span>Mobile No:</span> <span class="info"><?= $members_phone?></span></div>
            <div class="col-xs-12 col-sm-4"><span>Email:</span> <span class="info"><?= $members_email?></span></div>
        </div>
    </div>
        
    <div class="lined-section">
    	<p class="title-a">Product Information</p>
        
    	<div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cart_contents as $key => $value): ?>
                        <tr>
                            <td>
                                <?php
                                //chinese text use short description
                                if(isset($value['product_detail']->short_description) && !empty($value['product_detail']->short_description)) {
                                    $cntext = '<br><span class="title-cn">'.$value['product_detail']->short_description.'</span>';
                                } else {
                                    $cntext = false;
                                }
                                ?>
                                <?= nl2br($value['product_detail']->product_name)?><?= ($cntext) ? $cntext : '' ?><?= ($value['product_detail']->is_new == 1) ? '<sup>New</sup>' : '' ?>
                            </td>
                            <td><?= $value['product_detail']->price?></td>
                            <td><?= $value['qty']?></td>
                            <td><?= $this->cart->format_number($value['subtotal'])?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    
	<?php
        $total_discount =$this->session->userdata('total_discount');
        $session_discount = $this->session->userdata('session_discount');
        $total_discount_percent = $session_discount['discount'];
        $card = $this->session->userdata('session_card');
        //Check fulfillment is complete
        $fulfillments = $this->session->userdata('fulfillment_products'); 
        $show_fulfillment_form = false;
        foreach ($fulfillments as $key => $value){
            if ($value['value'] > 0){
                $show_fulfillment_form = true;
                break;
            }
        } 
                    
		$gst_charge = ($this->cart->total() - $total_discount) * (GST_PERCENT/100);
		//hack: remove gst charge for frhi
		$gst_charge = 0;
		$overall_amount = $this->cart->total() - $total_discount + $gst_charge;
                    
    ?>
    <div class="lined-section">
    	<p class="title-a">Payment Information</p>
         <?php
		if($session_discount){
			$CI->load->library("discount_module/".$session_discount['type']);	
		}
		?>
		Pay using : <?php echo $CI->{$session_discount['type']}->title;?>
    </div>
    
    <div class="row lined-section total">

        <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
            <?php
            /*<div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Sub Total:</p></div>
                <div class="col-xs-6"><p class="total-txt-value">SGD <?= $this->cart->format_number($this->cart->total()) ?></p></div>
            </div>
            <div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Discount:(<?= $total_discount_percent?>%)</p></div>
                <div class="col-xs-6"><p class="total-txt-value text-danger">SGD -<?= number_format($total_discount, 2)?></p></div>
            </div>
                            <!-- <div class="row">
                                <div class="col-xs-6"><p class="total-txt-label">GST Charge:</p></div>
                                <div class="col-xs-6"><p class="total-txt-value">SGD <?=number_format($gst_charge, 2) ?></p></div>
                            </div> -->
            <div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Total Amount:</p></div>
                <div class="col-xs-6"><p class="total-txt-value">SGD <?=number_format($overall_amount, 2);?></p></div>
            </div>*/
            ?>
            <?php $this->load->view("cart/partials/total"); ?>
        </div>
        <? //These buttons below will only show up when the fullfillment is done ?>

        <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
            <div class="row btn-grp-total">
                <div class="col-xs-12 col-sm-6"><a href="#" id="cancel-checkout-btn" class="btn btn-primary btn-block btn-total">Cancel</a></div>
                <div class="col-xs-12 col-sm-6"><a href="<?= site_url("cart/proceed_checkout");?>" class="btn btn-default btn-block btn-total" <?= ($show_fulfillment_form) ? 'disabled' : ''; ?>> Proceed to Checkout</a></div>
            </div>
        </div>
        
        
    </div><!-- total -->
		<a name="fulfillmentsection"></a>
		<?php if ($show_fulfillment_form): ?>
			<?php $this->load->view('cart/partials/fulfillment_form.php'); ?>
		<?php endif ?>
		
		<?php $this->load->view('cart/partials/current_fulfillments.php'); ?>

</div><!-- section -->