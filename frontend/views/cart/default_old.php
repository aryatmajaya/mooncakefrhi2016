<div class="container bt-ca">
	<div class="row">
		<div class="sub-page-title col-xs-12">
			<h1><?= $header_title ?></h1>
		</div><!-- sub-page-title -->
	</div><!-- row -->
</div><!-- container -->
<div class="section">
	<div class="container">
		<!-- end cart listing -->
		<?php if ($this->session->flashdata('basket_error')): ?>
    		<div class="alert alert-success">'.$this->session->flashdata('basket_error').'</div>';
  		<?php endif ?>
		<?php if (sizeof($cart_contents) > 0): ?>
			<form action="<?= base_url() ?>cart/update" method="POST" name="calculateform">
				<div class="row basket-section">
					<div class="row">
						<div class="col-xs-5 col-sm-6">
							<h2>Description</h2>
						</div>
						<div class="col-xs-7 col-sm-5 col-md-6">
							<div class="row">
								<div class="col-xs-4 col-sm-5"><h2 class="hidden-xs">Quantity per Box</h2><h2 class="visible-xs">Qty/<br>Box</h2></div>
								<div class="col-xs-4 col-sm-3"><h2>Price</h2></div>
								<div class="col-xs-4 col-sm-3 col-md-2"><h2 class="hidden-xs">Quantity</h2><h2 class="visible-xs">Qty</h2></div>
							</div>
						</div>
					</div>
					<!-- product list group -->
					<?php foreach ($cart_contents as $key => $value): ?>
						<div class="row basket-list">
							<div class="col-xs-5 col-sm-6">
								<h3><?= $value['product_detail']->product_name?><?= ($value['product_detail']->is_new == 1) ? '<sup>New</sup>' : '' ?></h3>
							</div>
							<div class="col-xs-7 col-sm-5 col-md-6">
								<div class="row">
									<?php if ($value['product_detail']->pc_per_box > 1): ?>
										<div class="col-xs-4 col-sm-5"><p><?= $value['product_detail']->pc_per_box ?>pcs per box</p></div>
									<?php endif ?>
									<div class="col-xs-4 col-sm-3"><p>$<?= intval($value['price'])?></p></div>
									<div class="col-xs-4 col-sm-3 col-md-2">
										<input type="hidden" name="products[<?= $key ?>][id]" value="<?= $value['id'] ?>">
										<input type="text" class="form-control" name="products[<?= $key?>][qty]" placeholder="0" value="<?= $value['qty']?>">
									</div>
								</div>
							</div>
						</div><!-- row -->
					<?php endforeach ?>
					<!-- end product list group -->
				</div><!-- basket-section -->
				<div class="row basket-section payment-option">
					<div class="col-xs-12 col-sm-8">
						<div class="row payment-option-holder">
							<div class="col-xs-12 col-sm-3 col-lg-2">
								<p>Pay using:</p>
							</div>
							<div class="col-xs-12 col-sm-9 col-lg-10">
								<?php $card_discount = $this->session->userdata('card_discount'); ?>
								<?php foreach ($cards as $ckey => $cvalue): ?>
									<div class="radio">
										<label>
											<input type="radio" name="cards" value="<?= $cvalue ?>" class="last" <?= ($ckey == 0) ? 'checked' : '' ?> 
												<?= ($cvalue==$card_discount['card'])? 'checked' : '' ?>>
											<?= strtoupper($cvalue)?>
										</label>
									</div>
								<?php endforeach ?>
							</div>
						</div>
					</div>
				</div><!-- payment-option -->
				<div class="row basket-section payment-option">
					<div class="col-xs-12 col-sm-8">
						<div class="row payment-option-holder">
							<div class="col-xs-12 col-sm-3 col-lg-3">
								<p>FAR Promo Code:</p>
							</div>
							<div class="col-xs-12 col-sm-9 col-lg-9">
								<div class="col-sm-6">
								<?php
									$promo = $this->session->userdata('FAR_discount');
								?>
								<input name="promocode" type="text" class="form-control" 
									<?= ($promo['promocode'])? 'readonly' : '' ?> value="<?= ($promo['promocode'])? $promo['promocode'] : '' ?>">
									<?php if ($promo['promocode']): ?>
										<span id="promo_validate" class="text-success text-right">
											VALID! <!-- <button name="remove_promo" href="" class="text-danger pull-right" value="1">Remove</button> -->
										</span>
									<?php else: ?>
										<span id="promo_validate" class="text-danger text-right">
											INVALID!
										</span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-sm-offset-1 col-md-2 col-md-offset-2">
						<button class="btn btn-default btn-block btn-calculate">Calculate</button>
					</div>
				</div>
			</form> <!-- end of calculate form/cart list -->
			<div class="row basket-section total">
				<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
					<div class="row">
						<div class="col-xs-6"><p class="total-txt-label">Sub Total:</p></div>
						<?php
							$total_discount =$this->session->userdata('total_discount');
							$card = $this->session->userdata('card_discount');
							$far = $this->session->userdata('FAR_discount');
							$total_discount_percent = $card['discount'] + $far['discount'];
						?>
						<div class="col-xs-6"><p class="total-txt-value">SGD <?= $this->cart->format_number($this->cart->total()) ?></p></div>
					</div>
					<div class="row">
						<div class="col-xs-6"><p class="total-txt-label">Discount:</p></div>
						<div class="col-xs-6"><p class="total-txt-value">-<?= $total_discount_percent?>%</p></div>
					</div>
					<div class="row">
						<div class="col-xs-6"><p class="total-txt-label">Total Amount:</p></div>
						<div class="col-xs-6"><p class="total-txt-value">SGD <?= $this->cart->format_number($this->cart->total() - $total_discount) ?></p></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
					<div class="row btn-grp-total">
						<div class="col-xs-12 col-sm-6"><a href="<?= site_url("products");?>" class="btn btn-primary btn-block btn-total">Continue Shopping</a></div>
						<div class="col-xs-12 col-sm-6"><a href="<?= site_url("cart/checkout");?>" class="btn btn-default btn-block btn-total">Checkout</a></div>
					</div>
				</div>
			</div><!-- total -->
		<?php else: ?>
			<div class="row basket-section">
				<div class="col-xs-12 col-sm-12">
					<div class="text-center form-title">You basket is empty. Please make an order <a href="<?= base_url()?>products">here</a></div>
				</div>
			</div>
			
		<?php endif ?>		
		<!-- end cart listing -->
		<!-- User's Registration and Login form -->
		<?php if (!$this->session->userdata('user_id')): ?>
			<div class="row basket-section last basket-form">
				<div class="col-xs-12 col-sm-6 col-md-5 basket-form-sections">
					<div class="form-title">New Customer?</div>
					<? if (validation_errors()): ?>
		    			<div class="alert alert-error"><?= validation_errors() ?></div>
		    		<? endif; ?>
		    		<? if ($this->session->flashdata('register_error')): ?>
		    			<div class="alert alert-error"><p><?=$this->session->flashdata('register_error')?></p></div>
		    		<? endif; ?>

					<form name="register" class="form-horizontal" role="form" method="POST"
						action="<?=base_url()?>user/register">
					  <div class="form-group">
					    <label for="fName" class="col-xs-12 col-sm-6 control-label">First Name<span class="hidden-xs">:</span><span class="required">*</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="text" class="form-control" id="fName" name="register[first_name]" value="<?= isset($register) ? $register['first_name'] : ''; ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="lName" class="col-xs-12 col-sm-6 control-label">Last Name<span class="hidden-xs">:</span><span class="required">*</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="text" class="form-control" id="lName" name="register[last_name]" value="<?= isset($register) ? $register['last_name'] : ''; ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="mNumber" class="col-xs-12 col-sm-6 control-label">Mobile Number<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="tel" class="form-control" id="mNumber" name="register[mobile]" value="<?= isset($register) ? $register['mobile'] : ''; ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="altNumber" class="col-xs-12 col-sm-6 control-label">Alt Contact Number<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="tel" class="form-control" id="altNumber" name="register[alt_contact]" value="<?= isset($register) ? $register['alt_contact'] : ''; ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="email" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Email<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="email" class="form-control" id="email" name="register[email]" value="<?= isset($register) ? $register['email'] : ''; ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="password" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Password<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="password" class="form-control" id="password" name="register[password]" value="">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="cPassword" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Confirm Password<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-6">
					      <input type="password" class="form-control" id="cPassword" name="confirm_password" value="">
					      <button class="btn btn-default mt20" value="register" name="form_action">Register</button>
					    </div>
					  </div>
					  
					</form>
				</div>
				<div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-6 col-lg-5 col-lg-offset-2 basket-form-sections last">
					<div class="form-title">New Customer?</div>
					<? if (validation_errors() && !isset($register)): ?>
		    			<div class="alert alert-error"><?= validation_errors() ?></div>
		    		<? endif; ?>
		    		<? if ($this->session->flashdata('login_error')): ?>
		    			<div class="alert alert-error"><p>Invalid Username or password</p></div>
		    		<? endif; ?>
					<form action="<?= base_url()?>user/login" name="login" class="form-horizontal log-in" role="form" method="POST">
					  <div class="form-group">
					    <label for="uName" class="col-xs-12 col-sm-5 col-md-4 col-lg-3 control-label">Login Email<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
					      <input type="text" class="form-control" name="login[username]" value="<?= isset($username)?$username:''; ?>" placeholder="">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="password" class="col-xs-12 col-sm-5 col-md-4 col-lg-3 control-label">Password<span class="hidden-xs">:</span></label>
					    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
					      <input type="password" class="form-control" id="password" name="login[password]" value="" >
					      <button class="btn btn-default mt20 col-xs-6 " value="login" name="form_action">Login</button>
					  	  <a href="/user/forgotpassword" class="btn-forgot col-xs-6">Forgot Password?</a>
					    </div>
					  </div>
					  
					</form>
				</div>
			</div>
		<?php endif ?>
	</div><!-- container -->
</div><!-- section -->