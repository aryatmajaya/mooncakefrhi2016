<?php $display_fulfillments = $this->session->userdata('display_fulfillments');?>
<?php if ($display_fulfillments): ?>
	<div class="fulfillment-section">
		
		<?php foreach ($display_fulfillments as $key => $value): ?>

			<div class="row fulfillment-list">
				<div class="col-xs-11 col-sm-11 col-md-11">
					<strong>Type:</strong> <span><?= str_replace('_',' ', ucfirst($value['collection_type']))?></span><br/>
					<address>
						<?php if ($value['collection_type'] == 'delivery'): ?>
                            
                                <strong>Delivery Date:</strong> <span><?= $value['delivery_date']?></span><br/>
                                <strong>Time Slot:</strong> <span><?= $value['collection_slot']?></span><br/>
                                <strong>Address:</strong> <span><?= $value['address'] ?>, <?=$value['postal_code']?></span><br/>
                                <strong>Recipient:</strong> <span><?= $value['recipient']?></span><br>
                                <strong>Contact No.:</strong> <span><?= $value['contact_number']?></span><br>
            
                        <?php else: ?>
                            <strong>Collection Date:</strong> <span><?= $value['selfcollect_date']?></span><br/>
                        <?php endif ?>
                        
                        <?php if ($value['collection_type'] == 'delivery' && $value['delivery_additional_request']){ ?>
                            <strong>Additional Request:</strong> <span><?php echo $value['delivery_additional_request'];?></span>
                        <?php }elseif($value['collection_type'] == 'self_collection' && $value['selfcollect_additional_request']){ ?>
                            <strong>Additional Request:</strong> <span><?php echo $value['selfcollect_additional_request'];?></span>
                        <?php } ?>
                            
                    </address>
				</div>
				
					<a href="<?= site_url("cart/cancel_fulfillment_item?idx=".$key); ?>" class="close remove-fulfillment btn btn-danger">&times;</a>
				
				<div class="col-xs-11 col-sm-11">
					<div class="row">
						<div class="col-sm-11 col-xs-11"><strong>Product</strong></div>
						<div class="col-sm-1 col-xs-1"><strong>Quantity</strong></div>
					</div>
					<?php foreach ($value['products'] as $idx => $qty): ?>
							<?php if ($qty == 0) continue; ?>
							<?php $product  = Products_model::getProductById($idx); ?>
							<div class="row">
								<div class="col-sm-11 col-xs-11"><?= $product->product_name ?></div>
								<div class="col-sm-1 col-xs-1"><?= $qty ?></div>
							</div>
						<?php endforeach ?>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>
