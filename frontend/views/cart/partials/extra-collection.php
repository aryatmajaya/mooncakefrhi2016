<div class="form-group">
	<label for="fDate" class="col-xs-12 col-sm-4">Date<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-8">
		<div class="input-group">
			<input type="text" class="form-control" readonly id="delivery_date" required placeholder="" name="fulfillment[delivery_date]">
			<span class="input-group-addon" id="delivery_date_button"><i class="glyphicon glyphicon-calendar"></i></span>
		</div>
		<div id="delivery_date_error"></div>
	</div>
</div>
<div class="form-group">
	<label for="fStyle" class="col-xs-12 col-sm-4">Time Slot<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-8">
		<SELECT name="fulfillment[collection_slot]" class="form-control">
			<option value="Morning (9am-12am)">Morning (9am-12am)</option>
			<option value="Afternoon (2pm-5pm)">Afternoon (2pm-5pm)</option>
		</SELECT>
	</div>
</div>
<div class="form-group">
	<label for="" class="col-xs-12 col-sm-6">Address<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-12">
		<textarea class="form-control" name="fulfillment[address]"></textarea>
	</div>
</div>
<div class="form-group">
	<label for="" class="col-xs-12 col-sm-6">Postal Code<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-6">
		<input class="form-control" name="fulfillment[postal_code]">
	</div>
</div>
<div class="form-group">
	<label for="" class="col-xs-12 col-sm-6">Recipient<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-6">
		<input class="form-control" name="fulfillment[recipient]">
	</div>
</div>
<div class="form-group">
	<label for="" class="col-xs-12 col-sm-6">Contact No.<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-6">
		<input class="form-control" name="fulfillment[contact_number]">
	</div>
</div>

<div class="form-group">
	<label for="" class="col-xs-12 col-sm-4">Additional Request<span class="hidden-xs">:</span></label>
	<div class="col-xs-12 col-sm-8">
		<textarea class="form-control" name="fulfillment[delivery_additional_request]"></textarea>
	</div>
</div>