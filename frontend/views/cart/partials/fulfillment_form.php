<form name="fulfilment" class="form-horizontal" role="form" id="fulfilment-form" method="POST">
	
	
	<div class="row fulfillment-section">
		<div class="col-xs-12 col-sm-5 col-md-4 fulfillment-form">
			<p class="title-a">Fulfillment Options</p>
			  <div class="form-group">
				<label for="fStyle" class="col-xs-12 col-sm-4">Select Type<span class="hidden-xs">:</span></label>
				<div class="col-xs-12 col-sm-8">
					<SELECT name="fulfillment[collection_type]" class="form-control" id="collection_type">
						<option value="self_collection" default>Self Collection</option>
						<option value="delivery">Delivery</option>
					</SELECT>
				</div>
			  </div>
			  <div class="form-group" id="selfcollect_date_wrapper">
				<label for="fDate" class="col-xs-12 col-sm-4">Date<span class="hidden-xs">:</span></label>
				<div class="col-xs-12 col-sm-8">
					<div class="input-group">
						<input type="text" class="form-control" readonly id="selfcollect_date" required placeholder="" name="fulfillment[selfcollect_date]">
						<span class="input-group-addon" id="selfcollect_date_button"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
					<div id="collection_date_error"></div>
				</div>
			  </div>
              <div id="selfcollect_additional_request_wrapper">
              	<div class="form-group">
                	<label class="col-xs-12 col-sm-4">Additional Request<span class="hidden-xs">:</span></label>
                    <div class="col-xs-12 col-sm-8">
                    	<textarea name="fulfillment[selfcollect_additional_request]" class="form-control"></textarea>
                    </div>
                </div>
              </div>
			  <div class="delivery-extra">
				  	<?= $this->load->view('cart/partials/extra-collection.php') ?>
			  </div>
			  <div id="capping_error" class="error"></div>
		</div><!-- fulfillment-form -->
		<div class="col-xs-12 col-sm-6 col-sm-offset-1 col-md-7 fulfillment-form">
			<?= $this->load->view('cart/partials/fulfillment_products.php') ?>
		</div> <!-- fulfillment-form last -->
		<div class="clearfix"></div>
		<?php if ($this->session->flashdata('fulfillment_error')): ?>
			<div class="col-xs-12 col-sm-6 col-sm-offset-1 col-md-7 fulfillment-form last">
				<labal class="error"><?= $this->session->flashdata('fulfillment_error')?></label>
			</div>
		<?php endif ?>
		
	</div><!-- fulfillment-section -->

	<div class="row checkout-section last">
		<div class="col-xs-12 col-sm-6 col-md-8">
			<p>Select Self Collection or Delivery and choose a date.</p> 
			<p><strong>Collection:</strong> Fairmont Singapore’s Mooncake Booth, Level 2, Fairmont Singapore, 80 Bras Basah Road, Singapore 189560</p>
			<p><strong>Delivery:</strong> An additional $50 nett delivery charge per location for orders less than 50 boxes</p>

		</div>
		<div class="col-xs-12 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-1 col-lg-offset-1">
			<button type="submit" class="btn btn-default btn-block btn-total" id="create-fulfillment">Create Fulfilment</button>
		</div>
	</div>
</form>