<div class="row">
	<div class="col-xs-8 col-sm-9 col-md-offset-1">
		<p class="title-a">Items</p>
	</div>
	<div class="col-xs-4 col-sm-3 col-md-2">
		<p class="title-a">Quantity</p>
	</div>
</div>

<?php $fulfillments = $this->session->userdata('fulfillment_products'); ?>
<?php foreach ($fulfillments as $key => $value): ?>
	<?php if($value['value'] > 0): ?>
		<?php 
			foreach ($cart_contents as $ckey => $cvalue) {
				if($key == $cvalue['id']){
					$name = $cvalue['product_detail']->product_name;
					$is_new = $cvalue['product_detail']->is_new;
				}
			}
		?>
		<div class="row fulfilment-list">
			<div class="col-xs-8 col-sm-9 col-md-offset-1">
				<p><?= $name?><?= ($is_new == 1) ? '<sup>New</sup>' : '' ?></p>
			</div>
			<div class="col-xs-4 col-sm-3 col-md-2">
				<div><?= $value['value']?></div>
				<input type="hidden" name="fulfillment[products][<?=$key?>]" value ="<?= $value['value']?>">
				<!-- <select name="fulfillment[products][<?=$key?>]" id="fulfillment-qty" class="form-control">
					<?php for($i = $value['value']; $i >= 0; $i--): ?>
						<option value="<?= $i?>"><?= $i ?></option>
					<?php endfor ?>
				</select> -->
			</div>
		</div>
	<?php endif; ?>
<?php endforeach ?>
