<?php
	$CI =& get_instance();
	$CI->load->model("total_model");
	$data_totals = $CI->total_model->get_all();
	
	$total_data=array();
	$total = 0;
	foreach($data_totals as $data){
		$CI->load->library("total/".$data->code);
		$CI->{$data->code}->getTotal($total_data,$total);
	}
	
	?>
	
	<?php 
		foreach($total_data as $data){
		$text_class = array();
		$text_class[] = "total-txt-value";
		if($data['class']){
			foreach($data['class'] as $class){
				$text_class[] = $class;
			}
		}
	?>
        <div class="row">
            <div class="col-xs-6"><p class="total-txt-label"><?php echo $data['title'];?>:</p></div>
            <div class="col-xs-6"><p class="<?php echo implode(" ", $text_class);?>"><?php echo $data['text'];?></p></div>
        </div>
	<?php } ?>