<div class="sub-page-title">
	<div class="bg-flower"></div>
    <h1><?= $header_title ?></h1>
</div>

<div class="sections">

    <div class="lined-section">
        <p class="title-a">Customer Information</p>
        <div class="row">
            <?php
                $mobile = $this->session->userdata('mobile');
                $members_name = $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name') ;
                $members_phone = (null !== $mobile && !empty($mobile)) ? $this->session->userdata('mobile') : ' - ';
                $members_email = $this->session->userdata('email');
            ?>
            <div class="col-xs-12 col-sm-4"><span>Name:</span> <span class="info"><?= $members_name?></span></div>
            <div class="col-xs-12 col-sm-4"><span>Mobile No:</span> <span class="info"><?= $members_phone?></span></div>
            <div class="col-xs-12 col-sm-4"><span>Email:</span> <span class="info"><?= $members_email?></span></div>
        </div>
    </div>
    
    <div class="lined-section border-bottom-removed">
    	<p class="title-a">Product Information</p>
        
    	<div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
				<?php $total_qty = 0;?>
                <?php foreach ($cart_contents as $key => $value): ?>
                    <tr>
                    	<td>
                            <?php
                            //chinese text use short description
                            if(isset($value['product_detail']->short_description) && !empty($value['product_detail']->short_description)) {
                                $cntext = '<br><span class="title-cn">'.$value['product_detail']->short_description.'</span>';
                            } else {
                                $cntext = false;
                            }
                            ?>
                            <?= nl2br($value['product_detail']->product_name)?><?= ($cntext) ? $cntext : '' ?><?= ($value['product_detail']->is_new == 1) ? '<sup>New</sup>' : '' ?>
                        </td>
                        <td><?= $value['product_detail']->price?></td>
                        <td><?= $value['qty']?></td>
                        <td><?= $this->cart->format_number($value['subtotal'])?></td>
                    </tr>
                    <?php $total_qty += $value['qty']?>
        		<?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div><!-- lined-section -->
    
    <?php $this->load->view('cart/partials/review_fulfillments') ?>
    
    <?php
        $total_discount =$this->session->userdata('total_discount');
        $session_discount = $this->session->userdata('session_discount');
        $total_discount_percent = $session_discount['discount'];
        $order_ref = $this->session->userdata('order_ref');
        $delivery_charges = Delivery_model::Get_Total_Delivery_Qty($order_ref);

        $total_delivery_charge = Utilities::Calculate_Delivery_Charges($delivery_charges, $total_qty);
        $gst_charge = ($this->cart->total() - $total_discount + $total_delivery_charge) * (GST_PERCENT/100);
        //hack: remove gst charge for frhi
        $gst_charge = 0;
        $overall_amount = $this->cart->total() - $total_discount + $total_delivery_charge + $gst_charge;
        
    ?>
    <!-- 
        <?php print_r($delivery_charges); ?>
    -->
    <div class="row checkout-section total">
        <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
            <?php /*<div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Sub Total:</p></div>
                <div class="col-xs-6"><p class="total-txt-value">SGD <?= $this->cart->format_number($this->cart->total()) ?></p></div>
            </div>
            <div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Discount: <?= $total_discount_percent?>%</p></div>
                <div class="col-xs-6"><p class="total-txt-value text-danger">SGD -<?= number_format($total_discount,2)?></p></div>
            </div>
            <div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Delivery Charge:</p></div>
                <div class="col-xs-6"><p class="total-txt-value">SGD <?=number_format($total_delivery_charge, 2) ?></p></div>
            </div>
            <!-- <div class="row">
                <div class="col-xs-6"><p class="total-txt-label">GST Charge:</p></div>
                <div class="col-xs-6"><p class="total-txt-value">SGD <?=number_format($gst_charge, 2) ?></p></div>
            </div> -->
            <div class="row">
                <div class="col-xs-6"><p class="total-txt-label">Total Amount:</p></div>
                <div class="col-xs-6"><p class="total-txt-value">SGD <?= $this->cart->format_number($overall_amount); ?></p></div>
            </div>*/ ?>
            
            <?php $this->load->view("cart/partials/total");?>
        </div>
        <? //These buttons below will only show up when the fullfillment is done ?>
        <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7">
            <div class="row btn-grp-total">
                <div class="col-xs-12 col-sm-6"><a href="<?= site_url("cart/checkout#fulfillmentsection");?>" class="btn btn-primary btn-block btn-total">Fulfillment Options</a></div>
                <div class="col-xs-12 col-sm-6"><a href="<?= site_url("cart/proceed_payment");?>" class="btn btn-default btn-block btn-total"> Proceed to Payment</a></div>
            </div>
        </div>
    </div><!-- total -->
</div><!-- sections -->