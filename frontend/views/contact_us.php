<div class="sub-page-title">
	<div class="bg-flower"></div>
    <h1><?= $header_title ?></h1>
</div>
​
<div class="sections">
    <div class="row">
        <div class="col-xs-12 col-md-6 mt20">
           	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.800688138621!2d103.85153331475395!3d1.294093699056144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19a5d3614af9%3A0xd8961f08c92ca48c!2sFairmont+Singapore!5e0!3m2!1sen!2sid!4v1468898655053" class="map-contact" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="col-xs-12 col-md-6 mt20">
        
            <div class="section-title">
				<h2 class="title">Corporate Sales Office</h2>
			</div>
            
            <p><span class="title-a">Fairmont SIngapore</span><br>
            80 Bras Basah Road, Singapore 189560<br>
            10:00am - 10:00pm (daily)</span>
            
            <p>Tel: +65 6338 8785 (9am – 5.30pm daily)<br>
            Fax: +65 6338 8529<br>
            Email: <a href="mailto:fairmontsingapore.mooncakes@fairmont.com">fairmontsingapore.mooncakes@fairmont.com</a></p>
            
            <div class="section-title mt20">
				<h2 class="title">Mooncake Booths</h2>
			</div>
            
            
            <div class="row mt20">
                    <div class="col-md-5">
                    	<div class="title-a">FAIRMONT SINGAPORE</div>
                    	level 2, Fairmont Singapore’s Mooncake Booth
                    </div>
                    <div class="col-md-7">
                        8 Aug – 15 Sep<br>10am -10pm
                    </div>
                </div>
                
                
                <div class="row mt20">
                    <div class="col-md-5">
                    	<div class="title-a">VivoCity</div>
                        Level 1, Atrium
                    </div>
                    <div class="col-md-7">
                        16 Aug – 14 Sep<br>10am – 10pm
                    </div>
                </div>
                
                
                <div class="row mt20">
                    <div class="col-md-5">
                    <div class="title-a">Takashimaya</div>
                        Basement 2 Atrium,
                        Takashimaya Square
                    </div>
                    <div class="col-md-7">
                        18 Aug – 15 Sep<br>10.30am – 9.30pm
                    </div>
                </div>
​
​
​
            
        </div>
    </div><!-- row -->
</div><!-- sections -->