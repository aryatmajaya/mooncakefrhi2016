<div class="sub-page-title">
	<div class="bg-flower"></div>
	<h1><?= $content->title ?></h1>
</div>
​
<div class="sections">
	<div class="row">
    	<div class="col-xs-12 col-md-6 last mt20">
        
			<div class="section-title">
				<h2 class="title">Delivery</h2>
			</div>
            <ul>
            	<li>Please allow 7 working days for delivery of mooncakes.</li>
                <li>Delivery dates are from 15 August to 15 September 2016.</li>
                <li>A delivery fee of $50 nett will be charged per location.</li>
                <li>Complimentary delivery to one location is applicable for a minimum order of 50 boxes.</li>
                <li>Delivery date and slot, subject to availability.</li>
                <li>Purchases made online are non-cancellable and non-refundable.</li>
            </ul>
            
            <div class="section-title mt20">
				<h2 class="title">Collection</h2>
			</div>
            <ul>
            	<li>Collection of mooncakes can be made 5 working days from date of online order.</li> 
				<li>Collection dates are from 8 August to 15 September 2016.</li>
            </ul>
            
            <div class="section-title mt20">
				<h2 class="title">Location</h2>
			</div>
            <p>Self-collection for online orders will be available at:</p>
            <ul>
				<li>Fairmont Singapore’s Mooncake Booth, Level 2, Fairmont Singapore, 80 Bras Basah Road Singapore 189560</li>
				<li>Collection from 8 August to 15 September 2016, 10:00 AM to 10:00 PM daily.<br>
				</li>
            </ul>
            <p>For pre-orders, a minimum purchase of 2 boxes is required.</p>
        </div>
        <div class="col-xs-12 col-md-6 last mt20">
			<div class="block-panel block-grey block-mooncake-booths">
                <h2 class="mt0">Mooncake Booths</h2>
                
                
                <div class="row mt20">
                    <div class="col-md-5">
                    	<div class="title-a">FAIRMONT SINGAPORE</div>
                    	level 2, Fairmont Singapore’s Mooncake Booth
                    </div>
                    <div class="col-md-7">
                        8 Aug – 15 Sep<br>10am -10pm
                    </div>
                </div>
                
                
                <div class="row mt20">
                    <div class="col-md-5">
                    	<div class="title-a">VivoCity</div>
                        Level 1, Atrium
                    </div>
                    <div class="col-md-7">
                        16 Aug – 14 Sep<br>10am – 10pm
                    </div>
                </div>
                
                
                <div class="row mt20">
                    <div class="col-md-5">
                    <div class="title-a">Takashimaya</div>
                        Basement 2 Atrium,
                        Takashimaya Square
                    </div>
                    <div class="col-md-7">
                        18 Aug – 15 Sep<br>10.30am – 9.30pm
                    </div>
                </div>
                
        	</div>
        </div>
	</div><!-- row -->
</div><!-- sections -->