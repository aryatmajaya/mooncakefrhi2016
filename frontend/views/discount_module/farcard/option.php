<?php
$ci =& get_instance();
$session_discount = $ci->session->userdata("session_discount");
?>
<div class="radio">
    <label>
    	<?php if($session_discount && $session_discount['type']==$code){ ?>
        	<input type="radio" name="discount_module" id="farcard_check" value="<?php echo $code;?>" checked>
        <?php }elseif(!$session_discount && $default && $default->class==$code){ ?>
        	<input type="radio" name="discount_module" id="farcard_check" value="<?php echo $code;?>" checked>
        <?php }else{ ?>
        	<input type="radio" name="discount_module" id="farcard_check" value="<?php echo $code;?>">
        <?php } ?>
        
        <?php echo $title;?>
    </label>
</div>

<input type="text" style="margin-bottom:10px; display:none;" id="farcard_validation" name="farcard_validation">

<script language="javascript">
	$(function(){ 
		$("input[name=discount_module]:radio").change(function () {
			if($('#farcard_check').is(':checked')) {
				$("#farcard_validation").show();
			} else {
				$("#farcard_validation").hide();
			}
		});
	})
</script>