            <footer>
                <div class="row mt10">
                    <div class="footer-content col-xs-12 col-sm-5 col-lg-4">
                        <p>&copy;
                            <script type="text/javascript">
                                var dteNow = new Date();
                                var intYear = dteNow.getFullYear();
                                document.write(intYear);
                            </script>
                        RC Hotels (Pte). Ltd.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-lg-offset-2 col-lg-6">
                        <div class="row fbLike">
                            <div class="col-sm-6 col-xs-12">
                            	<ul class="list-inline">
                                	<li>
                                		<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FFairmontSingapore%2F%3Ffref%3Dts&width=75&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId=366775843451872" width="75" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                                	</li>
                                    <li>Fairmont Singapore</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                            	<ul class="list-inline">
                                	<li>
                                		<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FSwissotelTheStamford%2F%3Ffref%3Dnf&width=75&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId=366775843451872" width="75" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                                	</li>
                                    <li>Swissotel The Stamford</li>
                                 </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            
            <div class="container visible-xs">
                <div class="row">
                    <a href="#topPage" class="btn btn-danger btn-lg btn-block">Back to top</a>
                </div>
            </div>
		
        </div><!-- container -->
        <script src="<?= base_url("assets/js/jquery.min.js");?>"></script>
        <script src="<?php echo base_url("assets/js/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
		<script src="<?= base_url("assets/js/bootstrap.min.js");?>"></script>
        <script src="<?= base_url("assets/js/page.js");?>"></script>

		<script>
            $(function(){
      
                // START Generic dateblocking functions
                /* utility functions */
                function BlockCollectionDays(date) {
                    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
                    for (i = 0; i < disabledCollectionDays.length; i++) {
                        if($.inArray((m+1) + '-' + d + '-' + y,disabledCollectionDays) != -1 || new Date() > date) {
                            return [false];
                        }
                    }
                    return [true];
                }
        
                function BlockCollectionDaysConfig(date) {            
                    return BlockCollectionDays(date);
                }
                //End Generic dateblocking functions
        
                // START collection settings
                /* create an array of days which need to be disabled */
                <?
                $the_bc = "'".date("n-j-Y")."'";
                if ($bc){
                    foreach($bc as $bc){        
                        $value[] = "'".date("n-j-Y",strtotime($bc->block_date))."'";
                    }
                    $the_bc = implode(",", $value);
                }
                ?>
                var disabledCollectionDays = [<?=$the_bc?>]; 
                <?
                    //calculating minDate for Self Collect
                    if (date('Y-m-d') >= '2014-07-24')
                        $selfcollect_mindate = date('Y,m-1,d', strtotime('+3 days'));
                    else
                        $selfcollect_mindate = date('Y,m-1,d', strtotime('July 24, 2014 + 3 days'));
                ?>
                $('#selfcollect_date').datepicker({
                    showOn: "focus",
                    dateFormat: "dd MM yy",
                    altField: "#delivery_date_field",
                    altFormat: "yy-mm-dd",
                    minDate: new Date(2016,8-1,8),
                    maxDate: new Date(2016,9-1,15),
                    buttonImageOnly: true,           
                    beforeShowDay: BlockCollectionDaysConfig
                });
                // END collection Settings
        
                // FOR DELIVERY SETTINGS
                /* utility functions */
                function BlockDeliveryDays(date) {
                    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
                    for (i = 0; i < disabledDeliveryDays.length; i++) {
                        if($.inArray((m+1) + '-' + d + '-' + y,disabledDeliveryDays) != -1 || new Date() > date) {
                            return [false];
                        }
                    }
                    return [true];
                }
                function BlockDeliveryDaysConfig(date) {
                    return BlockDeliveryDays(date);
                }
                //End Generic dateblocking functions
        
                // START collection settings
                /* create an array of days which need to be disabled */
                <?
                    $the_bd = "'".date("n-j-Y")."'";
                    if ($bd){
                        foreach($bd as $bd){        
                            $bdvalue[] = "'".date("n-j-Y",strtotime($bd->block_date))."'";
                        }
                        $the_bd = implode(",", $bdvalue);
                    }
                ?>
                var disabledDeliveryDays = [<?=$the_bd?>]; 
                <?
                //calculating minDate for Delivery
                if (date('Y-m-d') >= '2014-07-20')
                    $delivery_mindate = date('Y,m-1,d', strtotime('+7 days'));
                else
                    $delivery_mindate = date('Y,m-1,d', strtotime('July 20, 2014 + 7 days'));
                ?>
                $('#delivery_date').datepicker({
                    showOn: "focus",
                    dateFormat: "dd MM yy",
                    altField: "#delivery_date_field",
                    altFormat: "yy-mm-dd",
                    //minDate: new Date(<?=$delivery_mindate;?>),
                    //maxDate: new Date(2014,9-1,05),
                    minDate: new Date(2016,8-1,15),
                    maxDate: new Date(2016,9-1,15),
                    buttonImageOnly: true,
                    beforeShowDay: BlockDeliveryDaysConfig
                });
        
                //Button trigger
                $("#selfcollect_date_button").on('click', function(e){
                    e.preventDefault();
                    $("#selfcollect_date").datepicker('show');
                })
        
                $("#delivery_date_button").on('click', function(e){
                    e.preventDefault();
                    $("#delivery_date").datepicker('show');
                })
                
                <?
                    //Collection caps
                    $collection_caps = $this->session->userdata('collection_caps');
                    if(!$collection_caps){
                        $collection_caps = array(
                            'delivery' => 0,
                            'self_collection' => 0
                        );
                    }
                ?>
                //Collection cap to be used for validation
                var collection_delivery_count = <?= $collection_caps['delivery']?>;
                var collection_selfcollection_count = <?= $collection_caps['self_collection']?>;
                collection_message = "";
                jQuery.validator.addMethod("delivery_count", function(value, element) {
                    //console.log(element, value);
                    var is_valid = false;
                    if(value == 0){
                        is_valid = true;
                    }
                    var collection_type = $("#collection_type").val();
                    if(collection_type == 'delivery' && collection_delivery_count < 2){
                        is_valid = true;
                    }else if(collection_type == 'self_collection' && collection_selfcollection_count < 3){
                        is_valid = true;
                    }			
                    return is_valid;
                },"You can only add 3 Self Collections and 2 Deliveries.");
        
                $('#fulfilment-form').validate({
                    rules: {
                        'fulfillment[collection_date]': 'required',
                        'fulfillment[delivery_date]': 'required',
                        'fulfillment[address]': 'required',
                        'fulfillment[postal_code]': 'required',
                        'fulfillment[recipient]': 'required',
                        'fulfillment[contact_number]': 'required'
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "fulfillment[selfcollect_date]") {
                            error.insertAfter("#collection_date_error");
                        } else if (element.attr("name") == "fulfillment[delivery_date]") {
                            error.insertAfter("#delivery_date_error");
                        }else if (element.attr("id") =="fulfillment-qty"){
                            error.insertAfter("#capping_error");
                        }else{
                            error.insertAfter(element);
                        }
                    }
                });
        
                
        
                $("#collection_type").on('change', function(){
                //	$("#fulfillment-qty").trigger('blur');
                });
            });
        </script>
        
        <script>
			$(function(){
				$("#cancel-checkout-btn").on('click', function(e){
					e.preventDefault();
					var confirmation = confirm("Canceling this page might remove your fulfillment records. Press Okay to continue.");
					if(confirmation){
						location.href="<?= site_url("cart/cancel_checkout");?>";
					}
				});	
				
				
				$(function(){ 
		$("input[name=discount_module]:radio").change(function () {
			if($('#farcard_check').is(':checked')) {
				$("#farcard_validation").show();
			} else {
				$("#farcard_validation").hide();
			}
		});
	})
				
				
			});
			
		</script>
        
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-36651814-7', 'auto');
		  ga('send', 'pageview');
		</script>
		
    </body>
</html>
