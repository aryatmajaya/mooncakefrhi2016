<?php
	$this->load->library('cart');
	$cart_items = $this->cart->total_items();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title><?= $header_title ?> - Celebrations Central Mooncakes</title>
        <meta name="description" content="">
        <meta name="author" content="">
		<link href="<?php echo base_url("assets/font-awesome/css/font-awesome.min.css");?>" type="text/css" rel="stylesheet">
        <!-- ICO -->
        <!--<link rel="shortcut icon" href="<?= base_url() ?>ico/favicon.ico">-->
        <!-- CSS -->
        <link href="<?= base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url("assets/css/jquery-ui/base/jquery-ui.css");?>" />
        <link href="<?= base_url("assets/css/main.css");?>" rel="stylesheet">
        <link href="<?= base_url("assets/css/overrides.css");?>" rel="stylesheet">
        
        <meta property="og:url"                content="<?php echo site_url();?>" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="Celebrations Central Mooncakes" />
        <meta property="og:description"        content="Savour an enthralling ensemble of mid-autumn treasures, cocooned in premium and elegant mooncake boxes. These creations resonate our commitment to timeless culinary heritage and authentically local traditions." />
        <meta property="og:image"              content="<?php echo base_url("assets/images/facebook-share.jpg");?>" />
 
        <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-36651814-7', 'auto');
        ga('send', 'pageview');
        </script>
    
    </head>
	<body>
    
    	
    	<div id="fb-root"></div>
		<script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    	
        <div class="container">
        	
            <div class="clearfix">
                <div class="top-cart pull-right">
                	<ul class="list-inline">
						<?php if ($this->session->userdata('user_id')): ?>
                            <li>Welcome <?= $this->session->userdata('first_name')?></li>
                        <?php endif ?>
                        <li><?php echo anchor("cart","My Cart (".$cart_items.")","class='cart-button'");?></li>     
						<?php if ($this->session->userdata('user_id')): ?>
                        	<li><?php echo anchor("myaccount","My Account");?></li>
                            <li><?php echo anchor("user/logout","Logout");?></li>
                        <?php else: ?>
                            <li><?php echo anchor("user/login","Login");?></li>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
                            
		
        <div class="brand-wrapper">
            <div class="row mt25">
                <div class="col-md-7 col-lg-7">
                    <div class="row">
                        <div class="col-xs-6 col-md-4 portal">
                        	<?php echo anchor('home','<img src="'.base_url("assets/images/logo-celebrations.jpg").'" class="img-responsive" alt="Celebration Central">');?>
                        </div><!-- portal -->
                        <div class="col-xs-6 col-md-8 event">
                        	<?php echo anchor('home','<img src="'.base_url("assets/images/logo-mooncake-selections.jpg").'" class="img-responsive" alt="Mooncake Selections">');?>
                        </div><!-- event -->
                    </div><!-- row -->
                </div>
                <div class="col-md-5 col-lg-5">
                	<div class="hotel-holder">
                        <ul class="list-inline">
                            <li>By:</li>
                            <li>
                                <a href="http://www.fairmont.com" target="_blank">
                                    <img src="<?= base_url("assets/images/logo-hotel-fairmont.jpg");?>" class="img-responsive" alt="Fairmont Singapore">
                                </a>
                            </li>
                            <li>
                                <a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank">
                                    <img src="<?= base_url("assets/images/logo-hotel-swissotel.jpg");?>" class="img-responsive" alt="Swissotel Singapore">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- brand-wrapper -->
		
		
		<div class="navigation">
			<div class="container">
				<div class="row">
					<nav class="navbar navbar-custom" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNav">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button> <!-- button -->
						</div><!-- navbar-header -->
		
						<!-- nav for larger screen -->
						<div class="collapse navbar-collapse" id="mainNav">
							<ul class="nav navbar-nav">
                            	<li <?php echo isset($menu_active) && $menu_active=='home'?'class="active"':'';?>>
									<?php echo anchor("home","Home");?>
                                </li>
                                <li  <?php echo isset($menu_active) && $menu_active=='products'?'class="active"':'';?>>
									<?php echo anchor("products","Mooncake Selection");?>
                                </li>
                                <li  <?php echo isset($menu_active) && $menu_active=='promotions'?'class="active"':'';?>>
									<?php echo anchor("promotions","Promotions");?>
                                </li>
                                <li  <?php echo isset($menu_active) && $menu_active=='delivery-collection'?'class="active"':'';?>>
									<?php echo anchor("delivery-collection","Delivery/Collection");?>
                                </li>
                                <li  <?php echo isset($menu_active) && $menu_active=='contact-us'?'class="active"':'';?>>
									<?php echo anchor("contact-us","Contact Us");?>
                                </li>
                                <li>
                                	<div class="fb-share-button" data-href="<?php echo site_url();?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
                                </li>
							</ul>
						</div>
					</nav> <!-- navbar -->
				</div><!-- row -->
			</div><!-- container -->
		</div><!-- navigation -->	
        
		<!-- page anchor -->
	  	<div id="topPage"></div><!-- topPage -->
	  	<!-- container -->