<div class="home-carousel-container">
    <div class="bg-flower"></div>
    <div id="home-carousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#home-carousel" data-slide-to="1">
            <li data-target="#home-carousel" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="<?= base_url("assets/images/slider/slider1.jpg");?>" alt="Home Slider 1">
            </div>
            <div class="item">
                <img src="<?= base_url("assets/images/slider/slider2.jpg");?>" alt="Home Slider 2">
            </div>
			<div class="item">
                <img src="<?= base_url("assets/images/slider/slider3.jpg");?>" alt="Home Slider 3">
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#home-carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#home-carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div><!-- carousel -->
</div>
​
​
<div class="sections">
    
    <div class="section-title clearfix mt20">
        <h2 class="title pull-left">What's New</h2>
        <a href="<?= site_url("products");?>" class="btn btn-primary pull-right hidden-xs">View All Products</a>
    </div>
​
    <div class="row">
        <?php foreach ($new_products_line_up as $key => $product_value): ?>
            <div class="col-xs-12 col-sm-6 col-md-4">
            	<div class="panel-item">
                    <div class="product-image-container">
                        <?php if ($product_value->is_new): ?>
                            <div class="ribbon-new"><img src="<?= base_url("assets/images/ribbon-new.png");?>"></div>
                        <?php endif ?>
                        <?php $product_images = json_decode($product_value->images); ?>
                        <img src="<?= base_url("img/products/".$product_images[0]); ?>" alt="thumb" class="img-responsive">
                    </div>
                	<h3><?= $product_value->product_name ?></h3>
                    <p>
                        <?php
                            if(empty($product_value->short_description)){
                                $product_value->short_description = '';
                            }
                        ?>
                        
                    </p>
            		<a href="<?= site_url("products");?>" class="btn btn-default order-now">Order Now</a>
            	</div><!-- panel-item -->
            </div>
        <?php endforeach ?>
    </div><!-- row -->
    
    
    <div class="section-title clearfix">
        <h2 class="title pull-left">Special Promotions</h2>
        <a href="<?= site_url("promotions");?>" class="btn btn-primary pull-right hidden-xs">View All Promotions</a>
    </div>
​
    <div class="section-item-holder">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <img src="<?= base_url("assets/images//promotions.jpg");?>" alt="cc" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
                <p>Savour an enthralling ensemble of mid-autumn treasures, cocooned in premium and elegant mooncake boxes. These creations resonate our commitment to timeless culinary heritage and authentically local traditions. Promotions as follow;</p>
                <ul>
                    <li>Online Early Bird: 30% discount (25 Jul 2016 – 07 Aug 2016)</li>
                    <li>Early Bird: 25% discount (08 Aug 2016 – 28 Aug 2016)
</li>
                    <li>Exclusive Discount for OCBC Credit Card: 20% discount (29 Aug 2016 – 15 Sep 2016)</li>
                    <li>Exclusive Discount for The FAR Card Members: 20% discount (Mon, 29 Aug till Wed, 07 Sept)</li>
                </ul>
              
              <p><a href="<?= site_url("promotions");?>" ><strong>Terms & Conditions apply</strong></a></p>
              
            </div>
        </div><!-- row -->
    </div>
    
</div><!-- sections -->