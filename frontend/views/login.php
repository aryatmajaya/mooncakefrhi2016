<div class="sub-page-title">
    <div class="bg-flower"></div>
    <h1><?= $header_title?></h1>
</div>
	
<div class="sections">
    <div class="login-section last basket-form">
    	<div class="row">
			<?php if ($this->session->flashdata('login_error')): ?>
                <div class="alert alert-danger"><?= $this->session->flashdata('login_error')?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('register_error')): ?>
                <div class="alert alert-danger"><p><?=$this->session->flashdata('register_error')?></p></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('inactive')): ?>
                <div class="alert alert-danger"><p>Your account has been blocked or set to inactive.</p></div>
            <?php endif; ?>
            
            <div class="col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-5 col-lg-push-7">
            	<div class="basket-form-sections last">
                    <div class="form-title">Existing Customer Login?</div>
                    <form name="login" class="form-horizontal log-in" role="form" method="POST" action="<?=site_url("user/login");?>">
                        <div class="form-group">
                            <label for="uName" class="col-xs-12 col-sm-5 col-md-4 col-lg-3 control-label">Login Email<span class="hidden-xs">:</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                                <input type="text" class="form-control" name="login[username]" value="<?= isset($username)?$username:''; ?>" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-xs-12 col-sm-5 col-md-4 col-lg-3 control-label">Password<span class="hidden-xs">:</span></label>
                            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                                <input type="password" class="form-control" id="password" name="login[password]" value="" >
                                <button class="btn btn-default mt20 col-xs-6 " type="submit">Login</button>
                                <a href="<?=site_url("user/forgotpassword");?>" class="btn-forgot col-xs-6">Forgot Password?</a>
                            </div>
                        </div>
                    </form>
               	</div>
            </div>

            <div class="col-sm-6 col-sm-pull-6 col-md-6 col-md-pull-6 col-lg-5 col-lg-pull-5">
                <div class="form-title">New Customer?</div>
                                <p>We respect your privacy and we will use the information provided to process your order and communicate with you via email upon confirmation of your purchase.</p>
                                
                <form name="register" class="form-horizontal" role="form" method="POST"
                    action="<?=site_url("user/register");?>">
                    <div class="form-group">
                        <label for="fName" class="col-xs-12 col-sm-6 control-label">First Name<span class="hidden-xs">:</span><span class="required">*</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" class="form-control" id="fName" name="register[first_name]" value="<?= isset($register) ? $register['first_name'] : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lName" class="col-xs-12 col-sm-6 control-label">Last Name<span class="hidden-xs">:</span><span class="required">*</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" class="form-control" id="lName" name="register[last_name]" value="<?= isset($register) ? $register['last_name'] : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mNumber" class="col-xs-12 col-sm-6 control-label">Mobile Number<span class="hidden-xs">:</span><span class="required">*</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="tel" class="form-control" id="mNumber" name="register[mobile]" value="<?= isset($register) ? $register['mobile'] : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="altNumber" class="col-xs-12 col-sm-6 control-label">Alt Contact Number<span class="hidden-xs">:</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="tel" class="form-control" id="altNumber" name="register[alt_contact]" value="<?= isset($register) ? $register['alt_contact'] : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Email<span class="hidden-xs">:</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="email" class="form-control" id="email" name="register[email]" value="<?= isset($register) ? $register['email'] : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Password<span class="hidden-xs">:</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="password" class="form-control" id="password" name="register[password]" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cPassword" class="col-xs-12 col-sm-6 control-label"><span class="required">*</span>Confirm Password<span class="hidden-xs">:</span></label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="password" class="form-control" id="cPassword" name="confirm_password" value="">
                            
                        </div>
                    </div>
                                        <div class="checkbox">
                        
                        
                            <input type="checkbox" class="" id="subscribe" name="register[subscribe]" value="1">I would like to receive exclusive news and updates from Fairmont Singapore &amp; Swiss&ocirc;tel The Stamford.
                        
                    </div>
                                        <div class="form-group">
                        <label for="password" class="col-xs-12 col-sm-6 control-label">&nbsp;</label>
                        <div class="col-xs-12 col-sm-6">
                            <button class="btn btn-default mt20" value="register" name="form_action">Register</button>
                        </div>
                    </div>
                                    
                </form>
            </div>

    	</div>
    </div>
</div><!-- section -->