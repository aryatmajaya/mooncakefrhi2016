<div class="sub-page-title">
	<div class="bg-flower"></div>
    <h1><?= $header_title ?></h1>
</div>

<div class="sections">

		<div class="row basket-section last basket-form">
			<div class="col-xs-8 col-sm-10 col-md-8 basket-form-sections">
			
				<? if (validation_errors()): ?>
					<div class="alert alert-error mt20"><?= validation_errors() ?></div>
				<? endif; ?>
				<? if ($this->session->flashdata('account_error')): ?>
					<div class="alert alert-error mt20"><p><?=$this->session->flashdata('account_error')?></p></div>
				<? endif; ?>
				<? if ($this->session->flashdata('account_success')): ?>
					<div class="alert alert-success mt20">User profile saved!</div>
				<? endif; ?>
                
				<form name="customer" class="form-horizontal mt20" role="form" method="POST" action="<?=site_url("myaccount/save");?>">
				  <div class="form-group">
					<label for="fName" class="col-xs-12 col-sm-6 control-label">First Name<span class="hidden-xs">:</span><span class="required">*</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="text" class="form-control" id="fName" name="customer[first_name]" 
					  	value="<?= isset($customer) ? $customer->first_name : ''; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label for="lName" class="col-xs-12 col-sm-6 control-label">Last Name<span class="hidden-xs">:</span><span class="required">*</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="text" class="form-control" id="lName" name="customer[last_name]" value="<?= isset($customer) ? $customer->last_name : ''; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label for="mNumber" class="col-xs-12 col-sm-6 control-label">Mobile Number<span class="hidden-xs">:</span><span class="required">*</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="tel" class="form-control" id="mNumber" name="customer[mobile]" value="<?= isset($customer) ? $customer->mobile : ''; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label for="altNumber" class="col-xs-12 col-sm-6 control-label">Alt Contact Number<span class="hidden-xs">:</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="tel" class="form-control" id="altNumber" name="customer[alt_contact]" value="<?= isset($customer) ? $customer->alt_contact : ''; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label for="email" class="col-xs-12 col-sm-6 control-label">Email<span class="hidden-xs">:</span><span class="required">*</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="email" class="form-control" id="email" name="customer[email]" value="<?= isset($customer) ? $customer->email : ''; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label for="password" class="col-xs-12 col-sm-6 control-label">Password<span class="hidden-xs">:</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="password" class="form-control" id="password" name="customer[password]" value="">
					</div>
				  </div>
				  <div class="form-group">
					<label for="cPassword" class="col-xs-12 col-sm-6 control-label">Confirm Password<span class="hidden-xs">:</span></label>
					<div class="col-xs-12 col-sm-6">
					  <input type="password" class="form-control" id="cPassword" name="confirm_password" value="">
					  <button class="btn btn-default mt20" name="form_action">Save profile</button>
					</div>
				  </div>
				</form>
			</div>
		</div>

</div>