<?


$subtotal = 0;
$discount = 0;
$discounted_amount = 0;
$number_item_discount = 0;
$number_item_discounted_amount = 0;
$gst_charge = 0;
$total_delivery_charge = 0;
$grandtotal = 0;

$total_qty =0;
foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal += $price_total;
	$total_qty += $item->qty;
}

if($order->promo_code_discount > 0){
	$discount = $order->promo_code_discount;
	$discounted_amount = ($subtotal*$order->promo_code_discount)/100;
}elseif($order->card_discount > 0){
	$discount = $order->card_discount;
	$discounted_amount = ($subtotal*$order->card_discount)/100;
}

/*if($order->number_item_discount){
	$number_item_discount = $order->number_item_discount;
	$number_item_discounted_amount = ($subtotal*$order->number_item_discount)/100;
}*/


$total_delivery_charge = Utilities::Calculate_Delivery_Charges($total_qty, $fo[0]->service_type);

//$gst_charge = ($subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge) * (GST_PERCENT /100);

$grandtotal = $subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge + $gst_charge;

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Your Order Confirmation at Exquisite Mooncake Selection</title>
		<!--[if mso]>
        <style type="text/css">
        	body, table, td {font-family: Arial, Helvetica, sans-serif !important;}
        </style>
        <![endif]-->
        
        <style>
        	table{
				border-collapse:collapse;	
			}
        </style>
    </head>
    
    <body>
    	
        <table width="100%" cellpadding="0" cellspacing="0">
    	<tr>
        	<td align="center" bgcolor="#e8e8e8" style="font-family:Helvetica, arial, sans-serif; font-size:14px;">
            	<table width="700" cellpadding="0" cellspacing="0">
                	<tr>
                    	<td bgcolor="#ffffff">
                        
                        	<!-- header begin -->
                            <table width="100%" style="border-top:3px solid #673b25;">
                            	<tr>
                            		<td style="padding:20px;">
                                    	<table width="100%">
                                        	<tr>
                                            	<td width="50%" align="left">
                                                	<a href="<?php echo site_url();?>"><img src="<?php echo base_url("assets/images/logo-celebrations.jpg");?>"></a>
                                                </td>
                                                <td width="50%" align="right">
                                                	<a href="<?php echo site_url();?>"><img src="<?php echo base_url("assets/images/logo-mooncake-selections.jpg");?>"></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- header end -->
							
                            <table width="100%">
                            	<tr>
                            		<td style="padding:20px;" bgcolor="#f6f6f6">
                                    	<table width="100%">
                                        	<tr>
                                            	<td colspan="2" style="font-size:16px;" width="100%"><strong>Dear <?=$order->first_name.' '.$order->last_name?>,</strong></td>
                                            </tr>
                                            <tr><td height="3" colspan="2"></td></tr>
                                        	<tr>
                                            	<td width="50%" style="color:#696969; font-size:14px;">Thank you for ordering with us at Celebrations Central, please find the details of your orders below.</td>
                                                <td width="50%" align="right" style="font-size:15px;"><strong>ORDER REF: <?=$order->order_ref;?></strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            
                            <!-- order information begin -->
                            <table width="100%">
                            	<tr>
                            		<td style="padding:20px;">
                                    	<table width="100%">
                                        	<tr>
                                            	<td width="50%" valign="top">
                                                	<table width="100%">
                                                    	<tr><td style="color:#f99e48" width="100%"><strong>ORDER DETAILS</strong></td></tr>
                                                        <tr><td height="12" width="100%"></td></tr>
                                                    </table>
                                                	<table width="100%">
                                                    	<tr><td style="font-size:13px; color:#6b6b6b">Order Date</td></tr>
                                                        <tr><td><strong><?=date("d F Y", strtotime($order->date_ordered))?></strong></td></tr>
                                                        <tr><td height="10"></td></tr>
                                                        <tr><td style="font-size:13px; color:#6b6b6b">Customer</td></tr>
                                                        <tr><td><strong><?=$order->first_name.' '.$order->last_name?></strong></td></tr>
                                                        <tr><td height="10"></td></tr>
                                                        <tr><td style="font-size:13px; color:#6b6b6b">Mobile</td></tr>
                                                        <tr><td><strong><?=$order->mobile;?></strong></td></tr>
                                                        <tr><td height="10"></td></tr>
                                                        <tr><td style="font-size:13px; color:#6b6b6b">Email</td></tr>
                                                        <tr><td><strong><?=$order->email?></strong></td></tr>
                                                        <tr><td height="10"></td></tr>
                                                    </table>
                                                </td>
                                                <td width="50%" valign="top">
                                                	<table width="100%">
                                                    	<tr><td style="color:#f99e48" width="100%"><strong>FULLFILLMENT</strong></td></tr>
                                                        <tr><td height="12" width="100%"></td></tr>
                                                    </table>
                                                    
                                                	<table width="100%" style="border:1px solid #dcdcdc">
                                                    	<tr>
                                                        	<td bgcolor="#fafafa" style="padding:20px;">
                                                            	<table width="100%">
                                                                    <tr><td style="font-size:13px; color:#6b6b6b">Option</td></tr>
                                                                    <tr><td><strong><?php 
																	if($fo[0]->service_type=="self_collection"){
																		echo "Self Collection";
																	}elseif($fo[0]->service_type=="delivery"){
																		echo "Delivery";
																	}else{
																		echo $fo[0]->service_type;	
																	}
																	?></strong></td></tr>
                                                                    <tr><td height="10"></td></tr>
                                                                    <tr><td style="font-size:13px; color:#6b6b6b">Date</td></tr>
                                                                    <tr><td><strong><?php echo date("d F Y", strtotime($fo[0]->service_date));?></strong></td></tr>
                                                                    <tr><td height="10"></td></tr>
                                                                    
																	<?php if($fo[0]->service_type == "delivery" && $fo[0]->time_slot){?>
                                                                    <tr><td style="font-size:13px; color:#6b6b6b">Time Slot</td></tr>
                                                                    <tr><td><strong><?php echo $fo[0]->time_slot;?></strong></td></tr>
                                                                    <tr><td height="10"></td></tr>
                                                                    <?php } ?>
                                                                    
                                                                    <?php if ($fo[0]->service_type == "delivery"){ ?>
                                                                        <tr><td style="font-size:13px; color:#6b6b6b">Address</td></tr>
                                                                        <tr><td><strong><?php echo $fo[0]->address.', '.$fo[0]->postal_code;?></strong></td></tr>
                                                                        <tr><td height="10"></td></tr>
                                                                        
                                                                        <tr><td style="font-size:13px; color:#6b6b6b">Delivered To</td></tr>
                                                                        <tr><td><strong><?php echo $fo[0]->delivered_to;?></strong></td></tr>
                                                                        <tr><td height="10"></td></tr>
                                                                        
                                                                        <tr><td style="font-size:13px; color:#6b6b6b">Contact No</td></tr>
                                                                        <tr><td><strong><?php echo $fo[0]->delivered_contact;?></strong></td></tr>
                                                                        <tr><td height="10"></td></tr>
                                                                        
                                                                     <?php } ?>
                                                                     
                                                                     <?php if($order->ordering_method=="corporate" && $order->special_requirements){ ?>
                                                                     	<tr><td style="font-size:13px; color:#6b6b6b">Additional Request</td></tr>
                                                                        <tr><td><strong><?php echo $order->special_requirements;?></strong></td></tr>
                                                                        <tr><td height="10"></td></tr>
                                                                     <?php }elseif($order->ordering_method!="corporate" && $fo[0]->additional_request){ ?>
                                                                     	<tr><td style="font-size:13px; color:#6b6b6b">Additional Request</td></tr>
                                                                        <tr><td><strong><?php echo $fo[0]->additional_request;?></strong></td></tr>
                                                                        <tr><td height="10"></td></tr>
                                                                     <?php } ?>
                                                                    
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- order information end -->
							
                            <!-- Order Summary -->
                            <table width="100%">
                            	<tr>
                            		<td style="padding:20px;">
                                    	<table width="100%">
                                            <tr><td style="color:#f99e48" width="100%"><strong>ORDER SUMMARY</strong></td></tr>
                                            <tr><td height="12" width="100%"></td></tr>
                                        </table>
                                        <table width="100%" style="border:1px solid #dcdcdc">
                                            <tr>
                                                <td bgcolor="#fafafa" style="padding:20px;">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="50%" style="font-size:13px; color:#6b6b6b">Product Name</td>
                                                                        <td width="50%">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td width="40%" style="font-size:13px; color:#6b6b6b">Unit Price</td>
                                                                                    <td width="20%" style="font-size:13px; color:#6b6b6b">Qty</td>
                                                                                    <td width="40%" style="font-size:13px; color:#6b6b6b">Total</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td colspan="2" height="3"></td></tr>
                                                                    
                                                                    <?
                                                                    $total_qty = 0;
                                                                    $subtotal = 0;
                                                        
                                                                    foreach ($items as $item) {
                                                                    $price_total = $item->qty * $item->price;
                                                                    ?>
                                                                    
                                                                    <tr>
                                                                        <td width="50%" style="font-size:14px;"><strong><? echo html_entity_decode($item->product_name); ?></strong></td>
                                                                        <td width="50%">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td width="40%" style="font-size:14px;">SGD <? echo number_format($item->price, 2); ?></td>
                                                                                    <td width="20%" style="font-size:14px;"><? echo $item->qty; ?></td>
                                                                                    <td width="40%" style="font-size:14px;">SGD <? echo number_format($price_total, 2); ?></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td colspan="2" height="3"></td></tr>
                                                                    <?
                                                                        $total_qty = $total_qty + $item->qty;
                                                                        $subtotal = $subtotal + $price_total;
                                                                        $discounted_amount = $subtotal * (($order->promo_code_discount + $order->card_discount + $order->corporate_discount)/100);
                                                                    }?>
                                                                    <tr><td colspan="2" height="12"></td></tr>
                                                                    <tr><td width="50%"></td><td width="50%" style="border-top:1px solid #dcdcdc;"></td></tr>
                                                                    <tr><td colspan="2" height="15"></td></tr>
                                                                    <tr>
                                                                        <td width="50%"></td>
                                                                        <td width="50%">
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td width="50%"><strong>Sub Total</strong></td>
                                                                                    <td width="50%" align="right"><strong>SGD <? echo number_format($subtotal,2); ?></strong></td>
                                                                                </tr>
                                                                                <tr><td colspan="2" height="3"></td></tr>
                                                                                <tr>
                                                                                    <td width="50%">Discount (<?php echo $discount?>%)</td>
                                                                                    <td width="50%" align="right" style="color:#f99e48;">SGD -<? echo number_format($discounted_amount,2); ?></td>
                                                                                </tr>
                                                                                <tr><td colspan="2" height="3"></td></tr>
                                                                                <?php if($fo[0]->service_type == "delivery"){?>
                                                                                    <tr>
                                                                                        <td width="50%">Delivery Charge</td>
                                                                                        <td width="50%" align="right">SGD <?=number_format($total_delivery_charge,2); ?></td>
                                                                                    </tr>
                                                                                    <tr><td height="15" colspan="2"></td></tr>
                                                                                <?php } ?>
                                                                                <tr><td style="border-top:1px solid #dcdcdc;" colspan="2"></td></tr>
                                                                                <tr><td height="15" colspan="2"></td></tr>
                                                                                <tr>
                                                                                    <td width="50%" style="font-size:16px;"><strong>Grand Total</strong></td>
                                                                                    <td width="50%" style="font-size:16px;" align="right"><strong>SGD <?=number_format($grandtotal,2);?></strong></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            
                            <!-- separator -->
                            <table width="100%">
                            	<tr><td height="44" width="100%"></td></tr>
                            </table>
                            
                            <!-- footer begin -->
                            <table width="100%">
                            	<tr>
                                	<td width="100%" style="padding-bottom:40px; background-position:bottom -6px center; background-size:100% auto; background-repeat:no-repeat; background-image:url('<?php echo base_url("assets/images/bg-footer.png");?>')">
                                        <table width="100%">
                                            <tr>
                                                <td style="padding:20px;" align="center">
                                                    <table>
                                                        <tr><td align="center">&copy; <?php echo date("Y");?> Fairmont Singapore & Swiss&ocirc;tel The Stamford.</td></tr>
                                                        <tr><td colspan="2" height="3"></td></tr>
                                                        <tr><td align="center">For enquiries, please call +65 6338 8785 (9am - 5.30pm daily)</td></tr>
                                                        <tr><td colspan="2" height="3"></td></tr>
                                                        <td style="font-style:italic; color:#6b6b6b; font-size:12px;" align="center">This is system generated, please do not reply to this email.</td>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- footer end -->
                            
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        
    </body>
</html>
    
