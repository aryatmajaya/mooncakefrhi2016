<?
$subtotal = 0;
 
$grandtotal = 0;
foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal = $subtotal + $price_total;
}

//$grandtotal = $subtotal - $order->discounted_amount;



$total_delivery_charge = 0;
//if ($review_page == "on"){
$total_delivery_charge = Delivery_model::get_total_delivery_charge($delivery_qty);
$grandtotal = $subtotal - $order->discounted_amount;
$gst_charge = ($subtotal - $order->discounted_amount + $total_delivery_charge) * (GST_PERCENT /100);
//hack: remove gst charge for frhi
$gst_charge = 0;
$grandtotal = $grandtotal + $total_delivery_charge + $gst_charge;

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Your Order Confirmation at Exquisite Mooncake Selection</title>

    </head>
    
    <body>
    <table width="100%" style="border-collapse:collapse;">
        <tr>
            <td style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><img src="<?=base_url("assets/images/logo-celebrations.jpg");?>" border="0"></td>
            <td align="right" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">Exquisite Mooncake Selection</td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">
                <strong>Dear <?=$order->first_name.' '.$order->last_name?></strong><br />
                Thank you for ordering your mooncakes.<br />
                Your order confirmation details are as follows:
            </td>   
        </tr>
    </table>
    <br /><br />

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
              <td width="20%" align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Order Date: </strong></td>
              <td width="30%" align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=date("d.m.y", strtotime($order->date_ordered))?></td>
              <td width="20%" align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Customer: </strong></td>
              <td width="30%" align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=$order->first_name.' '.$order->last_name?></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Order Ref: </strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=$order->order_ref;?></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Mobile: </strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=$order->mobile;?></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Approval Code: </strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=$order->approval_code?></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Alt Contact #:</strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=(($order->alt_contact)?$order->alt_contact:'&nbsp;');?></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Discount: </strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">
				<?=($order->promo_code_discount + $order->card_discount + $order->corporate_discount);?>%
            </td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Email: </strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=$order->email?></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Amount: </strong></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD <?=number_format($grandtotal,2);?></td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">&nbsp;</td>
            <td align="left" valign="top" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">&nbsp;</td>
          </tr>
        </table>
    
        
        
    
        <br />
        <table width="100%" border="1" cellspacing="0" cellpadding="5" class="text_12px" style="border-collapse:collapse;">
            <tr>

                <td width="50%" align="left" valign="top" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong> Product Name </strong></td>
                <td width="20%" align="center" valign="top" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong> Unit Price </strong></td>
                <td width="10%" align="center" valign="top" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong> Qty </strong></td>
                <td width="20%" align="center" valign="top" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong> Total </strong></td>
                
            </tr>
            <?
            $total_qty = 0;
            $subtotal = 0;

            foreach ($items as $item) {
                $price_total = $item->qty * $item->price;
                ?>        
                <tr>
                    <td style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><? echo html_entity_decode($item->product_name); ?></td>
                    <td align="center" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD <? echo number_format($item->price, 2); ?></td>
                    <td align="center" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><? echo $item->qty; ?></td>
                    <td align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD <? echo number_format($price_total, 2); ?></td>
                </tr>
                <?
                $total_qty = $total_qty + $item->qty;
                $subtotal = $subtotal + $price_total;
                $discounted_amount = $subtotal * (($order->promo_code_discount + $order->card_discount + $order->corporate_discount)/100);
            }
            ?>
                <tr>
                    <td colspan="3" align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Sub-total:</strong></td>
                    <td align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD <? echo number_format($subtotal,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Discount: <?=($order->promo_code_discount + $order->card_discount + $order->corporate_discount );?>%</strong></td>
                    <td align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD -<? echo number_format($discounted_amount,2); ?></td>
                </tr>
                <?
                
                ?>
                <tr>
                    <td colspan="3" align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Delivery Charge(s): </strong></td>
                    <td align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD <?=number_format($total_delivery_charge,2); ?></td>
                 </tr>
                 <?php
                /* <tr>
                    <td colspan="3" align="right"><strong>GST Charge: </strong></td>
                    <td align="right">SGD <?=number_format($gst_charge,2); ?></td>
                 </tr>*/
                  ?>  
                 <? 
                    //}
                    ?>
                <tr>
                    <td colspan="3" align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Total Amount:</strong></td>
                    <td align="right" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">SGD <?=number_format($grandtotal,2);?></td>
                </tr>
        </table>
        <!-- END items table -->
        
    <!-- START FULFILLMENT OPTIONS -->
    <? if ($fo){ ?>
    <h2>FULFILLMENT OPTIONS</h2>
    <?
    foreach ($fo as $fo){
    ?>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" style="border-collapse:collapse;">
        <tr>
            <td style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">
                <?
                echo '<strong>'.ucwords($fo->service_type).': </strong>'.date("d F Y", strtotime($fo->service_date)).(($fo->time_slot)?', '.$fo->time_slot:'');
                if ($fo->service_type == "delivery"){
                    echo '<br /><strong>Address:</strong> '.$fo->address.', '.$fo->postal_code;
                    echo '<br /><strong>Delivered To:</strong> '.$fo->delivered_to;
                    echo '<br /><strong>Contact No.:</strong> '.$fo->delivered_contact;
                }
                ?>
                <br /><br /><strong>Collection:</strong> Szechuan Court Mooncake Booth, Level 2, Fairmont Singapore, 80 Bras Basah Road Singapore 189560.
            </td>
        </tr>
    </table>
    <table width="100%" border="1" cellpadding="5" cellspacing="0" style="border-collapse:collapse;">
        
            <tr>
                <td style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Items</strong></td>
                <td align="center" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><strong>Qty</strong></td>
            </tr>
            
       
            <? 
            $delivery_items = Delivery_model::get_delivery_option_items2($fo->order_ref);
                   
            foreach ($delivery_items as $di) {
            ?>
            <tr>
                <td style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><?=$di->product_name?></td>
                <td align="center" style="border:1px solid #333; font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica"><? echo $di->qty;?></td>
            </tr>
            <? } ?>
        
    </table>
    <br />
    <? } ?>
    <!-- END FULFILLMENT OPTIONS -->
    <? } //End if ($fo)?>

<table width="100%" border="0" cellpadding="20" cellspacing="0" style="border-collapse:collapse;">
    <tr>
        <td align="center" style="font-family:Arial, sans-serif, Gotham, 'Helvetica Neue', Helvetica">
          <p>For enquiries, please call <strong>+65 6338 8785</strong> or email <strong><a href="mailto:fairmontsingapore.mooncakes@fairmont.com">fairmontsingapore.mooncakes@fairmont.com</a></strong>.</p>
          <p>This is system generated, please do not reply to this email.</p>
        </td>
    </tr>
</table>
        
    </body>
</html>
    
