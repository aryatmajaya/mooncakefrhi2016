<div class="sub-page-title">
	<div class="bg-flower"></div>
	<h1><?= $header_title ?></h1>
</div>

<div class="sections">
	<div class="alert alert-danger mt20" role="alert">
  		<?= $order_detail->error_message; ?>
	</div>
</div>