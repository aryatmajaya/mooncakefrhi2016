<?php
	$total_qty = 0;
	$subtotal = 0;
	
	foreach ($order_items as $key => $value) {
		$total_qty += $value->qty;
		
		$price_total = $value->qty * $value->price;    
		$subtotal += $price_total;
	
	}
	
	
	$total_delivery_charge = Utilities::Calculate_Delivery_Charges($total_qty, $fullfilments[0]->service_type);
?>
<div class="sub-page-title">
	<div class="bg-flower"></div>
	<h1><?= $header_title ?></h1>
</div>

<div class="sections">

    <div class="section-title mt20">
        <h2 class="title">Your order has been successfully processed.</h2>
    </div>
   
    <div class="row">
        <div class="col-md-5 col-sm-5">
        	<p class="title-a">Order Details</p>
            <p>Order date:<br><?= $order_detail->date_ordered?></p>
            <p>Order reference:<br><?= $order_detail->order_ref ?></p>
            <p>Approval code:<br><?= $order_detail->approval_code?></p>
            <p>Discount:<br><?= $order_detail->card_discount?>%</p>
            <p>Delivery Charge:<br>SGD <?= $total_delivery_charge ?></p>
            <p>Amount:<br>SGD <?= number_format($order_detail->total_amount - $order_detail->discounted_amount + $total_delivery_charge, 2) ?></p>
        </div>
        <div class="col-md-5 col-sm-5 col-md-offset-1">
        	<p class="title-a">Customer Details</p>
            <p>Customer:<br><?= $customer->first_name . ' ' . $customer->last_name ?></p>
            <p>Mobile:<br><?= $customer->mobile ?></p>
            <p>Alt contact:<br><?= $customer->alt_contact ?></p>
            <p>Email:<br><?= $customer->email ?></p>
        </div>
    </div>
    
    <div class="section-title mt20">
        <h2 class="title">Product Items</h2>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <td class="col-md-1">#</td>
                <th class="col-md-9">Item</th>
                <th>Quantity</th>
            </thead>
            <tbody>
                <?php foreach ($order_items as $key => $value): ?>
                    <tr>
                        <td>
                            <?= $key+1 ?>
                        </td>
                        <td>
                            <?= $value->product_name ?>
                        </td>
                        <td>
                            <?= $value->qty ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2"><span class="text-right">Total Qty:</span></th>
                    <td><?= $total_qty ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    
    <?php foreach ($fullfilments as $key => $value): ?>
        <div class="fulfillment-list">
            <div class="">
                <?php
				$service_type = "Delivery";
				if($value->service_type=="self_collection"){
					$service_type = "Self Collection";
				}
                echo '<strong>'.$service_type.': </strong>'.date("d F Y", strtotime($value->service_date)).(($value->time_slot)?', '.$value->time_slot:'').'<br />';
                ?>
                
                <address>
                    <?php if ($value->service_type == "delivery"){ ?>
                        <strong>Address:</strong> <span><?php echo $value->address.', '.$value->postal_code;?></span><br />
                        <strong>Delivered To:</strong> <span><?php echo $value->delivered_to;?></span><br />
                        <strong>Contact No.:</strong> <span><?php echo $value->delivered_contact;?></span><br />
                    <?php } else { ?>
                        <strong>Collection:</strong> <?php echo $site_config['collection_address'] ?>.<br />
                    <?php } ?>
                    
                    <?php if($value->additional_request){ ?>
                        <strong>Additional Request:</strong> <?php echo $value->additional_request;?>
                    <?php } ?>
                    
                </address>
                
                
            </div>
        </div>
    <?php endforeach ?>
    
    <p style="margin-top:30px;"><a href="<?= site_url("products");?>" class="btn btn-default">Order More</a></p>
	
</div>