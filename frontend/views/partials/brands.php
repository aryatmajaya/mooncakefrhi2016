<!-- design structure -->
<div class="brand-wrapper hidden-xs">
	<div class="container">
		<div class="row mt25">
			<div class="col-md-7 col-lg-8">
				<div class="row">
					<div class="col-sm-6 col-md-4 portal">
						<a href="<?= base_url() ?>"><img src="<?= base_url() ?>/img/logo-celebrations.jpg" class="img-responsive" alt="Celebration Central"></a>
					</div><!-- portal -->
					<div class="col-sm-6 col-md-8 event">
						<a href="<?= base_url() ?>"><img src="<?= base_url() ?>/img/logo-premium.jpg" class="img-responsive" alt="Premium Mooncake"></a>
					</div><!-- event -->
				</div><!-- row -->
			</div>
			<div class="col-md-5 col-lg-4">
				<div class="hotel-holder clearfix">
					<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
						<?php if('www.swissotel.com' == parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST ) ): ?>
							<a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-swissotel.jpg" class="img-responsive" alt="Swissotel Singapore"></a>
						<?php endif ?>
						<?php if('www.fairmont.com' == parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST)): ?>
							<a href="http://www.fairmont.com" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-fairmont.jpg" class="img-responsive" alt="Fairmont Singapore"></a>
						<?php endif ?>
						<?php if(!in_array(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), array('www.swissotel.com', 'www.fairmont.com')) ): ?>
							<a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-swissotel.jpg" class="img-responsive" alt="Swissotel Singapore"></a>
							<a href="http://www.fairmont.com" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-fairmont.jpg" class="img-responsive" alt="Fairmont Singapore"></a>
						<?php endif ?>
					<?php else: ?>
						<a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-swissotel.jpg" class="img-responsive" alt="Swissotel Singapore"></a>
						<a href="http://www.fairmont.com" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-fairmont.jpg" class="img-responsive" alt="Fairmont Singapore"></a>
					<?php endif ?>
					
					<p>By:</p>
				</div>
			</div>
		</div>
	</div><!-- container --> 
</div><!-- brand-wrapper -->