<div class="brand-wrapper-xs container visible-xs">
	<div  class="row">
		<div class="col-xs-6 portal">
			<a href="<?= base_url() ?>"><img src="<?= base_url() ?>/img/logo-celebrations.jpg" class="img-responsive" alt="Celebration Central"></a>
		</div><!-- brand-holder -->
		<div class="col-xs-6 event">
			<a href="<?= base_url() ?>"><img src="<?= base_url() ?>/img/logo-premium.jpg" class="img-responsive" alt="Premium Mooncake"></a>
		</div>
		<div class="col-xs-12">
			<div class="row hotel-holder">
				<div class="col-xs-12">
					<p>By:</p>
				</div>
				<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
					<?php if('www.fairmont.com' == parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST ) ): ?>
						<div class="col-xs-12">
							<a href="http://www.fairmont.com" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-fairmont.jpg" class="img-responsive" alt="Fairmont Singapore"></a>
						</div>
					<?php endif ?>
					<?php if('www.swissotel.com' == parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST ) ): ?>
						<div class="col-xs-12">
							<a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-swissotel.jpg" class="img-responsive" alt="Swissotel Singapore"></a>
						</div>
					<?php endif ?>

					<?php if(!in_array(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), array('www.swissotel.com', 'www.fairmont.com')) ): ?>
						<div class="col-xs-12">
							<a href="http://www.fairmont.com" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-fairmont.jpg" class="img-responsive" alt="Fairmont Singapore"></a>
						</div>
						<div class="col-xs-12">
							<a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-swissotel.jpg" class="img-responsive" alt="Swissotel Singapore"></a>
						</div>
					<?php endif ?>
				<?php else: ?>
					<div class="col-xs-12">
						<a href="http://www.fairmont.com" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-fairmont.jpg" class="img-responsive" alt="Fairmont Singapore"></a>
					</div>
					<div class="col-xs-12">
						<a href="http://www.swissotel.com/hotels/singapore-stamford/" target="_blank"><img src="<?= base_url() ?>/img/logo-hotel-swissotel.jpg" class="img-responsive" alt="Swissotel Singapore"></a>
					</div>
				<?php endif ?>

			</div><!-- hotel-holder -->      
		</div>
  </div> <!-- row visible-xs  -->
</div><!-- container -->