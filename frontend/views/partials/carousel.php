<div class="header hidden-xs">
	<div class="container">
		<div class="row">
			<div id="banner-carousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#banner-carousel" data-slide-to="1"></li>
					<li data-target="#banner-carousel" data-slide-to="2"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img src="<?= base_url() ?>img/img-hdr-01.jpg" alt="hdr1">
					</div>
					<div class="item">
						<img src="<?= base_url() ?>img/img-hdr-02.jpg" alt="hdr2">
					</div>
					<div class="item">
						<img src="<?= base_url() ?>img/img-hdr-03.jpg" alt="hdr13">
					</div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#banner-carousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#banner-carousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div><!-- carousel -->
		</div><!-- row -->
	</div><!-- container -->
</div><!-- header -->