<footer>
	<div class="container">
		<div class="row mt10">
			<div class="footer-content col-xs-12 col-sm-5 col-lg-4">
				<p>&copy;
					<script type="text/javascript">
						var dteNow = new Date();
						var intYear = dteNow.getFullYear();
						document.write(intYear);
					</script>
				RC Hotels (Pte). Ltd.
				</p>
			</div>
			<div class="col-xs-12 col-sm-7 col-lg-offset-2 col-lg-6">
				<div class="row fbLike">
					<div class="col-xs-6">
						
						<!-- <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FFairmontSingapore&amp;width=95&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; width:95px;" allowTransparency="true"></iframe> -->
						<!--<p>Fairmont Singapore</p>-->
						
					</div>
					<div class="col-xs-6">

					<!--  <iframe src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FSwissotelTheStamfordSingapore&amp;width=95&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; width:95px;" allowTransparency="true"></iframe> -->
						<!--<p>Swissotel The Stamford</p>-->
					</div>
				</div>
			</div>
		</div>
	</div><!-- container -->
</footer>