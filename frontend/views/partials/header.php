<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title><?= $header_title ?> - Celebrations Central Mooncakes</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
	<!-- ICO -->
	<!--<link rel="shortcut icon" href="<?= base_url() ?>ico/favicon.ico">-->
	<!-- CSS -->
	<link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>css/main.css" rel="stylesheet">
	<link href="<?= base_url() ?>css/overrides.css" rel="stylesheet">
	
	<!-- <link href="<?= base_url() ?>css/main-non-responsive.css" rel="stylesheet"> -->

	<script src="<?= base_url() ?>/js/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
    
    <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-36651814-7', 'auto');
    ga('send', 'pageview');
    </script>

</head>