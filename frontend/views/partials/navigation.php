<?php
	$this->load->library('cart');
	$cart_items = $this->cart->total_items();
?>
<div class="navigation">
	<div class="container">
		<div class="row">
			<nav class="navbar navbar-custom" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNav">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button> <!-- button -->
					<div class="secondary-nav visible-xs">
						<?php if ($this->session->userdata('user_id')): ?>
							Welcome <a href="<?= base_url() ?>myaccount" class="<?= ($this->router->fetch_class() == 'myaccount') ? 'selected' : '' ?>"><?= $this->session->userdata('first_name')?></a>	
						<?php endif ?>&nbsp;
						<span><a style="color:#af0000;" href="<?= base_url() ?>cart">My Cart (<?= $cart_items ?>)</a></span>
						<?php if ($this->session->userdata('user_id')): ?>
							<a href="<?= base_url() ?>user/logout">Logout</a>
						<?php else: ?>
							<a href="<?= base_url() ?>user/login">Login</a>
						<?php endif ?>
						
					</div><!-- secondary-nav -->
				</div><!-- navbar-header -->

				<!-- nav for larger screen -->
				<div class="collapse navbar-collapse" id="mainNav">
					<ul class="nav navbar-nav">
						<li class="<?= ($this->router->fetch_class() == 'home') ? 'active' : '' ?>"><a href="<?= base_url() ?>">Home</a></li>
						<li class="<?= ($this->router->fetch_class() == 'products') ? 'active' : '' ?>"><a href="<?= base_url() ?>products">Mooncake Selection</a></li>
						<li class="<?= ($this->router->fetch_class() == 'promo') ? 'active' : '' ?>"><a href="<?= base_url() ?>promo">Promotions</a></li>
						<li class="<?= ($this->router->fetch_class() == 'delivery_collection') ? 'active' : '' ?>"><a href="<?= base_url() ?>delivery_collection">Delivery/Collection</a></li>
						<li class="<?= ($this->router->fetch_class() == 'contact') ? 'active' : '' ?>"><a href="<?= base_url() ?>contact">Contact Us</a></li>
					</ul>
					<div class="secondary-nav hidden-xs">
						<?php if ($this->session->userdata('user_id')): ?>
							Welcome <a href="<?= base_url() ?>myaccount" class="<?= ($this->router->fetch_class() == 'myaccount') ? 'selected' : '' ?>"><?= $this->session->userdata('first_name')?></a>	
						<?php endif ?>&nbsp;

						<span><a style="color:#af0000;"  href="<?= base_url() ?>cart">My Cart (<?= $cart_items ?>)</a></span>
                        
						<?php if ($this->session->userdata('user_id')): ?>
							<a href="<?= base_url() ?>myaccount" class="<?= ($this->router->fetch_class() == 'myaccount') ? 'selected' : '' ?>">My Account</a>
							<a href="<?= base_url() ?>user/logout">Logout</a>
						<?php else: ?>
							<a href="<?= base_url() ?>user/login">Login</a>
						<?php endif ?>

					</div><!-- secondary-nav -->
				</div>
			</nav> <!-- navbar -->
		</div><!-- row -->
	</div><!-- container -->
</div><!-- navigation -->	