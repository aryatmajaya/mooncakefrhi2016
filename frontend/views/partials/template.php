<!DOCTYPE html>
<html lang="en">
	<? $this->load->view('partials/header'); ?>
	<body>
		<!-- <div class="alert alert-info">
			<h4 class="text-center">We are currently in a maintenance mode.</h4>
		</div> -->
		<!--[if lt IE 7]>
			<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
		<![endif]-->
		<? $this->load->view('partials/brands'); ?>
		
		<? $this->load->view('partials/navigation'); ?>
		<!-- page anchor -->
	  	<div id="topPage"></div><!-- topPage -->
	  	<? $this->load->view('partials/brands_mobile'); ?>
	 	<!-- CONTENT VIEW -->
	 	<? $this->load->view($view); ?>
	 	<!-- END CONTENT VIEW -->
		<!-- FOOTER -->
		<? $this->load->view('partials/footer'); ?>
		<!-- END FOOTER -->
		<!-- back to top button -->
		<div class="container visible-xs">
			<div class="row">
				<a href="#topPage" class="btn btn-danger btn-lg btn-block">Back to top</a>
			</div>
		</div><!-- container -->
		<!-- end of design structure-->

		<? $this->load->view('partials/scripts'); ?>
	</body>
</html>