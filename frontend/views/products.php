<div class="sub-page-title">
	<div class="bg-flower"></div>
    <h1><?= $header_title ?></h1>
</div>

<?php if ($this->session->flashdata('cart_success')): ?>
	<div class="alert alert-success cart-alert mt20" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <p class="text-center">	<?= $this->session->flashdata('cart_success')?></p>
    </div>
<?php endif ?>

<form action="<?= site_url("cart/add");?>" method="POST">

<?php foreach ($product_line_up as $key => $value): ?>

<div class="sections sections-product">

    <?php if ($this->session->flashdata('basket_error')): ?>
        <div class="alert alert-danger"><?= $this->session->flashdata('basket_error')?></div>
    <?php endif ?>
    
    <div class="section-title mt20">
        <h2 class="title"><span class="title-text"><?= $value->category_name ?></span><span class="title-cn"><?= $value->category_china_name ?></span></h2>
    </div>

    <div class="section-item-holder">
        
            <div class="row">
                <?php foreach ($value->products as $product_key => $product_value): ?>
                    <div class="panel-item products col-xs-12 col-sm-6 col-md-4 <?= ($product_key == (sizeof($value->products) -1 )) ? 'last' : '' ?>">
                        <div class="product-image-container">
                            <?php if ($product_value->is_new): ?>
                                <div class="ribbon-new"><img src="<?= base_url("assets/images/ribbon-new.png");?>"></div>
                            <?php endif ?>
                            <? // Parse product images from json stored in database ?>
                            <?php $product_images = json_decode($product_value->images); ?>
                            
                            <img src="<?= base_url() ?>img/products/<?= $product_images[0] ?>" alt="thumb" class="img-responsive">
                        </div>
                        <?php
                        //chinese text use short description
                        if(isset($product_value->short_description) && !empty($product_value->short_description)) {
                            $cntext = '<br><span class="title-cn">'.$product_value->short_description.'</span>';
                        } else {
                            $cntext = false;
                        }
                        ?>
                        <h3><?= nl2br($product_value->product_name) ?><?= ($cntext) ? $cntext : '' ?><!-- <?= ($product_value->is_new) ? '<sup>New</sup>' : '' ?>--></h3>
                        <p class="price">$<?= intval($product_value->price) ?> <span>(<?= $product_value->pc_per_box; ?>pcs per box)</span></p>
                        
                            <div class="form-group">
                                <input type="hidden" name="item[id]" value="<?= $product_value->id?>">
                                <div class="row">	
                                    <div class="col-xs-12 col-sm-5">
                                        <ul class="list-inline">
                                            <li>
                                                <label for="" class="col-xs-1 control-label">Qty</label>
                                            </li>
                                            <li>
                                                <input type="text" class="form-control qty" placeholder="0" name="items[<?= $product_value->id?>][qty]" style="width:60px;">
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <button class="btn btn-default add-to-cart btn-block" type="submit">
                                            <i aria-hidden="true" class="fa fa-shopping-cart"></i> Add to cart
                                        </button>
                                    </div>
                                </div>
                            </div><!-- form-group -->
                        
                </div><!-- panel-item -->
            <?php endforeach ?>
            </div>
    </div><!-- section-item-holder -->
</div><!-- sections -->

<?php endforeach ?>
</form>