<div class="sub-page-title">
	<div class="bg-flower"></div>
	<h1>Promotions</h1>
</div>
​
<div class="sections">
​
    <div class="panel-item promo first">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <img src="<?= base_url("assets/images/promotion/promotion-1.jpg");?>" alt="ONLINE EARLY BIRD" class="img-responsive">
            </div>
            <div class="col-xs-12 col-md-9">
                <h4>ONLINE EARLY BIRD</h4>
                <h5>30% Discount</h5>
                <p><strong>Mon, 25 Jul till Sun, 07 Aug</strong></p>
              	<ul>
                   	<li>Online pre-order with min 2 boxes.</li>
                    <li>Online full payment with The FAR Card or any credit card.</li>
					<li>Self collection at Fairmont Singapore’s Mooncake Booth, Level 2, from 8 Aug 2016 to 15 Sep 2016.
</li>
              	</ul>
          </div>
        </div>
    </div><!-- panel-item -->
    
    
    <div class="panel-item promo">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <img src="<?= base_url("assets/images/promotion/promotion-2.jpg");?>" alt="EARLY BIRD" class="img-responsive">
            </div>
            <div class="col-xs-12 col-md-9">
                <h4>EARLY BIRD</h4>
                <h5>25% Discount</h5>
                <p><strong>Mon, 08 Aug till Sun, 28 Aug</strong></p>
                <ul>
					<li>Pre-order via online website or pre-order at Fairmont Singapore’s Mooncake Booth, Level 2 </li>
					<li>Pre-order with min 2 boxes</li>
					<li>Full payment via online website or at Fairmont Singapore’s Mooncake Booth, Level 2 (with The FAR Card or any credit card)</li>
					<li>Self collection at Fairmont Singapore’s Mooncake Booth, Level 2, before or on 8 Sept 2016.</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="panel-item promo">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <img src="<?= base_url("assets/images/promotion/promotion-3.jpg");?>" alt="EXCLUSIVE DISCOUNT FOR OCBC CREDIT CARD" class="img-responsive">
            </div>
            <div class="col-xs-12 col-md-9">
                <h4>EXCLUSIVE DISCOUNT FOR OCBC CARD MEMBERS</h4>
                <h5>20% Discount</h5>
                <p><strong>Mon, 29 Aug till Wed, 07 Sep</strong></p>
                <ul>
					<li>Pre-order via online website or pre-order at Fairmont Singapore’s Mooncake Booth, Level 2 
</li>
					<li>Pre-order with min 2 boxes</li>
					<li>Full payment via online website or at Fairmont Singapore’s Mooncake Booth, Level 2 (with OCBC card)
</li>
<li>Pre-order ends on 7 Sep 2016</li>
					<li>Self collection at Fairmont Singapore’s Mooncake Booth, Level 2, before or on 15 Sep 2016.
</li>
                </ul>
            </div>
        </div>
    </div><!-- panel-item -->
    
    <div class="panel-item promo">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <img src="<?= base_url("assets/images/promotion/promotion-4.jpg");?>" alt="EXCLUSIVE DISCOUNT FOR THE FAR CARD MEMBERS" class="img-responsive">
            </div>
            <div class="col-xs-12 col-md-9">
                <h4>EXCLUSIVE DISCOUNT FOR THE FAR CARD MEMBERS</h4>
                <h5>20% Discount</h5>
                <p><strong>Mon, 29 Aug till Wed, 07 Sep</strong></p>
                <ul>
					<li>Pre-order via online website or pre-order at Fairmont Singapore’s Mooncake Booth, Level 2 
</li>
					<li>Pre-order with min 2 boxes</li>
					<li>Full payment via online website or at Fairmont Singapore’s Mooncake Booth, Level 2 (with The FAR Card)</li>
                    <li>Pre-order ends on 7 Sep 2016</li>
					<li>Self collection at Fairmont Singapore’s Mooncake Booth, Level 2, before or on 15 Sep 2016
</li>
                </ul>
           </div>
        </div>
    </div>
            
</div><!-- sections -->