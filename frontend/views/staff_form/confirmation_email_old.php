<?php

/*$subtotal = 0;
$discount = 0;
$discounted_amount = 0;
$number_item_discount = 0;
$number_item_discounted_amount = 0;
$gst_charge = 0;
$total_delivery_charge = 0;
$grandtotal = 0;


foreach ($items as $item) {
    $price_total = $item->qty * $item->price;    
    $subtotal += $price_total;
}

if($order->promo_code_discount > 0){
	$discount = $order->promo_code_discount;
	$discounted_amount = ($subtotal*$order->promo_code_discount)/100;
}elseif($order->card_discount > 0){
	$discount = $order->card_discount;
	$discounted_amount = ($subtotal*$order->card_discount)/100;
}

if($order->number_item_discount){
	$number_item_discount = $order->number_item_discount;
	$number_item_discounted_amount = ($subtotal*$order->number_item_discount)/100;
}


$total_delivery_charge = Utilities::Calculate_Delivery_Charges_Cafenoel($subtotal, $fo[0]->service_type);

$gst_charge = ($subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge) * (GST_PERCENT /100);

$grandtotal = $subtotal - ($discounted_amount + $number_item_discounted_amount) + $total_delivery_charge + $gst_charge;*/


/*$this->ci->email->to("aryatmajaya@gmail.com");
			$this->ci->email->message(print_r($data,true));
            $this->ci->email->send();*/

?>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Your Order Confirmation at <?php echo $site_config['name'] ?></title>

    </head>
    
    <body>

    <table width="700" border="0" align="center" cellpadding="10" cellspacing="0" style="border:5px solid #cccccc;">
      <tbody>
        <tr>
          <td>
          
          <table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td><img src="<?php echo base_url() . $site_config['logo'] ?>" border="0"></td>
            <td align="right" style="font-family: Arial, sans-serif; font-size:20px; color: #43474A; line-height:20px;""><?php echo $site_config['name'] ?></td>
            
        </tr>
       
        <tr>
            <td colspan="2" style="font-family: Arial, sans-serif; font-size:14px; color: #43474A; line-height:20px;">
                <strong>Dear <?=$order->first_name.' '.$order->last_name?></strong><br />
                Thank you for order at <?php echo $site_config['name'] ?>.<br />
                Your order confirmation details are as follows:
            </td>   
        </tr>
         <tr>
          <td colspan="2" style="border-bottom:1px solid #d4d9de;">&nbsp;</td>
        </tr>
    </table><br />

        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, sans-serif; font-size:14px; color: #43474A; line-height:20px;">
          <tr>
              <td width="20%" align="left" valign="top"><strong>Order Date: </strong></td>
              <td width="30%" align="left" valign="top"><?=date("d.m.y", strtotime($order->date_ordered))?></td>
              <td width="20%" align="left" valign="top"><strong>Customer: </strong></td>
              <td width="30%" align="left" valign="top"><?=$order->first_name.' '.$order->last_name;?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Order Ref: </strong></td>
            <td align="left" valign="top"><?=$order->order_ref;?></td>
            <td align="left" valign="top"><strong>Mobile: </strong></td>
            <td align="left" valign="top"><?=$order->mobile;?></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Discount: </strong></td>
            <td align="left" valign="top">
				<?php
                	echo $order->corporate_discount."%";
				?>
            </td>
            
            
            <td align="left" valign="top"><strong>Alt Contact #:</strong></td>
            <td align="left" valign="top"><?=(($order->alt_contact)?$order->alt_contact:'&nbsp;');?></td>
          </tr>
          
          <tr>
           	<td align="left" valign="top"><strong>Amount: </strong></td>
            <td align="left" valign="top">SGD <?=number_format($grandtotal,2);?></td>
            <td align="left" valign="top"><strong>Email: </strong></td>
            <td align="left" valign="top"><?=$order->email?></td>
          </tr>
        </table>
    
       
    
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="text_12px" style="font-family: Arial, sans-serif; font-size:14px; color: #43474A; line-height:20px;">
            <tr>

              <td width="50%" align="left" valign="top" bgcolor="#e6e9ee" style="border-bottom:1px solid #d4d9de;"><strong> Product Name </strong></td>
              <td width="20%" align="right" valign="top" bgcolor="#e6e9ee" style="border-bottom:1px solid #d4d9de;"><strong> Unit Price </strong></td>
              <td width="10%" align="right" valign="top" bgcolor="#e6e9ee" style="border-bottom:1px solid #d4d9de;"><strong> Qty </strong></td>
              <td width="20%" align="right" valign="top" bgcolor="#e6e9ee" style="border-bottom:1px solid #d4d9de;"><strong> Total </strong></td>
                
            </tr>
            <?php foreach ($products_items as $item) { ?>        
                <tr>
                  <td style="border-bottom:1px solid #d4d9de;"><?php echo html_entity_decode($item['product_name']); ?></td>
                  <td align="right" style="border-bottom:1px solid #d4d9de;">SGD <?php echo number_format($item['price'], 2); ?></td>
                  <td align="right" style="border-bottom:1px solid #d4d9de;"><?php echo $item['qty']; ?></td>
                  <td align="right" style="border-bottom:1px solid #d4d9de;">SGD <?php echo number_format($item['total'], 2); ?></td>
                </tr>
            <?php } ?>
            <tr>
              <td colspan="3" align="right" style="border-bottom:1px solid #d4d9de;"><strong>Sub total:</strong></td>
              <td align="right" style="border-bottom:1px solid #d4d9de;"><?php echo number_format($subtotal,2);?></td>
            </tr>
         	
            <tr>
              <td colspan="3" align="right" style="border-bottom:1px solid #d4d9de;"><strong>Discount (<?php echo $discount;?>%):</strong></td>
              <td align="right" style="border-bottom:1px solid #d4d9de;">- <?php echo number_format($discount_amount,2);?></td>
            </tr>

            <tr>
              <td colspan="3" align="right" style="border-bottom:1px solid #d4d9de;"><strong>Delivery Charge:</strong></td>
              <td align="right" style="border-bottom:1px solid #d4d9de;"><?php echo number_format($delivery_charge,2);?></td>
            </tr>
            
            <tr>
              <td colspan="3" align="right" style="border-bottom:1px solid #d4d9de;"><strong>GST Charge (<?php echo $gst_persen;?>%):</strong></td>
              <td align="right" style="border-bottom:1px solid #d4d9de;"><?php echo number_format($gst,2);?></td>
            </tr>
            <tr>
              <td colspan="3" align="right" style="border-bottom:1px solid #d4d9de;"><strong>Grand total:</strong></td>
              <td align="right" style="border-bottom:1px solid #d4d9de;"><?php echo number_format($grandtotal,2);?></td>
            </tr>
           
        </table>
        <!-- END items table -->
        
    <!-- START FULFILLMENT OPTIONS -->
    <?php if ($fo){ ?>
    <br/>
    <span style="font-family: Arial, sans-serif; font-size:16px; color: #43474A; line-height:20px;"><strong>Fulfillment Options</strong></span>
    <br/>
    <?php
    foreach ($fo as $fo){
    ?>
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td style="font-family: Arial, sans-serif; font-size:14px; color: #43474A; line-height:20px; border-bottom:1px solid #d4d9de;">
                <?php

                echo '<strong>'.ucwords($fo->service_type).': </strong>'.date("d F Y", strtotime($fo->service_date)).(($fo->time_slot)?', '.$fo->time_slot:'');
                
				if ($fo->service_type == "delivery"){
                    echo '<br /><strong>Address:</strong> '.$fo->address.', '.$fo->postal_code;
                    echo '<br /><strong>Delivered To:</strong> '.$fo->delivered_to;
                    echo '<br /><strong>Contact No.:</strong> '.$fo->delivered_contact;
                }else{ ?>
                	<br /><strong>Collection:</strong> <?php echo $site_config['collection_address'] ?>.
                <?php } ?>
                
				<?php if($fo->additional_request){ ?>
                	<br /><strong>Additional Request:</strong> <?php echo $fo->additional_request;?>
                <?php } ?>
                
            </td>
        </tr>
    </table>
    
    <table width="100%" border="0" cellpadding="5" cellspacing="0" style="font-family: Arial, sans-serif; font-size:14px; color: #43474A; line-height:20px;">
        
            <tr>
              <td bgcolor="#e6e9ee" style="border-bottom:1px solid #d4d9de;"><strong>Items</strong></td>
              <td align="center" bgcolor="#e6e9ee" style="border-bottom:1px solid #d4d9de;"><strong>Qty</strong></td>
            </tr>
            
       
            <?php 
            $delivery_items = Delivery_model::get_delivery_option_items2($fo->order_ref);
                   
            foreach ($delivery_items as $di) {
            ?>
            <tr>
              <td style="border-bottom:1px solid #d4d9de;"><?=$di->product_name?></td>
              <td align="center" style="border-bottom:1px solid #d4d9de;"><?php echo $di->qty;?></td>
            </tr>
            <?php } ?>
        
    </table>
    <br />
    <?php } ?>
    <!-- END FULFILLMENT OPTIONS -->
    <?php } //End if ($fo)?>

<table width="100%" border="0" cellpadding="20" cellspacing="0">
    <tr>
        <td align="center" style="font-family: Arial, sans-serif; font-size:14px; color: #43474A; line-height:20px;">
            <span>For enquiries, please call <strong><?php echo $site_config['enquiry_phone_number'] ?></strong>.</span><br/>
            <span style="font-size:11px;">This is system generated, please do not reply to this email.</span>
        </td>
    </tr>
</table>
          
          </td>
        </tr>
      </tbody>
    </table>
    
        
</body>
</html>
    
