<?php
	/**
	 * HEADER
	 */
	$this->load->view('staff_form/partials/header.php');
?>

	<div class="container">
    	
		<div id="staffForm" ng-controller="staffFormController" ng-cloak>
			<!-- <form method="post"> -->
            
            
			<div class="panel panel-default" ng-show="!order_confirmation && !success">
	  			<!--<div class="panel-heading">
	    			<h3 class="panel-title">Customer</h3>
	  			</div>-->
			
	  			<div class="panel-body">			
                    <uib-tabset justified="true">
                        <uib-tab heading="Existing" select="change_customer_type('existing')">
                            <div class="body-content"> 
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-4">
                                                    <p class="label-search">Search By:</p>
                                                </div>
                                                <div class="col-md-9 col-sm-8">
                                                
                                                
                                                
                                                    <div class="form-group">	
                                                        <select class="form-control input-lg" ng-model="searchType">
                                                            <option value="first_name">First Name</option>
                                                            <option value="last_name">Last Name</option>
                                                            <option value="email">Email</option>
                                                            <option value="mobile">Contact Number</option>
                                                        </select> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <div class="form-group">								    
                                                        <input type="text" class="form-control input-lg" ng-model="searchKeywords" placeholder="keywords">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <button type="submit" ng-click="searchCustomer(searchType, searchKeywords)" class="btn btn-lg btn-block btn-primary">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            
                                <div class="table-responsive">
                                    <table class="table table-striped mt20 mb0" ng-show="customers">
                                        <thead>
                                            <tr>
                                                <th>Customer ID</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Contact Number</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="customer in customers" ng-show="customers">
                                                <td>{{customer.id}}</td>
                                                <td>{{customer.first_name}}</td>
                                                <td>{{customer.last_name}}</td>
                                                <td>{{customer.email}}</td>
                                                <td>{{customer.mobile}}</td>
                                                <td><a ng-click="selectCustomer(customer.id)" class="btn btn-xs btn-info btn-block">Select</a></td>
                                            </tr>
                                            <tr ng-show="customers.length<=0">
                                                <td colspan="6" class="text-center">Data not found</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            
                                <div class="alert alert-info mt20 mb0" role="alert" ng-show="customer_id">{{customer_selected.first_name}} {{customer_selected.last_name}} is selected.</div>
                                
                            </div>
                        </uib-tab>
                        
                        <uib-tab heading="New" select="change_customer_type('new')">
                        
                        	<div class="body-content"> 
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">First Name<sup class="redclr">*</sup></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-lg" ng-model="customer_new.first_name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Last Name<sup class="redclr">*</sup></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-lg" ng-model="customer_new.last_name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Email<sup class="redclr">*</sup></label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control input-lg" ng-model="customer_new.email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Contact No.<sup class="redclr">*</sup></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-lg" ng-model="customer_new.contact">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Company</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-lg" ng-model="customer_new.company">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Address</label>
                                        <div class="col-sm-9">
                                            <textarea ng-model="customer_new.address" class="form-control input-lg"></textarea>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        </uib-tab>					   			  
                    </uib-tabset>
                </div>
			</div>
			            		        
			<div class="panel panel-default" ng-show="!order_confirmation && !success && (customer_selected || customer_type=='new')">
	  			
                <div class="panel-heading">
	    			<h3 class="panel-title">Order</h3>
	  			</div>
	  			<div class="panel-body">
	    			<div id="order-tab">
                        <uib-tabset justified="true">
                            <uib-tab ng-repeat="category in products" heading="{{category.name}}">
                            	<div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr ng-repeat="product_item in category.products">
                                                <td ng-bind-html="product_item.product_name"></td>
                                                <td class="text-right">${{product_item.price}}</td>
                                                <td style="width:350px" class="form-inline text-right">
                                                    <a class="btn btn-lg btncustom1" ng-click="update_quantity(category.id, product_item.id, false)">-</a>
                                                    <input valid-number type="text" class="form-control text-right input-lg" ng-change="update_summary()" size="4" ng-model="product_quantities[category.id][product_item.id]">
                                                    <a class="btn btn-lg btncustom1" ng-click="update_quantity(category.id, product_item.id, true)">+</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </uib-tab>				   			  
				  		</uib-tabset>
					</div>
	  			</div>
			</div>
            

                        
            
            <div class="panel panel-default" ng-show="!order_confirmation && !success && (customer_selected || customer_type=='new')">
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Summary</h3>
	  			</div>
                <div class="panel-body">
                	<div class="body-content">
                        <div class="row">
                            <div class="col-md-6 col-md-push-6">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Sub total</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control text-right input-lg" ng-model="subtotal" readonly numberformat>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Discount (in %)</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control text-right input-lg" ng-model="discount" ng-change="update_summary()" valid-number>
                                        </div>
                                    </div>
                                    <div class="form-group" ng-show="fullfillment_type=='delivery'">
                                        <label class="col-sm-5 control-label">Delivery Charge</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control text-right input-lg" ng-model="delivery_charge" ng-change="update_summary()" valid-number>
                                        </div>
                                    </div>
                                    <!--<div class="form-group">
                                        <label class="col-sm-5 control-label">GST Charge ({{gst_persen}}%)</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control text-right input-lg" ng-model="gst" readonly numberformat>
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Grand total</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control text-right input-lg" ng-model="grandtotal" readonly numberformat>
                                        </div>
                                    </div>
                                </div>
                             </div>
                         </div>
                	</div>
                </div>
            </div>

			<div class="panel panel-default" ng-show="!order_confirmation && !success && (customer_selected || customer_type=='new')">
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Fulfillment</h3>
	  			</div>
                
	  			<div class="panel-body">
                	
                    <uib-tabset justified="true">
					    <uib-tab heading="Self Collection" select="change_fullfillment_type('self_collection')">
                        	<div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Date<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                    	 <div class="input-group">
              								<input type="text" class="form-control input-lg" uib-datepicker-popup="dd-MMMM-yyyy" 
                                            ng-model="fullfillment_self_collection.date" is-open="self_collection_date.opened" min-date="minDate" max-date="maxDate" close-text="Close" datepicker-options="dateOptions" />
              								<span class="input-group-btn">
                								<button type="button" class="btn btn-default btn-lg" ng-click="self_collection_date_open()"><i class="glyphicon glyphicon-calendar"></i></button>
              								</span>
            							</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Time Slot<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">   
                                        <select class="form-control input-lg" ng-model="fullfillment_self_collection.time_slot" ng-options="time_slot for time_slot in time_slots_self_collection"></select>
                                    </div>
                                </div>
                                
                            </div>
                        </uib-tab>
                        
                        <uib-tab heading="Delivery" select="change_fullfillment_type('delivery')">
                        	<div class="form-horizontal">
                            	<div class="form-group">
                                    <label class="col-sm-2 control-label">Date<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                    	 <div class="input-group">
              								<input type="text" class="form-control input-lg" uib-datepicker-popup="dd-MMMM-yyyy" 
                                            ng-model="fullfillment_delivery.date" is-open="delivery_date.opened" min-date="minDate" max-date="maxDate" close-text="Close" datepicker-options="dateOptions" />
              								<span class="input-group-btn">
                								<button type="button" class="btn btn-default btn-lg" ng-click="delivery_date_open()"><i class="glyphicon glyphicon-calendar"></i></button>
              								</span>
            							</div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Time Slot<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                    	<select class="form-control input-lg" ng-model="fullfillment_delivery.time_slot" ng-options="time_slot for time_slot in time_slots_delivery"></select>          
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Recipient<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control input-lg" ng-model="fullfillment_delivery.recipient">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Contact<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control input-lg" ng-model="fullfillment_delivery.contact">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Address<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control input-lg" ng-model="fullfillment_delivery.address"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Postal Code<sup class="redclr">*</sup></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control input-lg" ng-model="fullfillment_delivery.postal_code">
                                    </div>
                                </div>

                            </div>
                        </uib-tab>			   			  
				    </uib-tabset>
                    
	  			</div>
			</div>
            
            <div class="panel panel-default" ng-show="!order_confirmation && !success && (customer_selected || customer_type=='new')">
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Order Section</h3>
	  			</div>
	  			<div class="panel-body order-section">
                	<div class="body-content">
                        <div class="form-horizontal ng-scope">
                        
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Sales Manager</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-lg" ng-model="sales_manager">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Payment Type</label>
                                <div class="col-sm-9">
                                    <select class="form-control input-lg" ng-model="payment_mode">
                                        <option value="">-- select payment type --</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Credit Card">Credit Card</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Additional Request</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control input-lg" ng-model="special_requirements"></textarea>
                                </div>
                            </div>
                            
                        </div>  
                    </div>  
                </div>
            </div>
            
            
            <div class="panel panel-default" ng-show="order_confirmation">
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Customer Details</h3>
	  			</div>
	  			<div class="panel-body">
                	<div class="body-content">
                        <p ng-show="customer_type=='existing'"><strong>Customer Type:</strong> Existing</p>
                        <p ng-show="customer_type=='new'"><strong>Customer Type:</strong> New</p>
                        
                        <hr>
                        
                        <p ng-show="customer_type=='existing'">
                            <strong>Customer ID:</strong> {{customer_selected.customer_id}}<br>
                            <strong>First Name:</strong> {{customer_selected.first_name}}<br>
                            <strong>Last Name:</strong> {{customer_selected.last_name}}<br>
                            <strong>Email:</strong> {{customer_selected.email}}<br>
                            <strong>Contact No.:</strong> {{customer_selected.contact}}<br>
                            <strong>Company:</strong> {{customer_selected.company}}<br>
                            <strong>Address:</strong> {{customer_selected.address}}
                        </p>
                        
                        <p ng-show="customer_type=='new'">
                            <strong>First Name:</strong> {{customer_new.first_name}}<br>
                            <strong>Last Name:</strong> {{customer_new.last_name}}<br>
                            <strong>Email:</strong> {{customer_new.email}}<br>
                            <strong>Contact No.:</strong> {{customer_new.contact}}<br>
                            <strong>Company:</strong> {{customer_new.company}}<br>
                            <strong>Address:</strong> {{customer_new.address}}
                        </p>
                    </div>
	  			</div>
			</div>
 			
            <div class="panel panel-default" ng-show="order_confirmation">
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Order Details</h3>
	  			</div>
	  			<div class="panel-body">
	    			<div class="body-content">
                        <table class="table table-stripeds" ng-init="current=1">
                            <thead>
                                <th>No.</th>
                                <th>Product Name</th>
                                <th class="text-right">Unit Price</th>
                                <th class="text-right">Qty</th>
                                <th class="text-right">Total Price</th>
                            </thead>
                            <tbody>
                                
                                <tr ng-repeat="item in products_conf">
                                	<td ng-show="item.type=='heading'"></td><td colspan="4" ng-show="item.type=='heading'">{{item.title}}</td>
                                    
                                    <td ng-show="item.type=='data'">{{item.no}}</td>
                                    <td ng-show="item.type=='data'" ng-bind-html="item.product_name"></td>
                                    <td ng-show="item.type=='data'" class="text-right">${{item.product_price | number:2}}</td>
                                    <td ng-show="item.type=='data'" class="text-right">{{item.qty}}</td>
                                    <td ng-show="item.type=='data'" class="text-right">${{item.total | number:2}}</td>
                                </tr>
                           </tbody>
                        </table>
                        <br><br>
                        <table class="table">
                        	<tr>
                            	<td class="text-right"><strong>Sub total:</strong></td>
                                <td class="text-right">{{subtotal | number:2}}</td>
                            </tr>
                            <tr>
                            	<td class="text-right"><strong>Discount ({{discount}}%):</strong></td>
                                <td class="text-right">- {{discount_amount | number:2}}</td>
                            </tr>
                            <tr>
                            	<td class="text-right"><strong>Delivery Charge:</strong></td>
                                <td class="text-right">{{delivery_charge | number:2}}</td>
                            </tr>
                            <!--<tr>
                            	<td class="text-right"><strong>GST Charge ({{gst_persen}}%):</strong></td>
                                <td class="text-right">{{gst | number:2}}</td>
                            </tr>-->
                            <tr>
                            	<td class="text-right"><strong>Grand total:</strong></td>
                                <td class="text-right">{{grandtotal| number:2}}</td>
                            </tr>
                        </table>
                        
					</div>
	  			</div>
			</div>
            
            
            <div class="panel panel-default" ng-show="order_confirmation">
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Fullfillments Options</h3>
	  			</div>
	  			<div class="panel-body">
                    <div class="body-content">
                        <div ng-show="fullfillment_type=='self_collection'">
                            <p><strong>Fullfillment Type:</strong> Self Collection</p>
                            <hr>
                            <p>
                            <strong>Delivery Date:</strong> {{fullfillment_self_collection.date | date: 'dd MMMM yyyy'}} at {{fullfillment_self_collection.time_slot}}
                            </p>
                        </div>
                        
                        <div ng-show="fullfillment_type=='delivery'">
                            <p><strong>Fullfillment Type:</strong> Delivery</p>
                            <hr>
                            <p>
                            <strong>Delivery Date:</strong> {{fullfillment_delivery.date | date: 'dd MMMM yyyy'}} at {{fullfillment_delivery.time_slot}}<br>
                            <strong>Address:</strong> {{fullfillment_delivery.address}} ({{fullfillment_delivery.postal_code}})<br>
                            <strong>Delivered To:</strong> {{fullfillment_delivery.recipient}}<br>
                            <strong>Contact No.:</strong> {{fullfillment_delivery.contact}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="panel panel-default" ng-show="order_confirmation">
            	
	  			<div class="panel-heading">
	    			<h3 class="panel-title">Order Section</h3>
	  			</div>
	  			<div class="panel-body">
                	<div class="body-content">
                        <p><strong>Sales Manager:</strong> {{sales_manager}}</p>
                        <p><strong>Payment Type:</strong> {{payment_mode}}</p>
                        <p ng-show="special_requirements"><strong>Additional Request:</strong> {{special_requirements}}</p>
                        <p ng-show="!special_requirements"><strong>Additional Request:</strong> -</p>
                    </div>
	  			</div>
			</div>
            
            
            <div class="row" ng-show="success">
            	<div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="body-content">
                                <h1 class="h2">Order Success</h1>
                                <p>Your Order successfuly added to database.</p>
                                <a class="btn btn-primary btn-lg" ng-click="back()">Create New Order</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
			<button type="submit" class="btn btn-lg btn-primary boxsize1 fltright" ng-click="continue()" ng-show="!order_confirmation && !success && (customer_selected || customer_type=='new')">Continue</button>
            
            <button class="btn btn-primary btn-lg" ng-click="set_confirmation()" ng-show="order_confirmation">Back</button>
            <button type="submit" class="btn btn-primary btn-lg fltright boxsize1" ng-click="submit()" ng-show="order_confirmation">Save Order</button>
            
		</div>
	</div>


<script type="text/ng-template" id="modal_info.html">
	<div class="modal-header">
		<h3 class="modal-title">{{title}}</h3>
	</div>
	<div class="modal-body">
		<p>{{text}}</p>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
	</div>
</script>
	

<?php
	/**
	 * FOOTER
	 */
	$this->load->view('staff_form/partials/footer.php');
?>