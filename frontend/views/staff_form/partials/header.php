<!DOCTYPE html>
<html lang="en" ng-app="staffForm">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Staff Form</title>

		<!-- Bootstrap -->
		<link href="<?= base_url("assets/bootstrap/css/bootstrap.css") ?>" rel="stylesheet">
		<link href="<?= base_url("assets/css/loading-bar.css") ?>" rel="stylesheet">
		<link href="<?= base_url("assets/css/staffform.css") ?>" rel="stylesheet">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-5 marg20">
						<div class="row">
							<div class="col-xs-6 col-sm-6 logocel">
								<a href="<?= site_url() ?>">
                                	<img src="<?= base_url("assets/images/logo-celebrations.jpg");?>" class="img-responsive acenter" alt="Celebration Central">
                                </a>				 	
							</div>
							<div class="col-xs-6 col-sm-6 logocn" >
								<a href="<?php echo site_url() ?>" id="brand-logo">
				                    <img src="<?php echo base_url("assets/images/logo-mooncake-selections.jpg");?>" class="img-responsive acenter" alt="Exquisite Mooncak Selection">
				                </a>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>