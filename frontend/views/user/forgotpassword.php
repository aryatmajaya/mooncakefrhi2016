<div class="sub-page-title">
    <h1><?= $header_title?></h1>
</div>

<div class="sections mt20">
    <div class="row basket-section last basket-form">
        <div class="col-md-6">
        <?php if ($this->session->flashdata('forgot_password_success')): ?>
            <div class="alert alert-success">
                <?= $this->session->flashdata('forgot_password_success')?>
            </div>
        <?php endif ?>
        <?php if ($this->session->flashdata('forgot_password_error')): ?>
            <div class="alert alert-success">
                <?= $this->session->flashdata('forgot_password_error')?>
            </div>
        <?php endif ?>
        <form name="login" class="form-horizontal log-in" role="form" method="POST">
              <div class="form-group">
                <label for="uName" class="col-xs-12 col-sm-5 col-md-4 col-lg-4 control-label">Email Address<span class="hidden-xs">:</span></label>
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                  <input type="email" class="form-control" name="email" value="<?= isset($email)?$email:''; ?>" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 text-right">
                  <button class="btn btn-default mt20 col-xs-6 pull-right" value="login" name="form_action">Submit</button>
                </div>
              </div>	  
        </form>
        </div>	
    </div>
</div>
